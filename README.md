# Odd Connect

[![pipeline status](https://gitlab.com/oddnetworks/odd_connect/badges/develop/pipeline.svg)](https://gitlab.com/oddnetworks/odd_connect/commits/develop)

[Check out the Wiki](https://github.com/oddnetworks/odd_connect/wiki) for more information.

# Apps

  - [Api](apps/api)
  - [Connect](apps/connect)
  - [FrontEnd](apps/front_end)
  - [Messenger](apps/messenger)
  - [Proxy](apps/proxy)
  - [Transactions](apps/transactions)
  - [Verifier](apps/verifier)
  - [Webhooks](apps/webhooks)

# Development

[Install Docker](https://docs.docker.com/engine/installation/)

Use the `make` commands to start/stop a development or test container.

    $ make

Will show you what is available and where they should be used.

#### [HOST] commands

To start your dev environment, use:

    $ make dev
    > starts a development container in a bash session


Other available commands:

```
dev                            [HOST] - docker-compose run --rm --service-ports web
docs-build                     [HOST] - builds docs in docker container
docs-serve                     [HOST] - serves the docs independent of odd_connect on port 4001
```

#### [GUEST] commands

To provision your dev environment within a container use:

    $ make bootstrap
    > gets elixir dependencies, creates/migrates database, seeds database, gets npm dependencies

Other available commands:

```
bootstrap                      [GUEST] - deps-get db-reset db-seed npm-install
db-drop                        [GUEST] - mix ecto.drop
db-migrate                     [GUEST] - mix ecto.migrate
db-reset                       [GUEST] - db-drop db-setup
db-seed                        [GUEST] - mix run /opt/app/apps/connect/priv/repo/seeds.exs
db-setup                       [GUEST] - mix do ecto.create, ecto.migrate
deps-get                       [GUEST] - mix do deps.get, deps.compile
npm-install                    [GUEST] - npm install
psql                           [GUEST] - psql -U postgres -h postgres -d odd_connect_dev
```

# Environment

## Stack

- __Erlang__ - 20.1
- __Elixir__ - 1.5.2
- __Node.js__ - 8.6.0
- __NPM__ - 5.3.0
- __Postgres__ - 9.6+

Please see the following to manage versions

- [.gitlab-ci.yml](.gitlab-ci.yml) - manages CI infrastructure versions
- [Dockerfile.development](Dockerfile.development) - manages development infrastructure versions
- [Dockerfile](Dockerfile) - the production Docker container

## Variables

The following are production environment variables for each of the apps.

#### connect

- `AMAZON_RVS_SANDBOX_BASE_URL` - default `http://amazonrvs:8080/RVSSandbox`
- `TRANSACTION_VALIDATION_THRESHOLD_ROKU` - default `900` (seconds)
- `TRANSACTION_VALIDATION_THRESHOLD_APPLE` - default `900` (seconds)
- `TRANSACTION_VALIDATION_THRESHOLD_GOOGLE` - default `900` (seconds)
- `TRANSACTION_VALIDATION_THRESHOLD_AMAZON` - default `900` (seconds)
- `DATABASE_URL` - Note: in `dev` or `test` environment this can be expressed via the following individual variables:
  - `DB_USER`
  - `DB_PASSWORD`
  - `DB_HOST` - default `127.0.0.1`
  - `DB_PORT` - default `5432`
  - `DB_NAME` - default `odd_connect_{MIX_ENV}`
- `POOL_SIZE` - default `10`
- `CONNECT_TOKEN_ISSUER` - required
- `CONNECT_RSA_PRIVATE_KEY` - required
- `CONNECT_RSA_PUBLIC_KEY` - required
- `CONNECT_DATA_ENCRYPTION_KEY` - required
  - key generation example (random 256-bit key)
    `:crypto.strong_rand_bytes(32) |> Base.encode6`


#### front_end

- `HOST` - default `odd-connect-example.herokuapp.com`
- `SECRET_KEY_BASE` - required
- `CONNECT_WEB_TOKEN_ISSUER` - required
- `CONNECT_WEB_TOKEN_SECRET` - required
- `CONNECT_WEB_API_TOKEN_TTL_YEARS` - default `3`

#### messenger

- `MAILGUN_API_KEY` - required
- `MAILGUN_DOMAIN_NAME` - required

#### verifier

- `VERIFIER_CONNECTION_TTL_IN_SECONDS` - default `108000`

## Docker

### Releases

Currently releases are made automatically with GitLab CI. Head over to [Pipelines](https://gitlab.com/oddnetworks/odd_connect/pipelines) to check out recent runs.

The `Dockerfile` is a [multi-stage Docker build](https://docs.docker.com/develop/develop-images/multistage-build/) used for both [Production](#production) and [Staging](#staging).

All successful deploys are kept in this project's [Container Registry](https://gitlab.com/oddnetworks/odd_connect/container_registry)

#### Configuring Releases

We're using [distillery](https://github.com/bitwalker/distillery) to configure our release build of the apps.

The configuration is located in `rel/config.exs`

The version number for releases corresponds to the version number of the [Connect](apps/connect) application.

#### Staging

Staging releases are built upon successful completion of our test pipeline when any commits or merges occur on the `develop` branch. Upon successful build of a staging container, it is pushed to Heroku's registry.

Our staging site is [https://oddconnect.science](http://oddconnect.science)

#### Production

Production releases are built upon successful completion of our test pipeline when any commits or merges occur on the `master` branch. Upon successful build of a staging container, it is pushed to Heroku's registry.

Our staging site is [https://oddconnect.com](http://oddconnect.com)

#### Migrations

There is a migration task created by our Distillery setup. To run this on heroku, use the following:

    $ heroku run migrate -a {app-name}

It runs the command located in `rel/commands/migrate.sh`

### Reference Links

- https://github.com/bitwalker/distillery/blob/master/docs/Running%20Migrations.md
- https://devcenter.heroku.com/articles/container-registry-and-runtime
- https://gist.github.com/brkattk/1cfc6b4585bba09160418942c2d5b3be
