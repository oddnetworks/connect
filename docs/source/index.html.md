---
title: Odd Connect v1.0.0
language_tabs:
  - http: HTTP
  - javascript: JavaScript
  - javascript--nodejs: Node.js
  - shell: Shell
toc_footers: []
includes:
  - _guide.md
  - endpoints/_authentication.md
  - endpoints/_connections.md
  - endpoints/_entitlements.md
  - endpoints/_transactions.md
  - endpoints/_device_users.md
  - _webhooks.md
  - _definitions.md
  - _schemas.md
  - _errors.md
search: true
highlight_theme: darkula
---

# Odd Connect v1.0.0

> Scroll down for code samples, example requests and responses. Select a language for code samples from the tabs above or the mobile navigation menu.

A tool for Authentication and Authorization

Base URLs:

* <a href="https://oddconnect.com/api">https://oddconnect.com/api</a>

Email: <a href="mailto:hi@oddnetworks.com">Support</a>













