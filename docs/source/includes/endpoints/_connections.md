# Connections

Passwordless Authentication related requests

## POST /connections

> Code samples

```http
POST https://oddconnect.com/api/connections HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Content-Type: application/json
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/connections',
  method: 'post',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');
const inputBody = '{
  "data": {
    "type": "connection",
    "attributes": {
      "email": "foo@example.com",
      "platform": "GOOGLE",
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24"
    }
  }
}';
const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/connections',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X POST https://oddconnect.com/api/connections \
  -H 'Authorization: Bearer your-organization-jwt' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

*Create a pending Connection*

Attempts to create a connection by sending the device user a unique link via the specified email address. If a 201 response is received from this request, you may use the returned ID to poll for a verified connection until the expires_at date returned. The pending connection will expire at the returned date unless the device user opens the link that was sent to them via email.

_NOTE: It is important to stop polling after the expires\_at date._

> Body parameter

```json
{
  "data": {
    "type": "connection",
    "attributes": {
      "email": "foo@example.com",
      "platform": "GOOGLE_MOBILE",
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24"
    }
  }
}
```

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
body|body|[PendingConnection](#schemapendingconnection)|true|A pending Connection
» data|body|object|true|No description
»» type|body|string|true|`connection`
»» attributes|body|object|true|No description
»»» email|body|string(email)|true|The Device User's email address
»»» platform|body|string|true|One of `AMAZON_MOBILE`, `AMAZON_TV`, `APPLE_MOBILE`, `APPLE_TV`, `GOOGLE_MOBILE`, `GOOGLE_TV`, `ROKU`, `WEB`
»»» device_identifier|body|string|true|An identifier globally unique to the specified platform


> Example responses

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "connection",
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "attributes": {
      "organization_urn": "your-org-urn",
      "expires_at": "2017-05-03T02:25:42Z",
      "email": "foo@example.com",
      "platform": "GOOGLE_MOBILE",
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|[PendingConnection](#schemapendingconnection)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
422|[Unprocessable Entity](https://tools.ietf.org/html/rfc2518#section-10.3)|Unprocessable Entity|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## GET /connections/{id}

> Code samples

```http
GET https://oddconnect.com/api/connections/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/connections/{id}',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/connections/{id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/connections/{id} \
  -H 'Accept: application/json'

```

*Poll for verified Connection by ID*

You may use this route to poll for a verified connection. A 404 response will be returned until the device user verifies the connection by clicking on the link that was sent to them via email. If the device user does not verify the connection before the given expiry a 404 response will always be returned.
Upon verifying the connection, a 200 response will be returned and you will receive a signed JWT with: aud[0]=organization-urn, aud[1]=device-platform, aud[2]=device-identifier, sub=device-user-email, jti=connection-id, and iss=odd-connect:verified-connection. The public signing key used to verify this JWT can be retrieved via Odd Connect.
The response will include any currently valid Entitlement `external_identifier`s for the DeviceUser.
NOTE: It is important to stop polling after the expires_at date given by the POST /connections response.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|ID of connection to return


> Example responses

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "connection",
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "attributes": {
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24",
      "email": "foo@example.com",
      "jwt": "a-jwt-for-your-connection",
      "organization_urn": "your-org-urn",
      "platform": "GOOGLE_MOBILE",
      "verified_at": "2017-05-03T02:21:42"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[VerifiedConnection](#schemaverifiedconnection)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## POST /device_users/{email}/connections

> Code samples

```http
POST https://oddconnect.com/api/device_users/{email}/connections HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Content-Type: application/json
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/device_users/{email}/connections',
  method: 'post',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');
const inputBody = '{
  "data": {
    "type": "connection",
    "attributes": {
      "platform": "GOOGLE_MOBILE",
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24"
    }
  }
}';
const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/device_users/{email}/connections',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X POST https://oddconnect.com/api/device_users/{email}/connections \
  -H 'Authorization: Bearer your-organization-jwt' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

*Create a verified Connection for a Device User*

You may use this route to create a verified connection directly. This endpoint would be used when a Device User does not have any entitlements and authentication via platform-level accounts could be done in place of passwordless authentication via email. Think purchasing an entitlement via Google Play Store with a Google account and reqeusting permission from the user to authenticate via Google account.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
email|path|string(email)|true|The Device User's email address

> Body parameter

```json
{
  "data": {
    "type": "connection",
    "attributes": {
      "platform": "GOOGLE_MOBILE",
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24"
    }
  }
}
```

> Example responses

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "connection",
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "attributes": {
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24",
      "email": "foo@example.com",
      "jwt": "a-jwt-for-your-connection",
      "organization_urn": "your-org-urn",
      "platform": "GOOGLE_MOBILE",
      "verified_at": "2017-05-03T02:21:42"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[VerifiedConnection](#schemaverifiedconnection)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>
