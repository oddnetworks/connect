# Device Users

## GET /device_users/{email}

> Code samples

```http
GET https://oddconnect.com/api/device_users/{email} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/device_users/{email}',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/device_users/{email}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/device_users/{email} \
  -H 'Accept: application/json'

```

*Get device user by email*

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
email|path|string(email)|true|The device user's email address


> Example responses

```json

```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[DeviceUser](#schemadeviceuser)
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## GET /device_users/{email}/entitlements

> Code samples

```http
GET https://oddconnect.com/api/device_users/{email}/entitlements HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/device_users/{email}/entitlements',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/device_users/{email}/entitlements',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/device_users/{email}/entitlements \
  -H 'Accept: application/json'

```

*Fetch a Device User's current Entitlements*

Returns all current Entitlements of a given Device User, scoped to your Organization. "Current Entitlements" are those linked to a valid Transaction.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
email|path|string(email)|true|The device user's email address


> Example responses

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": [
    {
      "type": "subscription",
      "id": "d9b2f385-98ad-4129-85ca-d217ad2de86a",
      "attributes": {
        "name": "Monthly Subscriber",
        "description": "Subscribe monthly for access to things",
        "entitlement_identifier": "monthly-subscription",
        "product_identifier": "roku-product-id",
        "platform": "ROKU"
      }
    }
  ]
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[Entitlements](#schemaentitlements)
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>
