# Transactions

Transaction related requests

## PUT /transactions/{id}

> Code samples

```http
PUT https://oddconnect.com/api/transactions/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Content-Type: application/json
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/transactions/{id}',
  method: 'put',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');
const inputBody = '{
  "data": {
    "id": "c06bba31-3ecf-4950-babc-f10ec824ea15",
    "type": "transaction",
    "attributes": {
      "invalidated_at": "2017-09-18T13:12:00Z",
      "invalidation_reason": "Transaction was refunded",
      "receipt": {
        "this": "is-just-an-object",
        "amount": "$0.00",
        "code": "PROD1",
        "freeTrialQuantity": 0,
        "freeTrialType": "None",
        "issuccess": true,
        "productType": "Consumable",
        "purchaseId": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
        "qty": 1,
        "total": "$0.00"
      }
    }
  }
}';
const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/transactions/{id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X PUT https://oddconnect.com/api/transactions/{id} \
  -H 'Authorization: Bearer your-organization-jwt' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

*Update a Transaction*

Used to manage a Transaction's life-cycle.

> Body parameter

```json
{
  "data": {
    "id": "c06bba31-3ecf-4950-babc-f10ec824ea15",
    "type": "transaction",
    "attributes": {
      "invalidated_at": "2017-09-18T13:12:00Z",
      "invalidation_reason": "Transaction was refunded",
      "receipt": {
        "this": "is-just-an-object",
        "amount": "$0.00",
        "code": "PROD1",
        "freeTrialQuantity": 0,
        "freeTrialType": "None",
        "issuccess": true,
        "productType": "Consumable",
        "purchaseId": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
        "qty": 1,
        "total": "$0.00"
      }
    }
  }
}
```
### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|The Transaction ID
body|body|[Transaction](#schematransaction)|true|The Transaction updates
» data|body|object|true|No description
»» id|body|string|true|No description
»» type|body|string|true|`transaction`
»» attributes|body|object|true|No description
»»» invalidated_at|body|string(date-time)|false|When given, the Transaction becomes invalidated.
»»» invalidation_reason|body|string|false|No description
»»» receipt|body|object|false|An object containing arbitrary metadata.

> Example responses

```json
{
  "data": {
    "id": "c06bba31-3ecf-4950-babc-f10ec824ea15",
    "type": "transaction",
    "attributes": {
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
      "inserted_at": "2017-09-01T14:53:00Z",
      "invalidated_at": "2017-09-18T13:12:00Z",
      "invalidation_reason": "Transaction was refunded",
      "last_validated_at": "2017-09-18T13:00:00Z",
      "platform": "ROKU",
      "product_identifier": "PROD1",
      "receipt": {
        "this": "is-just-an-object",
        "amount": "$0.00",
        "code": "PROD1",
        "freeTrialQuantity": 0,
        "freeTrialType": "None",
        "issuccess": true,
        "productType": "Consumable",
        "purchaseId": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
        "qty": 1,
        "total": "$0.00"
      },
      "updated_at": "2017-09-18T13:12:00Z"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[Transaction](#schematransaction)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
422|[Unprocessable Entity](https://tools.ietf.org/html/rfc2518#section-10.3)|Unprocessable Entity|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## GET /transactions/{id}

> Code samples

```http
GET https://oddconnect.com/api/transactions/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/transactions/{id}',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/transactions/{id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/transactions/{id} \
  -H 'Accept: application/json'

```

*Get an existing Transaction by ID*

Returns an existing Transaction if found.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|ID of transaction to return


> Example responses

```json
{
  "data": {
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "type": "transaction",
    "attributes": {
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
      "inserted_at": "2017-10-01T14:53:00Z",
      "invalidated_at": "2017-10-02T14:53:00Z",
      "invalidation_reason": "User Cancelled",
      "last_validated_at": "2017-10-02T14:45:00Z",
      "platform": "AMAZON",
      "product_identifier": "PROD1",
      "receipt": {
        "description": "object of any type allowed here"
      },
      "updated_at": "2017-10-01T14:53:00Z"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[Transaction](#schematransaction)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## GET /device_users/{email}/transactions

> Code samples

```http
GET https://oddconnect.com/api/device_users/{email}/transactions HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/device_users/{email}/transactions',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/device_users/{email}/transactions',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/device_users/{email}/transactions \
  -H 'Accept: application/json'

```

*List a Device User's Transactions*

Returns all Transactions for a specified Device User

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
email|path|string(email)|true|The device user's email address


> Example responses

```json
{
  "data": [
    {
      "data": {
        "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
        "type": "transaction",
        "attributes": {
          "email": "foo@example.com",
          "entitlement_identifier": "monthly-subscription",
          "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
          "inserted_at": "2017-10-01T14:53:00Z",
          "invalidated_at": "2017-10-02T14:53:00Z",
          "invalidation_reason": "User Cancelled",
          "last_validated_at": "2017-10-02T14:45:00Z",
          "platform": "AMAZON",
          "product_identifier": "PROD1",
          "receipt": {
            "description": "object of any type allowed here"
          },
          "updated_at": "2017-10-01T14:53:00Z"
        }
      }
    }
  ]
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[Transactions](#schematransactions)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## POST /device_users/{email}/transactions

### Note

- `AMAZON` transactions require a `receipt` object with the key `user_id` containing the Amazon user's ID.
- `APPLE` transactions require a `receipt` object with the key `latest_receipt` containing the binary receipt value.

> Code samples

```http
POST https://oddconnect.com/api/device_users/{email}/transactions HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Content-Type: application/json
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/device_users/{email}/transactions',
  method: 'post',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');
const inputBody = '{
  "data": {
    "type": "transaction",
    "attributes": {
      "platform": "ROKU",
      "product_identifier": "PROD1",
      "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
      "receipt": {
        "this": "is-just-an-object",
        "amount": "$0.00",
        "code": "PROD1",
        "freeTrialQuantity": 0,
        "freeTrialType": "None",
        "issuccess": true,
        "productType": "Consumable",
        "purchaseId": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
        "qty": 1,
        "total": "$0.00"
      }
    }
  }
}';
const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/device_users/{email}/transactions',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X POST https://oddconnect.com/api/device_users/{email}/transactions \
  -H 'Authorization: Bearer your-organization-jwt' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

*Create a Transaction for a Device User*

Used to notify Odd Connect about a purchase made from a Device User's device.
NOTE: Depending upon the platform, the values used for external_identifier and product_identifier will vary.

> Body parameter

```json
{
  "data": {
    "type": "transaction",
    "attributes": {
      "platform": "ROKU",
      "product_identifier": "PROD1",
      "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
      "receipt": {
        "this": "is-just-an-object",
        "amount": "$0.00",
        "code": "PROD1",
        "freeTrialQuantity": 0,
        "freeTrialType": "None",
        "issuccess": true,
        "productType": "Consumable",
        "purchaseId": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
        "qty": 1,
        "total": "$0.00"
      }
    }
  }
}
```
### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
email|path|string(email)|true|The device user's email address
body|body|[Transaction](#schematransaction)|true|The Transaction
» data|body|object|true|No description
»» type|body|string|true|`transaction`
»» attributes|body|object|true|No description
»»» platform|body|string|true|One of `AMAZON`, `APPLE`, `GOOGLE`, `ROKU`, `WEB`
»»» product_identifier|body|string|true|The platform's given product identifier. _Note: Must be set up as a product within your organization on Odd Connect dashboard_
»»» external_identifier|body|string|true|The platform's given identifier for this transaction.
»»» receipt|body|object|true|An object containing arbitrary metadata.


> Example responses

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "transaction",
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "attributes": {
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
      "inserted_at": "2017-09-17T16:01:47.506842",
      "invalidated_at": null,
      "invalidation_reason": null,
      "platform": "ROKU",
      "product_identifier": "PROD1",
      "receipt": {
        "this": "is-just-an-object",
        "amount": "$0.00",
        "code": "PROD1",
        "freeTrialQuantity": 0,
        "freeTrialType": "None",
        "issuccess": true,
        "productType": "Consumable",
        "purchaseId": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
        "qty": 1,
        "total": "$0.00"
      },
      "last_validated_at": "2017-09-18T15:34:00.123456",
      "updated_at": "2017-09-17T16:01:47.506857"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|[Transaction](#schematransaction)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
422|[Unprocessable Entity](https://tools.ietf.org/html/rfc2518#section-10.3)|Unprocessable Entity|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## DELETE /transactions/{id}

> Code samples

```http
DELETE https://oddconnect.com/api/transactions/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/transactions/{id}',
  method: 'delete',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/transactions/{id}',
{
  method: 'DELETE',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X DELETE https://oddconnect.com/api/transactions/{id} \
  -H 'Accept: application/json'

```

*Delete an existing Transaction by ID*

Deletes an existing Transaction if found.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|ID of Transaction to delete


> Example responses

```
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|OK|Inline
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>
