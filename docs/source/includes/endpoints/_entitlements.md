# Entitlements

Entitlement related requests

## POST /entitlements

> Code samples

```http
POST https://oddconnect.com/api/entitlements HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Content-Type: application/json
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/entitlements',
  method: 'post',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');
const inputBody = '{
  "data": {
    "type": "subscription",
    "attributes": {
      "name": "Monthly Subscription",
      "description": "Subscribe monthly for access to things",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "roku-product-id",
      "platform": "ROKU"
    }
  }
}';
const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/entitlements',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X POST https://oddconnect.com/api/entitlements \
  -H 'Authorization: Bearer your-organization-jwt' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

*Create an Entitlement for your Organization*

Used to create an Entitlement, within Odd Connect, that corresponds to an external entitlement for your organization.

> Body parameter

```json
{
  "data": {
    "type": "subscription",
    "attributes": {
      "name": "Monthly Subscription",
      "description": "Subscribe monthly for access to things",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "roku-product-id",
      "platform": "ROKU"
    }
  }
}
```
### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
body|body|[Entitlement](#schemaentitlement)|true|The Entitlement
» data|body|object|true|No description
»» type|body|string|true|One of `subscription`, `asset`
»» attributes|body|object|true|No description
»»» name|body|string|true|No description
»»» description|body|string|false|No description
»»» entitlement_identifier|body|string|true|A string unique to your organization.
»»» product_identifier|body|string|true|The identifier of your product on the platform store/marketplace.
»»» platform|body|string|true|One of `AMAZON`, `APPLE`, `GOOGLE`, `ROKU`, `WEB`


> Example responses

```json
{
  "data": {
    "id": "844a7770-fc63-4ffb-9674-d8db671a96f6",
    "type": "subscription",
    "attributes": {
      "name": "Monthly Subscription",
      "description": "Subscribe monthly for access to things",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "roku-product-id",
      "platform": "ROKU",
      "inserted_at": "2017-10-10T18:00:00Z",
      "updated_at": "2017-10-10T18:00:00Z"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|[Entitlement](#schemaentitlement)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
422|[Unprocessable Entity](https://tools.ietf.org/html/rfc2518#section-10.3)|Unprocessable Entity|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## GET /entitlements

> Code samples

```http
GET https://oddconnect.com/api/entitlements HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/entitlements',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/entitlements',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/entitlements \
  -H 'Accept: application/json'

```

*Fetch your Organization's Entitlements*

Returns all Entitlements scoped to your Organization.

> Example responses

```json
{
  "data": [
    {
      "id": "844a7770-fc63-4ffb-9674-d8db671a96f6",
      "type": "subscription",
      "attributes": {
        "name": "Monthly Subscription",
        "description": "Subscribe monthly for access to things",
        "entitlement_identifier": "monthly-subscription",
        "product_identifier": "roku-product-id",
        "platform": "ROKU",
        "inserted_at": "2017-10-10T18:00:00Z",
        "updated_at": "2017-10-10T18:00:00Z"
      }
    }
  ]
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[Entitlements](#schemaentitlements)
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## GET /entitlements/{id}

> Code samples

```http
GET https://oddconnect.com/api/entitlements/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/entitlements/{id}',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/entitlements/{id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X GET https://oddconnect.com/api/entitlements/{id} \
  -H 'Accept: application/json'

```

*Get an existing Entitlement by ID*

Returns an existing Entitlement if found.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|ID of Entitlement to return


> Example responses

```json
{
  "data": {
    "id": "844a7770-fc63-4ffb-9674-d8db671a96f6",
    "type": "subscription",
    "attributes": {
      "name": "Monthly Subscription",
      "description": "Subscribe monthly for access to things",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "roku-product-id",
      "platform": "ROKU",
      "inserted_at": "2017-10-10T18:00:00Z",
      "updated_at": "2017-10-10T18:00:00Z"
    }
  }
}
```
### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|None
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## PUT /entitlements/{id}

> Code samples

```http
PUT https://oddconnect.com/api/entitlements/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Content-Type: application/json
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/entitlements/{id}',
  method: 'put',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');
const inputBody = '{
  "data": {
    "id": "a7308aa8-2d2d-4047-9af9-16ff3251427f",
    "type": "subscription",
    "attributes": {
      "name": "New Monthly Subscription Name",
      "description": "Updated description"
    }
  }
}';
const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Content-Type':'application/json',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/entitlements/{id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X PUT https://oddconnect.com/api/entitlements/{id} \
  -H 'Authorization: Bearer your-organization-jwt' \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

*Update an Entitlement*

Used to manage an Entitlement's attributes.

> Body parameter

```json
{
  "data": {
    "id": "a7308aa8-2d2d-4047-9af9-16ff3251427f",
    "type": "subscription",
    "attributes": {
      "name": "New Monthly Subscription Name",
      "description": "Updated description"
    }
  }
}
```

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|The Entitlement ID
body|body|[Entitlement](#schemaentitlement)|true|The Entitlement updates
» data|body|object|true|No description
»» id|body|string(uuid)|true|No description
»» type|body|string|true|One of `subscription`, `asset`
»» attributes|body|object|true|No description
»»» name|body|string|false|No description
»»» description|body|string|false|No description
»»» entitlement_identifier|body|string|true|A string unique to your organization.
»»» product_identifier|body|string|true|The identifier of your product on the platform store/marketplace.
»»» platform|body|string|true|One of `AMAZON`, `APPLE`, `GOOGLE`, `ROKU`, `WEB`


> Example responses

```json
{
  "data": {
    "id": "844a7770-fc63-4ffb-9674-d8db671a96f6",
    "type": "subscription",
    "attributes": {
      "name": "New Monthly Subscription Name",
      "description": "Updated description",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "roku-product-id",
      "platform": "ROKU",
      "inserted_at": "2017-10-10T18:00:00Z",
      "updated_at": "2017-10-10T18:00:00Z"
    }
  }
}
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[Entitlement](#schemaentitlement)
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
422|[Unprocessable Entity](https://tools.ietf.org/html/rfc2518#section-10.3)|Unprocessable Entity|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>

## DELETE /entitlements/{id}

> Code samples

```http
DELETE https://oddconnect.com/api/entitlements/{id} HTTP/1.1
Host: oddconnect.com
Authorization: Bearer your-organization-jwt
Accept: application/json

```

```javascript
var headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

$.ajax({
  url: 'https://oddconnect.com/api/entitlements/{id}',
  method: 'delete',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})
```

```javascript--nodejs
const request = require('node-fetch');

const headers = {
  'Authorization':'Bearer your-organization-jwt',
  'Accept':'application/json'

};

request('https://oddconnect.com/api/entitlements/{id}',
{
  method: 'DELETE',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});
```

```shell
# You can also use wget
curl -X DELETE https://oddconnect.com/api/entitlements/{id} \
  -H 'Accept: application/json'

```

*Delete an existing Entitlement by ID*

Deletes an existing Entitlement if found.

### Parameters

Parameter|In|Type|Required|Description
---|---|---|---|---|
id|path|string(uuid)|true|ID of Entitlement to delete


> Example responses

```
```

### Responses

Status|Meaning|Description|Schema
---|---|---|---|
204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|OK|Inline
400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad Request|None
401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None
403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None
404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None

<aside class="warning">
To perform this operation, you must be authenticated via the `Authorization` header.
</aside>
