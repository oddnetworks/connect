# Authentication

Authentication is done via the `Authorization` header using a bearer token. You can retreive your organization's JWT from the Odd Connect dashboard.

* Bearer
  - `Authorization: Bearer your-organization-jwt`
