# Schemas

## Transactions

<a name="schematransactions"></a>

```json
{
  "data": [
    {
        "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
        "type": "transaction",
        "attributes": {
          "email": "foo@example.com",
          "entitlement_identifier": "monthly-subscription",
          "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
          "inserted_at": "2017-10-01T14:53:00Z",
          "invalidated_at": "2017-10-02T14:53:00Z",
          "invalidation_reason": "User Cancelled",
          "last_validated_at": "2017-10-02T14:45:00Z",
          "platform": "AMAZON",
          "product_identifier": "PROD1",
          "receipt": {
            "description": "object of any type allowed here"
          },
          "updated_at": "2017-10-01T14:53:00Z"
        }
    }
  ]
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
data|[[Transaction](#schematransaction)]|false|No description


## Transaction

<a name="schematransaction"></a>

```json
{
  "data": {
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "type": "transaction",
    "attributes": {
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "external_identifier": "53890817-32A4-4DC0-AA41-F1FB15C37DD6",
      "inserted_at": "2017-10-01T14:53:00Z",
      "invalidated_at": "2017-10-02T14:53:00Z",
      "invalidation_reason": "User Cancelled",
      "last_validated_at": "2017-10-02T14:45:00Z",
      "platform": "AMAZON",
      "product_identifier": "PROD1",
      "receipt": {
        "description": "object of any type allowed here"
      },
      "updated_at": "2017-10-01T14:53:00Z"
    }
  }
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
data|object|true|No description
» id|string(uuid)|true|No description
» type|string|true|`transaction`
» attributes|object|true|No description
»» email|string(email)|true|The Device User's email address
»» entitlement_identifier|string|true|The `external_identifier` for the associated Entitlement
»» external_identifier|string|true|The identifier for this transaction within the specified platform
»» inserted_at|string(date-time)|true|No description
»» invalidated_at|string(date-time)|true|When not `null`, this transaction is invalidated
»» invalidation_reason|string|true|No description
»» last_validated_at|string(date-time)|true|No description
»» platform|string|true|One of `AMAZON`, `APPLE`, `GOOGLE`, `ROKU`, `WEB`
»» product_identifier|string|true|The `exernal_identifier` for the associated Product
»» receipt|object|true|No description
»» updated_at|string(date-time)|true|An object containing arbitrary metadata


## PendingConnection

<a name="schemapendingconnection"></a>

```json
{
  "data": {
    "type": "connection",
    "attributes": {
      "email": "foo@example.com",
      "platform": "GOOGLE_MOBILE",
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24"
    }
  }
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
data|object|true|No description
» type|string|true|No description
» attributes|object|true|No description
»» email|string(email)|true|The Device User's email address
»» platform|string|true|One of `APPLE_MOBILE`, `APPLE_TV`, `AMAZON_MOBILE`, `AMAZON_TV`, `GOOGLE_MOBILE`, `GOOGLE_TV`, `ROKU`, `WEB`
»» device_identifier|string|true|An identifier unique to the specified platform.


## VerifiedConnection

<a name="schemaverifiedconnection"></a>

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "connection",
    "id": "ecf96bde-42e0-46a5-9e30-bd641250b6e2",
    "attributes": {
      "device_identifier": "09d89bcb-2de9-45b9-82a7-eeeb4db0cb24",
      "email": "foo@example.com",
      "entitlement_identifiers": [
        "monthly-subscription"
      ],
      "jwt": "a-jwt-for-your-connection",
      "organization_urn": "your-org-urn",
      "platform": "GOOGLE_MOBILE",
      "verified_at": "2017-05-03T02:21:42"
    }
  }
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
data|object|true|No description
» type|string|true|`connection`
» attributes|object|true|No description
»» device_identifier|string|true|An identifier unique to the specified platform.
»» email|string(email)|true|The Device User's email address
»» entitlement_identifiers|[string]|true|Any currently verified Entitlements the Device User has
»» jwt|string|true|A signed [JWT](http://jwt.io) with: `aud[0]=organization-urn`, `aud[1]=device-platform`, `aud[2]=device-identifier`, `sub=device-user-email`, `jti=connection-id`, and `iss=odd-connect:verified-connection`. The public signing key used to verify this JWT can be retrieved via Odd Connect dashboard.
»» organization_urn|string|true|Your Organization's urn
»» platform|string|true|One of `APPLE_MOBILE`, `APPLE_TV`, `AMAZON_MOBILE`, `AMAZON_TV`, `GOOGLE_MOBILE`, `GOOGLE_TV`, `ROKU`, `WEB`
»» verified_at|string(date-time)|true|When this user was verified for this device.


## Entitlements

<a name="schemaentitlements"></a>

```json
{
  "data": [
    {
      "id": "844a7770-fc63-4ffb-9674-d8db671a96f6",
      "type": "subscription",
      "attributes": {
        "name": "Monthly Subscription",
        "description": "Subscribe monthly for access to things",
        "entitlement_identifier": "monthly-subscription",
        "product_identifier": "roku-product-id",
        "platform": "ROKU",
        "inserted_at": "2017-10-10T18:00:00Z",
        "updated_at": "2017-10-10T18:00:00Z"
      }
    }
  ]
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
data|[[Entitlement](#schemaentitlement)]|true|No description


## Entitlement

<a name="schemaentitlement"></a>

```json
{
  "data": {
    "id": "844a7770-fc63-4ffb-9674-d8db671a96f6",
    "type": "subscription",
    "attributes": {
      "name": "Monthly Subscription",
      "description": "Subscribe monthly for access to things",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "roku-product-id",
      "platform": "ROKU",
      "inserted_at": "2017-10-10T18:00:00Z",
      "updated_at": "2017-10-10T18:00:00Z"
    }
  }
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
data|object|true|No description
» id|string(uuid)|true|No description
» type|string|true|One of `subscription`, `asset`
» attributes|object|true|No description
»» name|string|true|A human readable way to identify this Entitlement
»» description|body|string|false|No description
»» entitlement_identifier|body|string|true|A string unique to your organization.
»» product_identifier|body|string|true|The identifier of your product on the platform store/marketplace.
»» platform|body|string|true|One of `AMAZON`, `APPLE`, `GOOGLE`, `ROKU`, `WEB`
»» inserted_at|string(date-time)|true|No description
»» updated_at|string(date-time)|true|No description
