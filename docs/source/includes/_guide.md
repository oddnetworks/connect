# Guide

## Device User Connections

## Transactions and Entitlements

With the exception the `WEB` platform, the life-cycle of transactions associated with our supported 3rd party platforms can be automatically kept in sync by our system. What this means for your organization is that any subscription life-cycles that are not managed by one of our supported platforms (`APPLE`, `AMAZON`, `GOOGLE`, `ROKU`) must be manually managed via our API.

Essentially this means ensuring that any legacy or external subscriptions are known and kept in sync within our system.

## Planning Questions

Some good things to think about ahead of time would be:

### What types of entitlements will my organization require?

You will need a unique `external_identifier` for the entitlement. Does not have to be fancy (in the past we have used things like `silver`, `gold`, `member`, etc.). Whatever scheme is comfortable for your organization will work here.

### How will these entitlements be represented as products on the platforms to which my organization will be launching apps?

You will need the "external_identifier" of the product (ex. Roku's productID) when creating a product within our system.

### What requests will my organization's API/website need to send?

Prepare to make API requests to let Odd Connect know about the life-cycle of any transaction (entitlement link) that is managed by your system.

### What requests will my organization's API/website need to receive?

Prepare to receive web-hooks to know about the life-cycle of any transaction (entitlement link) that is managed by Odd and our supported platforms (Roku, Apple, Amazon, Google).
