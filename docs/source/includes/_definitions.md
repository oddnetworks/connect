# Definitions

__organization__ - This resource represents your organization within our system. It as an URN that you define, with no specific pattern, that must be unique within our system. The `URN` may be used to identify your organization in API requests/responses.

__device__ - This resource is essentially the identifier of a unique physical device itself or the identifier provided by an app install on a physical device (depending on how the platform decides what apps can know about a device). It can used to tell how many devices (or apps) an individual is currently authenticated to or has been linked to in the past.

__platform__ - This is an identifier representing one of two different things.

For a `transaction` or `product`, the platform will be one of the following values, representing the external marketplace of the given resource: `APPLE`, `AMAZON`, `GOOGLE`, `ROKU`, `WEB`

For a `connection`, the platform is a bit more specific so that we can specify the type of device making the connection request. The following values are valid: `APPLE_MOBILE`, `APPLE_TV`, `AMAZON_MOBILE`, `AMAZON_TV`, `GOOGLE_MOBILE`, `GOOGLE_TV`, `ROKU`, `WEB`.

__entitlement__ - This resource represents either a `subscription`, or a digital `asset` that you want to give access to. What you will need here is a unique (to your organization) `entitlement_identifier` - which does not have a specific pattern, just a uniqueness constraint within your organization - which will be used to relate this Odd Connect Entitlement to either a subscription level or other digital resource that can be accessed via your Organization's users. `entitlement_identifier` will be used in API requests/responses to identify your organization's entitlement(s). The `platform` and `product_identifier` values are used together to determine which product is being represented on the external platform store/marketplace and therefore must be a unique combination to your organization.

__device user__ - This is the representation user who logs in on devices, purchases products, and has entitlements via transactions. We use the device user's `email` attribute as a unique identifier within API requests/responses.

__transaction__ - This resource is the representation of a completed purchase on a device platform. This resource's `external_identifier` will be unique to the `platform` attribute within Odd Connect. The `external_identifier` will be used to identify a transaction in API requests/responses.
