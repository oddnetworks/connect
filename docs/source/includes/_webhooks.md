# Web-Hooks

Odd Connect provides the following Web-Hooks that are configurable via your Organization settings at <a href="https://oddconnect.com/app">oddconnect.com</a>

## Entitlement

This web-hook is triggered when an entitlement is created or updated for an organization.

```http
POST /your-entitlement-endpoint HTTP/1.1
Host: your-host.com
Content-Type: application/json; charset=utf-8

{
  "jsonapi": {
    "version":"1.0"
  },
  "data": {
    "type": "subscription",
    "id": "an_id",
    "attributes": {
      "name": "Gold Subscription",
      "description": "A Golden Subscription",
      "entitlement_identifier": "entitlement-external-id",
      "product_identifier": "platfrom-product-id",
      "platform": "WEB",
      "inserted_at": "2018-04-05T17:32:18Z",
      "updated_at": "2018-04-05T17:32:18Z"
    }
  }
}
```

## Verified Connection

This web-hook is triggered when a device user has completed the authentication flow on a platform, linking them to a device

```http
POST /your-verified-connection-endpoint HTTP/1.1
Host: your-host.com
Content-Type: application/json; charset=utf-8

{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "connection",
    "id": "2ba26de3-4a34-44d7-ab87-18c2b92ab8b3",
    "attributes": {
      "email": "foo@example.com",
      "organization_urn": "my-org",
      "device_identifier": "81ac8798-ed9e-40b4-b728-8949a3afd50a",
      "platform": "ROKU",
      "verified_at": "2017-10-20T01:23:45Z"
    }
  }
}
```


## Invalidated Transaction

This web-hook is triggered when device user's link to an entitlement has been revoked due to invalidation of a transaction (cancellation, expiration, etc).

```http
POST /your-invalidated-transaction-endpoint HTTP/1.1
Host: your-host.com
Content-Type: application/json; charset=utf-8

{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "transaction",
    "id": "2ba26de3-4a34-44d7-ab87-18c2b92ab8b3",
    "attributes": {
      "external_identifier": "A602ABF7-77BA-4B74-84ED-34EAB72BBC77",
      "platform": "ROKU",
      "invalidated_at": "2017-10-20T01:45:30Z",
      "invalidation_reason": "Cancelled Subscription",
      "receipt": {
        "transaction_id": "A602ABF7-77BA-4B74-84ED-34EAB72BBC77",
        "cancelled": true,
        "purchase_date": "2017-10-15T15:05:00Z",
        "channel_name": "My Store",
        "product_name": "Monthly Subscription",
        "product_id": "MONTHLY1",
        "amount": "9.99",
        "currency": "USD",
        "quantity": "1",
        "roku_customer_id": "rokuCustomerId",
        "expiration_date": "2017-11-15T15:05:00Z",
        "original_purchase_date": "2017-10-15T15:05:00Z"
      },
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "MONTHLY1",
      "last_validated_at": "2017-10-20T00:30:15Z",
      "inserted_at": "2017-10-15T15:05:00Z",
      "updated_at": "2017-10-20T01:45:30Z"
    }
  }
}
```


## New Transaction

This web-hook is triggered when a device user has purchased a product from a platform and has been linked to an entitlement

```http
POST /your-new-transaction-endpoint HTTP/1.1
Host: your-host.com
Content-Type: application/json; charset=utf-8

{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "transaction",
    "id": "2ba26de3-4a34-44d7-ab87-18c2b92ab8b3",
    "attributes": {
      "external_identifier": "A602ABF7-77BA-4B74-84ED-34EAB72BBC77",
      "platform": "ROKU",
      "invalidated_at": null,
      "invalidation_reason": null,
      "receipt": {
        "transaction_id": "A602ABF7-77BA-4B74-84ED-34EAB72BBC77",
        "cancelled": false,
        "purchase_date": "2017-10-15T15:05:00Z",
        "channel_name": "My Store",
        "product_name": "Monthly Subscription",
        "product_id": "MONTHLY1",
        "amount": "9.99",
        "currency": "USD",
        "quantity": "1",
        "roku_customer_id": "rokuCustomerId",
        "expiration_date": "2017-11-15T15:05:00Z",
        "original_purchase_date": "2017-10-15T15:05:00Z"
      },
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "MONTHLY1",
      "last_validated_at": "2017-10-20T00:30:15Z",
      "inserted_at": "2017-10-15T15:05:00Z",
      "updated_at": "2017-10-20T01:45:30Z"
    }
  }
}
```

## Deleted Transaction

This web-hook is triggered when a device user has purchased a product from a platform and has been linked to an entitlement

```http
POST /your-deleted-transaction-endpoint HTTP/1.1
Host: your-host.com
Content-Type: application/json; charset=utf-8

{
  "jsonapi": {
    "version": "1.0"
  },
  "data": {
    "type": "transaction",
    "id": "2ba26de3-4a34-44d7-ab87-18c2b92ab8b3",
    "attributes": {
      "external_identifier": "A602ABF7-77BA-4B74-84ED-34EAB72BBC77",
      "platform": "ROKU",
      "invalidated_at": null,
      "invalidation_reason": null,
      "receipt": {
        "transaction_id": "A602ABF7-77BA-4B74-84ED-34EAB72BBC77",
        "cancelled": false,
        "purchase_date": "2017-10-15T15:05:00Z",
        "channel_name": "My Store",
        "product_name": "Monthly Subscription",
        "product_id": "MONTHLY1",
        "amount": "9.99",
        "currency": "USD",
        "quantity": "1",
        "roku_customer_id": "rokuCustomerId",
        "expiration_date": "2017-11-15T15:05:00Z",
        "original_purchase_date": "2017-10-15T15:05:00Z"
      },
      "email": "foo@example.com",
      "entitlement_identifier": "monthly-subscription",
      "product_identifier": "MONTHLY1",
      "last_validated_at": "2017-10-20T00:30:15Z",
      "inserted_at": "2017-10-15T15:05:00Z",
      "updated_at": "2017-10-20T01:45:30Z"
    }
  }
}
```
