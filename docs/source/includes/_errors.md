# Errors

The Odd Connect API uses the following error codes:


Error Code | Meaning
---------- | -------
400 | Bad Request -- Your request is malformed.
401 | Unauthorized -- Your API key is wrong.
403 | Forbidden -- The resource requested is not accessible by you.
404 | Not Found -- The specified resource could not be found.
405 | Method Not Allowed -- You tried to access a something with an invalid method.
406 | Not Acceptable -- You requested a format that isn't json.
429 | Too Many Requests -- You're requesting too many resources.
500 | Internal Server Error -- We had a problem with our server. Try again later.
503 | Service Unavailable -- We're temporarily offline for maintenance. Please try again later.


## error

<a name="schemaerror"></a>

```json
{
  "errors": [
    {
      "status": "404",
      "title": "Not Found",
      "detail": "Resource not found",
    }
  ]
}
```

### Properties

Name|Type|Required|Description
---|---|---|---|
errors|array|true|Contains one or more error objects
»error|object|true|No description
»»status|string|false|The HTTP status code applicable to this problem, expressed as a string value.
»»title|string|false|A short, human-readable summary of the problem. It **SHOULD NOT** change from occurrence to occurrence of the problem, except for purposes of localization.
»»detail|string|false|A human-readable explanation specific to this occurrence of the problem.
