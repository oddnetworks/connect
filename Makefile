.PHONY: help
.DEFAULT_GOAL := help


SHELL=/bin/bash

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

dev: ## [HOST] - docker-compose run --rm --service-ports web
	@docker-compose -p connect -f ./docker/docker-compose.yml run --rm --service-ports web

docs-build: ## [HOST] - builds docs in docker container
	@docker-compose -p connect -f ./docker/docker-compose.yml run --rm -e BUILT=true docs

docs-serve: ## [HOST] - serves the docs independent of odd_connect on port 3000
	@docker-compose -p connect -f ./docker/docker-compose.yml run --rm -p 3000:3000 docs sh -c "bundle exec middleman server -p 3000"

bootstrap: deps-get db-reset db-seed npm-install ## [GUEST] - deps-get db-reset db-seed npm-install

db-drop: ## [GUEST] - mix ecto.drop
	@mix ecto.drop
db-setup: ## [GUEST] - mix do ecto.create, ecto.migrate
	@mix do ecto.create, ecto.migrate
db-seed: ## [GUEST] - mix run /opt/app/apps/connect/priv/repo/seeds.exs
	@mix run /opt/app/apps/connect/priv/repo/seeds.exs
db-reset: db-drop db-setup ## [GUEST] - db-drop db-setup
db-migrate: ## [GUEST] - mix ecto.migrate
	@mix ecto.migrate

psql: ## [GUEST] - psql -U postgres -h postgres -d odd_connect_dev
	@psql -U postgres -h postgres -d odd_connect_dev

npm-install: ## [GUEST] - npm install
	@pushd /opt/app/apps/front_end/assets && \
   	npm prune && \
   	npm install && \
   	npm rebuild 2>&1 && \
   	npm prune && \
	popd

deps-get: ## [GUEST] - mix do deps.get, deps.compile
	@mix do deps.get, deps.compile
