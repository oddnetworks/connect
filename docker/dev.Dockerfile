# https://dev.bleacherreport.com/starting-a-phoenix-project-with-docker-ab2a171fa2f4
# https://dev.bleacherreport.com/brunch-in-a-container-phoenix-frontend-builds-and-docker-bbf15e03585b
FROM elixir:1.7-alpine

# dev dependencies
RUN \
	mkdir -p /opt/app && \
	chmod -R 777 /opt/app && \
	apk update && \
	apk --no-cache --update add \
	bash git make g++ wget curl inotify-tools \
	nodejs=8.11.4-r0 npm=8.11.4-r0 postgresql-client=10.5-r0 && \
	npm install npm -g --no-progress && \
	update-ca-certificates --fresh && \
	rm -rf /var/cache/apk/*

# install hex & phoenix
RUN mix local.hex --force && \
	mix local.rebar --force && \
	mix archive.install https://github.com/phoenixframework/archives/raw/master/phoenix_new-1.2.5.ez --force

ENV PS1='$MIX_ENV-${debian_chroot:+($debian_chroot)}\u@\h:\w\$ ' \
	TERM=xterm \
	PROJECT_HOME=/opt/app \
	PHOENIX_HOME=/opt/app/apps/front_end \
	ASSETS_DIR=assets \
	PATH=$PHOENIX_HOME/$ASSETS_DIR/node_modules/.bin:$PATH \
	PGPASSWORD=postgres

WORKDIR $PROJECT_HOME

CMD sh -c "/bin/bash"
