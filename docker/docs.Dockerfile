FROM ruby:2.4-alpine3.6 as docs-builder

# install nodejs, npm & postgresql-client
RUN mkdir -p /opt/app && \
	apk update && \
	apk --no-cache --update add make g++ nodejs=6.10.3-r2 nodejs-npm=6.10.3-r2

ENV HOME=/opt/app \
	BUILT=true

COPY docs/ $HOME/

WORKDIR $HOME

RUN bundle install
CMD bundle exec middleman build --clean $HOME/build
