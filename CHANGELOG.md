v1.2.0

- adds front-end trigger for entitlement webhook when create/update occurs

v1.1.0

- improves organization select view
- adds ability to edit account details when logged in #34
- adds ability to change password when logged in #34
- removes account registration related code

v1.0.1

- adds entitlement webhook with documentation

v1.0.0

- began semver for project
- adds forgot password functionality #33
- updates Docker files for alpine linux, elixir, node versions (not using alpine for node in prod rel cuz node-sass)
- ensures ISO8601 date format for dates in API/Webhook JSON #65
- fixes URL host issue in connection email #64
