defmodule Connections do
  alias Connections.{Organization, PendingConnection, VerifiedConnection}
  alias Connect.Repo
  alias Ecto.Multi

  @webhook_module Application.get_env(:connections, :webhook_module) || Webhooks

  def get_verified_connection_by(args) do
    VerifiedConnection
    |> Repo.get_by(args)
    |> Repo.preload([:device, :device_user, :organization])
  end

  def create(params) do
    with {:ok, connection} <- insert_pending_connection(params),
         connection when not is_nil(connection) <- reload_connection_with_organization(connection) do
      deliver_device_connection_email(connection, connection.organization)

      {:ok, connection}
    else
      nil -> {:error, "Organization Not Found"}
      {:error, _reason} = error -> error
    end
  end

  def create_verified(params) do
    Multi.new()
    |> Multi.run(:pending_connection, fn _ ->
      %{urn: urn} =
        Organization
        |> Repo.get(Map.get(params, :organization_id))

      id = UUID.uuid4()

      {:ok, jwt} =
        Auth.ConnectionToken.generate_jwt(%{
          organization_urn: urn,
          platform: Map.get(params, :platform),
          device_identifier: Map.get(params, :device_identifier),
          email: Map.get(params, :email),
          id: id
        })

      %PendingConnection{}
      |> PendingConnection.pending_changeset(params)
      |> Ecto.Changeset.put_change(:id, id)
      |> Ecto.Changeset.put_change(:jwt, jwt)
      |> Ecto.Changeset.put_change(:verified_at, NaiveDateTime.utc_now())
      |> Repo.insert()
    end)
    |> Multi.merge(fn %{pending_connection: pending_connection} ->
      Connections.VerifiedConnectionService.verify_pending_connection(pending_connection)
    end)
    |> Multi.run(:triggered_webhook, fn %{
                                          verified_connection: %{id: vcid},
                                          organization: %{id: oid}
                                        } ->
      @webhook_module.trigger(:verified_connection, {vcid, oid})

      {:ok, "triggered"}
    end)
    |> Repo.transaction()
  end

  def verify(access_token) do
    Connections.VerificationService.run(access_token)
    |> Repo.transaction()
  end

  defp insert_pending_connection(params) do
    %PendingConnection{}
    |> PendingConnection.pending_changeset(params)
    |> Repo.insert()
  end

  defp reload_connection_with_organization(%{id: id}) do
    PendingConnection
    |> Repo.get(id)
    |> Repo.preload(organization: [:templates])
  end

  defp deliver_device_connection_email(%{in_review_mode: true}, _organization),
    do: :ok

  defp deliver_device_connection_email(
         %{access_token: access_token, email: email, uri: uri_string},
         organization
       ) do
    uri = URI.parse(uri_string)

    link =
      %{
        uri
        | path: "#{uri.path}#{access_token}"
      }
      |> URI.to_string()

    Organization.Email.prepare_device_connection_email(organization, link, email)
    |> Messenger.deliver_email()
  end
end
