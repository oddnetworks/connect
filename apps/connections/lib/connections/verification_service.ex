defmodule Connections.VerificationService do
  @moduledoc """
  Responsible for creating a VerifiedConnection out of a PendingConnection
  """
  alias Connections.PendingConnection
  alias Connect.Repo
  alias Ecto.Multi

  @webhook_module Application.get_env(:connections, :webhook_module) || Webhooks

  @spec run(String.t()) :: {:error, atom, %Ecto.Changeset{}, map} | {:ok, map}
  def run(access_token) do
    Multi.new()
    |> Multi.run(:pending_connection, fn _ ->
      PendingConnection
      |> Repo.get_by(access_token: access_token)
      |> Repo.preload([:organization])
      |> case do
        nil ->
          {:error, "Not Found"}

        %{verified_at: verified_at} = pending_connection when not is_nil(verified_at) ->
          {:ok, pending_connection}

        %{expires_at: expires_at} = pending_connection ->
          now = NaiveDateTime.utc_now()

          if NaiveDateTime.compare(expires_at, now) == :gt do
            create_pending_connection_changeset(pending_connection, now)
            |> Repo.insert_or_update()
          else
            {:error, "Expired"}
          end
      end
    end)
    |> Multi.merge(fn %{pending_connection: pending_connection} ->
      Connections.VerifiedConnectionService.verify_pending_connection(pending_connection)
    end)
    |> Multi.run(:triggered_webhook, fn %{
                                          verified_connection: %{id: vcid},
                                          organization: %{id: oid}
                                        } ->
      @webhook_module.trigger(:verified_connection, {vcid, oid})

      {:ok, "triggered"}
    end)
  end

  defp create_pending_connection_changeset(%{verified_at: vat} = connection, _now)
       when not is_nil(vat) do
    connection
    |> PendingConnection.verified_changeset(%{})
  end

  defp create_pending_connection_changeset(connection, now) do
    {:ok, jwt} =
      Auth.ConnectionToken.generate_jwt(%{
        organization_urn: connection.organization.urn,
        platform: connection.platform,
        device_identifier: connection.device_identifier,
        email: connection.email,
        id: connection.id
      })

    connection
    |> PendingConnection.verified_changeset(%{verified_at: now, jwt: jwt})
  end
end
