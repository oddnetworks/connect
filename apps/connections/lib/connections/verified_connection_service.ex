defmodule Connections.VerifiedConnectionService do
  alias Connections.{Device, DeviceUser, Organization, PlatformIdentity, VerifiedConnection}
  alias Connect.Repo
  alias Ecto.Multi

  def verify_pending_connection(pending_connection) do
    Multi.new()
    |> Multi.run(:organization, Connections.VerifiedConnectionService, :find_organization, [
      pending_connection
    ])
    |> Multi.run(:device, Connections.VerifiedConnectionService, :insert_or_find_device, [
      pending_connection
    ])
    |> Multi.run(
      :device_user,
      Connections.VerifiedConnectionService,
      :insert_or_find_device_user,
      [
        pending_connection
      ]
    )
    |> Multi.run(
      :platform_identity,
      Connections.VerifiedConnectionService,
      :insert_or_find_platform_identity,
      [
        pending_connection
      ]
    )
    |> Multi.run(
      :verified_connection,
      Connections.VerifiedConnectionService,
      :insert_or_find_verified_connection,
      [
        pending_connection
      ]
    )
  end

  def find_organization(_, pending_connection) do
    case Repo.get(Organization, pending_connection.organization_id) do
      nil -> {:error, "Organization not found"}
      organization -> {:ok, organization}
    end
  end

  def insert_or_find_device(_, pending_connection) do
    case Repo.get_by(Device,
           external_identifier: pending_connection.device_identifier,
           platform: pending_connection.platform
         ) do
      nil -> %Device{}
      device -> device
    end
    |> Device.build_changeset(%{
      external_identifier: pending_connection.device_identifier,
      platform: pending_connection.platform
    })
    |> Repo.insert_or_update()
  end

  def insert_or_find_device_user(_, pending_connection) do
    case Repo.get_by(DeviceUser, email: pending_connection.email) do
      nil -> %DeviceUser{}
      device_user -> device_user
    end
    |> DeviceUser.build_changeset(%{email: pending_connection.email})
    |> Repo.insert_or_update()
  end

  def insert_or_find_platform_identity(_, %{platform_identity: nil}), do: {:ok, nil}

  def insert_or_find_platform_identity(%{device_user: %{id: device_user_id}}, pending_connection) do
    base_platform = get_base_platform(pending_connection.platform)

    case Repo.get_by(PlatformIdentity,
           device_user_id: device_user_id,
           platform: base_platform,
           external_identifier: pending_connection.platform_identity
         ) do
      nil ->
        %PlatformIdentity{}
        |> PlatformIdentity.build_changeset(%{
          device_user_id: device_user_id,
          platform: base_platform,
          external_identifier: pending_connection.platform_identity
        })
        |> Repo.insert()

      platform_identity ->
        {:ok, platform_identity}
    end
  end

  def insert_or_find_verified_connection(
        %{device: device, device_user: device_user, organization: organization},
        pending_connection
      ) do
    Repo.get(VerifiedConnection, pending_connection.id)
    |> Repo.preload([:organization])
    |> case do
      nil ->
        %VerifiedConnection{}
        |> VerifiedConnection.build_changeset(%{
          id: pending_connection.id,
          device_id: device.id,
          device_user_id: device_user.id,
          organization_id: organization.id,
          verified_at: pending_connection.verified_at,
          jwt: pending_connection.jwt,
          verified_during_review: pending_connection.verified_during_review
        })
        |> Repo.insert()
        |> case do
          {:error, _} = error ->
            error

          {:ok, %{id: vcid}} ->
            {:ok,
             Repo.get(VerifiedConnection, vcid)
             |> Repo.preload([:organization, :device_user, :device])}
        end

      verified_connection ->
        {:ok, verified_connection}
    end
  end

  defp get_base_platform(platform) do
    platform
    |> String.split("_")
    |> hd()
  end
end
