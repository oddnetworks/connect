defmodule Connections.PlatformIdentity do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  alias Connections.DeviceUser

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "platform_identities" do
    field(:platform, :string)
    field(:external_identifier, :string)

    belongs_to(:device_user, DeviceUser)

    timestamps()
  end

  @params [:platform, :external_identifier, :device_user_id]

  @valid_platforms [
    "AMAZON",
    "APPLE",
    "GOOGLE",
    "ROKU",
    "WEB"
  ]

  def build_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params)
    |> validate_required(@params)
    |> validate_inclusion(:platform, @valid_platforms)
    |> unique_constraint(:external_identifier,
      name: :platform_identities_platform_external_identifier_index
    )
    |> assoc_constraint(:device_user)
  end
end
