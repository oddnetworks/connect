defmodule Connections.Organization.Email do
  defstruct from: nil, html: nil, subject: nil, text: nil, to: nil

  def device_connection_email_subject_default, do: "{{organization_name}} - New Device Connection"

  def device_connection_email_text_default,
    do: """
    An attempt has been made to connect a new device to {{organization_name}} for the following account:

    {{email}}

    To verify this connection, please click the following link:

    {{link}}

    If you did not attempt to connect your device to {{organization_name}}, you may safely ignore this message.
    """

  def device_connection_email_html_default,
    do: """
    <h3>New Device Connection</h3>
    <p>An attempt has been made to connect a new device to <strong>{{organization_name}}</strong> for the following account:</p>
    <p>{{email}}</p>
    <p>To verify this connection, please click the following link:</p>
    <p><a href="{{link}}">{{link}}</a><br></p>
    <hr>
    <p>If you did not attempt to connect your device to {{organization_name}}, you may safely ignore this message.</p>
    """

  def device_connection_email_fields, do: [:email, :link, :organization_name]

  def prepare_device_connection_email(organization, link, email) do
    params = %{
      email: email,
      link: link,
      organization_name: organization.name
    }

    subject =
      Map.get(
        organization.templates,
        :device_connection_email_subject,
        device_connection_email_subject_default()
      )

    html_template =
      Map.get(
        organization.templates,
        :device_connection_email_html,
        device_connection_email_html_default()
      )

    text_template =
      Map.get(
        organization.templates,
        :device_connection_email_text,
        device_connection_email_text_default()
      )

    %Connections.Organization.Email{
      to: email,
      from: "no-reply@#{organization.urn}.oddconnect.com",
      subject: parse_template(subject, params),
      html: parse_template(html_template, params),
      text: parse_template(text_template, params)
    }
  end

  defp parse_template(template, params) do
    params
    |> Map.keys()
    |> Enum.reduce(template, fn field, tpl ->
      placeholder = "{{#{field}}}"
      value = Map.get(params, field)

      tpl
      |> String.replace(placeholder, value)
    end)
  end
end
