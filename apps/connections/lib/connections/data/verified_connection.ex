defmodule Connections.VerifiedConnection do
  use Ecto.Schema
  import Ecto.Changeset

  alias Connections.{Device, DeviceUser, Organization}

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "verified_connections" do
    field(:verified_at, :naive_datetime)
    field(:verified_during_review, :boolean, default: false)
    field(:jwt, :string)

    belongs_to(:device, Device)
    belongs_to(:device_user, DeviceUser)
    belongs_to(:organization, Organization)
  end

  @params [
    :id,
    :device_id,
    :device_user_id,
    :organization_id,
    :verified_at,
    :jwt,
    :verified_during_review
  ]

  @required_params [
    :id,
    :device_id,
    :device_user_id,
    :organization_id,
    :verified_at,
    :jwt
  ]

  def build_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params)
    |> validate_required(@required_params)
    |> assoc_constraint(:device)
    |> assoc_constraint(:device_user)
    |> assoc_constraint(:organization)
  end
end
