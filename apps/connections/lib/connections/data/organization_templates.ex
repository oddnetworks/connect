defmodule Connections.Organization.Templates do
  use Ecto.Schema

  alias Connections.Organization

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organization_templates" do
    field(:device_connection_email_subject)
    field(:device_connection_email_html)
    field(:device_connection_email_text)

    belongs_to(:organization, Organization)
  end
end
