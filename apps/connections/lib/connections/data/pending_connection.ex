defmodule Connections.PendingConnection do
  use Ecto.Schema
  import Ecto.Changeset

  alias Connections.Organization

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "pending_connections" do
    field(:access_token, :string)
    field(:email, :string)
    field(:device_identifier, :string)
    field(:platform, :string)
    field(:platform_identity, :string)
    field(:uri, :string)
    field(:expires_at, :naive_datetime)
    field(:verified_at, :naive_datetime)
    field(:jwt, :string)
    field(:verified_during_review, :boolean, default: false)

    belongs_to(:organization, Organization)

    timestamps()
  end

  @params [
    :access_token,
    :email,
    :device_identifier,
    :platform,
    :platform_identity,
    :uri,
    :expires_at,
    :verified_during_review,
    :organization_id
  ]

  @required_params [
    :access_token,
    :email,
    :device_identifier,
    :platform,
    :uri,
    :expires_at,
    :organization_id
  ]

  @verified_params [
    :verified_at,
    :jwt
  ]

  def pending_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params)
    |> ensure_access_token()
    |> ensure_expires_at()
    |> validate_required(@required_params)
    |> validate_length(:device_identifier, min: 1, max: 255)
    |> validate_length(:email, min: 4, max: 255)
    |> validate_format(:email, ~r/@/)
    |> assoc_constraint(:organization)
    |> unique_constraint(:access_token)
  end

  def verified_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params ++ @verified_params)
    |> ensure_access_token()
    |> ensure_expires_at()
    |> validate_required(@required_params ++ @verified_params)
    |> validate_length(:device_identifier, min: 1, max: 255)
    |> validate_length(:email, min: 4, max: 255)
    |> validate_format(:email, ~r/@/)
    |> assoc_constraint(:organization)
    |> unique_constraint(:access_token)
  end

  defp ensure_access_token(changeset) do
    case get_field(changeset, :access_token) do
      v when is_binary(v) ->
        changeset

      _ ->
        changeset
        |> put_change(:access_token, generate_token())
    end
  end

  defp ensure_expires_at(changeset) do
    case get_field(changeset, :expires_at) do
      nil ->
        changeset
        |> put_change(:expires_at, generate_expires_at())

      _ ->
        changeset
    end
  end

  defp generate_token() do
    UUID.uuid4()
  end

  defp generate_expires_at() do
    NaiveDateTime.utc_now()
    |> NaiveDateTime.add(get_ttl_in_seconds(), :second)
  end

  defp get_ttl_in_seconds() do
    {ttl_in_seconds, _} =
      Application.get_env(:connections, :connection_ttl_in_seconds)
      |> to_string()
      |> Integer.parse()

    ttl_in_seconds
  end
end
