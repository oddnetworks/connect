defmodule Connections.Device do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "devices" do
    field(:external_identifier, :string)
    field(:platform, :string)

    timestamps()
  end

  @params [
    :external_identifier,
    :platform
  ]
  @valid_platforms [
    "AMAZON_MOBILE",
    "AMAZON_TV",
    "APPLE_MOBILE",
    "APPLE_TV",
    "GOOGLE_MOBILE",
    "GOOGLE_TV",
    "ROKU",
    "WEB"
  ]

  def build_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params)
    |> validate_required(@params)
    |> validate_length(:external_identifier, min: 1, max: 255)
    |> validate_inclusion(:platform, @valid_platforms)
    |> unique_constraint(:external_identifier)
  end
end
