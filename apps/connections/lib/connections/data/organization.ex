defmodule Connections.Organization do
  use Ecto.Schema

  alias Connections.Organization.{Settings, Templates}

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organizations" do
    field(:urn, :string)
    field(:name, :string)

    has_one(:templates, Templates)
    has_one(:settings, Settings)

    timestamps()
  end
end
