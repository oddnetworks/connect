use Mix.Config

config :connections, :webhook_module, Webhooks.TestTrigger

config :logger,
  backends: [:console],
  utc_log: true,
  log_level: :warn

config :logger, :console,
  format: "[$level] $message\n",
  level: :warn
