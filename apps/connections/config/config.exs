use Mix.Config

config :connections,
       :connection_ttl_in_seconds,
       System.get_env("VERIFIER_CONNECTION_TTL_IN_SECONDS") || "108000"

import_config "#{Mix.env()}.exs"
