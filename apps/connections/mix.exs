defmodule Connections.Mixfile do
  use Mix.Project

  def project do
    [
      app: :connections,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [main: "Connections"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :timber],
      mod: {Connections.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:immortal, "~> 0.2.2"},
      {:uuid, "~> 1.1.7"},
      {:timber, "~> 2.6"},
      {:auth, in_umbrella: true},
      {:connect, in_umbrella: true},
      {:messenger, in_umbrella: true},
      {:webhooks, in_umbrella: true}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
