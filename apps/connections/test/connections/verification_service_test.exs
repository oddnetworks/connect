defmodule Connections.VerificationServiceTest do
  use Connections.Case

  alias Connections.VerificationService

  setup _tags do
    org = insert(:organization)
    pend = insert(:pending_connection, organization: org)

    {:ok, org: org, pend: pend}
  end

  describe "run/1" do
    test "invalid access token" do
      assert {:error, :pending_connection, "Not Found", %{}} = VerificationService.run("blah")
      |> Repo.transaction()
    end

    test "valid access token", %{pend: pend} do
      assert Repo.aggregate(Connections.PendingConnection, :count, :id) == 1
      assert Repo.aggregate(Connections.DeviceUser, :count, :id) == 0
      assert Repo.aggregate(Connections.Device, :count, :id) == 0
      assert Repo.aggregate(Connections.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Connections.VerifiedConnection, :count, :id) == 0

      assert {:ok, verified_connection} =
               VerificationService.run(pend.access_token)
               |> Repo.transaction()

      assert Repo.aggregate(Connections.PendingConnection, :count, :id) == 1
      assert Repo.aggregate(Connections.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Connections.Device, :count, :id) == 1
      assert Repo.aggregate(Connections.PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(Connections.VerifiedConnection, :count, :id) == 1
    end
  end
end
