defmodule Connections.VerifiedConnectionServiceTest do
  use Connections.Case

  alias Connections.{
    Device,
    DeviceUser,
    PlatformIdentity,
    VerifiedConnection,
    VerifiedConnectionService
  }

  describe "run/1" do
    setup _tags do
      plats = [
        "AMAZON_MOBILE",
        "AMAZON_TV",
        "APPLE_MOBILE",
        "APPLE_TV",
        "GOOGLE_MOBILE",
        "GOOGLE_TV",
        "ROKU",
        "WEB"
      ]

      org = insert(:organization)
      duser = insert(:device_user)
      dev = insert(:device)

      pi_plat =
        dev.platform
        |> String.split("_")
        |> hd()

      pi = insert(:platform_identity, platform: pi_plat, device_user: duser)

      pc = %{
        id: "76d44f46-16b6-4661-a5c6-cfdff9dbba36",
        access_token: "30d07c5d-73f7-49e9-ba88-4f303a26cdb7",
        email: duser.email,
        device_identifier: dev.external_identifier,
        platform: dev.platform,
        platform_identity: pi.external_identifier,
        uri: "http://www.example.com",
        expires_at: NaiveDateTime.add(NaiveDateTime.utc_now(), 3600, :second),
        organization_id: org.id,
        verified_at: NaiveDateTime.utc_now(),
        verified_during_review: false,
        jwt: "pretend-this-is-a-jwt,ok?"
      }

      vc =
        insert(:verified_connection,
          id: pc.id,
          organization: org,
          device_user: duser,
          device: dev
        )

      {:ok, org: org, duser: duser, dev: dev, pc: pc, vc: vc, plats: plats}
    end

    test "given unknown organization", %{pc: pc} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      pending = %{pc | organization_id: "500672f7-640e-4b5c-99b6-db6155894590"}

      assert {:error, :organization, "Organization not found", %{}} =
               VerifiedConnectionService.verify_pending_connection(pending)
               |> Repo.transaction()

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given PendingConnection with new device_identifier, email, and platform_identity - creates Device, DeviceUser, PlatformIdentity and VerifiedConnection",
         %{
           org: org
         } do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      pc = %{
        id: "2341c2e7-f067-4d0f-a0df-af068d0bdbc7",
        access_token: "6ce21b0e-e6d2-4244-b3f8-c569a04ae372",
        email: "fooer@foobar.foo",
        device_identifier: "device-id-newish",
        platform: "AMAZON_MOBILE",
        platform_identity: "platform-id-user-guy",
        uri: "https://www.example.com",
        expires_at: NaiveDateTime.add(NaiveDateTime.utc_now(), 3600, :second),
        organization_id: org.id,
        verified_at: NaiveDateTime.utc_now(),
        verified_during_review: false,
        jwt: "i-like-pretending-stuff-is-real"
      }

      assert {:ok, %{verified_connection: verified_connection}} =
               VerifiedConnectionService.verify_pending_connection(pc)
               |> Repo.transaction()

      assert %{id: device_user_id} = Repo.get_by(DeviceUser, email: pc.email)

      assert %{id: device_id} =
               Repo.get_by(Device,
                 platform: pc.platform,
                 external_identifier: pc.device_identifier
               )

      assert verified_connection.id == pc.id
      assert verified_connection.organization_id == pc.organization_id
      assert verified_connection.device_user_id == device_user_id
      assert verified_connection.device_id == device_id
      assert verified_connection.verified_at == pc.verified_at
      assert verified_connection.jwt == pc.jwt

      assert Repo.aggregate(DeviceUser, :count, :id) == 2
      assert Repo.aggregate(Device, :count, :id) == 2
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 2
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2
    end

    test "given new PendingConnection with existing email, device_identifier and platform_identity - only creates VerifiedConnection",
         %{pc: existing_pc} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      pc = %{
        id: "c13b5775-2153-434c-a75d-f170577f7f80",
        access_token: "dc9d8f63-8344-4832-af50-29589ec16ad3",
        email: existing_pc.email,
        device_identifier: existing_pc.device_identifier,
        platform: existing_pc.platform,
        platform_identity: existing_pc.platform_identity,
        organization_id: existing_pc.organization_id,
        verified_at: NaiveDateTime.utc_now(),
        verified_during_review: false,
        jwt: "pretend-this-is-another-jwt,got-it?"
      }

      assert {:ok, %{verified_connection: verified_connection}} =
               VerifiedConnectionService.verify_pending_connection(pc)
               |> Repo.transaction()

      assert %{id: device_user_id} = Repo.get_by(DeviceUser, email: pc.email)

      assert %{id: device_id} =
               Repo.get_by(Device,
                 platform: pc.platform,
                 external_identifier: pc.device_identifier
               )

      assert verified_connection.id == pc.id
      assert verified_connection.organization_id == pc.organization_id
      assert verified_connection.device_user_id == device_user_id
      assert verified_connection.device_id == device_id
      assert verified_connection.verified_at == pc.verified_at
      assert verified_connection.jwt == pc.jwt

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2
    end

    test "given a PendingConnection that already has a corresponding VerifiedConnection - returns existing VerifiedConnection",
         %{
           pc: pc,
           vc: %{
             id: id,
             organization_id: organization_id,
             device_id: device_id,
             device_user_id: device_user_id,
             verified_at: verified_at,
             jwt: jwt,
             verified_during_review: verified_during_review
           }
         } do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      assert {:ok,
              %{
                verified_connection: %{
                  id: ^id,
                  organization_id: ^organization_id,
                  device_id: ^device_id,
                  device_user_id: ^device_user_id,
                  verified_at: ^verified_at,
                  jwt: ^jwt,
                  verified_during_review: ^verified_during_review
                }
              }} =
               VerifiedConnectionService.verify_pending_connection(pc)
               |> Repo.transaction()

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given a PendingConnection without platform_identity - creates Device, DeviceUser, and VerifiedConnection",
         %{org: org} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      pc = %{
        id: "572196fd-2bad-42fa-8031-62ccd2638546",
        access_token: "8abb56a0-84bf-43ca-8d0c-31e2aebb7ab2",
        email: "fooier@foobar.foo",
        device_identifier: "newer-device-id-123",
        platform: "ROKU",
        platform_identity: nil,
        uri: "https://www.example.com",
        expires_at: NaiveDateTime.add(NaiveDateTime.utc_now(), 3600, :second),
        organization_id: org.id,
        verified_at: NaiveDateTime.utc_now(),
        verified_during_review: false,
        jwt: "yet-another-pretend-jwt"
      }

      assert {:ok, %{verified_connection: verified_connection}} =
               VerifiedConnectionService.verify_pending_connection(pc)
               |> Repo.transaction()

      assert Repo.aggregate(DeviceUser, :count, :id) == 2
      assert Repo.aggregate(Device, :count, :id) == 2
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2
    end

    test "given bad data - does not insert new records", %{org: org} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      pc = %{
        id: "",
        access_token: "",
        email: "not real",
        device_identifier: "",
        platform: "UNKNOWN",
        platform_identity: nil,
        uri: "",
        expires_at: nil,
        organization_id: org.id,
        verified_at: nil,
        verified_during_review: false,
        jwt: nil
      }

      assert {:error, :device,
              %Ecto.Changeset{
                errors: [
                  platform: {"is invalid", [validation: :inclusion]},
                  external_identifier: {"can't be blank", [validation: :required]}
                ],
                valid?: false
              },
              _changes} =
               VerifiedConnectionService.verify_pending_connection(pc)
               |> Repo.transaction()

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end
  end
end
