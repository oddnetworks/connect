defmodule ConnectionsTest do
  use Connections.Case
  use Bamboo.Test

  describe "create/1" do
    setup _tags do
      org = insert(:organization)

      {:ok, org: org}
    end

    test "with invalid params", %{org: org} do
      params = %{organization_id: org.id}

      assert {:error, ch} = Connections.create(params)

      refute ch.valid?
    end

    test "with valid params", %{org: org} do
      params = %{
        organization_id: org.id,
        email: email = "foo@foobar.foo",
        device_identifier: device_identifier = "device-id-1",
        platform: platform = "APPLE_TV",
        verified_during_review: false,
        uri: uri = "example.com"
      }

      assert {:ok,
              %{
                id: _id,
                access_token: access_token,
                email: ^email,
                device_identifier: ^device_identifier,
                platform: ^platform,
                uri: ^uri,
                verified_at: nil,
                verified_during_review: false,
                expires_at: expires_at
              }} = Connections.create(params)

      assert {:ok, _} = UUID.info(access_token)

      assert :gt == NaiveDateTime.compare(expires_at, NaiveDateTime.utc_now())

      {ttl_seconds, _} =
        Application.get_env(:connections, :connection_ttl_in_seconds)
        |> to_string()
        |> Integer.parse()

      future_time =
        NaiveDateTime.utc_now()
        |> NaiveDateTime.add(ttl_seconds, :second)

      assert :lt == NaiveDateTime.compare(expires_at, future_time)

      assert_delivered_with(subject: "#{org.name} - New Device Connection")
    end
  end
end
