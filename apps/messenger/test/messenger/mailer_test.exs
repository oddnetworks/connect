defmodule Messenger.MailerTest do
  use ExUnit.Case
  use Bamboo.Test
  import Messenger.Email
  alias Messenger.Mailer

  test "creating an email" do
    params = %{
      to: "foo@example.com",
      from: "odd@oddconnect.com",
      subject: "testing",
      html: "<b>HI</b>",
      text: "HI"
    }

    email = prepare_message(params)

    assert email.to == params.to
    assert email.from == params.from
    assert email.subject == params.subject
    assert email.html_body == params.html
    assert email.text_body == params.text
  end

  test "delivering an email" do
    params = %{
      to: "foo@example.com",
      from: "odd@oddconnect.com",
      subject: "testing",
      html: "<b>HI</b>",
      text: "HI"
    }

    email = prepare_message(params)

    email
    |> Mailer.deliver_later()

    assert_delivered_email(email)
  end
end
