defmodule MessengerTest do
  use ExUnit.Case
  use Bamboo.Test

  describe "deliver_email/1" do
    test "delivers Messenger.Email_email/1" do
      params = %{
        to: "foo@example.com",
        from: "odd@oddconnect.com",
        subject: "testing",
        html: "<b>HI</b>",
        text: "HI"
      }

      Messenger.deliver_email(params)

      assert_delivered_email(Messenger.Email.prepare_message(params))
    end
  end
end
