defmodule Messenger.Email do
  import Bamboo.Email

  def prepare_message(email) do
    new_email()
    |> from(email.from)
    |> to(email.to)
    |> subject(email.subject)
    |> html_body(email.html)
    |> text_body(email.text)
  end
end
