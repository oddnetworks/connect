defmodule Messenger do
  alias Messenger.{Email, Mailer}

  def deliver_email(email) do
    Email.prepare_message(email)
    |> Mailer.deliver_later()
  end
end
