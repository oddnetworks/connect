# Messenger

Provides messaging services to the platform.

Currently, `Messenger` just sends email via Mailgun.

In the future this could be extended to support:

- SMS
- rate-limiting
- bouncing
- idempotence
- failure handling using e.g. retrying with back-off, circuit breakers, fallback providers
- analytics

See [`Messenger`](lib/messenger.ex) for docs.
