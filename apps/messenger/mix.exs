defmodule Messenger.Mixfile do
  use Mix.Project

  def project do
    [
      app: :messenger,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [main: "Messenger"]
    ]
  end

  def application do
    [extra_applications: [:logger, :timber], mod: {Messenger.Application, []}]
  end

  defp deps do
    [
      {:bamboo, "~> 0.8"},
      {:timber, "~> 2.6"}
    ]
  end
end
