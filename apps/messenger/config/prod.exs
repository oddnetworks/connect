use Mix.Config

config :messenger, Messenger.Mailer,
  adapter: Bamboo.MailgunAdapter,
  api_key: "${MAILGUN_API_KEY}",
  domain: "${MAILGUN_DOMAIN_NAME}"
