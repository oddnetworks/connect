use Mix.Config

config :messenger, Messenger.Mailer, adapter: Bamboo.TestAdapter

config :logger,
  backends: [:console],
  utc_log: true,
  log_level: :warn

config :logger, :console,
  format: "[$level] $message\n",
  level: :warn
