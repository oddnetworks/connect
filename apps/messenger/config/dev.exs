use Mix.Config

config :messenger, Messenger.Mailer, adapter: Bamboo.LocalAdapter

# config :logger,
#   backends: [:console],
#   utc_log: true,
#   log_level: :debug

# config :logger, :console,
#   format: "[$level] $message\n",
#   level: :debug
