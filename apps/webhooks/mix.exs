defmodule Webhooks.Mixfile do
  use Mix.Project

  def project do
    [
      app: :webhooks,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [main: "Webhooks"]
    ]
  end

  def application do
    [extra_applications: [:logger, :timber], mod: {Webhooks.Application, []}]
  end

  defp deps do
    [
      {:httpoison, "~> 0.12.0"},
      {:poison, "~> 3.1.0", override: true},
      {:ja_serializer, "~> 0.12.0"},
      {:timber, "~> 2.6"},
      {:connect, in_umbrella: true},
      {:exvcr, "~> 0.8", only: :test}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
