# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :logger,
  backends: [:console],
  utc_log: true,
  log_level: :warn

config :logger, :console,
  format: "[$level] $message\n",
  level: :warn

config :webhooks, :task_supervisor, Webhooks.TestTaskSupervisor
