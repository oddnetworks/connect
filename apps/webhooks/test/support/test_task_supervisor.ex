defmodule Webhooks.TestTaskSupervisor do
  def async_nolink(_, fun), do: fun.()
  def start_child(_, fun), do: fun.()

  def start_child(_, module, function, args, _opts \\ []),
    do: apply(module, function, args)
end
