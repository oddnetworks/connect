defmodule Webhooks.Factory do
  use ExMachina.Ecto, repo: Connect.Repo

  alias Webhooks.{
    DeviceUser,
    Device,
    Entitlement,
    Organization,
    PlatformIdentity,
    Transaction,
    VerifiedConnection,
    Webhook
  }

  def device_factory() do
    platform =
      [
        "AMAZON_MOBILE",
        "AMAZON_TV",
        "APPLE_MOBILE",
        "APPLE_TV",
        "GOOGLE_MOBILE",
        "GOOGLE_TV",
        "ROKU",
        "WEB"
      ]
      |> Enum.random()

    %Device{
      external_identifier: sequence(:device_identifier, &"device-#{&1}"),
      platform: platform
    }
  end

  def device_user_factory() do
    %DeviceUser{
      email: sequence(:device_user_email, &"devuser-#{&1}@foo.com")
    }
  end

  def entitlement_factory() do
    org = build(:organization)

    platform =
      [
        "AMAZON",
        "APPLE",
        "GOOGLE",
        "ROKU",
        "WEB"
      ]
      |> Enum.random()

    type =
      [
        "subscription",
        "asset"
      ]
      |> Enum.random()

    %Entitlement{
      type: type,
      organization: org,
      name: sequence(:entitlement_name, &"#{type |> String.capitalize()} Entitlement #{&1}"),
      platform: platform,
      entitlement_identifier: sequence(:entitlement_entitlement_identifier, &"external-id-#{&1}"),
      product_identifier: sequence(:entitlement_product_identifier, &"product-id-#{&1}")
    }
  end

  def organization_factory() do
    name = sequence(:organization_name, &"ORG #{&1}")

    %Organization{
      name: name,
      urn: Slugger.slugify(name |> String.downcase())
    }
  end

  def platform_identity_factory do
    platform =
      [
        "AMAZON",
        "APPLE",
        "GOOGLE",
        "ROKU",
        "WEB"
      ]
      |> Enum.random()

    %PlatformIdentity{
      device_user: build(:device_user),
      platform: platform,
      external_identifier: sequence(:platform_identity, &"platform-id-#{&1}")
    }
  end

  def transaction_factory do
    entitlement = build(:entitlement)

    %Transaction{
      platform: entitlement.platform,
      device_user: build(:device_user),
      entitlement: entitlement,
      receipt: %{},
      external_identifier: sequence(:transaction_external_identifier, &"external-id-#{&1}"),
      last_validated_at: DateTime.utc_now(),
      invalidated_at: nil,
      invalidation_reason: nil
    }
  end

  def verified_connection_factory do
    %VerifiedConnection{
      organization: build(:organization),
      device_user: build(:device_user),
      verified_at: NaiveDateTime.utc_now()
    }
  end

  def webhook_factory do
    type =
      [
        "verified_connection",
        "new_transaction",
        "invalidated_transaction",
        "deleted_transaction",
        "entitlement"
      ]
      |> Enum.random()

    %Webhook{
      organization: build(:organization),
      type: type,
      url: sequence(:webhook_url, &"https://www.example.com/webhook-#{&1}"),
      headers: %{
        authorization: "bearer xyz"
      }
    }
  end
end
