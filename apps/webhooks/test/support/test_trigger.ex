defmodule Webhooks.TestTrigger do
  require Logger

  def trigger(type, payload) do
    Logger.info("Webhooks.trigger/2 #{type}, #{inspect(payload)}")
    :ok
  end
end
