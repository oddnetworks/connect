defmodule Webhooks.Case do
  use ExUnit.CaseTemplate

  using do
    quote do
      import Webhooks.Factory
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Connect.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Connect.Repo, {:shared, self()})
    end

    :ok
  end
end
