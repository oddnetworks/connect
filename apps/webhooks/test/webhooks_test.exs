defmodule WebhooksTest do
  use ExUnit.Case


  import ExUnit.CaptureLog

  describe ":entitlement webhook" do
    test "triggers :entitlement webhooks" do
      capture_log([level: :error], fn ->
        assert :ok ==
          Webhooks.trigger(
            :entitlement,
            {"3c444b13-4874-4525-94f7-e6075d58fece", "70c0b9ff-7701-474b-a629-2e12990ca822"}
          )
      end)
    end
  end

  describe ":verified_connection webhook" do
    test "triggers :verified_connection webhooks" do
      capture_log([level: :error], fn ->
        assert :ok ==
               Webhooks.trigger(
                 :verified_connection,
                 {"3c444b13-4874-4525-94f7-e6075d58fece", "70c0b9ff-7701-474b-a629-2e12990ca822"}
               )
      end)
    end
  end

  describe ":new_transaction webhook" do
    test "triggers :new_transaction webhooks" do
      capture_log([level: :error], fn ->
        assert :ok ==
               Webhooks.trigger(
                 :new_transaction,
                 {"3c444b13-4874-4525-94f7-e6075d58fece", "70c0b9ff-7701-474b-a629-2e12990ca822"}
               )
      end)
    end
  end

  describe ":invalidated_transaction webhook" do
    test "triggers :invalidated_transaction webhooks" do
      capture_log([level: :error], fn ->
        assert :ok ==
               Webhooks.trigger(
                 :invalidated_transaction,
                 {"3c444b13-4874-4525-94f7-e6075d58fece", "70c0b9ff-7701-474b-a629-2e12990ca822"}
               )
      end)
    end
  end

  describe ":deleted_transaction webhook" do
    test "triggers :deleted_transaction webhooks" do
      capture_log([level: :error], fn ->
        assert :ok ==
               Webhooks.trigger(
                 :deleted_transaction,
                 {"3c444b13-4874-4525-94f7-e6075d58fece", "70c0b9ff-7701-474b-a629-2e12990ca822"}
               )
      end)
    end
  end
end
