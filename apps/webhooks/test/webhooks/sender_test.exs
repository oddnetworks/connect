defmodule Webhooks.SenderTest do
  use ExUnit.Case
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes"

  setup_all do
    HTTPoison.start()
  end

  setup _tags do
    ExVCR.Config.cassette_library_dir(@cassette_dir)

    webhook = %{url: "http://www.example.com/webhook", headers: []}

    {:ok, webhook: webhook}
  end

  test "send_webhook/3 with :verified_connection", %{webhook: webhook} do
    verified_connection = %{
      id: "55ed217d-c100-4147-8da5-e45b7f61e629",
      email: "foo@foobar.foo",
      urn: "myorg",
      device_identifier: "my-ott-device",
      platform: "ROKU",
      verified_at: ~N[2018-10-09 17:17:17.171717]
    }

    use_cassette "verified_connection" do
      assert :ok =
               Webhooks.Sender.send_webhook(:verified_connection, webhook, verified_connection)
    end
  end

  test "send_webhook/3 with :new_transaction", %{webhook: webhook} do
    transaction = %{
      id: "23ee4ad5-ae4e-4e2d-ada1-223decd56454",
      external_identifier: "mashed-potatoes",
      platform: "ROKU",
      invalidated_at: nil,
      invalidation_reason: nil,
      receipt: %{},
      email: "foo@foobar.foo",
      entitlement_identifier: "subscription",
      product_identifier: "subscription-item",
      last_validated_at: ~N[2018-10-09 17:17:17.171717],
      inserted_at: ~N[2018-10-09 17:17:17.171717],
      updated_at: ~N[2018-10-09 17:17:17.171717]
    }

    use_cassette "new_transaction" do
      assert :ok = Webhooks.Sender.send_webhook(:new_transaction, webhook, transaction)
    end
  end

  test "send_webhook/3 with :invalidated_transaction", %{webhook: webhook} do
    transaction = %{
      id: "23ee4ad5-ae4e-4e2d-ada1-223decd56454",
      external_identifier: "mashed-potatoes",
      platform: "ROKU",
      invalidated_at: ~N[2018-10-09 17:17:17.171717],
      invalidation_reason: "Just cuz",
      receipt: %{},
      email: "foo@foobar.foo",
      entitlement_identifier: "subscription",
      product_identifier: "subscription-item",
      last_validated_at: ~N[2018-10-09 17:17:17.171717],
      inserted_at: ~N[2018-10-09 17:17:17.171717],
      updated_at: ~N[2018-10-09 17:17:17.171717]
    }

    use_cassette "invalidated_transaction" do
      :ok = Webhooks.Sender.send_webhook(:invalidated_transaction, webhook, transaction)
    end
  end

  test "send_webhook/3 with :deleted_transaction", %{webhook: webhook} do
    transaction = %{
      id: "23ee4ad5-ae4e-4e2d-ada1-223decd56454",
      external_identifier: "mashed-potatoes",
      platform: "ROKU",
      invalidated_at: nil,
      invalidation_reason: nil,
      receipt: %{},
      email: "foo@foobar.foo",
      entitlement_identifier: "subscription",
      product_identifier: "subscription-item",
      last_validated_at: ~N[2018-10-09 17:17:17.171717],
      inserted_at: ~N[2018-10-09 17:17:17.171717],
      updated_at: ~N[2018-10-09 17:17:17.171717]
    }

    use_cassette "deleted_transaction" do
      :ok = Webhooks.Sender.send_webhook(:deleted_transaction, webhook, transaction)
    end
  end

  test "send_webhook/3 with :entitlement", %{webhook: webhook} do
    entitlement = %{
      id: "1394f22a-38bd-48f9-9b47-2f19027dbeea",
      type: "subscription",
      name: "Subscription",
      description: "A Subscription",
      entitlement_identifier: "subscription",
      product_identifier: "subscription-item",
      platform: "ROKU",
      inserted_at: ~N[2018-10-09 17:17:17.171717],
      updated_at: ~N[2018-10-09 17:17:17.171717]
    }

    use_cassette "entitlement" do
      :ok = Webhooks.Sender.send_webhook(:entitlement, webhook, entitlement)
    end
  end
end
