defmodule Webhooks.Service.TransactionEventsTest do
  use Webhooks.Case

  import ExUnit.CaptureLog

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes"

  setup_all do
    HTTPoison.start()
  end

  alias Webhooks.Service.TransactionEvents

  describe "run/3" do
    setup _tags do
      ExVCR.Config.cassette_library_dir(@cassette_dir)

      org = insert(:organization)

      hook =
        insert(:webhook,
          organization: org,
          type: "new_transaction",
          url: "http://www.example.com/webhook",
          headers: %{}
        )

      dev = insert(:device_user, email: "foo@foobar.foo")

      ent =
        insert(:entitlement,
          organization: org,
          product_identifier: "subscription-item",
          entitlement_identifier: "subscription",
          platform: "GOOGLE"
        )

      trans =
        insert(:transaction,
          device_user: dev,
          entitlement: ent,
          id: "23ee4ad5-ae4e-4e2d-ada1-223decd56454",
          platform: "GOOGLE",
          last_validated_at: ~N[2018-10-09 17:17:17.171717],
          invalidated_at: nil,
          inserted_at: ~N[2018-10-09 17:17:17.171717],
          updated_at: ~N[2018-10-09 17:17:17.171717],
          external_identifier: "mashed-potatoes"
        )

      {:ok, org: org, hook: hook, trans: trans}
    end

    test "transaction not found" do
      assert {:error, "Transaction Not Found"} =
               TransactionEvents.run(
                 :new_transaction,
                 "36d2cb49-0e3b-45db-ba30-a8e4e9dbdb9d",
                 "31d10d77-ddfa-45fb-8463-81c91e9be7b4"
               )
    end

    test "organization has no webhooks", %{trans: trans, org: org} do
      assert {:error, "No Webhooks Found"} =
               TransactionEvents.run(:bad_transaction, trans.id, org.id)
    end

    test "POST success", %{trans: trans, org: org} do
      use_cassette "new_transaction" do
        log =
          capture_log([level: :info], fn ->
            TransactionEvents.run(:new_transaction, trans.id, org.id)
          end)

        assert log =~ "Sent POST to http://www.example.com/webhook from Webhooks.Sender"
        assert log =~ "Received 200 response from Webhooks.Sender in"
      end
    end

    test "POST fail", %{trans: trans, org: org} do
      use_cassette "new_transaction_failure" do
        log =
          capture_log([level: :info], fn ->
            TransactionEvents.run(:new_transaction, trans.id, org.id)
          end)

        assert log =~ "Sent POST to http://www.example.com/webhook from Webhooks.Sender"
        assert log =~ "Received 404 response from Webhooks.Sender in"
      end
    end
  end
end
