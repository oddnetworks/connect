defmodule Webhooks.Service.ConnectionEventsTest do
  use Webhooks.Case

  import ExUnit.CaptureLog

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes"

  setup_all do
    HTTPoison.start()
  end

  alias Webhooks.Service.ConnectionEvents

  describe "run/3" do
    setup _tags do
      ExVCR.Config.cassette_library_dir(@cassette_dir)

      org = insert(:organization, urn: "myorg")

      hook =
        insert(:webhook,
          organization: org,
          type: "verified_connection",
          url: "http://www.example.com/webhook",
          headers: %{}
        )

      dev = insert(:device, external_identifier: "my-ott-device", platform: "ROKU")
      du = insert(:device_user, email: "foo@foobar.foo")

      conn =
        insert(:verified_connection,
          id: "55ed217d-c100-4147-8da5-e45b7f61e629",
          device_user: du,
          organization: org,
          device: dev,
          verified_at: ~N[2018-10-09 17:17:17.171717]
        )

      {:ok, org: org, hook: hook, conn: conn}
    end

    test "connection not found" do
      assert {:error, "Verified Connection Not Found"} =
               ConnectionEvents.run(
                 :verified_connection,
                 "f7d2585b-fce5-41f0-acc7-badb02c0111b",
                 "20795c12-9909-4fed-b74a-5692dfd0e9b7"
               )
    end

    test "organization has no webhooks", %{org: org, conn: conn} do
      assert {:error, "No Webhooks Found"} =
               ConnectionEvents.run(:bad_connection, conn.id, org.id)
    end

    test "POST success", %{org: org, conn: conn} do
      use_cassette "verified_connection" do
        log =
          capture_log([level: :info], fn ->
            ConnectionEvents.run(:verified_connection, conn.id, org.id)
          end)

        assert log =~ "Sent POST to http://www.example.com/webhook from Webhooks.Sender"
        assert log =~ "Received 200 response from Webhooks.Sender in"
      end
    end

    test "POST failure", %{org: org, conn: conn} do
      use_cassette "verified_connection_failed" do
        log =
          capture_log([level: :info], fn ->
            ConnectionEvents.run(:verified_connection, conn.id, org.id)
          end)

        assert log =~ "Sent POST to http://www.example.com/webhook from Webhooks.Sender"
        assert log =~ "Received 404 response from Webhooks.Sender in"
      end
    end
  end
end
