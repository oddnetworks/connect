defmodule Webhooks.Service.EntitlementEventsTest do
  use Webhooks.Case

  import ExUnit.CaptureLog

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes"

  setup_all do
    HTTPoison.start()
  end

  alias Webhooks.Service.EntitlementEvents

  describe "run/3" do
    setup _tags do
      ExVCR.Config.cassette_library_dir(@cassette_dir)

      org = insert(:organization)

      hook =
        insert(:webhook,
          organization: org,
          type: "entitlement",
          url: "http://www.example.com/webhook",
          headers: %{}
        )

      ent =
        insert(:entitlement,
          id: "1394f22a-38bd-48f9-9b47-2f19027dbeea",
          organization: org,
          type: "entitlement",
          name: "Subscription",
          description: "A Subscription",
          entitlement_identifier: "subscription",
          product_identifier: "subscription-item",
          platform: "ROKU",
          inserted_at: ~N[2018-10-09 17:17:17.171717],
          updated_at: ~N[2018-10-09 17:17:17.171717]
        )

      {:ok, org: org, hook: hook, ent: ent}
    end

    test "connection not found" do
      assert {:error, "Entitlement Not Found"} =
               EntitlementEvents.run(
                 :entitlement,
                 "f7d2585b-fce5-41f0-acc7-badb02c0111b",
                 "20795c12-9909-4fed-b74a-5692dfd0e9b7"
               )
    end

    test "organization has no webhooks", %{org: org, ent: ent} do
      assert {:error, "No Webhooks Found"} =
               EntitlementEvents.run(:bad_entitlement, ent.id, org.id)
    end

    test "POST success", %{org: org, ent: ent} do
      use_cassette "entitlement" do
        log =
          capture_log([level: :info], fn ->
            EntitlementEvents.run(:entitlement, ent.id, org.id)
          end)

        assert log =~ "Sent POST to http://www.example.com/webhook from Webhooks.Sender"
        assert log =~ "Received 200 response from Webhooks.Sender in"
      end
    end

    test "POST failure", %{org: org, ent: ent} do
      use_cassette "entitlement_failed" do
        log =
          capture_log([level: :info], fn ->
            EntitlementEvents.run(:entitlement, ent.id, org.id)
          end)

        assert log =~ "Sent POST to http://www.example.com/webhook from Webhooks.Sender"
        assert log =~ "Received 404 response from Webhooks.Sender in"
      end
    end
  end
end
