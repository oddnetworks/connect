defmodule Webhooks do
  use GenServer

  require Logger

  alias Webhooks.Service

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    {:ok, %{}}
  end

  def trigger(type, payload) do
    GenServer.cast(Webhooks, {type, payload})
  end

  @transaction_events [
    :new_transaction,
    :invalidated_transaction,
    :deleted_transaction
  ]
  def handle_cast({type, {transaction_id, organization_id}}, state)
      when type in @transaction_events do
    Service.TransactionEvents.run(type, transaction_id, organization_id)

    {:noreply, state}
  end

  @connection_events [
    :verified_connection
  ]
  def handle_cast({type, {verified_connection_id, organization_id}}, state)
      when type in @connection_events do
    Service.ConnectionEvents.run(type, verified_connection_id, organization_id)

    {:noreply, state}
  end

  @entitlement_events [
    :entitlement
  ]
  def handle_cast({type, {entitlement_id, organization_id}}, state)
      when type in @entitlement_events do
    Service.EntitlementEvents.run(type, entitlement_id, organization_id)

    {:noreply, state}
  end

  def handle_cast({type, payload}, state) do
    Logger.warn(fn ->
      {"Webhooks.trigger/2 called with unknown type - #{type}",
       [
         event: %{
           type: "webhooks_trigger",
           data: %{
             payload: Tuple.to_list(payload)
           }
         }
       ]}
    end)

    {:noreply, state}
  end
end
