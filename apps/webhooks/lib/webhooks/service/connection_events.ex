defmodule Webhooks.Service.ConnectionEvents do
  @moduledoc """
  Responsible for notifying of Connection Events
  """

  alias Webhooks.{Sender, VerifiedConnection, Webhook}
  alias Connect.Repo

  @task_supervisor Application.get_env(:webhooks, :task_supervisor) || Task.Supervisor

  @doc """
  Finds the verified connection and any relevant webhooks for the organization, then fires Webhooks.Sender.send_webhook/3 for each webhook that was found

  ## Parameters

  - type
  - verified_connection_id
  - organization_id
  """
  @spec run(atom, String.t(), String.t()) :: :ok | {:error, String.t()}
  def run(type, verified_connection_id, organization_id) do
    with verified_connection when not is_nil(verified_connection) <-
           find_verified_connection(organization_id, verified_connection_id),
         webhooks when webhooks != [] <- find_webhooks(organization_id, to_string(type)) do
      webhooks
      |> Enum.each(
        &@task_supervisor.start_child(Webhooks.TaskSupervisor, Sender, :send_webhook, [
          type,
          &1,
          verified_connection
        ])
      )
    else
      nil ->
        {:error, "Verified Connection Not Found"}

      _ ->
        {:error, "No Webhooks Found"}
    end
  end

  defp find_verified_connection(organization_id, verified_connection_id) do
    VerifiedConnection.find_by_organization_and_id_query(organization_id, verified_connection_id)
    |> Repo.one()
  end

  defp find_webhooks(organization_id, type) do
    Webhook.find_by_organization_and_type_query(organization_id, type)
    |> Repo.all()
  end
end
