defmodule Webhooks.Service.EntitlementEvents do
  @moduledoc """
  Responsible for notifying of entitlement events
  """

  alias Webhooks.{Entitlement, Sender, Webhook}
  alias Connect.Repo

  @task_supervisor Application.get_env(:webhooks, :task_supervisor) || Task.Supervisor

  @doc """
  Finds the entitlement and any relevant webhooks for the organization, then fires Webhooks.Sender.send_webhook/3 for each webhook that was found

  ## Parameters

  - type
  - transaction_id
  - organization_id
  """
  @spec run(atom, String.t(), String.t()) :: :ok | {:erro, String.t()}
  def run(type, entitlement_id, organization_id) do
    with entitlement when not is_nil(entitlement) <-
           find_entitlement(organization_id, entitlement_id),
         webhooks when webhooks != [] <- find_webhooks(organization_id, to_string(type)) do
      webhooks
      |> Enum.each(
        &@task_supervisor.start_child(Webhooks.TaskSupervisor, Sender, :send_webhook, [
          type,
          &1,
          entitlement
        ])
      )
    else
      nil ->
        {:error, "Entitlement Not Found"}

      _ ->
        {:error, "No Webhooks Found"}
    end
  end

  defp find_entitlement(organization_id, id) do
    Entitlement
    |> Repo.get_by(organization_id: organization_id, id: id)
  end

  defp find_webhooks(organization_id, type) do
    Webhook.find_by_organization_and_type_query(organization_id, type)
    |> Repo.all()
  end
end
