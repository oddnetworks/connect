defmodule Webhooks.Service.TransactionEvents do
  @moduledoc """
  Responsible for sending the new_transaction webhook
  """

  alias Webhooks.{Sender, Transaction, Webhook}
  alias Connect.Repo

  @task_supervisor Application.get_env(:webhooks, :task_supervisor) || Task.Supervisor

  @doc """
  Finds the transaction and any relevant webhooks for the organization, then fires Webhooks.Sender.send_webhook/3 for each webhook that was found

  ## Parameters

  - type
  - transaction_id
  - organization_id
  """
  @spec run(atom, String.t(), String.t()) :: :ok | {:error, String.t()}
  def run(type, transaction_id, organization_id) do
    with transaction when not is_nil(transaction) <-
           find_transaction(organization_id, transaction_id),
         webhooks when webhooks != [] <- find_webhooks(organization_id, to_string(type)) do
      webhooks
      |> Enum.each(
        &@task_supervisor.start_child(Webhooks.TaskSupervisor, Sender, :send_webhook, [
          type,
          &1,
          transaction
        ])
      )
    else
      nil ->
        # TODO - log this?

        {:error, "Transaction Not Found"}

      _ ->
        # TODO - log this?

        {:error, "No Webhooks Found"}
    end
  end

  defp find_transaction(organization_id, transaction_id) do
    Transaction.find_by_organization_and_id_query(organization_id, transaction_id)
    |> Repo.one()
  end

  defp find_webhooks(organization_id, type) do
    Webhook.find_by_organization_and_type_query(organization_id, type)
    |> Repo.all()
  end
end
