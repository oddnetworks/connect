defmodule Webhooks.Sender do
  @moduledoc """
  Responsible for sending various webhooks
  """
  require Logger

  alias Webhooks.Serializers.{Entitlement, Transaction, VerifiedConnection}
  alias Timber.Events.{HTTPRequestEvent, HTTPResponseEvent}

  @post_options [
    timeout: 10000,
    recv_timeout: 10000
  ]

  @transaction_types [
    :new_transaction,
    :invalidated_transaction,
    :deleted_transaction
  ]

  def send_webhook(type, webhook, payload) when type in @transaction_types,
    do: send_webhook(Transaction, type, webhook, payload)

  def send_webhook(:verified_connection, webhook, payload),
    do: send_webhook(VerifiedConnection, :verified_connection, webhook, payload)

  def send_webhook(:entitlement, webhook, payload),
    do: send_webhook(Entitlement, :entitlement, webhook, payload)

  def send_webhook(serializer, type, webhook, payload) do
    with body when is_map(body) <- format_body(serializer, payload),
         {:ok, encoded} <- Poison.encode(body),
         headers <- format_headers(webhook.headers, type),
         timer <- Timber.start_timer(),
         {:ok, results} <- post_request(webhook.url, encoded, headers) do
      log_success(results, timer)
    else
      {:error, {:invalid, part}} ->
        log_failure("Failed to encode #{inspect(part)} of body", webhook, payload)

      {:error, error} ->
        log_failure(error, webhook, payload)

      error ->
        log_failure(error, webhook, payload)
    end
  end

  defp format_headers(headers, type) when headers == %{}, do: format_headers(:default, type)

  defp format_headers(headers, type) when is_map(headers),
    do: Enum.into(headers, format_headers(:default, type))

  defp format_headers(_, type),
    do: [
      {"content-type", "application/json"},
      {"accept", "application/json"},
      {"x-event-type", format_type(type)}
    ]

  defp format_type(type) do
    case type do
      :entitlement -> "entitlement.created"
      :deleted_transaction -> "transaction.deleted"
      :invalidated_transaction -> "transaction.invalidated"
      :new_transaction -> "transaction.created"
      :verified_connection -> "connection.verified"
    end
  end

  defp format_body(serializer, data) do
    try do
      serializer
      |> JaSerializer.format(data)
    rescue
      e ->
        {:error, e}
    end
  end

  defp post_request(url, body, headers) do
    log_outgoing(url, body, headers)

    HTTPoison.post(url, body, headers, @post_options)
  end

  defp log_outgoing(url, body, headers) do
    Logger.info(fn ->
      event =
        HTTPRequestEvent.new(
          direction: "outgoing",
          service_name: "Webhooks.Sender",
          method: :post,
          url: url,
          headers: headers,
          body: body
        )

      {"Sent POST to #{url} from Webhooks.Sender", [event: event]}
    end)
  end

  defp log_success(%{body: resp_body, headers: resp_headers, status_code: status}, timer) do
    Logger.info(fn ->
      duration = Timber.duration_ms(timer)

      event =
        HTTPResponseEvent.new(
          direction: "incoming",
          service_name: "Webhooks.Sender",
          status: status,
          headers: resp_headers,
          body: resp_body,
          time_ms: duration
        )

      {"Received #{status} response from Webhooks.Sender in #{duration}ms", [event: event]}
    end)
  end

  defp log_failure(error, webhook, payload) when is_binary(error) do
    Logger.error(error,
      event: %{
        type: "webhook_failure",
        data: %{
          inspect: error,
          webhook: webhook,
          payload: payload
        }
      }
    )
  end

  defp log_failure(%HTTPoison.Error{reason: message} = error, webhook, payload) do
    Logger.error(message,
      event: %{
        type: "webhook_failure",
        data: %{
          inspect: inspect(error),
          webhook: webhook,
          payload: payload
        }
      }
    )
  end

  defp log_failure(error, webhook, payload) when is_map(error) do
    Logger.error(Exception.message(error),
      event: %{
        type: "webhook_failure",
        data: %{
          inspect: inspect(error),
          webhook: webhook,
          payload: payload
        }
      }
    )
  end
end
