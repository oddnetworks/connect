defmodule Webhooks.Transaction do
  use Ecto.Schema
  import Ecto.Query, only: [from: 2]

  alias Webhooks.{DeviceUser, Entitlement, Transaction}

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "transactions" do
    field(:external_identifier, :string)
    field(:platform, :string)
    field(:last_validated_at, :naive_datetime)
    field(:invalidated_at, :naive_datetime)
    field(:invalidation_reason, :string)
    field(:receipt, :map)
    field(:deleted_at, :naive_datetime)

    belongs_to(:device_user, DeviceUser)
    belongs_to(:entitlement, Entitlement)

    timestamps()
  end

  def find_by_organization_and_id_query(organization_id, transaction_id) do
    from(t in Transaction,
      join: e in Entitlement,
      on: e.id == t.entitlement_id,
      join: d in DeviceUser,
      on: d.id == t.device_user_id,
      where: t.id == ^transaction_id and e.organization_id == ^organization_id,
      select: %{
        id: t.id,
        external_identifier: t.external_identifier,
        platform: t.platform,
        invalidated_at: t.invalidated_at,
        invalidation_reason: t.invalidation_reason,
        receipt: t.receipt,
        email: d.email,
        entitlement_identifier: e.entitlement_identifier,
        product_identifier: e.product_identifier,
        last_validated_at: t.last_validated_at,
        inserted_at: t.inserted_at,
        updated_at: t.updated_at
      }
    )
  end
end
