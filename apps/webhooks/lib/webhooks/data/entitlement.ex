defmodule Webhooks.Entitlement do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  alias Webhooks.Organization

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "entitlements" do
    field(:type, :string)
    field(:name, :string)
    field(:description, :string)
    field(:entitlement_identifier, :string)
    field(:product_identifier, :string)
    field(:platform, :string)

    belongs_to(:organization, Organization)

    timestamps()
  end
end
