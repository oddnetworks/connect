defmodule Webhooks.VerifiedConnection do
  use Ecto.Schema
  import Ecto.Query, only: [from: 2]

  @type t :: %__MODULE__{}

  alias Webhooks.{Device, DeviceUser, Organization, VerifiedConnection}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "verified_connections" do
    field(:verified_at, :naive_datetime)
    field(:verified_during_review, :boolean, default: false)

    belongs_to(:device_user, DeviceUser)
    belongs_to(:organization, Organization)
    belongs_to(:device, Device)
  end

  def find_by_organization_and_id_query(organization_id, verified_connection_id) do
    from(v in VerifiedConnection,
      join: d in Device,
      on: d.id == v.device_id,
      join: o in Organization,
      on: o.id == v.organization_id,
      join: du in DeviceUser,
      on: du.id == v.device_user_id,
      where: v.organization_id == ^organization_id and v.id == ^verified_connection_id,
      select: %{
        id: v.id,
        urn: o.urn,
        email: du.email,
        device_identifier: d.external_identifier,
        platform: d.platform,
        verified_at: v.verified_at
      }
    )
  end
end
