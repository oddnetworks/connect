defmodule Webhooks.Device do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  alias Webhooks.VerifiedConnection

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "devices" do
    field(:external_identifier, :string)
    field(:platform, :string)

    has_many(:verified_connections, VerifiedConnection)
    has_many(:device_users, through: [:verified_connections, :device_user])
    has_many(:organizations, through: [:verified_connections, :organization])

    timestamps()
  end
end
