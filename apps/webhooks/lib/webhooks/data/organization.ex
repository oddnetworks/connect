defmodule Webhooks.Organization do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  alias Webhooks.Webhook

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organizations" do
    field(:urn, :string)
    field(:name, :string)

    has_many(:webhooks, Webhook)

    timestamps()
  end
end
