defmodule Webhooks.DeviceUser do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  alias Webhooks.PlatformIdentity

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "device_users" do
    field(:email, :string)

    has_many(:platform_identities, PlatformIdentity)

    timestamps()
  end
end
