defmodule Webhooks.PlatformIdentity do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  alias Webhooks.DeviceUser

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "platform_identities" do
    field(:platform, :string)
    field(:external_identifier, :string)

    belongs_to(:device_user, DeviceUser)

    timestamps()
  end
end
