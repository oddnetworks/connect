defmodule Webhooks.Serializers.Transaction do
  use Webhooks.Serializers.Base

  def type(_, _), do: "transaction"

  def attributes(t, _conn),
    do: %{
      external_identifier: t.external_identifier,
      platform: t.platform,
      invalidated_at: format_date(t.invalidated_at),
      invalidation_reason: t.invalidation_reason,
      receipt: t.receipt,
      email: t.email,
      entitlement_identifier: t.entitlement_identifier,
      product_identifier: t.product_identifier,
      last_validated_at: format_date(t.last_validated_at),
      inserted_at: format_date(t.inserted_at),
      updated_at: format_date(t.updated_at)
    }
end
