defmodule Webhooks.Serializers.Base do
  defmacro __using__(_opts) do
    quote do
      use JaSerializer

      import Webhooks.Serializers.Base
    end
  end

  def format_date(date = %NaiveDateTime{}) do
    date =
      date
      |> NaiveDateTime.to_iso8601()

    [date | _tail] =
      date
      |> String.split(".")

    date <> "Z"
  end

  def format_date(date) when is_integer(date) do
    case DateTime.from_unix(date) do
      {:ok, date} ->
        DateTime.to_iso8601(date)

      _ ->
        date
    end
  end

  def format_date(date), do: date
end
