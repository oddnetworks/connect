defmodule Webhooks.Serializers.VerifiedConnection do
  use Webhooks.Serializers.Base

  def type(_, _) do
    "connection"
  end

  def attributes(vc, _),
    do: %{
      email: vc.email,
      organization_urn: vc.urn,
      device_identifier: vc.device_identifier,
      platform: vc.platform,
      verified_at: format_date(vc.verified_at)
    }
end
