defmodule Webhooks.Serializers.Entitlement do
  use Webhooks.Serializers.Base

  def type(e, _conn), do: e.type

  def attributes(e, _conn),
    do: %{
      name: e.name,
      description: e.description,
      entitlement_identifier: e.entitlement_identifier,
      product_identifier: e.product_identifier,
      platform: e.platform,
      inserted_at: format_date(e.inserted_at),
      updated_at: format_date(e.updated_at)
    }
end
