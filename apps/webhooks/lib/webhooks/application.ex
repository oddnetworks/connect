defmodule Webhooks.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(Webhooks, []),
      {Task.Supervisor, name: Webhooks.TaskSupervisor}
    ]

    opts = [strategy: :one_for_one, name: Webhooks.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
