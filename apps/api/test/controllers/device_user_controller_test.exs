defmodule Api.DeviceUserControllerTest do
  use Api.ConnCase

  import Connect.Factory

  setup %{conn: conn} do
    dev = insert(:device_user)

    {:ok, conn: conn, dev: dev}
  end

  describe "GET /api/device_users/:email while unauthenticated" do
    test "returns 401 with error json", %{conn: conn, dev: dev} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(device_user_path(conn, :show, dev.email))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "GET /api/device_users/:email while authenticated as a customer" do
    setup %{conn: conn} do
      # customer = insert(:customer)

      # bad_token = generate_api_token(customer)

      # conn =
      #   conn
      #   |> put_req_header("authorization", "bearer #{bad_token}")

      {:ok, conn: conn}
    end

    @tag :skip
    test "returns 401 with error json", %{conn: conn, dev: dev} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(device_user_path(conn, :show, dev.email))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "GET /api/device_users/:email while authenticated as an organization" do
    setup %{conn: conn} do
      org = insert(:organization)
      vc = insert(:verified_connection, organization: org)

      token = generate_api_token(org)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")

      {:ok, conn: conn, org: org, dev: vc.device_user}
    end

    test "returns 404 when device user not found", %{conn: conn} do
      response =
        assert_error_sent(404, fn ->
          conn
          |> get(device_user_path(conn, :show, "foobar"))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "returns 404 when device user exists but not in the scope of the current organization",
         %{conn: conn} do
      dev = insert(:device_user)

      response =
        assert_error_sent(404, fn ->
          conn
          |> get(device_user_path(conn, :show, dev.email))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "returns 200 when device user exists within the scope of the current organization", %{
      conn: conn,
      dev: dev
    } do
      # Note: this test will cause an error similar to the following,
      #
      # Task #PID<0.1870.0> started from Transactions terminating
      # ** (stop) exited in: GenServer.call(#PID<0.1865.0>, {:checkout, #Reference<0.52896798.3777232897.240874>, true, 15000}, 5000)
      #
      # Since we are calling out to our Transactions service to validate things
      # via `GenServer.cast`, we don't actually care about this error, so, while annoying, it is safe to ignore.

      conn =
        conn
        |> get(device_user_path(conn, :show, dev.email))

      id = dev.id
      email = dev.email

      %{
        "data" => %{
          "id" => ^id,
          "type" => "device_user",
          "attributes" => %{
            "email" => ^email
          }
        }
      } = json_response(conn, 200)
    end
  end
end
