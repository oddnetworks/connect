defmodule Api.ConnectionControllerTest do
  use Api.ConnCase

  import Connections.Factory

  describe "api connection routes while unauthenticated" do
    test "GET /api/connections/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(connection_path(conn, :show, "123"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "POST /api/connections returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(connection_path(conn, :create), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "POST /api/device_users/:email/connections returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(device_user_connection_path(conn, :verified_create, "foo@example.com"), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api connection routes while authenticated as a customer" do
    setup %{conn: conn} do
      # customer = insert(:customer)
      # bad_token = generate_api_token(customer)

      # conn =
      #   conn
      #   |> put_req_header("authorization", "bearer #{bad_token}")

      {:ok, conn: conn}
    end

    test "GET /api/connections/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(connection_path(conn, :show, "123"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "POST /api/connections returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(connection_path(conn, :create), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "POST /api/device_users/:email/connections returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(device_user_connection_path(conn, :verified_create, "foo@example.com"), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api connection routes while authenticated as an organization" do
    setup %{conn: conn} do
      org = insert(:organization)
      token = generate_api_token(org)

      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> put_req_header("authorization", "bearer #{token}")

      {:ok, conn: conn, org: org}
    end

    test "POST /api/device_users/:email/connections with invalid params returns 422 with error json",
         %{conn: conn} do
      invalid_params = %{
        data: %{
          type: "connection",
          attributes: %{
            platform: "not real",
            device_identifier: "not real"
          }
        }
      }

      response =
        assert_error_sent(422, fn ->
          conn
          |> post(
            device_user_connection_path(conn, :verified_create, "foo@example.com"),
            invalid_params
          )
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("422", "Unprocessable Entity", "Device: platform is invalid")
    end

    test "POST /api/device_users/:email/connections with missing params returns 422 with error json",
         %{conn: conn} do
      invalid_params = %{
        data: %{
          type: "connection",
          attributes:
            %{
              # missing attributes
            }
        }
      }

      response =
        assert_error_sent(422, fn ->
          conn
          |> post(
            device_user_connection_path(conn, :verified_create, "foo@example.com"),
            invalid_params
          )
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body(
                 "422",
                 "Unprocessable Entity",
                 "Pending connection: device identifier can't be blank, platform can't be blank"
               )
    end

    test "POST /api/device_users/:email/connections with valid params returns 201 with serialized verified connection",
         %{conn: conn, org: org} do
      platform =
        [
          "AMAZON_MOBILE",
          "AMAZON_TV",
          "APPLE_MOBILE",
          "APPLE_TV",
          "GOOGLE_MOBILE",
          "GOOGLE_TV",
          "ROKU",
          "WEB"
        ]
        |> Enum.random()

      device_identifier = UUID.uuid4()
      email = "foo@example.com"

      params = %{
        data: %{
          type: "connection",
          attributes: %{
            platform: platform,
            device_identifier: device_identifier
          }
        }
      }

      conn =
        conn
        |> post(device_user_connection_path(conn, :verified_create, email), params)

      org_urn = org.urn

      assert %{
               "data" => %{
                 "id" => id,
                 "type" => "connection",
                 "attributes" => %{
                   "email" => ^email,
                   "device_identifier" => ^device_identifier,
                   "platform" => ^platform,
                   "jwt" => jwt,
                   "organization_urn" => ^org_urn,
                   "verified_at" => verified_at
                 }
               }
             } = json_response(conn, 201)

      assert {:ok, _, 0} = DateTime.from_iso8601(verified_at)
      pub = Auth.ConnectionToken.public_key()
      pem = JOSE.JWK.from_pem(pub)

      assert {:ok, claims} =
               jwt
               |> Joken.token()
               |> Joken.with_signer(Joken.rs256(pem))
               |> Joken.verify!()

      assert claims["sub"] == email
      assert claims["aud"] == [org_urn, platform, device_identifier]
      assert {:ok, _} = UUID.info(claims["jti"])
      assert id == claims["jti"]
      assert claims["iat"] <= :os.system_time(:second)
    end

    test "POST /api/connections returns 400 with error json", %{conn: conn} do
      response =
        assert_error_sent(400, fn ->
          conn
          |> post(connection_path(conn, :create), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("400", "Bad Request", "Invalid/malformed request")
    end

    test "POST /api/connections returns 422 with error json", %{conn: conn} do
      platform =
        Connect.Platform.valid_device_platforms()
        |> Enum.random()

      response =
        assert_error_sent(422, fn ->
          conn
          |> post(connection_path(conn, :create), %{
            data: %{
              type: "connection",
              attributes: %{platform: platform, device_identifier: "uuidokok", email: "bademail"}
            }
          })
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body_with_pointer(
                 "422",
                 "Unprocessable Entity",
                 "Email has invalid format",
                 "/data/attributes/email"
               )
    end

    test "POST /api/connections when organization in review mode returns 201 with serialized verified connection",
         %{conn: conn, org: org} do
      org.settings
      |> Ecto.Changeset.cast(%{in_review_mode: true}, [:in_review_mode])
      |> Connect.Repo.update!()

      platform =
        [
          "AMAZON_MOBILE",
          "AMAZON_TV",
          "APPLE_MOBILE",
          "APPLE_TV",
          "GOOGLE_MOBILE",
          "GOOGLE_TV",
          "ROKU",
          "WEB"
        ]
        |> Enum.random()

      email = "ok@email.com"
      device_identifier = "uuidokok"

      conn =
        conn
        |> post(connection_path(conn, :create), %{
          data: %{
            type: "connection",
            attributes: %{
              device_identifier: device_identifier,
              platform: platform,
              email: email
            }
          }
        })

      org_urn = org.urn

      assert %{
               "data" => %{
                 "id" => id,
                 "type" => "connection",
                 "attributes" => %{
                   "device_identifier" => ^device_identifier,
                   "email" => ^email,
                   "platform" => ^platform,
                   "jwt" => jwt,
                   "organization_urn" => ^org_urn,
                   "verified_at" => verified_at
                 }
               },
               "jsonapi" => %{
                 "version" => "1.0"
               }
             } = json_response(conn, 201)

      assert {:ok, _, 0} = DateTime.from_iso8601(verified_at)
      pub = Auth.ConnectionToken.public_key()
      pem = JOSE.JWK.from_pem(pub)

      assert {:ok, claims} =
               jwt
               |> Joken.token()
               |> Joken.with_signer(Joken.rs256(pem))
               |> Joken.verify!()

      assert claims["sub"] == email
      assert claims["aud"] == [org_urn, platform, device_identifier]
      assert {:ok, _} = UUID.info(claims["jti"])
      assert id == claims["jti"]
      assert claims["iat"] <= :os.system_time(:second)
    end

    test "POST /api/connections returns 201 with serialized unverified connection", %{
      conn: conn,
      org: org
    } do
      platform =
        Connect.Platform.valid_device_platforms()
        |> Enum.random()

      conn =
        conn
        |> post(connection_path(conn, :create), %{
          data: %{
            type: "connection",
            attributes: %{
              device_identifier: "uuidokok",
              platform: platform,
              email: "ok@email.com"
            }
          }
        })

      org_urn = org.urn

      %{"data" => %{"id" => id, "type" => "connection", "attributes" => attributes}} =
        json_response(conn, 201)

      assert {:ok, _} = UUID.info(id)

      assert Map.keys(attributes) == [
               "device_identifier",
               "email",
               "expires_at",
               "organization_urn",
               "platform"
             ]

      assert %{
               "device_identifier" => "uuidokok",
               "platform" => ^platform,
               "email" => "ok@email.com",
               "organization_urn" => ^org_urn,
               "expires_at" => expires_at
             } = attributes

      assert expires_at =~ formatted_date_regex()
    end

    test "GET /api/connections/:id when resource not found returns 404 with error json", %{
      conn: conn
    } do
      response =
        assert_error_sent(404, fn ->
          conn
          |> get(connection_path(conn, :show, "4efd49e7-a114-462c-ab33-f178e99b8f49"))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "GET /api/connections/:id when resource id invalid returns 400 with error json", %{
      conn: conn
    } do
      response =
        assert_error_sent(400, fn ->
          conn
          |> get(connection_path(conn, :show, "4efd49e7"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("400", "Bad Request", "Invalid/malformed request")
    end

    test "GET /api/connections/:id when resource id found returns 200 with serialized verified connection",
         %{conn: conn, org: org} do
      connection = insert(:verified_connection, organization: org)
      id = connection.id

      conn =
        conn
        |> get(connection_path(conn, :show, id))

      assert %{"data" => %{"id" => ^id, "type" => "connection", "attributes" => attributes}} =
               json_response(conn, 200)

      org_urn = org.urn
      dev_id = connection.device.external_identifier
      dev_platform = connection.device.platform
      email = connection.device_user.email

      assert %{
               "device_identifier" => ^dev_id,
               "platform" => ^dev_platform,
               "jwt" => jwt,
               "email" => ^email,
               "organization_urn" => ^org_urn,
               "verified_at" => verified_at
             } = attributes

      assert {:ok, _, 0} = DateTime.from_iso8601(verified_at)

      assert jwt == connection.jwt
    end
  end
end
