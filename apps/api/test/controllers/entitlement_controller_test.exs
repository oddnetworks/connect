defmodule Api.EntitlementControllerTest do
  use Api.ConnCase

  import Connect.Factory

  setup %{conn: conn} do
    org = insert(:organization)
    ent = insert(:entitlement, organization: org)

    {:ok, conn: conn, org: org, ent: ent}
  end

  describe "api entitlement routes while unauthenticated" do
    test "GET /api/entitlements returns 401", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(api_entitlement_path(conn, :index))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "GET /api/entitlements/:id returns 401", %{conn: conn, ent: ent} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(api_entitlement_path(conn, :show, ent.id))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "POST /api/entitlements returns 401", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(api_entitlement_path(conn, :create), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "PATCH /api/entitlements/:id returns 401", %{conn: conn, ent: ent} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> patch(api_entitlement_path(conn, :update, ent.id), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "DELETE /api/entitlements/:id returns 401", %{conn: conn, ent: ent} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> delete(api_entitlement_path(conn, :delete, ent.id))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api entitlement routes while authenticated as a customer" do
    setup %{conn: conn} do
      # customer = insert(:customer)

      # bad_token = generate_api_token(customer)

      # conn =
      #   conn
      #   |> put_req_header("authorization", "bearer #{bad_token}")

      {:ok, conn: conn}
    end

    @tag :skip
    test "GET /api/entitlements returns 401", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(api_entitlement_path(conn, :index))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "GET /api/entitlements/:id returns 401", %{conn: conn, ent: ent} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(api_entitlement_path(conn, :show, ent.id))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "POST /api/entitlements returns 401", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(api_entitlement_path(conn, :create), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "PATCH /api/entitlements/:id returns 401", %{conn: conn, ent: ent} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> patch(api_entitlement_path(conn, :update, ent.id), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "DELETE /api/entitlements/:id returns 401", %{conn: conn, ent: ent} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> delete(api_entitlement_path(conn, :delete, ent.id))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api entitlement routes while authenticated as an organization" do
    setup %{conn: conn, org: org} do
      token = generate_api_token(org)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")

      {:ok, conn: conn, org: org}
    end

    test "GET /api/entitlements returns 200", %{conn: conn, ent: ent} do
      _unscoped_ent = insert(:entitlement)

      conn =
        conn
        |> get(api_entitlement_path(conn, :index))

      id = ent.id
      type = ent.type
      name = ent.name
      entitlement_identifier = ent.entitlement_identifier
      product_identifier = ent.product_identifier
      platform = ent.platform

      %{
        "data" => [
          %{
            "id" => ^id,
            "type" => ^type,
            "attributes" => %{
              "name" => ^name,
              "description" => nil,
              "entitlement_identifier" => ^entitlement_identifier,
              "product_identifier" => ^product_identifier,
              "platform" => ^platform,
              "inserted_at" => inserted_at,
              "updated_at" => updated_at
            }
          }
        ]
      } = json_response(conn, 200)

      assert inserted_at =~ formatted_date_regex()
      assert updated_at =~ formatted_date_regex()
    end

    test "GET /api/entitlements/:id when entitlement not scoped to current organization returns 404",
         %{conn: conn} do
      ent = insert(:entitlement)

      response =
        assert_error_sent(404, fn ->
          conn
          |> get(api_entitlement_path(conn, :show, ent.id))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "GET /api/entitlements/:id when entitlement scoped to current organization returns 200",
         %{conn: conn, ent: ent} do
      conn =
        conn
        |> get(api_entitlement_path(conn, :show, ent.id))

      id = ent.id
      type = ent.type
      name = ent.name
      entitlement_identifier = ent.entitlement_identifier
      product_identifier = ent.product_identifier
      platform = ent.platform

      %{
        "data" => %{
          "id" => ^id,
          "type" => ^type,
          "attributes" => %{
            "name" => ^name,
            "description" => nil,
            "entitlement_identifier" => ^entitlement_identifier,
            "product_identifier" => ^product_identifier,
            "platform" => ^platform,
            "inserted_at" => inserted_at,
            "updated_at" => updated_at
          }
        }
      } = json_response(conn, 200)

      assert inserted_at =~ formatted_date_regex()
      assert updated_at =~ formatted_date_regex()
    end

    test "POST /api/entitlements with invalid attributes returns 422", %{conn: conn, ent: ent} do
      invalid_params = %{
        data: %{
          type: ent.type,
          attributes: %{
            name: ent.name,
            entitlement_identifier: ent.entitlement_identifier,
            product_identifier: ent.product_identifier,
            platform: ent.platform
          }
        }
      }

      response =
        assert_error_sent(422, fn ->
          conn
          |> post(api_entitlement_path(conn, :create), invalid_params)
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body(
                 "422",
                 "Unprocessable Entity",
                 "product identifier already exists for this platform"
               )
    end

    test "POST /api/entitlements with valid attributes returns 201", %{conn: conn} do
      type =
        [
          "subscription",
          "asset"
        ]
        |> Enum.random()

      name = "Gold"

      platform =
        [
          "AMAZON",
          "APPLE",
          "GOOGLE",
          "ROKU",
          "WEB"
        ]
        |> Enum.random()

      entitlement_identifier = "gold"
      product_identifier = "#{platform}-gold"

      valid_params = %{
        data: %{
          type: type,
          attributes: %{
            name: name,
            entitlement_identifier: entitlement_identifier,
            product_identifier: product_identifier,
            platform: platform
          }
        }
      }

      conn =
        conn
        |> post(api_entitlement_path(conn, :create), valid_params)

      %{
        "data" => %{
          "id" => _id,
          "type" => ^type,
          "attributes" => %{
            "name" => ^name,
            "description" => nil,
            "entitlement_identifier" => ^entitlement_identifier,
            "product_identifier" => ^product_identifier,
            "platform" => ^platform,
            "inserted_at" => inserted_at,
            "updated_at" => updated_at
          }
        }
      } = json_response(conn, 201)

      assert inserted_at =~ formatted_date_regex()
      assert updated_at =~ formatted_date_regex()
    end

    test "PATCH /api/entitlements/:id when entitlement does not belong to current organization returns 404",
         %{conn: conn} do
      unscoped_ent = insert(:entitlement)

      valid_params = %{
        data: %{
          id: unscoped_ent.id,
          type: unscoped_ent.type,
          attributes: %{
            name: "FooBar"
          }
        }
      }

      response =
        assert_error_sent(404, fn ->
          conn
          |> patch(api_entitlement_path(conn, :update, unscoped_ent.id), valid_params)
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "PATCH /api/entitlements/:id with invalid attributes returns 422", %{
      conn: conn,
      ent: ent
    } do
      invalid_params = %{
        data: %{
          id: ent.id,
          type: ent.type,
          attributes: %{
            entitlement_identifier: ""
          }
        }
      }

      response =
        assert_error_sent(422, fn ->
          conn
          |> patch(api_entitlement_path(conn, :update, ent.id), invalid_params)
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body(
                 "422",
                 "Unprocessable Entity",
                 "entitlement identifier can't be blank"
               )
    end

    test "PATCH /api/entitlements/:id with valid attributes returns 200", %{conn: conn, ent: ent} do
      entitlement_identifier = "different"
      description = "cuz"

      valid_params = %{
        data: %{
          id: ent.id,
          type: ent.type,
          attributes: %{
            entitlement_identifier: entitlement_identifier,
            description: description
          }
        }
      }

      conn =
        conn
        |> patch(api_entitlement_path(conn, :update, ent.id), valid_params)

      id = ent.id
      type = ent.type
      name = ent.name
      product_identifier = ent.product_identifier
      platform = ent.platform

      %{
        "data" => %{
          "id" => ^id,
          "type" => ^type,
          "attributes" => %{
            "name" => ^name,
            "description" => ^description,
            "entitlement_identifier" => ^entitlement_identifier,
            "product_identifier" => ^product_identifier,
            "platform" => ^platform,
            "inserted_at" => inserted_at,
            "updated_at" => updated_at
          }
        }
      } = json_response(conn, 200)

      assert inserted_at =~ formatted_date_regex()
      assert updated_at =~ formatted_date_regex()
    end

    test "DELETE /api/entitlements/:id when entitlement does not belong to current organization returns 404",
         %{conn: conn} do
      unscoped_ent = insert(:entitlement)

      response =
        assert_error_sent(404, fn ->
          conn
          |> delete(api_entitlement_path(conn, :delete, unscoped_ent.id))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "DELETE /api/entitlements/:id returns 204", %{conn: conn, ent: ent} do
      conn =
        conn
        |> delete(api_entitlement_path(conn, :delete, ent.id))

      assert response(conn, 204) == ""
    end
  end
end
