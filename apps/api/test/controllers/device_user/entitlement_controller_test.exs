defmodule Api.DeviceUser.EntitlementControllerTest do
  use Api.ConnCase

  import Connect.Factory

  setup %{conn: conn} do
    dev = insert(:device_user)

    {:ok, conn: conn, dev: dev}
  end

  describe "GET /api/device_users/:email/entitlements while unauthenticated" do
    test "returns 401 with error json", %{conn: conn, dev: dev} do
      response =
        assert_error_sent(401, fn ->
          conn =
            conn
            |> get(device_user_entitlement_path(conn, :index, dev.email))

          IO.inspect(conn)
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "GET /api/device_users/:email/entitlements while authenticated as a customer" do
    setup %{conn: conn} do
      # customer = insert(:customer)

      # bad_token = generate_api_token(customer)

      # conn =
      #   conn
      #   |> put_req_header("authorization", "bearer #{bad_token}")

      {:ok, conn: conn}
    end

    @tag :skip
    test "returns 401 with error json", %{conn: conn, dev: dev} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(device_user_entitlement_path(conn, :index, dev.email))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "GET /api/device_users/:email/entitlements while authenticated as an organization" do
    setup %{conn: conn} do
      org = insert(:organization)
      token = generate_api_token(org)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")

      {:ok, conn: conn, org: org}
    end

    test "returns empty array when device_user not found", %{conn: conn} do
      conn =
        conn
        |> get(device_user_entitlement_path(conn, :index, "foo"))

      %{
        "data" => []
      } = json_response(conn, 200)
    end

    test "returns only distinct, valid entitlements scoped to the organization", %{
      conn: conn,
      dev: dev,
      org: org
    } do
      # Note: this test will cause an error similar to the following,
      #
      # Postgrex.Protocol (#PID<0.830.0>) disconnected: ** (DBConnection.ConnectionError) owner #PID<0.1862.0> exited while client #PID<0.1868.0> is still running with: shutdown
      #
      # Task #PID<0.1870.0> started from Transactions terminating
      # ** (stop) exited in: GenServer.call(#PID<0.1865.0>, {:checkout, #Reference<0.52896798.3777232897.240874>, true, 15000}, 5000)
      #
      # Since we are calling out to our Transactions service to validate things
      # via `GenServer.cast`, we don't actually care about this error, so, while annoying, it is safe to ignore.
      ent = insert(:entitlement, organization: org)

      insert(
        :transaction,
        entitlement: ent,
        platform: ent.platform,
        device_user: dev,
        receipt: %{
          foo: "12345",
          bar: "67890"
        }
      )

      t2 =
        insert(
          :transaction,
          entitlement: ent,
          platform: ent.platform,
          device_user: dev,
          receipt: %{
            fizz: "abcde",
            buzz: "fghij"
          }
        )

      invalidated_ent = insert(:entitlement, organization: org)

      insert(
        :transaction,
        entitlement: invalidated_ent,
        platform: invalidated_ent.platform,
        invalidated_at: DateTime.utc_now(),
        device_user: dev
      )

      unscoped_ent = insert(:entitlement)

      insert(
        :transaction,
        entitlement: unscoped_ent,
        platform: unscoped_ent.platform,
        device_user: dev
      )

      conn =
        conn
        |> get(device_user_entitlement_path(conn, :index, dev.email))

      id = ent.id
      type = ent.type
      name = ent.name
      entitlement_identifier = ent.entitlement_identifier
      product_identifier = ent.product_identifier
      platform = ent.platform

      transaction_identifier = t2.external_identifier
      assigned_at = Api.Serializers.Base.format_date(t2.inserted_at)

      %{
        "data" => [
          %{
            "id" => ^id,
            "type" => ^type,
            "attributes" => %{
              "name" => ^name,
              "description" => nil,
              "entitlement_identifier" => ^entitlement_identifier,
              "product_identifier" => ^product_identifier,
              "transaction_identifier" => ^transaction_identifier,
              "receipt" => receipt,
              "platform" => ^platform,
              "assigned_at" => ^assigned_at
            }
          }
        ]
      } = json_response(conn, 200)

      fizz = t2.receipt.fizz
      buzz = t2.receipt.buzz
      assert %{"fizz" => ^fizz, "buzz" => ^buzz} = receipt
    end
  end
end
