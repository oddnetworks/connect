defmodule Api.TransactionRestoreControllerTest do
  use Api.ConnCase

  import Transactions.Factory

  import ExUnit.CaptureLog

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @apple_cassette_dir "test/fixture/vcr_cassettes/apple"
  @amazon_cassette_dir "test/fixture/vcr_cassettes/amazon"

  setup_all do
    HTTPoison.start()
  end

  setup %{conn: conn} do
    org = insert(:organization)

    {:ok, conn: conn, org: org}
  end

  describe "api transaction restore routes while unauthenticated" do
    test "POST /api/transactions/restore returns 401 with error json", %{
      conn: conn
    } do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(transaction_restore_path(conn, :restore), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api transaction restore routes while authenticated as an organization" do
    setup %{conn: conn} do
      du = Transactions.Factory.insert(:device_user)

      pi =
        Transactions.Factory.insert(:platform_identity,
          device_user: du,
          platform: "AMAZON",
          external_identifier: "amazon-user-1234"
        )

      org = insert(:organization)

      Ecto.Changeset.change(org.settings,
        amazon_shared_secret:
          "2:dbSJ286aE3mfwID7K57BIvY7DEPLxbu4FF-WwdZUBZku5-lrnLVfr-ce2Ltwwlp_:h8E8Y1EuDHbYUYWOz7ysag==",
        amazon_use_sandbox: false
      )
      |> Connect.Repo.update!()

      latest_receipt =
        %{
          quantity: 1,
          product_id: "thing",
          transaction_id: "12345",
          original_transaction_id: "12345",
          purchase_date: "2017-10-01T01:45:00Z",
          original_purchase_date: "2017-10-1T01:45:00Z",
          expires_date: "2017-11-1T01:45:00Z",
          cancellation_date: nil,
          app_item_id: "thing1",
          version_external_identifier: "version_external_identifier",
          web_order_line_item_id: "web_order_line_item_id"
        }
        |> JOSE.encode()
        |> Base.encode64()

      receipt = %{
        "latest_receipt" => latest_receipt
      }

      ent =
        insert(
          :entitlement,
          product_identifier: "com.oddconnect.monthly_subscription",
          platform: "APPLE",
          organization: org
        )

      ament =
        insert(:entitlement,
          product_identifier: "amazon.product.one",
          platform: "AMAZON",
          organization: org
        )

      apple_trans =
        insert(
          :transaction,
          platform: ent.platform,
          receipt: receipt,
          entitlement: ent
        )

      token = generate_api_token(org)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")

      {:ok, conn: conn, org: org, apple_trans: apple_trans, ament: ament, pi: pi}
    end

    test "POST /api/transactions/restore returns 400 when invalid parameters passed", %{
      conn: conn
    } do
      response =
        assert_error_sent(400, fn ->
          conn
          |> post(transaction_restore_path(conn, :restore), %{})
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("400", "Bad Request", "Invalid/malformed request")
    end

    test "POST /api/transactions/restore returns 422 when transaction not found and not creatable",
         %{conn: conn} do
      assert capture_log([level: :error], fn ->
               response =
                 assert_error_sent(422, fn ->
                   conn
                   |> post(transaction_restore_path(conn, :restore), %{
                     "data" => %{
                       "attributes" => %{
                         "external_identifier" => "23",
                         "platform" => "APPLE",
                         "receipt" => %{}
                       },
                       "type" => "transaction"
                     }
                   })
                 end)

               {_status, _headers, body} = response

               assert Poison.decode!(body) ==
                        json_error_body(
                          "422",
                          "Unprocessable Entity",
                          "Transaction Not Found - Unable To Create"
                        )
             end) =~ "Transaction Not Found - Unable To Create"
    end

    test "POST /api/transactions/restore returns 422 when platform not restorable", %{
      conn: conn,
      org: org
    } do
      ent = insert(:entitlement, platform: "ROKU", organization: org)
      transaction = insert(:transaction, entitlement: ent, platform: "ROKU")

      assert capture_log([level: :error], fn ->
               response =
                 assert_error_sent(422, fn ->
                   conn
                   |> post(transaction_restore_path(conn, :restore), %{
                     "data" => %{
                       "attributes" => %{
                         "external_identifier" => transaction.external_identifier,
                         "platform" => transaction.platform,
                         "receipt" => transaction.receipt
                       },
                       "type" => "transaction"
                     }
                   })
                 end)

               {_status, _headers, body} = response

               assert Poison.decode!(body) ==
                        json_error_body(
                          "422",
                          "Unprocessable Entity",
                          "Transactions from \"ROKU\" Platform are not able to be restored"
                        )
             end) =~ "not able to be restored"
    end

    test "POST /api/transactions/restore returns 422 when invalid receipt", %{
      conn: conn,
      apple_trans: trans
    } do
      ExVCR.Config.cassette_library_dir(@apple_cassette_dir)

      use_cassette "failure" do
        assert capture_log([level: :error], fn ->
                 response =
                   assert_error_sent(422, fn ->
                     conn
                     |> post(transaction_restore_path(conn, :restore), %{
                       "data" => %{
                         "attributes" => %{
                           "external_identifier" => trans.external_identifier,
                           "platform" => trans.platform,
                           "receipt" => trans.receipt
                         },
                         "type" => "transaction"
                       }
                     })
                   end)

                 {_status, _headers, body} = response

                 assert Poison.decode!(body) ==
                          json_error_body(
                            "422",
                            "Unprocessable Entity",
                            "Apple - The data in the receipt-data property was malformed or missing."
                          )
               end) =~ "Apple - The data in the receipt-data property was malformed or missing."
      end
    end

    test "POST /api/transactions/restore returns 200 when transaction is created and validation successful",
         %{conn: conn, ament: ament, pi: pi} do
      ExVCR.Config.cassette_library_dir(@amazon_cassette_dir)

      use_cassette "invalid_receipt_response" do
        conn =
          conn
          |> post(transaction_restore_path(conn, :restore), %{
            "data" => %{
              "attributes" => %{
                "external_identifier" => "amazon-receipt-id",
                "platform" => "AMAZON",
                "receipt" => %{
                  "user_id" => "my-user-id-idk"
                },
                "product_identifier" => ament.product_identifier,
                "platform_identity" => pi.external_identifier
              },
              "type" => "transaction"
            }
          })

        assert %{"data" => %{"id" => id, "type" => "transaction", "attributes" => attributes}} =
                 json_response(conn, 200)

        refute is_nil(id)

        assert Map.keys(attributes) == [
                 "email",
                 "entitlement_identifier",
                 "external_identifier",
                 "inserted_at",
                 "invalidated_at",
                 "invalidation_reason",
                 "last_validated_at",
                 "platform",
                 "product_identifier",
                 "receipt",
                 "updated_at"
               ]
      end
    end

    test "POST /api/transactions/restore returns 200 when receipt validation is successful", %{
      conn: conn,
      apple_trans: trans = %{id: id}
    } do
      ExVCR.Config.cassette_library_dir(@apple_cassette_dir)

      use_cassette "success_invalid_expired" do
        conn =
          conn
          |> post(transaction_restore_path(conn, :restore), %{
            "data" => %{
              "attributes" => %{
                "external_identifier" => trans.external_identifier,
                "platform" => trans.platform,
                "receipt" => trans.receipt
              },
              "type" => "transaction"
            }
          })

        assert %{"data" => %{"id" => ^id, "type" => "transaction", "attributes" => attributes}} =
                 json_response(conn, 200)

        assert Map.keys(attributes) == [
                 "email",
                 "entitlement_identifier",
                 "external_identifier",
                 "inserted_at",
                 "invalidated_at",
                 "invalidation_reason",
                 "last_validated_at",
                 "platform",
                 "product_identifier",
                 "receipt",
                 "updated_at"
               ]
      end
    end
  end
end
