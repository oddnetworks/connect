defmodule Api.TransactionControllerTest do
  use Api.ConnCase

  import Transactions.Factory


  setup %{conn: conn} do
    dev = insert(:device_user)
    org = insert(:organization)

    {:ok, conn: conn, org: org, dev: dev}
  end

  describe "api transaction routes while unauthenticated" do
    test "GET /api/device_users/:email/transactions returns 401 with error json", %{
      conn: conn,
      dev: dev
    } do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(device_user_transaction_path(conn, :index, dev.email, %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "GET /api/transactions/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(transaction_path(conn, :show, "c839fe28-f0f7-4158-a081-860bb32750a4"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "POST /api/device_users/:email/transactions returns 401 with error json", %{
      conn: conn,
      dev: dev
    } do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(device_user_transaction_path(conn, :create, dev.email, %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "PUT /api/transactions/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> put(transaction_path(conn, :update, "fbbe47ae-5ea7-4a9e-b2b9-4755fbd54f55", %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    test "DELETE /api/transactions/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> delete(transaction_path(conn, :delete, "fbbe47ae-5ea7-4a9e-b2b9-4755fbd54f55"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api transaction routes while authenticated as a customer" do
    setup %{conn: conn} do
      dev = insert(:device_user)
      # customer = insert(:customer)

      # bad_token = generate_api_token(customer)

      # conn =
      #   conn
      #   |> put_req_header("authorization", "bearer #{bad_token}")

      {:ok, conn: conn, dev: dev}
    end

    @tag :skip
    test "GET /api/device_users/:email/transactions returns 401 with error json", %{
      conn: conn,
      dev: dev
    } do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(device_user_transaction_path(conn, :index, dev.email))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "GET /api/transactions/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> get(transaction_path(conn, :show, "c839fe28-f0f7-4158-a081-860bb32750a4"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "POST /api/device_users/:email/transactions returns 401 with error json", %{
      conn: conn,
      dev: dev
    } do
      response =
        assert_error_sent(401, fn ->
          conn
          |> post(device_user_transaction_path(conn, :create, dev.email, %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "PUT /api/transactions/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> put(transaction_path(conn, :update, "fbbe47ae-5ea7-4a9e-b2b9-4755fbd54f55", %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end

    @tag :skip
    test "DELETE /api/transactions/:id returns 401 with error json", %{conn: conn} do
      response =
        assert_error_sent(401, fn ->
          conn
          |> delete(transaction_path(conn, :delete, "fbbe47ae-5ea7-4a9e-b2b9-4755fbd54f55"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("401", "Unauthorized", "Invalid authorization")
    end
  end

  describe "api transaction routes while authenticated as an organization" do
    setup %{conn: conn} do
      org = insert(:organization)
      dev = insert(:device_user)
      ent = insert(:entitlement, organization: org)

      trans =
        insert(
          :transaction,
          entitlement: ent,
          device_user: dev,
          platform: ent.platform
        )

      token = generate_api_token(org)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")

      {:ok, conn: conn, org: org, dev: dev, ent: ent, trans: trans}
    end

    test "GET /api/device_users/:email/transactions returns 200 with json array", %{
      conn: conn,
      dev: dev,
      trans: trans,
      ent: ent
    } do
      _unscoped = insert(:transaction, entitlement: ent)

      conn =
        conn
        |> get(device_user_transaction_path(conn, :index, dev.email))

      id = trans.id
      email = dev.email
      entitlement_identifier = ent.entitlement_identifier
      external_identifier = trans.external_identifier
      platform = trans.platform
      product_identifier = ent.product_identifier
      receipt = %{}

      assert %{
               "data" => [
                 %{
                   "id" => ^id,
                   "type" => "transaction",
                   "attributes" => %{
                     "email" => ^email,
                     "entitlement_identifier" => ^entitlement_identifier,
                     "external_identifier" => ^external_identifier,
                     "platform" => ^platform,
                     "product_identifier" => ^product_identifier,
                     "receipt" => ^receipt,
                     "invalidated_at" => nil,
                     "invalidation_reason" => nil,
                     "last_validated_at" => last_validated_at
                   },
                   "relationships" => rels
                 }
               ],
               "jsonapi" => %{"version" => "1.0"}
             } = json_response(conn, 200)

      assert last_validated_at =~ formatted_date_regex()
      dev_id = dev.id
      assert %{"data" => %{"id" => ^dev_id, "type" => "device_user"}} = rels["device_user"]
      ent_id = ent.id
      ent_type = ent.type
      assert %{"data" => %{"id" => ^ent_id, "type" => ^ent_type}} = rels["entitlement"]
    end

    test "GET /api/transactions/:id returns 404 when unscoped to current organization", %{
      conn: conn
    } do
      unscoped = insert(:transaction)

      response =
        assert_error_sent(404, fn ->
          conn
          |> get(transaction_path(conn, :show, unscoped.id))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "GET /api/transactions/:id returns 200 with json body", %{
      conn: conn,
      dev: dev,
      trans: trans,
      ent: ent
    } do
      conn =
        conn
        |> get(transaction_path(conn, :show, trans.id))

      id = trans.id
      email = dev.email
      entitlement_identifier = ent.entitlement_identifier
      external_identifier = trans.external_identifier
      platform = trans.platform
      product_identifier = ent.product_identifier
      receipt = %{}

      assert %{
               "data" => %{
                 "id" => ^id,
                 "type" => "transaction",
                 "attributes" => %{
                   "email" => ^email,
                   "entitlement_identifier" => ^entitlement_identifier,
                   "external_identifier" => ^external_identifier,
                   "platform" => ^platform,
                   "product_identifier" => ^product_identifier,
                   "receipt" => ^receipt,
                   "invalidated_at" => nil,
                   "invalidation_reason" => nil,
                   "last_validated_at" => last_validated_at
                 },
                 "relationships" => rels
               },
               "jsonapi" => %{"version" => "1.0"}
             } = json_response(conn, 200)

      assert last_validated_at =~ formatted_date_regex()
      dev_id = dev.id
      assert %{"data" => %{"id" => ^dev_id, "type" => "device_user"}} = rels["device_user"]
      ent_id = ent.id
      ent_type = ent.type
      assert %{"data" => %{"id" => ^ent_id, "type" => ^ent_type}} = rels["entitlement"]
    end

    test "POST /api/device_users/:email/transactions returns 422 with error json when entitlement not found",
         %{conn: conn} do
      valid_attributes = %{
        "platform" => Connect.Platform.valid_entitlement_platforms() |> Enum.random(),
        "product_identifier" => "product_identifier",
        "external_identifier" => "external_identifier",
        "receipt" => %{"a" => "b"}
      }

      response =
        assert_error_sent(422, fn ->
          conn
          |> post(
            device_user_transaction_path(conn, :create, "foo@example.com", %{
              "data" => %{"type" => "transaction", "attributes" => valid_attributes}
            })
          )
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("422", "Unprocessable Entity", "Entitlement not found")
    end

    test "POST /api/device_users/:email/transactions returns 422 with JSON error when device_user email is invalid",
         %{conn: conn} do
      valid_attributes = %{
        "platform" => Connect.Platform.valid_entitlement_platforms() |> Enum.random(),
        "product_identifier" => "product_identifier",
        "external_identifier" => "external_identifier",
        "receipt" => %{"a" => "b"}
      }

      response =
        assert_error_sent(422, fn ->
          conn
          |> post(
            device_user_transaction_path(conn, :create, "BAD EMAIL FORMAT", %{
              "data" => %{"type" => "transaction", "attributes" => valid_attributes}
            })
          )
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body(
                 "422",
                 "Unprocessable Entity",
                 "Device user: email has invalid format"
               )
    end

    test "POST /api/device_users/:email/transactions returns 400 with JSON error when invalid parameters given",
         %{conn: conn} do
      response =
        assert_error_sent(400, fn ->
          conn
          |> post(device_user_transaction_path(conn, :create, "ff", %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("400", "Bad Request", "Invalid/malformed request")
    end

    test "POST /api/device_users/:email/transactions returns 201 with serialized transaction", %{
      conn: conn,
      org: org
    } do
      ent = insert(:entitlement, organization: org)

      product_identifier = ent.product_identifier
      external_identifier = "04ccc8c8-8c6a-4b8e-b87e-c999155c3418"

      email = "foo1234@example.com"

      trans_attributes = %{
        "platform" => ent.platform,
        "product_identifier" => product_identifier,
        "external_identifier" => external_identifier,
        "receipt" => %{"bleep" => "bloop"}
      }

      conn =
        conn
        |> post(
          device_user_transaction_path(conn, :create, email, %{
            "data" => %{"attributes" => trans_attributes, "type" => "transaction"}
          })
        )

      %{"data" => %{"id" => id, "type" => "transaction", "attributes" => attributes}} =
        json_response(conn, 201)

      {:ok, _} = UUID.info(id)

      assert Map.keys(attributes) == [
               "email",
               "entitlement_identifier",
               "external_identifier",
               "inserted_at",
               "invalidated_at",
               "invalidation_reason",
               "last_validated_at",
               "platform",
               "product_identifier",
               "receipt",
               "updated_at"
             ]

      assert external_identifier == attributes["external_identifier"]
      assert email == attributes["email"]
      assert ent.entitlement_identifier == attributes["entitlement_identifier"]
      assert ent.product_identifier == attributes["product_identifier"]
      assert ent.platform == attributes["platform"]
      assert is_nil(attributes["invalidated_at"])
      assert %{"bleep" => "bloop"} == attributes["receipt"]
    end

    test "PUT /api/transactions/:id returns 400 when invalid parameters passed", %{conn: conn} do
      response =
        assert_error_sent(400, fn ->
          conn
          |> put(transaction_path(conn, :update, "fbbe47ae-5ea7-4a9e-b2b9-4755fbd54f55", %{}))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) ==
               json_error_body("400", "Bad Request", "Invalid/malformed request")
    end

    test "PUT /api/transactions/:id returns 404 when transaction not found", %{conn: conn} do
      id = "an id"

      params = %{
        "data" => %{
          "attributes" => %{
            "invalidation_reason" => "cuz"
          },
          "id" => id,
          "type" => "transaction"
        }
      }

      response =
        assert_error_sent(404, fn ->
          conn
          |> put(transaction_path(conn, :update, id, params))
        end)

      {_status, _headers, body} = response
      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "PUT /api/transactions/:id returns 200 when transaction successfully updated", %{
      conn: conn,
      trans: transaction
    } do
      params = %{
        "data" => %{
          "attributes" => %{
            "invalidated_at" => "2017-09-19T15:13:00Z",
            "invalidation_reason" => "cuz"
          },
          "id" => transaction.id,
          "type" => "transaction"
        }
      }

      conn =
        conn
        |> put(transaction_path(conn, :update, transaction.id, params))

      id = transaction.id

      %{
        "data" => %{
          "attributes" => attributes,
          "id" => ^id,
          "type" => "transaction"
        }
      } = json_response(conn, 200)

      assert attributes["invalidation_reason"] ==
               params["data"]["attributes"]["invalidation_reason"]

      assert attributes["invalidated_at"] =~ formatted_date_regex()
    end

    test "DELETE /api/transactions/:id returns 404 when transaction doesn't exist", %{
      conn: conn
    } do
      response =
        assert_error_sent(404, fn ->
          conn
          |> delete(transaction_path(conn, :delete, "garbage id"))
        end)

      {_status, _headers, body} = response

      assert Poison.decode!(body) == json_error_body("404", "Not Found", "Resource not found")
    end

    test "DELETE /api/transactions/:id returns 204 when transaction successfully deleted", %{
      conn: conn,
      trans: transaction
    } do
      conn =
        conn
        |> delete(transaction_path(conn, :delete, transaction.id))

      assert response(conn, 204) == ""
    end
  end
end
