defmodule Api.Test.Helpers do
  def generate_api_token(resource) do
    claims =
      Guardian.Claims.app_claims()
      |> Guardian.Claims.ttl({1, :day})

    {:ok, jwt, _claims} = Guardian.encode_and_sign(resource, :access, claims)
    jwt
  end

  def json_error_body(status, title, detail) do
    %{"errors" => [%{"detail" => detail, "status" => status, "title" => title}]}
  end
  def json_error_body_with_pointer(status, title, detail, pointer) do
    %{"errors" => [%{"detail" => detail, "status" => status, "title" => title, "source" => %{"pointer" => pointer}}]}
  end

  def formatted_date_regex, do: ~r/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z/
end
