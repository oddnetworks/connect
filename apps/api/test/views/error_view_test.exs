defmodule Api.ErrorViewTest do
  use Api.ConnCase

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert %{errors: [%{detail: "Resource not found", status: "404", title: "Not Found"}]} =
             render(Api.ErrorView, "404.json", [])
  end

  test "render 500.json" do
    assert %{
             errors: [
               %{
                 detail: "Internal server error",
                 status: "500",
                 title: "Internal Error"
               }
             ]
           } = render(Api.ErrorView, "500.json", [])
  end

  test "render any other" do
    assert %{
             errors: [
               %{
                 detail: "Internal server error",
                 status: "500",
                 title: "Internal Error"
               }
             ]
           } = render(Api.ErrorView, "505.json", [])
  end
end
