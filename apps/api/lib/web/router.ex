defmodule Api.Router do
  use Api.Web, :router
  use Plug.ErrorHandler

  pipeline :api do
    plug(:accepts, ["json-api", "json"])

    if Mix.env() == :prod do
      plug(Plug.SSL, rewrite_on: [:x_forwarded_proto])
      plug(:put_secure_browser_headers)
    end
  end

  pipeline :api_auth do
    plug(Guardian.Plug.VerifyHeader, realm: "Bearer")
    plug(Guardian.Plug.LoadResource)
  end

  scope "/api", Api do
    pipe_through([:api, :api_auth])

    resources("/connections", ConnectionController, only: [:show, :create])

    resources(
      "/entitlements",
      EntitlementController,
      only: [:index, :show, :create, :update, :delete],
      as: :api_entitlement
    )

    resources "/device_users", DeviceUserController, only: [:show], param: "email" do
      resources("/entitlements", DeviceUser.EntitlementController, only: [:index])
      resources("/transactions", TransactionController, only: [:index, :create])
      post("/connections", ConnectionController, :verified_create)
    end

    post("/transactions/restore", TransactionRestoreController, :restore)
    resources("/transactions", TransactionController, only: [:show, :update, :delete])
  end

  defp handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stacktrace}) do
    conn =
      conn
      |> Plug.Conn.fetch_cookies()
      |> Plug.Conn.fetch_query_params()
      |> Plug.Conn.fetch_session()

    %{
      "request" => %{
        "cookies" => conn.req_cookies,
        "url" => "#{conn.scheme}://#{conn.host}:#{conn.port}#{conn.request_path}",
        "user_ip" => conn.remote_ip |> Tuple.to_list() |> Enum.join("."),
        "headers" => Enum.into(conn.req_headers, %{}),
        "session" => conn.private[:plug_session] || %{},
        "params" => conn.params,
        "method" => conn.method
      },
      "server" => %{}
    }
  end
end
