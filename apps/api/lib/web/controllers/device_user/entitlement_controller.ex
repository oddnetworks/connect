defmodule Api.DeviceUser.EntitlementController do
  use Api.Web, :controller

  alias Connect.Entitlements

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Organization)
  plug(:scrub_params, "data" when action in [:create])

  def index(conn, %{"device_user_email" => email}, %{id: organization_id}, _claims) do
    entitlements =
      case Connect.DeviceUsers.find(organization_id, email) do
        nil ->
          []

        device_user ->
          Transactions.Validator.trigger_for_device_user({device_user.id, organization_id})

          Entitlements.list_for_device_user(
            device_user.email,
            organization_id
          )
      end

    conn
    |> jsonapi(entitlements, Api.Serializers.DeviceUser.Entitlement)
  end
end
