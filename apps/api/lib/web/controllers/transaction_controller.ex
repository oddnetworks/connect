defmodule Api.TransactionController do
  use Api.Web, :controller

  alias Api.Errors.{NotFound, UnprocessableEntity}
  alias Api.Serializers

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Organization)
  plug(:scrub_params, "data" when action in [:create, :update])

  def index(conn, %{"device_user_email" => email}, current_organization, _claims) do
    transactions = Transactions.list_device_user_transactions(current_organization.id, email)

    conn
    |> put_status(200)
    |> jsonapi(transactions, Serializers.Transaction)
  end

  def show(conn, %{"id" => id}, current_organization, _claims) do
    validate_uuid(id)

    case Transactions.find(current_organization.id, id) do
      nil ->
        raise NotFound

      transaction ->
        conn
        |> put_status(200)
        |> jsonapi(transaction, Serializers.Transaction)
    end
  end

  def create(
        conn,
        %{
          "data" => %{
            "attributes" => params,
            "type" => "transaction"
          },
          "device_user_email" => email
        },
        %{id: organization_id},
        _claims
      ) do
    case Transactions.CreationService.run(organization_id, email, params) do
      {:ok, %{transaction: transaction}} ->
        conn
        |> put_status(201)
        |> jsonapi(transaction, Serializers.Transaction)

      {:error, :entitlement_not_found} ->
        raise UnprocessableEntity, detail: "Entitlement not found"

      {:error, :duplicate} ->
        raise UnprocessableEntity, detail: "Duplicate transaction"

      {:error, prefix, changeset} ->
        raise UnprocessableEntity, detail: service_errors_to_string(changeset, to_string(prefix))

      {:error, reason} ->
        raise UnprocessableEntity, detail: service_errors_to_string(reason)
    end
  end

  def update(
        conn,
        %{
          "data" => %{"attributes" => params, "id" => transaction_id, "type" => "transaction"},
          "id" => transaction_id
        },
        %{id: organization_id},
        _claims
      ) do
    validate_uuid(transaction_id)

    case Transactions.update(organization_id, transaction_id, params) do
      {:ok, transaction} ->
        conn
        |> put_status(200)
        |> jsonapi(transaction, Serializers.Transaction)

      {:error, changeset} ->
        raise UnprocessableEntity, detail: service_errors_to_string(changeset)
    end
  end

  def delete(conn, %{"id" => transaction_id}, %{id: organization_id}, _claims) do
    validate_uuid(transaction_id)
    Transactions.delete!(organization_id, transaction_id)

    conn
    |> send_resp(204, "")
  end

  defp validate_uuid(uuid) do
    case UUID.info(uuid) do
      {:ok, _} -> :ok
      {:error, _} -> raise NotFound
    end
  end
end
