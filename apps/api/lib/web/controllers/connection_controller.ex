defmodule Api.ConnectionController do
  use Api.Web, :controller

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Organization)
  plug(:scrub_params, "data" when action in [:create, :verified_create])

  def create(
        conn,
        params = %{"data" => data = %{"attributes" => attributes = %{"email" => email}}},
        current_organization = %{settings: %{in_review_mode: true}},
        claims
      ) do
    attributes =
      attributes
      |> Map.put("verified_during_review", true)

    data =
      data
      |> Map.put("attributes", attributes)

    params =
      params
      |> Map.put("device_user_email", email)
      |> Map.put("data", data)

    verified_create(conn, params, current_organization, claims)
  end

  def create(
        conn,
        %{"data" => %{"attributes" => params, "type" => "connection"}},
        current_organization,
        _claims
      ) do
    params =
      params
      |> atomize_keys()
      |> Map.put(:organization_id, current_organization.id)
      |> Map.put(:uri, construct_verification_uri(conn, current_organization.urn))

    case Connections.create(params) do
      {:ok, pending_connection} ->
        conn
        |> put_status(:created)
        |> jsonapi(pending_connection, Api.Serializers.PendingConnection)

      {:error, %Ecto.Changeset{} = changeset} ->
        raise Api.Errors.UnprocessableEntity, changeset: changeset

      {:error, reason} ->
        raise Api.Errors.UnprocessableEntity, detail: reason
    end
  end

  def verified_create(
        conn,
        %{
          "data" => %{"attributes" => params, "type" => "connection"},
          "device_user_email" => email
        },
        current_organization,
        _claims
      ) do
    params =
      params
      |> atomize_keys()
      |> Map.put(:organization_id, current_organization.id)
      |> Map.put(:email, email)
      |> Map.put(:uri, construct_verification_uri(conn, current_organization.urn))

    case Connections.create_verified(params) do
      {:ok, %{verified_connection: verified_connection}} ->
        conn
        |> put_status(:created)
        |> jsonapi(verified_connection, Api.Serializers.VerifiedConnection)

      {:error, prefix, reason, _changes_so_far} ->
        raise Api.Errors.UnprocessableEntity, detail: service_errors_to_string(reason, prefix)

      {:error, reason} ->
        raise Api.Errors.UnprocessableEntity, detail: reason
    end
  end

  def show(conn, %{"id" => id}, current_organization, _claims) do
    case Connections.get_verified_connection_by(id: id, organization_id: current_organization.id) do
      nil ->
        raise Api.Errors.NotFound

      verified_connection ->
        conn
        |> jsonapi(verified_connection, Api.Serializers.VerifiedConnection)
    end
  end

  defp construct_verification_uri(conn, subdomain) do
    host = subdomain <> "." <> conn.host

    scheme =
      conn.scheme
      |> to_string()

    %URI{
      scheme: scheme,
      host: host,
      path: "/verify/"
    }
    |> to_string()
  end
end
