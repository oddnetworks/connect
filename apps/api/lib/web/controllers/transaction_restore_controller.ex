defmodule Api.TransactionRestoreController do
  use Api.Web, :controller

  alias Api.Errors.UnprocessableEntity
  alias Transactions.RestoreService
  alias Api.Serializers

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Organization)

  plug(:scrub_params, "data" when action in [:restore])

  def restore(
        conn,
        %{"data" => %{"attributes" => params, "type" => "transaction"}},
        %{id: organization_id},
        _claims
      ) do
    case RestoreService.run(params, organization_id) do
      {:ok, transaction} ->
        conn
        |> jsonapi(transaction, Serializers.Transaction)

      {:error, prefix, changeset} ->
        raise UnprocessableEntity, detail: service_errors_to_string(changeset, prefix)

      {:error, reason} ->
        raise UnprocessableEntity, detail: service_errors_to_string(reason)
    end
  end
end
