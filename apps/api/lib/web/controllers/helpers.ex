defmodule Api.Controller.Helpers do
  import Phoenix.Controller, only: [json: 2]

  def atomize_keys(params) do
    params
    |> Enum.reduce(%{}, fn {key, val}, acc -> Map.put(acc, String.to_atom(key), val) end)
  end

  def jsonapi(conn, resource, serializer) do
    serialized = jsonapi_serialize_resource(resource, serializer, conn)
    json(conn, serialized)
  end

  def service_errors_to_string(changeset) when is_binary(changeset), do: changeset

  def service_errors_to_string(changeset) do
    changeset.errors
    |> Enum.map(&changeset_error_to_string/1)
    |> Enum.join(", ")
  end

  def service_errors_to_string(changeset, prefix) do
    String.capitalize(humanize(prefix)) <> ": " <> service_errors_to_string(changeset)
  end

  defp changeset_error_to_string({key, {message, values}}) do
    humanize(key) <>
      " " <>
      Enum.reduce(values, message, fn {k, v}, acc ->
        String.replace(acc, "%{#{k}}", to_string(v))
      end)
  end

  defp changeset_error_to_string({_key, message}) do
    message
  end

  defp humanize(atom) when is_atom(atom), do: humanize(Atom.to_string(atom))

  defp humanize(bin) when is_binary(bin) do
    bin =
      if String.ends_with?(bin, "_id") do
        binary_part(bin, 0, byte_size(bin) - 3)
      else
        bin
      end

    bin |> String.replace("_", " ")
  end

  defp jsonapi_serialize_resource(resource, serializer, conn) do
    JaSerializer.format(serializer, resource, conn)
  end
end
