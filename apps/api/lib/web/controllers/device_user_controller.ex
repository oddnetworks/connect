defmodule Api.DeviceUserController do
  use Api.Web, :controller

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Organization)

  def show(conn, %{"email" => email}, %{id: organization_id}, _claims) do
    case Connect.DeviceUsers.find(organization_id, email) do
      nil ->
        raise Api.Errors.NotFound

      device_user ->
        Transactions.Validator.trigger_for_device_user({device_user.id, organization_id})

        conn
        |> jsonapi(device_user, Api.Serializers.DeviceUser)
    end
  end
end
