defmodule Api.EntitlementController do
  use Api.Web, :controller

  alias Api.Errors.UnprocessableEntity
  alias Connect.Entitlements

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Organization)
  plug(:scrub_params, "data" when action in [:create, :update])

  @webhook_module Application.get_env(:api, :webhook_module) || Webhooks

  @allowed_entitlement_types Entitlements.valid_types()

  def index(conn, _params, current_organization, _claims) do
    entitlements = Entitlements.list(current_organization.id)

    conn
    |> put_status(200)
    |> jsonapi(entitlements, Api.Serializers.Entitlement)
  end

  def show(conn, %{"id" => id}, current_organization, _claims) do
    entitlement = check_entitlement_belongs_to_organization(id, current_organization)

    conn
    |> put_status(200)
    |> jsonapi(entitlement, Api.Serializers.Entitlement)
  end

  def create(
        conn,
        %{"data" => %{"attributes" => params, "type" => type}},
        current_organization,
        _claims
      )
      when type in @allowed_entitlement_types do
    params = for {key, val} <- params, into: %{}, do: {String.to_atom(key), val}
    params = Map.put(params, :organization_id, current_organization.id)
    params = Map.put(params, :type, type)

    case Entitlements.create(params) do
      {:ok, %{id: eid, organization_id: oid} = entitlement} ->
        @webhook_module.trigger(:entitlement, {eid, oid})

        conn
        |> put_status(201)
        |> jsonapi(entitlement, Api.Serializers.Entitlement)

      {:error, changeset} ->
        raise UnprocessableEntity, detail: service_errors_to_string(changeset)
    end
  end

  def update(
        conn,
        %{"id" => id, "data" => %{"attributes" => params, "id" => id, "type" => type}},
        current_organization,
        _claims
      )
      when type in @allowed_entitlement_types do
    check_entitlement_belongs_to_organization(id, current_organization)

    params =
      params
      |> Map.put("type", type)

    case Entitlements.update(id, params) do
      {:ok, %{id: eid, organization_id: oid} = entitlement} ->
        @webhook_module.trigger(:entitlement, {eid, oid})

        conn
        |> put_status(200)
        |> jsonapi(entitlement, Api.Serializers.Entitlement)

      {:error, changeset} ->
        raise UnprocessableEntity, detail: service_errors_to_string(changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_organization, _claims) do
    check_entitlement_belongs_to_organization(id, current_organization)

    case Entitlements.delete(id) do
      {:ok, _} ->
        conn
        |> send_resp(204, "")

      {:error, changeset} ->
        raise UnprocessableEntity, detail: service_errors_to_string(changeset)
    end
  end

  defp check_entitlement_belongs_to_organization(id, organization) do
    case Entitlements.find(id: id, organization_id: organization.id) do
      nil ->
        raise Api.Errors.NotFound

      entitlement ->
        entitlement
    end
  end
end
