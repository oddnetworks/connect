defmodule Api.Serializers.DeviceUser do
  use Api.Serializers.Base

  def attributes(du, _conn),
    do: %{
      email: du.email
    }
end
