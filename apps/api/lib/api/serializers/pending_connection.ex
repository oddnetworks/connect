defmodule Api.Serializers.PendingConnection do
  use Api.Serializers.Base

  def type(_, _) do
    "connection"
  end

  def attributes(pc, _),
    do: %{
      email: pc.email,
      organization_urn: pc.organization.urn,
      device_identifier: pc.device_identifier,
      platform: pc.platform,
      expires_at: format_date(pc.expires_at)
    }
end
