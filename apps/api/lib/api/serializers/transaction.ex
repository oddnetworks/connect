defmodule Api.Serializers.Transaction do
  use Api.Serializers.Base

  def relationships(t, _conn) do
    %{
      device_user: %HasOne{
        serializer: Api.Serializers.DeviceUser,
        include: false,
        data: t.device_user
      },
      entitlement: %HasOne{
        serializer: Api.Serializers.Entitlement,
        include: false,
        data: t.entitlement
      }
    }
  end

  def attributes(t, _conn),
    do: %{
      external_identifier: t.external_identifier,
      platform: t.platform,
      invalidated_at: format_date(t.invalidated_at),
      invalidation_reason: t.invalidation_reason,
      receipt: t.receipt,
      email: t.device_user.email,
      entitlement_identifier: t.entitlement.entitlement_identifier,
      product_identifier: t.entitlement.product_identifier,
      last_validated_at: format_date(t.last_validated_at),
      inserted_at: format_date(t.inserted_at),
      updated_at: format_date(t.updated_at)
    }
end
