defmodule Api.Serializers.VerifiedConnection do
  use Api.Serializers.Base

  def type(_, _) do
    "connection"
  end

  def attributes(vc, _) do
    %{
      email: vc.device_user.email,
      jwt: vc.jwt,
      organization_urn: vc.organization.urn,
      device_identifier: vc.device.external_identifier,
      platform: vc.device.platform,
      verified_at: format_date(vc.verified_at)
    }
  end
end
