defmodule Api.Serializers.DeviceUser.Entitlement do
  use Api.Serializers.Base

  def type(e, _conn), do: e.type

  def attributes(e, _conn),
    do: %{
      name: e.name,
      description: e.description,
      entitlement_identifier: e.entitlement_identifier,
      product_identifier: e.product_identifier,
      transaction_identifier: e.transaction_identifier,
      receipt: e.receipt,
      platform: e.platform,
      assigned_at: format_date(e.assigned_at)
    }
end
