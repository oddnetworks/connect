defmodule Api.Web do
  def controller do
    quote do
      use Phoenix.Controller, log: false
      use Guardian.Phoenix.Controller

      alias Guardian.Plug.{EnsureAuthenticated, EnsurePermissions}
      alias Api.Plug.EnsureResourceType

      alias Api.ErrorView

      import Api.Router.Helpers
      import Api.Controller.Helpers

      def unauthenticated(_conn, _params) do
        raise Api.Errors.Unauthenticated
      end

      def unauthorized(_conn, _params) do
        raise Api.Errors.Forbidden
      end

      def no_resource(_conn, _params) do
        raise Api.Errors.NotFound
      end
    end
  end

  def router do
    quote do
      use Phoenix.Router
    end
  end

  def view do
    quote do
      import Api.Router.Helpers
      import Api.ErrorHelpers
      import Api.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
