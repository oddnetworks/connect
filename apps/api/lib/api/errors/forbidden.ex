defmodule Api.Errors.Forbidden do
  defexception message: "Forbidden",
               detail: "Invalid permissions",
               plug_status: 403
end
