defmodule Api.Errors.Unauthenticated do
  defexception message: "Unauthorized",
               detail: "Invalid authorization",
               plug_status: 401
end
