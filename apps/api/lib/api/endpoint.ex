defmodule Api.Endpoint do
  use Phoenix.Endpoint, otp_app: :api

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug(Phoenix.CodeReloader)
  end

  plug(Plug.RequestId)

  plug(
    Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison
  )

  plug(Plug.MethodOverride)
  plug(Plug.Head)

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug(
    Plug.Session,
    store: :cookie,
    key: "_connect_api_key",
    signing_salt: "M4xJzM5s"
  )

  # Add Timber plugs for capturing HTTP context and events
  plug(Timber.Integrations.SessionContextPlug)
  plug(Timber.Integrations.HTTPContextPlug)
  plug(Timber.Integrations.EventPlug)

  plug(Api.Router)
end
