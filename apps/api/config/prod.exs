use Mix.Config

config :api, Api.Endpoint,
  http: [port: 4002],
  url: [scheme: "https", host: "${HOST}", port: 4002],
  secret_key_base: "${SECRET_KEY_BASE}",
  server: true,
  root: "."
