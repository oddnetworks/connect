use Mix.Config

config :api, Api.Endpoint,
  url: [host: "example.com"],
  http: [port: 4009],
  server: false

config :logger,
  backends: [:console],
  utc_log: true,
  log_level: :warn

config :logger, :console,
  format: "[$level] $message\n",
  level: :warn

config :api, :webhook_module, Webhooks.TestTrigger
