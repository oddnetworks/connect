use Mix.Config

config :api, Api.Endpoint,
  instrumenters: [Timber.Integrations.PhoenixInstrumenter],
  url: [host: System.get_env("HOST") || "localhost"],
  secret_key_base: "wY3t134BUoBZ5yrsxUDofkCdmqPwPdU+n7I5vJRlQF1sU4gJW9WlJ9EIQ8FFGVJy",
  render_errors: [view: Api.ErrorView, accepts: ~w(json json-api)]

config :plug, :types, %{
  "application/vnd.api+json" => ["json-api"]
}

config :api, ecto_repos: []

config :phoenix, :format_encoders, "json-api": Poison

config :ja_serializer, key_format: :underscored

import_config "#{Mix.env()}.exs"
