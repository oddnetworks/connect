use Mix.Config

config :api, Api.Endpoint,
  http: [port: 4001],
  url: [scheme: "http", host: System.get_env("HOST") || "oddconnect.localhost"],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []
