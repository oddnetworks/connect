defmodule FrontEnd.Mixfile do
  use Mix.Project

  def project do
    [
      app: :front_end,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      docs: [main: "readme", extras: ["README.md"]],
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [extra_applications: [:logger, :timber], mod: {FrontEnd.Application, []}]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps() do
    [
      {:phoenix, "~> 1.2.1"},
      {:phoenix_ecto, "~> 3.0"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_html, "~> 2.6"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:poison, "~> 3.1.0", override: true},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
      {:scrivener_html, "~> 1.7"},
      {:timber, "~> 2.6"},
      {:timex, "~> 3.1"},
      {:auth, in_umbrella: true},
      {:connect, in_umbrella: true},
      {:transactions, in_umbrella: true}
    ]
  end

  defp aliases() do
    []
  end
end
