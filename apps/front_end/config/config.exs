use Mix.Config

config :front_end, FrontEnd.Endpoint,
  instrumenters: [Timber.Integrations.PhoenixInstrumenter],
  url: [host: System.get_env("HOST") || "localhost"],
  secret_key_base: "gD8r7e5+uCDzm5LxnJp6kmvwZnDy47YR+fOtHggnbEtOJimByIADdQ8mMaS5S/QS",
  render_errors: [view: FrontEnd.ErrorView, accepts: ~w(html json json-api)],
  pubsub: [name: FrontEnd.PubSub, adapter: Phoenix.PubSub.PG2]

config :front_end, ecto_repos: []

config :scrivener_html,
  routes_helper: FrontEnd.Router.Helpers,
  # If you use a single view style everywhere, you can configure it here. See View Styles below for more info.
  view_style: :bootstrap

import_config "#{Mix.env()}.exs"
