use Mix.Config

config :front_end, FrontEnd.Endpoint,
  http: [port: 4002],
  url: [scheme: "http", host: System.get_env("HOST") || "oddconnect.localhost"],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    node: [
      "node_modules/.bin/webpack",
      "--watch-stdin",
      "--color",
      cd: Path.expand("../assets", __DIR__)
    ]
  ],
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/web/views/.*(ex)$},
      ~r{lib/web/templates/.*(eex)$}
    ]
  ]

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# config :logger,
#   backends: [:console],
#   utc_log: true,
#   log_level: :debug

# config :logger, :console,
#   format: "[$level] $message\n",
#   level: :debug
