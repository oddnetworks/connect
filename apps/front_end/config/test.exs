use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :front_end, FrontEnd.Endpoint,
  url: [host: "example.com"],
  http: [port: 4008],
  server: false

config :logger,
  backends: [:console],
  utc_log: true,
  log_level: :warn

config :logger, :console,
  format: "[$level] $message\n",
  level: :warn

config :front_end, :webhook_module, Webhooks.TestTrigger
