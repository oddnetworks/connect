use Mix.Config

config :front_end, FrontEnd.Endpoint,
  http: [port: 4001],
  url: [scheme: "https", host: "${HOST}", port: 4001],
  # force_ssl: [rewrite_on: [:x_forwarded_proto]],
  # force_ssl: [hsts: true, host: nil],
  cache_static_manifest: "priv/static/manifest.json",
  secret_key_base: "${SECRET_KEY_BASE}",
  server: true,
  root: "."
