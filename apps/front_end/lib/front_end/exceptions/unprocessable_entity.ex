defmodule FrontEnd.UnprocessableEntity do
  defexception message: "Unprocessable Entity",
               detail: "Entity data not valid",
               plug_status: 422
end
