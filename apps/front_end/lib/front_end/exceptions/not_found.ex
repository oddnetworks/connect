defmodule FrontEnd.NotFound do
  defexception message: "Not Found",
               detail: "Resource not found",
               plug_status: 404
end
