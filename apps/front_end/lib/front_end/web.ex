defmodule FrontEnd.Web do
  @moduledoc """
  A module that keeps using definitions for controllers,
  views and so on.

  This can be used in your application as:

      use FrontEnd.Web, :controller
      use FrontEnd.Web, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below.
  """

  def model do
    quote do
      # Define common model functionality
    end
  end

  def controller do
    quote do
      use Phoenix.Controller, log: false, namespace: FrontEnd

      import FrontEnd.Controller.Helpers
      import FrontEnd.Router.Helpers
      import FrontEnd.Gettext

      def already_authenticated(conn, _params) do
        conn
        |> put_flash(:info, "Already logged in")
        |> redirect(to: dashboard_path(conn, :index))
      end
    end
  end

  def verification_controller do
    quote do
      use Phoenix.Controller, log: false, namespace: FrontEnd.Verification

      alias FrontEnd.ErrorView

      import FrontEnd.Controller.Helpers
      import FrontEnd.Router.Helpers
      import FrontEnd.Gettext
    end
  end

  def authenticated_browser_controller do
    quote do
      use Phoenix.Controller, log: false, namespace: FrontEnd.App
      use Guardian.Phoenix.Controller

      alias Guardian.Plug.{EnsureAuthenticated, EnsurePermissions}
      alias FrontEnd.Plug.EnsureResourceType

      import FrontEnd.Controller.Helpers
      import FrontEnd.Router.Helpers
      import FrontEnd.Gettext

      def unauthenticated(conn, _params) do
        conn
        |> Guardian.Plug.sign_out()
        |> put_flash(:error, "Please log in")
        |> redirect(to: session_path(conn, :new))
      end

      def unauthorized(conn, _params) do
        conn
        |> put_flash(:error, "Invalid Permissions")
        |> redirect(to: page_path(conn, :index))
      end

      def no_resource(conn, _params) do
        conn
        |> put_flash(:error, "Invalid Permissions")
        |> redirect(to: page_path(conn, :index))
      end
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/web/templates",
        namespace: FrontEnd

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_csrf_token: 0, get_flash: 2, view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import FrontEnd.Router.Helpers
      import FrontEnd.ErrorHelpers
      import FrontEnd.Gettext
      import FrontEnd.InputHelpers
      import FrontEnd.PaginationHelpers
      import FrontEnd.FormatHelpers

      @doc """
      Generates name for the JavaScript view we want to use
      in this combination of view/template
      """
      def js_view_name(conn, view_template) do
        view =
          view_name(conn)
          |> String.split("_")
          |> Enum.map(&String.capitalize/1)
          |> Enum.reverse()

        [template_name(view_template) | view]
        |> List.insert_at(0, "view")
        |> Enum.map(&String.capitalize/1)
        |> Enum.reverse()
        |> Enum.join("")
      end

      # Takes the resource name of the view module and removes the
      # the ending *_view* string.
      defp view_name(conn) do
        conn
        |> view_module
        |> Phoenix.Naming.resource_name()
        |> String.replace("_view", "")
      end

      # Removes the extion from the template and reutrns
      # just the name.
      defp template_name(template) when is_binary(template) do
        template
        |> String.split(".")
        |> Enum.at(0)
      end
    end
  end

  def router do
    quote do
      use Phoenix.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import FrontEnd.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
