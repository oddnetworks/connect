defmodule FrontEnd.Plug.CurrentAccount do
  import Plug.Conn
  import Guardian.Plug

  def init(opts), do: opts

  def call(conn, _opts) do
    resource =
      conn
      |> current_resource()

    conn
    |> assign(:current_account, current_account(resource))
    |> assign(:current_organization, current_organization(resource))
  end

  defp current_account(%{account: account}), do: account
  defp current_account(account = %{id: _}), do: account
  defp current_account(_), do: nil

  defp current_organization(%{organization: organization}), do: organization
  defp current_organization(_), do: nil
end
