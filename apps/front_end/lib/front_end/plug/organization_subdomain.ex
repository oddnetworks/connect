defmodule FrontEnd.Plug.OrganizationSubdomain do
  import Plug.Conn

  def init(default), do: default

  def call(conn, router) do
    case get_organization(conn) do
      :www ->
        conn

      :not_found ->
        raise FrontEnd.NotFound

      organization ->
        conn
        |> assign(:organization, organization)
        |> put_private(:subdomain, organization.urn)
        |> router.call(router.init({}))
        |> halt
    end
  end

  defp get_organization(conn) do
    subdomain = get_subdomain(conn.host)

    cond do
      subdomain == "www" ->
        :www

      byte_size(subdomain) > 0 ->
        case Connect.find_organization(urn: subdomain) do
          nil ->
            :not_found

          organization ->
            organization
        end

      true ->
        :www
    end
  end

  defp get_subdomain(host) do
    root_host = FrontEnd.Endpoint.config(:url)[:host]

    host
    |> String.replace(~r/.?#{root_host}/, "")
  end
end
