defmodule FrontEnd do
  @moduledoc """
  The brain of front_end.

  Phoenix 1.3 calls these business logic modules "Context" modules. Each context
  module gets a folder in lib/ to put its related modules in. (In this case, the
  lib/front_end directory)

  Since this app is so simple we only need one context module, with the same
  name as the app: `FrontEnd`.
  """
end
