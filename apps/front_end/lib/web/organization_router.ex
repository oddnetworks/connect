defmodule FrontEnd.OrganizationRouter do
  use FrontEnd.Web, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  scope "/verify", FrontEnd.Verification do
    pipe_through(:browser)

    resources("/", VerificationController, only: [:show])
  end
end
