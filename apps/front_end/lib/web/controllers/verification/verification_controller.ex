defmodule FrontEnd.Verification.VerificationController do
  use FrontEnd.Web, :verification_controller

  def show(conn, %{"id" => access_token}) do
    case Connections.verify(access_token) do
      {:ok, _verified_connection} ->
        conn
        |> render("show.html")

      {:error, prefix, reason, _changes_so_far} ->
        conn
        |> put_flash(:error, service_errors_to_string(reason, prefix))
        |> render("unverified.html")

      {:error, :not_found} ->
        raise FrontEnd.NotFound

      {:error, :verified} ->
        conn
        |> put_flash(:info, "Connection Previously Verified")
        |> render("show.html")

      {:error, :expired} ->
        conn
        |> put_flash(:error, "Connection Verification Expired")
        |> render("unverified.html")

      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> render("unverified.html")
    end
  end
end
