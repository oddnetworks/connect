defmodule FrontEnd.App.AccountController do
  use FrontEnd.Web, :authenticated_browser_controller

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)

  alias Auth.Accounts

  def edit(conn, _params, %{account: account}, _claims) do
    changeset = Accounts.edit_changeset(account)
    password_changeset = Accounts.password_changeset(account)

    render(
      conn,
      "edit.html",
      changeset: changeset,
      action: account_path(conn, :update),
      password_changeset: password_changeset,
      password_action: account_path(conn, :password),
      timezones: Timex.timezones()
    )
  end

  def update(
        conn,
        _params = %{"auth_account" => params},
        %{account: account},
        _claims
      ) do
    case Accounts.update(account, params) do
      {:ok, _account} ->
        conn
        |> put_flash(:success, "Account updated")
        |> redirect(to: account_path(conn, :edit))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "There was a problem updating your account")
        |> render(
          "edit.html",
          changeset: changeset,
          action: account_path(conn, :update),
          password_changeset: Accounts.password_changeset(account),
          password_action: account_path(conn, :password),
          timezones: Timex.timezones()
        )
    end
  end

  def password(
        conn,
        _params = %{"auth_account" => params},
        %{account: account},
        _claims
      ) do
    case Accounts.update_password(account, params) do
      {:ok, _account} ->
        conn
        |> put_flash(:success, "Password updated")
        |> redirect(to: account_path(conn, :edit))

      {:error, password_changeset} ->
        conn
        |> put_flash(:error, "There was a problem updating your password")
        |> render(
          "edit.html",
          changeset: Accounts.edit_changeset(account),
          action: account_path(conn, :update),
          password_changeset: password_changeset,
          password_action: account_path(conn, :password),
          timezones: Timex.timezones()
        )
    end
  end
end
