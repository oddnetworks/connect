defmodule FrontEnd.App.DeviceUserController do
  use FrontEnd.Web, :authenticated_browser_controller

  use Connect.Model

  # Make sure that we have a valid token
  # We've aliased Guardian.Plug.EnsureAuthenticated in our
  # FrontEnd.Web.authenticated_browser_controller macro
  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)
  # Make sure that the token's permissions are valid for the given actions
  # We may not want to use this if we're relying on user.roles, as
  # they can change while the token's permissions will not change
  # plug EnsurePermissions, [handler: __MODULE__, roles: ~w(admin)] when action in [:index, :show, :delete]

  def index(conn, _params, %{organization: organization}, _claims) do
    device_users = Connect.DeviceUsers.list(organization.id)

    conn
    |> render("index.html", organization: organization, device_users: device_users)
  end

  def show(conn, %{"email" => email}, %{organization: organization}, _claims) do
    case Connect.DeviceUsers.find(organization.id, email) do
      nil ->
        raise FrontEnd.NotFound

      device_user ->
        transactions = Transactions.list_device_user_transactions(organization.id, email)
        connections = Connect.Connections.list_device_user_connections(organization.id, email)

        conn
        |> render(
          "show.html",
          device_user: device_user,
          organization: organization,
          transactions: transactions,
          connections: connections
        )
    end
  end
end
