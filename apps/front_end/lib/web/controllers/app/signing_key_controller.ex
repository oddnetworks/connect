defmodule FrontEnd.App.SigningKeyController do
  use FrontEnd.Web, :authenticated_browser_controller

  # Make sure that we have a valid token
  # We've aliased Guardian.Plug.EnsureAuthenticated in our
  # FrontEnd.Web.authenticated_browser_controller macro
  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)
  # Make sure that the token's permissions are valid for the given actions
  # We may not want to use this if we're relying on user.roles, as
  # they can change while the token's permissions will not change
  # plug EnsurePermissions, [handler: __MODULE__, roles: ~w(admin)] when action in [:index, :show, :delete]

  def index(conn, _params, _current_user, _claims) do
    conn
    |> put_resp_content_type("text/plain")
    |> put_resp_header("content-disposition", "attachment; filename=\"odd_connect.pub\"")
    |> send_resp(200, Auth.ConnectionToken.public_key())
  end
end
