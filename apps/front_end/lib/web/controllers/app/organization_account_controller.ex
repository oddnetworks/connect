defmodule FrontEnd.App.OrganizationAccountController do
  use FrontEnd.Web, :authenticated_browser_controller

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.Account, handler: __MODULE__)

  def index(conn, _params, %{is_admin: true}, _claims) do
    organizations = Connect.list_organizations()

    conn
    |> render("index.html", organizations: organizations)
  end

  def index(conn, _params, account = %{id: account_id}, _claims) do
    Connect.list_organizations_for_account(account_id)
    |> display_organizations_for_customer(conn, account)
  end

  def select(conn, %{"urn" => organization_urn}, account, _claims) do
    organization_urn
    |> Auth.authenticate_organization_account(account)
    |> complete_sign_in(conn)
  end

  defp display_organizations_for_customer(orgs, conn, _account) when length(orgs) > 1 do
    conn
    |> render("index.html", organizations: orgs)
  end

  defp display_organizations_for_customer(orgs, conn, _account) when length(orgs) < 1 do
    conn
    |> Guardian.Plug.sign_out()
    |> put_flash(:error, "Invalid Permissions")
    |> redirect(to: page_path(conn, :index))
  end

  defp display_organizations_for_customer(orgs, conn, account) do
    orgs
    |> List.first()
    |> Auth.authenticate_organization_account(account)
    |> complete_sign_in(conn)
  end

  defp complete_sign_in({:ok, organization_account = %{account: %{name: name}}}, conn) do
    conn
    |> Guardian.Plug.sign_in(organization_account)
    |> put_flash(:success, "Welcome, #{name}")
    |> redirect(to: dashboard_path(conn, :index))
  end

  defp complete_sign_in({:error, _reason}, conn) do
    conn
    |> Guardian.Plug.sign_out()
    |> put_flash(:error, "Invalid Permissions")
    |> redirect(to: page_path(conn, :index))
  end
end
