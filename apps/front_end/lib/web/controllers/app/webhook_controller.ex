defmodule FrontEnd.App.WebhookController do
  use FrontEnd.Web, :authenticated_browser_controller

  # Make sure that we have a valid token
  # We've aliased Guardian.Plug.EnsureAuthenticated in our
  # FrontEnd.Web.authenticated_browser_controller macro
  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)
  # Make sure that the token's permissions are valid for the given actions
  # We may not want to use this if we're relying on user.roles, as
  # they can change while the token's permissions will not change
  # plug EnsurePermissions, [handler: __MODULE__, roles: ~w(admin)] when action in [:index, :show, :delete]

  @valid_types Connect.valid_webhook_types()

  def index(conn, params, %{organization: organization}, _claims) do
    webhooks = Connect.list_webhooks(organization, params)

    conn
    |> render("index.html", webhooks: webhooks)
  end

  def new(conn, _params, current_customer, _claims) do
    organization = current_customer.organization
    changeset = Connect.build_webhook(%{organization: organization})

    conn
    |> render(
      "new.html",
      webhook: changeset,
      valid_types: @valid_types,
      organization: organization,
      action: webhook_path(conn, :create),
      method: "post"
    )
  end

  def edit(conn, %{"id" => id}, current_customer, _claims) do
    organization = current_customer.organization
    webhook = check_webhook_belongs_to_organization(id, organization)

    changeset = Connect.build_webhook(webhook)

    conn
    |> render(
      "edit.html",
      webhook: changeset,
      valid_types: @valid_types,
      organization: organization,
      action: webhook_path(conn, :update, id),
      method: "put"
    )
  end

  def create(conn, %{"webhook" => webhook_params}, current_customer, _claims) do
    organization = current_customer.organization
    webhook_params = for {key, val} <- webhook_params, into: %{}, do: {String.to_atom(key), val}
    webhook_params = Map.put(webhook_params, :organization, organization)

    case Connect.create_webhook(%{webhook_params | organization: organization}) do
      {:ok, _webhook} ->
        conn
        |> put_flash(:success, "Successfully created webhook")
        |> redirect(to: webhook_path(conn, :index))

      {:error, changeset} ->
        conn
        |> render(
          "new.html",
          webhook: changeset,
          valid_types: @valid_types,
          organization: organization,
          action: webhook_path(conn, :create),
          method: "post"
        )
    end
  end

  def update(conn, %{"id" => id, "webhook" => webhook_params}, current_customer, _claims) do
    organization = current_customer.organization
    check_webhook_belongs_to_organization(id, organization)

    case Connect.update_webhook(id, webhook_params) do
      {:ok, _webhook} ->
        conn
        |> put_flash(:success, "Successfully updated webhook")
        |> redirect(to: webhook_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Some errors were found.")
        |> render(
          "edit.html",
          webhook: changeset,
          valid_types: @valid_types,
          organization: organization,
          action: webhook_path(conn, :update, id),
          method: "put"
        )
    end
  end

  def delete(conn, %{"id" => id}, current_customer, _claims) do
    organization = current_customer.organization
    check_webhook_belongs_to_organization(id, organization)

    case Connect.delete_webhook(id) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Successfully removed webhook from #{organization.name}")
        |> redirect(to: webhook_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, FrontEnd.ErrorHelpers.error_string_from_changeset(changeset))
        |> render(
          "edit.html",
          webhook: changeset,
          valid_types: @valid_types,
          organization: organization,
          action: webhook_path(conn, :update, id),
          method: "put"
        )
    end
  end

  defp check_webhook_belongs_to_organization(id, organization) do
    case Connect.find_webhook(id: id, organization_id: organization.id) do
      nil ->
        raise FrontEnd.NotFound

      webhook ->
        webhook
    end
  end
end
