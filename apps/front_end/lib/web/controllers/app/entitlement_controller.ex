defmodule FrontEnd.App.EntitlementController do
  use FrontEnd.Web, :authenticated_browser_controller

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)

  alias Connect.Entitlements

  @webhook_module Application.get_env(:front_end, :webhook_module) || Webhooks
  @valid_entitlement_types Entitlements.valid_types()
  @valid_entitlement_platforms Entitlements.valid_entitlement_platforms()

  def index(conn, params, %{organization: organization}, _claims) do
    entitlements = Entitlements.list_paged(organization.id, params)

    conn
    |> render("index.html", entitlements: entitlements)
  end

  def new(conn, _params, %{organization: %{id: organization_id} = organization}, _claims) do
    changeset =
      Entitlements.build_changeset(%Connect.Entitlement{}, %{organization_id: organization_id})

    conn
    |> render(
      "new.html",
      types: @valid_entitlement_types,
      platforms: @valid_entitlement_platforms,
      entitlement: changeset,
      organization: organization,
      action: entitlement_path(conn, :create),
      method: "post"
    )
  end

  def edit(conn, %{"id" => id}, oa, _claims) do
    organization = oa.organization
    entitlement = check_entitlement_belongs_to_organization(id, organization)

    changeset = Entitlements.build_changeset(entitlement)

    conn
    |> render(
      "edit.html",
      types: @valid_entitlement_types,
      platforms: @valid_entitlement_platforms,
      entitlement: entitlement,
      changeset: changeset,
      organization: organization,
      action: entitlement_path(conn, :update, id),
      method: "put"
    )
  end

  def create(conn, %{"entitlement" => entitlement_params}, %{organization: organization}, _claims) do
    entitlement_params =
      for {key, val} <- entitlement_params, into: %{}, do: {String.to_atom(key), val}

    entitlement_params = Map.put(entitlement_params, :organization_id, organization.id)

    case Entitlements.create(entitlement_params) do
      {:ok, %{id: eid, organization_id: oid}} ->
        @webhook_module.trigger(:entitlement, {eid, oid})

        conn
        |> put_flash(:success, "Successfully created entitlement")
        |> redirect(to: entitlement_path(conn, :index))

      {:error, changeset} ->
        conn
        |> render(
          "new.html",
          types: @valid_entitlement_types,
          platforms: @valid_entitlement_platforms,
          entitlement: changeset,
          organization: organization,
          action: entitlement_path(conn, :create),
          method: "post"
        )
    end
  end

  def update(
        conn,
        %{"id" => id, "entitlement" => entitlement_params},
        %{organization: organization},
        _claims
      ) do
    entitlement = check_entitlement_belongs_to_organization(id, organization)

    case Entitlements.update(id, entitlement_params) do
      {:ok, %{id: eid, organization_id: oid}} ->
        @webhook_module.trigger(:entitlement, {eid, oid})

        conn
        |> put_flash(:success, "Successfully updated entitlement")
        |> redirect(to: entitlement_path(conn, :index))

      {:error, changeset} ->
        conn
        |> render(
          "edit.html",
          types: @valid_entitlement_types,
          platforms: @valid_entitlement_platforms,
          changeset: changeset,
          entitlement: entitlement,
          organization: organization,
          action: entitlement_path(conn, :update, id),
          method: "put"
        )
    end
  end

  def delete(conn, %{"id" => id}, %{organization: organization}, _claims) do
    entitlement = check_entitlement_belongs_to_organization(id, organization)

    case Entitlements.delete(id) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Successfully removed entitlement from #{organization.name}")
        |> redirect(to: entitlement_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, FrontEnd.ErrorHelpers.error_string_from_changeset(changeset))
        |> render(
          "edit.html",
          types: @valid_entitlement_types,
          platforms: @valid_entitlement_platforms,
          changeset: changeset,
          entitlement: entitlement,
          organization: organization,
          action: entitlement_path(conn, :update, id),
          method: "put"
        )
    end
  end

  defp check_entitlement_belongs_to_organization(id, organization) do
    case Entitlements.find([id: id, organization_id: organization.id], [:organization]) do
      nil ->
        raise FrontEnd.NotFound

      entitlement ->
        entitlement
    end
  end
end
