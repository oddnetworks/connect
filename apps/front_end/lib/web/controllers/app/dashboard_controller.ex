defmodule FrontEnd.App.DashboardController do
  use FrontEnd.Web, :authenticated_browser_controller

  # Make sure that we have a valid token
  # We've aliased Guardian.Plug.EnsureAuthenticated in our
  # FrontEnd.Web.authenticated_browser_controller macro
  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)
  # Make sure that the token's permissions are valid for the given actions
  # We may not want to use this if we're relying on user.roles, as
  # they can change while the token's permissions will not change
  # plug EnsurePermissions, [handler: __MODULE__, roles: ~w(admin)] when action in [:index, :show, :delete]

  def index(conn, _params, _current_account = %{organization: %{id: organization_id}}, _claims) do
    transaction_stats = Connect.Stats.transaction_stats(organization_id)
    connection_stats = Connect.Stats.connection_stats(organization_id)

    render(
      conn,
      "index.html",
      transaction_stats: transaction_stats,
      connection_stats: connection_stats
    )
  end
end
