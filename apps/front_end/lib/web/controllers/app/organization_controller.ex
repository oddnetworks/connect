defmodule FrontEnd.App.OrganizationController do
  use FrontEnd.Web, :authenticated_browser_controller

  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)

  def index(conn, _params, %{account: %{is_admin: true}}, _claims) do
    organizations = Connect.list_organizations()

    conn
    |> render("index.html", organizations: organizations)
  end
  def index(conn, _params, %{account: account}, _claims) do
    organizations = Connect.list_organizations_for_account(account)

    # TODO - when organizations length == 1 just switch to that org
    conn
    |> render("index.html", organizations: organizations)
  end

  def switch(
        conn,
        %{"id" => id},
        %{account: %{id: account_id, is_admin: true}},
        _claims
      ) do
    Auth.find_organization_account(account_id, id, nil)
    |> complete_switch(conn)
  end

  def edit(conn, %{"urn" => urn}, current_account, _claims) do
    organization =
      current_account
      |> check_organization_access(urn)
      |> load_organization(urn)

    conn
    |> render(
      "edit.html",
      action: organization_path(conn, :update, urn),
      organization: Connect.edit_organization(organization),
      webhooks: organization.webhooks,
      entitlements: organization.entitlements
    )
  end

  def update(
        conn,
        %{"urn" => urn, "organization" => organization_params},
        current_account,
        _claims
      ) do
    organization =
      current_account
      |> check_organization_access(urn)
      |> load_organization(urn)

    case Connect.update_organization(urn, organization_params) do
      {:ok, organization} ->
        conn
        |> put_flash(:success, "Successfully updated #{organization.name}")
        |> redirect(to: organization_path(conn, :edit, organization.urn))

      {:error, changeset} ->
        changeset = %{changeset | action: :organization}

        conn
        |> render(
          "edit.html",
          action: organization_path(conn, :update, urn),
          organization: changeset,
          webhooks: organization.webhooks,
          entitlements: organization.entitlements
        )
    end
  end

  defp check_organization_access(%{permissions: :admin}, _id), do: true
  defp check_organization_access(%{organization: %{urn: urn}}, urn), do: true
  defp check_organization_access(_, _), do: false

  defp load_organization(_has_access = true, urn) do
    case Connect.find_organization(urn: urn) do
      nil -> raise FrontEnd.NotFound
      org -> org
    end
  end

  defp load_organization(_does_not_have_access, _), do: raise(FrontEnd.NotFound)

  defp complete_switch({:ok, organization_account}, conn) do
    conn
    |> Guardian.Plug.sign_in(organization_account)
    |> put_flash(:success, "You have successfully switched accounts")
    |> redirect(to: dashboard_path(conn, :index))
  end

  defp complete_switch(_unsuccessful, conn) do
    conn
    |> put_flash(:info, "Unable to switch accounts")
    |> redirect(to: dashboard_path(conn, :index))
  end
end
