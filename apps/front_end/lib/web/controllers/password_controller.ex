defmodule FrontEnd.PasswordController do
  use FrontEnd.Web, :controller
  alias Auth.PasswordReset

  plug(
    Guardian.Plug.EnsureNotAuthenticated,
    [handler: __MODULE__] when action in [:new, :create, :edit, :update]
  )

  def new(conn, _params) do
    changeset = PasswordReset.reset_changeset()
    render(conn, "new.html", changeset: changeset, action: password_path(conn, :create))
  end

  def create(conn, %{"auth_account" => params}) do
    Auth.initiate_password_reset(params)

    conn
    |> put_flash(
      :info,
      "If your account exists, password reset instructions have been sent to your email address."
    )
    |> redirect(to: session_path(conn, :new))
  end

  def edit(conn, %{"token" => token}) do
    case Auth.edit_password(token) do
      {:ok, changeset} ->
        conn
        |> render("edit.html", changeset: changeset, token: token)

      {:error, :invalid} ->
        conn
        |> put_flash(:error, "Invalid reset token")
        |> redirect(to: password_path(conn, :new))

      {:error, :expired} ->
        conn
        |> put_flash(:error, "Password reset token expired")
        |> redirect(to: password_path(conn, :new))
    end
  end

  def update(conn, %{"token" => token, "auth_account" => params}) do
    case Auth.reset_password(token, params) do
      {:ok, _auth} ->
        conn
        |> put_flash(:info, "Password reset successfully!")
        |> redirect(to: session_path(conn, :new))

      {:error, :expired} ->
        conn
        |> put_flash(:error, "Password reset token expired")
        |> redirect(to: password_path(conn, :new))

      {:error, :invalid} ->
        conn
        |> put_flash(:error, "Invalid reset token")
        |> redirect(to: password_path(conn, :new))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "There was a problem resetting your password")
        |> render("edit.html", changeset: changeset, token: token)
    end
  end
end
