defmodule FrontEnd.Controller.Helpers do
  import Plug.Conn

  def redirect_back(conn, alternative \\ "/") do
    path =
      conn
      |> get_req_header("referer")
      |> referrer

    path || alternative
  end

  def service_errors_to_string(changeset) when is_binary(changeset), do: changeset

  def service_errors_to_string(changeset) do
    changeset.errors
    |> Enum.map(&changeset_error_to_string/1)
    |> Enum.join(", ")
  end

  def service_errors_to_string(changeset, prefix) do
    String.capitalize(humanize(prefix)) <> ": " <> service_errors_to_string(changeset)
  end

  defp changeset_error_to_string({key, {message, values}}) do
    humanize(key) <>
      " " <>
      Enum.reduce(values, message, fn {k, v}, acc ->
        String.replace(acc, "%{#{k}}", to_string(v))
      end)
  end

  defp changeset_error_to_string({_key, message}) do
    message
  end

  defp humanize(atom) when is_atom(atom), do: humanize(Atom.to_string(atom))

  defp humanize(bin) when is_binary(bin) do
    bin =
      if String.ends_with?(bin, "_id") do
        binary_part(bin, 0, byte_size(bin) - 3)
      else
        bin
      end

    bin |> String.replace("_", " ")
  end

  defp referrer([]), do: nil
  defp referrer([h | _]), do: h
end
