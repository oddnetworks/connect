defmodule FrontEnd.SessionController do
  use FrontEnd.Web, :controller

  plug(Guardian.Plug.EnsureNotAuthenticated, [handler: __MODULE__] when action in [:new, :create])

  def new(conn, _params) do
    render(conn, "new.html", form: %{errors: nil})
  end

  def create(conn, %{
        "session" => %{"email" => email, "password" => password}
      }) do
    Auth.authenticate_account(email, password)
    |> complete_sign_in(conn)
  end

  def delete(conn, _) do
    conn
    |> Guardian.Plug.sign_out()
    |> put_flash(:info, "You have successfully logged out")
    |> redirect(to: page_path(conn, :index))
  end

  defp complete_sign_in({:ok, account}, conn) do
    conn
    |> Guardian.Plug.sign_in(account)
    |> put_flash(:success, "Welcome Back!")
    |> redirect(to: organization_account_path(conn, :index))
  end

  defp complete_sign_in({:error, _reason}, conn) do
    conn
    |> put_flash(:error, "Invalid email/password combination")
    |> redirect(to: session_path(conn, :new))
  end
end
