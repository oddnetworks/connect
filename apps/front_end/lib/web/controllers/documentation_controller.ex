defmodule FrontEnd.DocumentationController do
  use FrontEnd.Web, :authenticated_browser_controller

  # Make sure that we have a valid token
  # We've aliased Guardian.Plug.EnsureAuthenticated in our
  # FrontEnd.Web.authenticated_browser_controller macro
  plug(EnsureAuthenticated, handler: __MODULE__)
  plug(EnsureResourceType, type: Auth.OrganizationAccount, handler: __MODULE__)
  # Make sure that the token's permissions are valid for the given actions
  # We may not want to use this if we're relying on user.roles, as
  # they can change while the token's permissions will not change
  # plug EnsurePermissions, [handler: __MODULE__, roles: ~w(admin)] when action in [:index, :show, :delete]

  plug(:put_layout, false)

  def index(conn, _params, _current_customer, _claims) do
    conn
    |> render("index.html")
  end
end
