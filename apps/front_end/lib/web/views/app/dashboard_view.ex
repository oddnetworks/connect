defmodule FrontEnd.App.DashboardView do
  use FrontEnd.Web, :view

  def encode_stats(stats, key) do
    stats
    |> Enum.reduce(%{}, fn stat, acc ->
      platform =
        stat.platform
        |> String.split("_")
        |> Enum.map(&String.capitalize/1)
        |> Enum.join(" ")

      acc
      |> Map.put(platform, stat[key])
    end)
    |> Poison.encode!()
    |> raw()
  end
end
