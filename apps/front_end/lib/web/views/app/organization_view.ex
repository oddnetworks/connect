defmodule FrontEnd.App.OrganizationView do
  use FrontEnd.Web, :view

  alias Connect.Organization

  def device_connection_email_template_fields do
    Organization.Email.device_connection_email_fields()
    |> Enum.map(fn field ->
      "<code>{{#{field}}}</code>"
    end)
    |> Enum.join(", ")
  end
end
