defmodule FrontEnd.App.TransactionView do
  use FrontEnd.Web, :view
  import Scrivener.HTML

  def valid_transaction?(%{invalidated_at: invalidated_at})
      when is_nil(invalidated_at) do
    "<span class=\"label label-success\">Valid</span>"
    |> raw()
  end

  def valid_transaction?(%{invalidation_reason: reason}) do
    "<span class=\"label label-danger\">#{reason}</span>"
    |> raw()
  end
end
