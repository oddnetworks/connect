defmodule FrontEnd.FormatHelpers do
  use Timex
  import Phoenix.HTML, only: [raw: 1]

  def display_name(%{account: account}), do: display_name(account)

  def display_name(account = %{id: _id}) do
    "#{account.name} (#{account.email})"
    |> raw()
  end

  def sort_by_date(items, field) do
    items
    |> Enum.sort(fn a, b ->
      NaiveDateTime.compare(Map.get(a, field) |> to_naive(), Map.get(b, field) |> to_naive()) ==
        :gt
    end)
  end

  def format_date_in_timezone(date, _timezone) when is_nil(date), do: ""

  def format_date_in_timezone(date, timezone) do
    Timezone.convert(date, timezone)
    |> Timex.format!("%m/%d/%Y %l:%M %P", :strftime)
    |> raw()
  end

  def transaction_status_class(invalidated_at) when is_nil(invalidated_at), do: "label-success"
  def transaction_status_class(_invalidated_at), do: "label-danger"

  def transaction_status_text(invalidation_reason) when is_nil(invalidation_reason), do: "Valid"

  def transaction_status_text(invalidation_reason) do
    "Invalid: \"#{invalidation_reason}\""
  end

  def platform_label_class(platform) do
    p =
      platform
      |> String.split("_")
      |> hd()
      |> String.downcase()

    "label-#{p}"
  end

  def humanized_platform(platform) do
    platform
    |> String.split("_")
    |> Enum.join(" ")
  end

  def timezone_offset_utc(timezone) do
    tz = Timezone.get(timezone)
    tz.offset_utc
  end

  defp to_naive(dt = %Ecto.DateTime{}) do
    dt
    |> Ecto.DateTime.to_erl()
    |> NaiveDateTime.from_erl!()
  end

  defp to_naive(dt), do: dt
end
