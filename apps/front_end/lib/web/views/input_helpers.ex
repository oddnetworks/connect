defmodule FrontEnd.InputHelpers do
  use Phoenix.HTML

  @mapping %{
    "email" => :email_input,
    "password" => :password_input,
    "password_confirmation" => :password_input,
    "search" => :search_input,
    "url" => :url_input
  }

  def input(form, field, opts \\ []) do
    type = opts[:using] || Phoenix.HTML.Form.input_type(form, field, @mapping)
    {help_text, opts} = Keyword.pop(opts, :help)

    wrapper_opts = [class: "form-group #{state_class(form, field)}"]
    label_opts = [class: "control-label text-uppercase text-muted"]
    input_opts = [class: "form-control"]
    validations = Phoenix.HTML.Form.input_validations(form, field)
    input_opts = Keyword.merge(validations, input_opts)

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field), label_opts)

      input =
        if type == :select do
          {options, _opts} = Keyword.pop(opts, :options)
          apply(Phoenix.HTML.Form, :select, [form, field, options, input_opts])
        else
          apply(Phoenix.HTML.Form, type, [form, field, input_opts])
        end

      error = FrontEnd.ErrorHelpers.error_tag(form, field)

      help =
        unless is_nil(help_text) do
          content_tag :small, class: "form-text text-muted" do
            [help_text]
          end
        end

      [label, input, help || "", error || ""]
    end
  end

  def checkbox_input(form, field, _opts \\ []) do
    type = :checkbox
    # {help_text, opts} = Keyword.pop(opts, :help)

    wrapper_opts = [class: "checkbox #{state_class(form, field)}"]
    input_opts = []

    content_tag :div, wrapper_opts do
      content_tag :label do
        input = apply(Phoenix.HTML.Form, type, [form, field, input_opts])
        [input, humanize(field)]
      end
    end
  end

  def checkbox_switch(form, field, opts \\ []) do
    type = :checkbox
    {data_attributes, opts} = Keyword.pop(opts, :data)
    {title, _opts} = Keyword.pop(opts, :title)

    wrapper_opts = [class: "switch #{state_class(form, field)}"]
    input_opts = [class: "switch-input"]

    label_opts =
      [class: "switch-label"] |> Keyword.merge(data: data_attributes || [], title: title || "")

    content_tag :div, wrapper_opts do
      input = apply(Phoenix.HTML.Form, type, [form, field, input_opts])
      label = label(form, field, label_opts)
      [input, label]
    end
  end

  def disabled_input(form, field, opts \\ []) do
    type = opts[:using] || Phoenix.HTML.Form.input_type(form, field)

    wrapper_opts = [class: "form-group #{state_class(form, field)}"]
    label_opts = [class: "control-label text-uppercase text-muted"]
    input_opts = [class: "form-control", disabled: true]
    validations = Phoenix.HTML.Form.input_validations(form, field)
    input_opts = Keyword.merge(validations, input_opts)

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field), label_opts)
      input = apply(Phoenix.HTML.Form, type, [form, field, input_opts])
      error = FrontEnd.ErrorHelpers.error_tag(form, field)
      [label, input, error || ""]
    end
  end

  def json_input(form, field) do
    value = Phoenix.HTML.Form.input_value(form, field) || %{}

    value =
      cond do
        is_binary(value) -> value
        is_map(value) -> Poison.encode!(value)
        true -> ""
      end

    wrapper_opts = [class: "form-group #{state_class(form, field)}"]
    label_opts = [class: "control-label text-uppercase text-muted"]

    input_opts = [
      class: "form-control",
      value: value
    ]

    validations = Phoenix.HTML.Form.input_validations(form, field)
    input_opts = Keyword.merge(validations, input_opts)

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field), label_opts)
      input = apply(Phoenix.HTML.Form, :textarea, [form, field, input_opts])
      error = FrontEnd.ErrorHelpers.error_tag(form, field)
      [label, input, error || ""]
    end
  end

  def obfuscated_text_input(form, field) do
    wrapper_opts = [class: "form-group #{state_class(form, field)}"]
    label_opts = [class: "control-label text-uppercase text-muted"]
    input_opts = [class: "form-control", disabled: "disabled"]
    validations = Phoenix.HTML.Form.input_validations(form, field)
    input_opts = Keyword.merge(validations, input_opts)

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field), label_opts)

      input_group =
        content_tag :div, class: "input-group obfuscated-text-input" do
          input = apply(Phoenix.HTML.Form, :text_input, [form, field, input_opts])

          toggle_button =
            content_tag :span, class: "input-group-btn" do
              content_tag :button, class: "btn btn-secondary", type: "button" do
                "Edit"
              end
            end

          [input, toggle_button]
        end

      error = FrontEnd.ErrorHelpers.error_tag(form, field)
      [label, input_group, error || ""]
    end
  end

  defp state_class(form, field) do
    cond do
      # The form was not yet submitted
      !form.source.action ->
        ""

      form.errors[field] ->
        "has-error"

      true ->
        "has-success"
    end
  end

  # Implement clauses below for custom inputs.
  # defp input(:datepicker, form, field, input_opts) do
  #   raise "not yet implemented"
  # end

  # defp input(type, form, field, input_opts) do
  #   apply(Phoenix.HTML.Form, type, [form, field, input_opts])
  # end
end
