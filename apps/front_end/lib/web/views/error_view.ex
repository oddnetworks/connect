defmodule FrontEnd.ErrorView do
  use FrontEnd.Web, :view

  def render("400.json", _assigns) do
    %{
      errors: [
        %{
          status: "400",
          title: "Bad Request",
          detail: "Invalid/malformed request"
        }
      ]
    }
  end

  def render("401.json", %{reason: %FrontEnd.Unauthenticated{detail: detail, message: title}}) do
    %{
      errors: [
        %{
          status: "401",
          title: title,
          detail: detail
        }
      ]
    }
  end

  def render("401.json", %{detail: detail}) do
    %{
      errors: [
        %{
          status: "401",
          title: "Unauthorized",
          detail: detail
        }
      ]
    }
  end

  def render("401.json", _assigns) do
    %{
      errors: [
        %{
          status: "401",
          title: "Unauthorized",
          detail: "Invalid authorization"
        }
      ]
    }
  end

  def render("403.json", %{reason: %FrontEnd.Forbidden{detail: detail, message: title}}) do
    %{
      errors: [
        %{
          status: "403",
          title: title,
          detail: detail
        }
      ]
    }
  end

  def render("403.json", _assigns) do
    %{
      errors: [
        %{
          status: "403",
          title: "Forbidden",
          detail: "Invalid permissions"
        }
      ]
    }
  end

  def render("404.json", %{reason: %FrontEnd.NotFound{detail: detail, message: title}}) do
    %{
      errors: [
        %{
          status: "404",
          title: title,
          detail: detail
        }
      ]
    }
  end

  def render("404.json", _assigns) do
    %{
      errors: [
        %{
          status: "404",
          title: "Not Found",
          detail: "Resource not found"
        }
      ]
    }
  end

  def render("422.json", %{reason: %{changeset: changeset}}) do
    %{errors: to_json_api_changeset_errors(changeset)}
  end

  def render("422.json", %{reason: %{detail: detail}}) when is_binary(detail) do
    %{
      errors: [
        %{
          status: "422",
          title: "Unprocessable Entity",
          detail: detail
        }
      ]
    }
  end

  def render("422.json", %{
        reason: %FrontEnd.UnprocessableEntity{detail: detail, message: title}
      }) do
    %{
      errors: [
        %{
          status: "422",
          title: title,
          detail: detail
        }
      ]
    }
  end

  def render("422.json", _assigns) do
    %{
      errors: [
        %{
          status: "422",
          title: "Unprocessable Entity",
          detail: "Entity data not valid"
        }
      ]
    }
  end

  def render("500.json", _assigns) do
    %{
      errors: [
        %{
          status: "500",
          title: "Internal Error",
          detail: "Internal server error"
        }
      ]
    }
  end

  # In case no render clause matches or no
  # template is found, let's render it as 500
  def template_not_found(template, assigns) do
    is_json = template |> String.match?(~r/\.json$/)
    is_json_api = template |> String.match?(~r/\.json-api$/)

    cond do
      is_json_api ->
        template_name =
          template
          |> String.split(".json-api")
          |> List.first()

        render(template_name <> ".json", assigns)

      is_json ->
        render("500.json", assigns)

      true ->
        render("500.html", assigns)
    end
  end

  defp to_json_api_changeset_errors(changeset) do
    changeset.errors
    |> Enum.map(&to_json_api_changeset_error/1)
  end

  defp to_json_api_changeset_error({key, detail}) do
    %{
      status: "422",
      title: "Unprocessable Entity",
      detail: render_detail(key, detail),
      source: render_source(key)
    }
  end

  defp render_detail(key, {message, values}) do
    humanize(Atom.to_string(key)) <>
      " " <>
      Enum.reduce(values, message, fn {k, v}, acc ->
        String.replace(acc, "%{#{k}}", to_string(v))
      end)
  end

  defp render_detail(_key, message) do
    message
  end

  # defp render_source() do
  #   %{ pointer: "/data" }
  # end

  defp render_source(key) do
    %{pointer: "/data/attributes/" <> fmt_key(key)}
  end

  defp fmt_key(key) do
    # camel = camelize(Atom.to_string(key))
    # String.downcase(String.slice(camel, 0, 1)) <> String.slice(camel, 1..-1)
    Atom.to_string(key)
  end
end
