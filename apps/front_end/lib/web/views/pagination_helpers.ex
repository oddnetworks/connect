defmodule FrontEnd.PaginationHelpers do
  import Phoenix.HTML, only: [raw: 1]

  def display_current_record_set(%{page_number: 1, page_size: size, total_entries: total})
      when total < size do
    format_current_record_set(1, total, total)
  end

  def display_current_record_set(%{page_number: 1, page_size: size, total_entries: total}) do
    format_current_record_set(1, size, total)
  end

  def display_current_record_set(%{page_number: number, page_size: size, total_entries: total})
      when number * size > total do
    format_current_record_set((number - 1) * size + 1, total, total)
  end

  def display_current_record_set(%{page_number: number, page_size: size, total_entries: total}) do
    format_current_record_set((number - 1) * size + 1, number * size, total)
  end

  def display_current_page(%{page_number: number, total_pages: total}) do
    "<p>Page #{number} of #{total}</p>"
    |> raw()
  end

  defp format_current_record_set(first, last, total) do
    "<p>Showing #{first} - #{last} of #{total}</p>"
    |> raw()
  end
end
