defmodule FrontEnd.Router do
  use FrontEnd.Web, :router
  use Plug.ErrorHandler

  if Mix.env() == :dev do
    forward("/sent_emails", Bamboo.EmailPreviewPlug)
  end

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)

    if Mix.env() == :prod do
      plug(Plug.SSL, rewrite_on: [:x_forwarded_proto])
      plug(:put_secure_browser_headers)
    end
  end

  pipeline :browser_auth do
    plug(Guardian.Plug.VerifySession)
    plug(Guardian.Plug.LoadResource)
    plug(FrontEnd.Plug.CurrentAccount)
  end

  scope "/", FrontEnd do
    pipe_through([:browser, :browser_auth])

    get("/", PageController, :index)

    get("/docs", DocumentationController, :index)

    resources("/sessions", SessionController, only: [:new, :create, :delete])

    resources(
      "/passwords",
      PasswordController,
      only: [:new, :create, :edit, :update],
      param: "token"
    )
  end

  scope "/app", FrontEnd.App do
    pipe_through([:browser, :browser_auth])

    get("/", DashboardController, :index)

    get("/odd_connect.pub", SigningKeyController, :index)

    get("/accounts/edit", AccountController, :edit, as: :account)
    put("/accounts", AccountController, :update, as: :account)
    put("/accounts/password", AccountController, :password, as: :account)

    get("/accounts/:urn/select", OrganizationAccountController, :select)
    get("/accounts", OrganizationAccountController, :index)

    get("/organizations/:id/switch", OrganizationController, :switch, as: :organization)

    resources(
      "/organizations",
      OrganizationController,
      only: [:edit, :update, :index],
      param: "urn"
    )

    get("/transactions/search", TransactionController, :search, as: :search)
    resources("/transactions", TransactionController, only: [:index, :show])
    resources("/connections", ConnectionController, only: [:index, :show])

    resources(
      "/device_users",
      DeviceUserController,
      only: [:index, :show],
      param: "email",
      as: :app_device_user
    )

    resources(
      "/entitlements",
      EntitlementController,
      only: [:new, :create, :edit, :update, :delete, :index]
    )

    resources(
      "/webhooks",
      WebhookController,
      only: [:new, :create, :edit, :update, :delete, :index]
    )
  end

  defp handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stacktrace}) do
    conn =
      conn
      |> Plug.Conn.fetch_cookies()
      |> Plug.Conn.fetch_query_params()
      |> Plug.Conn.fetch_session()

    %{
      "request" => %{
        "cookies" => conn.req_cookies,
        "url" => "#{conn.scheme}://#{conn.host}:#{conn.port}#{conn.request_path}",
        "user_ip" => conn.remote_ip |> Tuple.to_list() |> Enum.join("."),
        "headers" => Enum.into(conn.req_headers, %{}),
        "session" => conn.private[:plug_session] || %{},
        "params" => conn.params,
        "method" => conn.method
      },
      "server" => %{}
    }
  end
end
