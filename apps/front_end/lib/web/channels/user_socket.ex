require Logger

defmodule FrontEnd.UserSocket do
  use Phoenix.Socket
  import Guardian.Phoenix.Socket

  ## Channels
  channel("organization:*", FrontEnd.OrganizationChannel)

  ## Transports
  transport(:websocket, Phoenix.Transports.WebSocket)
  # transport :longpoll, Phoenix.Transports.LongPoll

  def connect(%{"token" => jwt}, socket) do
    case sign_in(socket, jwt) do
      {:ok, authed_socket, _guardian_params} ->
        authed_socket =
          authed_socket
          |> assign_organization_account()

        {:ok, authed_socket}

      _ ->
        :error
    end
  end

  # def connect(_params, socket) do
  #   {:ok, socket}
  # end

  def id(_socket), do: nil

  defp assign_organization_account(socket) do
    [_acct, account_id, organization_id, customer_id] =
      socket.assigns.guardian_default_claims["sub"]
      |> String.split(":")

    oa = current_resource(socket)

    socket
    |> assign(:account_id, account_id)
    |> assign(:organization_id, organization_id)
    |> assign(:customer_id, customer_id)
    |> assign(:timezone, oa.account.timezone)
  end
end
