defmodule FrontEnd.OrganizationChannel do
  require Logger
  use Phoenix.Channel

  def join("organization:edit", _, socket) do
    {:ok, socket}
  end

  def handle_in("timezone", _params, socket) do
    push(socket, "timezone", %{timezone: socket.assigns.timezone})

    {:noreply, socket}
  end

  def handle_in(
        "generate_api_token",
        %{},
        socket = %{assigns: %{organization_id: organization_id}}
      ) do
    token = FrontEnd.ApiToken.generate(organization_id)
    push(socket, "generated_api_token", %{token: token})
    {:noreply, socket}
  end

  def handle_in(
        "search_transactions",
        %{"email" => email},
        socket = %{assigns: %{organization_id: organization_id}}
      ) do
    transactions = Transactions.search_by_email(organization_id, email)

    push(socket, "search_results", %{results: transactions})
    {:noreply, socket}
  end

  def handle_in(
        "invalidate_transactions",
        %{"transaction_ids" => transaction_ids, "invalidation_reason" => reason},
        socket = %{assigns: %{organization_id: organization_id}}
      ) do
    {total, _} =
      Transactions.invalidate_multiple!(organization_id, transaction_ids, reason)

    push(socket, "invalidated_transactions", %{total: total})
    {:noreply, socket}
  end

  def handle_in(
        "delete_transactions",
        %{"transaction_ids" => transaction_ids},
        socket = %{assigns: %{organization_id: organization_id}}
      ) do
    {total, _} = Transactions.delete_multiple!(organization_id, transaction_ids)
    push(socket, "deleted_transactions", %{total: total})
    {:noreply, socket}
  end

  def handle_in(
        "purge_review_connections",
        _params,
        socket = %{assigns: %{organization_id: organization_id}}
      ) do
    {total, _} = Connect.purge_verified_connections_verified_during_review!(organization_id)
    push(socket, "purged_review_connections", %{total: total})
    {:noreply, socket}
  end
end
