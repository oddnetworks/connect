const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

const vueDist = process.env.NODE_ENV == 'production' ? 'vue/dist/vue.min' : 'vue/dist/vue';

const extractSass = new ExtractTextPlugin({
	filename: 'css/app.css'
});

module.exports = {
	entry: ['./css/app.scss', './js/app.js'],
	output: {
		path: path.resolve(__dirname, '../priv/static'),
		filename: './js/app.js',
		publicPath: '/'
	},

	resolve: {
		modules: ['node_modules', path.resolve(__dirname, './js')],
		alias: {
			vue: vueDist
		}
	},

	module: {
		loaders: [{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				include: __dirname
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
				test: /\.scss$/,
				use: extractSass.extract({
					use: [{
							loader: 'css-loader',
							options: {
								includePaths: [path.resolve(__dirname, 'node_modules')],
								sourceMap: true
							}
						},
						'resolve-url-loader',
						{
							loader: 'sass-loader',
							options: {
								includePaths: [path.resolve(__dirname, 'node_modules')],
								sourceMap: true
							}
						}
					],
					fallback: 'style-loader'
				})
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				loader: 'file-loader',
				options: {
					name: '[name].[ext]',
					outputPath: './static/fonts/',
					publicPath: '/fonts/'
				}
			},
			{
				test: /\.(jpg|png|gif)$/,
				loader: 'file-loader',
				options: {
					name: '[name].[ext]',
					outputPath: './static/images/',
					publicPath: '/images/'
				}
			}
		]
	},

	plugins: [
		// short-circuits all Vue.js warning code
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV) || '"development"'
			}
		}),
		// minify with dead-code elimination
		new webpack.optimize.UglifyJsPlugin(),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
		new CopyWebpackPlugin([{
			from: path.resolve(__dirname, './static'),
			to: path.resolve(__dirname, '../priv/static/'),
			ignore: ['.DS_Store']
		}]),
		new WebpackNotifierPlugin(),
		extractSass,
	]
};
