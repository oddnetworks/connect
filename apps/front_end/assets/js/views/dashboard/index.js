import MainView from "../main";
import PieChart from "../../piechart";

export default class View extends MainView {
  mount() {
		super.mount();

		const charts = document.getElementsByClassName('pie-chart');
		for(let i=0; i < charts.length; i++) {
			const chart = charts.item(i);
			const legend = chart.getElementsByClassName('pie-legend').item(0);
			const canvas = chart.getElementsByClassName('pie-canvas').item(0);
			const data = $(chart).data('pie');
			const pieChart = new PieChart({
				canvas,
				data,
				legend,
				colors: this.shuffle(["#ffe119","#0082c8","#e6194b","#3cb44b","#f58231","#911eb4","#46f0f0","#f032e6"])
			});
			pieChart.draw();
		}
	}

	unmount() {
    super.unmount();
	}

	shuffle(items) {
		var currentIndex = items.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = items[currentIndex];
			items[currentIndex] = items[randomIndex];
			items[randomIndex] = temporaryValue;
		}

		return items;
	}
}
