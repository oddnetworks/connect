import MainView from './main';
import DashboardIndexView from './dashboard/index';
import OrganizationEditView from "./organization/edit";
import WebhookEditView from "./webhook/edit";

// Collection of specific view modules
const views = {
	DashboardIndexView,
	OrganizationEditView,
	WebhookEditView
};

export default function loadView(viewName) {
	return views[viewName] || MainView;
}
