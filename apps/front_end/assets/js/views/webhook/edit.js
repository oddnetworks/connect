import MainView from '../main';

export default class View extends MainView {
  mount() {
    super.mount();

		const removeElement = ({target}) => {
	    let el = document.getElementById(target.dataset.id);
	    let li = el.parentNode;
	    li.parentNode.removeChild(li);
	  }
	  Array.from(document.querySelectorAll(".remove-form-field"))
	    .forEach(el => {
	      el.onclick = (e) => {
	        removeElement(e);
	      }
	    });
	  Array.from(document.querySelectorAll(".add-form-field"))
	    .forEach(el => {
	      el.onclick = ({target}) => {
	        let container = document.getElementById(target.dataset.container);
	        let index = container.dataset.index;
	        let newRow = target.dataset.prototype;
	        container.insertAdjacentHTML('beforeend', newRow.replace(/__name__/g, index));
	        container.dataset.index = parseInt(container.dataset.index) + 1;
	        container.querySelector('a.remove-form-field').onclick = (e) => {
	          removeElement(e);
	        }
	      }
	    });
  }

  unmount() {
    super.unmount();
  }
}
