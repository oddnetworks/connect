import {
	SocketStore
} from "../../socket-store";
import SocketPlugin from '../../socket-store';
import moment from 'moment-timezone'
import Vue from 'vue'
import ApiToken from './api-token.vue'
import TransactionCleanup from './transaction-cleanup.vue'
import ConnectionCleanup from './connection-cleanup.vue'
import MainView from '../main'

Vue.use(SocketPlugin)

export default class View extends MainView {
	mount() {
		super.mount();

		const guardian_token = document
			.getElementsByName("guardian_token")
			.item(0)
			.getAttribute("content");

		SocketStore.connect(guardian_token, true)

		SocketStore.findChannel("edit", "organization").then(res => {
			const DataCleanup = Vue.extend({
				components: {
					TransactionCleanup,
					ConnectionCleanup
				},
				template: '<div class="panel-body"><connection-cleanup></connection-cleanup><transaction-cleanup></transaction-cleanup></div>'
			});
			const TokenGeneration = Vue.extend({
				template: '<div class="panel-body"><api-token></api-token></div>',
				components: {
					ApiToken
				}
			});

			new Vue(TokenGeneration).$mount('#api-token-body');
			new Vue(DataCleanup).$mount('#data-cleanup-body');
		})

		$('[data-toggle="tooltip"]').tooltip();
	}

	unmount() {
		super.unmount();
	}
}
