import $ from 'jquery';
import '../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js';
import 'phoenix_html';
// import { Socket } from 'phoenix;
import loadView from './views/loader';

// $(document).off('click.bs.dropdown.data-api', '.dropdown form');

function handleDOMContentLoaded() {
	const viewName = document.getElementsByTagName('body')[0].dataset.jsViewName;
	// Load view class and mount it
	const ViewClass = loadView(viewName);
	const view = new ViewClass();
	view.mount();

	window.currentView = view;

	$('[data-submit="parent"]').on('click', function(e) {
    e.preventDefault();
    $(this).parents('form.link').submit();
	});
}

function handleDocumentUnload() {
	window.currentView.unmount();
}

window.addEventListener('DOMContentLoaded', handleDOMContentLoaded, false);
window.addEventListener('unload', handleDocumentUnload, false);
