export default class PieChart {
	constructor (options) {
		this.options = options;
		this.canvas = options.canvas;
		this.ctx = this.canvas.getContext("2d");
		this.colors = options.colors;
	}

	draw() {
		var total_value = 0;
		var color_index = 0;
		for (var categ in this.options.data){
			var val = this.options.data[categ];
			total_value += val;
		}

		var start_angle = 0;
		for (categ in this.options.data){
			val = this.options.data[categ];
			var slice_angle = 2 * Math.PI * val / total_value;

			this.drawPieSlice(
				this.ctx,
				this.canvas.width/2,
				this.canvas.height/2,
				Math.min(this.canvas.width/2,this.canvas.height/2),
				start_angle,
				start_angle + slice_angle,
				this.colors[color_index%this.colors.length]
			);

			start_angle += slice_angle;
			color_index++;
		}

		start_angle = 0;
		for (categ in this.options.data){
			val = this.options.data[categ];
			slice_angle = 2 * Math.PI * val / total_value;
			var pieRadius = Math.min(this.canvas.width/2,this.canvas.height/2);
			var labelX = this.canvas.width/2 + (pieRadius / 1.75) * Math.cos(start_angle + slice_angle/2);
			var labelY = this.canvas.height/2 + (pieRadius / 1.75) * Math.sin(start_angle + slice_angle/2);

			if (this.options.doughnutHoleSize){
				var offset = (pieRadius * this.options.doughnutHoleSize ) / 2;
				labelX = this.canvas.width/2 + (offset + pieRadius / 2) * Math.cos(start_angle + slice_angle/2);
				labelY = this.canvas.height/2 + (offset + pieRadius / 2) * Math.sin(start_angle + slice_angle/2);
			}

			// var labelText = val;
			this.ctx.fillStyle = "black";
			this.ctx.font = "16px sans-serif";

			var labelText = (100 * val / total_value).toFixed(2);
			this.ctx.fillText(labelText+'%', labelX, labelY);
			start_angle += slice_angle;
		}

		if (this.options.legend){
			color_index = 0;
			var legendHTML = "";
			var idx = 0;
			for (categ in this.options.data){
				var stripe = idx % 2 == 0;
				if (stripe) {
					legendHTML += "<div style='background-color:#d3d3d3;'>";
				} else {
					legendHTML += "<div>";
				}
				legendHTML += "<span style='text-align:center;display:inline-block;padding:0 5px;background-color:"+this.colors[color_index++]+";'>&nbsp;</span><span style='padding-left:10px;display:inline-block;width:110px;text-align:left;'>" + categ + "</span><span style='display:inline-block;width:100px;float:right;text-align:right;padding-right:10px;'>" + this.options.data[categ] + "</span></div>";
				idx++;
			}
			var stripe = idx % 2 == 0;
			if (stripe) {
				legendHTML += "<div style='background-color:#d3d3d3;margin-bottom:10px;'>";
			} else {
				legendHTML += "<div style='margin-bottom:10px;'>";
			}
			legendHTML += "<span style='text-align:center;padding:0 5px;'>&nbsp;</span><span style='padding-left:10px;display:inline-block;width:110px;text-align:left;'>Total</span><span style='display:inline-block;width:100px;float:right;text-align:right;padding-right:10px;'>" + total_value + "</span></div>";
			this.options.legend.innerHTML = legendHTML;
		}
		if (total_value == 0) {
			this.canvas.style.display = 'none';
		}
	}

	drawPieSlice(ctx,centerX, centerY, radius, startAngle, endAngle, color ) {
			ctx.fillStyle = color;
			ctx.beginPath();
			ctx.moveTo(centerX,centerY);
			ctx.arc(centerX, centerY, radius, startAngle, endAngle);
			ctx.closePath();
			ctx.fill();
	}
}
