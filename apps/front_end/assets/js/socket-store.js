import {
	Socket as PhoenixSocket
} from 'phoenix'
import _ from 'lodash'

const socketInstance = new PhoenixSocket("/socket", {
	logger: (kind, msg, data) => {
		if (process.env.NODE_ENV == 'production') return
		console.log(`[PhoenixSocket][${kind}] ${msg}`, data)
	},
})

export const SocketStore = {
	log(level, kind, msg) {
		switch (level) {
			case 'error':
				console.error(`[SocketStore][${kind}] ${msg}`);
				break;
			case 'warn':
				console.warn(`[SocketStore][${kind}] ${msg}`);
				break;
			case 'info':
				console.info(`[SocketStore][${kind}] ${msg}`);
				break;
			default:
				console.log(`[SocketStore][${kind}] ${msg}`);
		}
	},
	connect(token, silent = false) {
		this.silent = silent;
		if (this.connClosed()) {
			socketInstance.params.token = token
			socketInstance.connect()
			this.log('debug', 'connect', 'Connected!')
		} else if (!this.connAvaiable()) {
			socketInstance.connect()
			this.log('debug', 'connect', 'Reconnected!')
		} else if (!silent) {
			this.log('warn', 'connect', 'Already Connected!')
		}
	},
	disconnect() {
		if (this.connClosed()) {
			return
		}
		socketInstance.disconnect(() => {
			socketInstance.reconnectTimer.reset()
			this.log('debug', 'disconnect', 'Disconnected!')
		})
	},
	connAvaiable() {
		return socketInstance && (socketInstance.connectionState() === 'open' ||
			socketInstance.connectionState() === 'connecting')
	},

	connClosed() {
		return socketInstance.connectionState() === 'closed'
	},

	findChannel(id, prefix = 'rooms') {
		return new Promise((resolve, reject) => {
			if (this.connClosed()) {
				this.log('error', 'findChannel', 'No socket connection, please connect first')
				reject('NO_SOCKET_CONNECTION')
			}

			const topicName = `${prefix}:${id}`
			let channel = _.find(socketInstance.channels, ch => ch.topic === topicName)

			if (!channel) {
				channel = socketInstance.channel(topicName, {})
			}

			if (channel.state === 'closed') {
				channel.join()
					.receive('ok', (response) => {
						this.log('debug', 'findChannel', `Join ${channel.topic}`)
						resolve({
							channel,
							response
						})
					})
					.receive('error', () => {
						this.log('warn', 'findChannel', `ERROR - Join ${channel.topic}`)
						reject()
					})
			} else {
				resolve({
					channel
				})
			}
		})
	},

	leaveChannel(id, prefix = 'rooms') {
		return new Promise((resolve, reject) => {
			if (this.connClosed()) {
				this.log('error', 'leaveChannel', 'No socket connection, please connect first')
				reject('NO_SOCKET_CONNECTION')
			}

			const topicName = `${prefix}:${id}`
			const channel = _.find(socketInstance.channels, ch => ch.topic === topicName)

			if (channel.state === 'closed') {
				reject()
			} else {
				channel.leave()
					.receive('ok', () => {
						this.log('debug', 'leaveChannel', `Left ${channel.topic}`)
						resolve({
							channel
						})
					})
					.receive('error', () => {
						this.log('warn', 'leaveChannel', `ERROR - Left ${channel.topic}`)
						reject({
							channel
						})
					})
			}
		})
	},
}

export default function install(Vue) {
	Object.defineProperty(Vue.prototype, '$socket', {
		get() {
			return SocketStore
		}
	})
}
