defmodule FrontEnd.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build and query models.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest

      import FrontEnd.Router.Helpers

      # for inserting/removing records
      import Connect.Factory

      # The default endpoint for testing
      @endpoint FrontEnd.Endpoint

      @default_opts [
        store: :cookie,
        key: "foobar",
        encryption_salt: "encrypted cookie salt",
        signing_salt: "signing salt"
      ]
      @secret String.duplicate("abcdef0123456789", 8)
      @signing_opts Plug.Session.init(Keyword.put(@default_opts, :encrypt, false))

      def generate_api_token(resource) do
        claims =
          Guardian.Claims.app_claims()
          |> Guardian.Claims.ttl({1, :day})

        {:ok, jwt, _claims} = Guardian.encode_and_sign(resource, :access, claims)
        jwt
      end

      @doc """
      Creates a session within the conn for the given resource
      """
      def sign_in(conn, resource) do
        conn
        |> conn_with_fetched_session()
        |> Guardian.Plug.sign_in(resource, :token)
        |> Guardian.Plug.VerifySession.call(%{})
      end

      @doc """
      Creates a Auth.OrganizationAccount with a customer
      """
      def create_organization_account("customer") do
        account = create_account(false)
        organization = create_organization()
        customer = create_customer(account, organization, "customer")
        make_organization_account(account, organization, customer)
      end

      def create_organization_account("admin") do
        account = create_account(false)
        organization = create_organization()
        customer = create_customer(account, organization, "admin")
        make_organization_account(account, organization, customer)
      end

      def create_organization_account("owner") do
        account = create_account(false)
        organization = create_organization()
        customer = create_customer(account, organization, "owner")
        make_organization_account(account, organization, customer)
      end

      @doc """
      Creates a super admin Auth.OrganizationAccount
      """
      def create_super_admin_account do
        account = create_account(true)
        organization = create_organization()
        make_organization_account(account, organization)
      end

      def json_error_body(status, title, detail) do
        %{"errors" => [%{"detail" => detail, "status" => status, "title" => title}]}
      end

      defp conn_with_fetched_session(the_conn) do
        put_in(the_conn.secret_key_base, @secret)
        |> Plug.Session.call(@signing_opts)
        |> Plug.Conn.fetch_session()
      end

      defp make_organization_account(account, organization, customer \\ nil) do
        case Auth.OrganizationAccount.new(account, organization, customer) do
          {:ok, oa} -> oa
          _ -> throw("Bad OrganizationAccount")
        end
      end

      defp create_account(_is_admin = true) do
        insert(:auth_account, is_admin: true)
      end

      defp create_account(_is_not_admin) do
        insert(:auth_account, is_admin: false)
      end

      defp create_organization() do
        insert(:organization)
      end

      defp create_customer(account, organization, role \\ "customer") do
        insert(
          :customer,
          role: role,
          auth_account: account,
          organization: organization
        )
      end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Connect.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Connect.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
