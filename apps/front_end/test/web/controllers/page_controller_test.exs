defmodule FrontEnd.PageControllerTest do
  use FrontEnd.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    response = html_response(conn, 200)
    assert response =~ "Welcome to Odd Connect!"
    assert response =~ "<a href=\"#{session_path(conn, :new)}\">"
    assert response =~ "Log In"
  end
end
