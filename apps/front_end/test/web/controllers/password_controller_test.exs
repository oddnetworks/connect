defmodule FrontEnd.PasswordControllerTest do
  use FrontEnd.ConnCase

  setup %{conn: conn} do
    oa = create_organization_account("customer")

    auth_conn =
      conn
      |> sign_in(oa)

    {:ok, conn: conn, auth_conn: auth_conn}
  end

  describe "GET /passwords/new" do
    test "when authenticated", %{auth_conn: conn} do
      conn =
        conn
        |> get(password_path(conn, :new))

      assert redirected_to(conn) == dashboard_path(conn, :index)
      assert get_flash(conn, :info) == "Already logged in"
    end

    test "when not authenticated", %{conn: conn} do
      conn =
        conn
        |> get(password_path(conn, :new))

      resp = html_response(conn, 200)

      assert resp =~ "Reset Password"

      assert resp =~
               "<input class=\"form-control\" id=\"account_email\" name=\"account[email]\" type=\"email\">"
    end
  end

  describe "POST /passwords" do
    test "when authenticated", %{auth_conn: conn} do
      conn =
        conn
        |> post(password_path(conn, :create), %{email: "foo@example.com"})

      assert redirected_to(conn) == dashboard_path(conn, :index)
      assert get_flash(conn, :info) == "Already logged in"
    end

    test "when unauthenticated", %{conn: conn} do
      conn =
        conn
        |> post(password_path(conn, :create), %{auth_account: %{email: "foo@example.com"}})

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :info) ==
               "If your account exists, password reset instructions have been sent to your email address."
    end
  end

  describe "GET /passwords/:token/edit" do
    setup %{conn: conn, auth_conn: auth_conn} do
      expired_reset_account =
        insert(
          :auth_account,
          password_reset_token: "expired",
          reset_token_sent_at: Timex.shift(NaiveDateTime.utc_now(), hours: -25)
        )

      valid_reset_account =
        insert(
          :auth_account,
          password_reset_token: "valid",
          reset_token_sent_at: NaiveDateTime.utc_now()
        )

      {:ok,
       conn: conn,
       auth_conn: auth_conn,
       expired_reset_account: expired_reset_account,
       valid_reset_account: valid_reset_account}
    end

    test "when authenticated", %{auth_conn: conn} do
      conn =
        conn
        |> get(password_path(conn, :edit, "12345"))

      assert redirected_to(conn) == dashboard_path(conn, :index)
      assert get_flash(conn, :info) == "Already logged in"
    end

    test "when unauthenticated and token is invalid", %{conn: conn} do
      conn =
        conn
        |> get(password_path(conn, :edit, "12345"))

      assert redirected_to(conn) == password_path(conn, :new)
      assert get_flash(conn, :error) == "Invalid reset token"
    end

    test "when unauthenticated and token is expired", %{
      conn: conn
    } do
      conn =
        conn
        |> get(password_path(conn, :edit, "expired"))

      assert redirected_to(conn) == password_path(conn, :new)
      assert get_flash(conn, :error) == "Password reset token expired"

      assert Connect.Repo.get_by(Connect.AuthAccount, password_reset_token: "expired") == nil
    end

    test "when unauthenticated and token is valid", %{conn: conn} do
      conn =
        conn
        |> get(password_path(conn, :edit, "valid"))

      response = html_response(conn, 200)

      assert response =~ "Reset Password"
      assert response =~ "Please enter a new password for your account"

      assert response =~
               "<input class=\"form-control\" id=\"account_password\" maxlength=\"32\" minlength=\"8\" name=\"account[password]\" required=\"required\" type=\"password\">"

      assert response =~
               "input class=\"form-control\" id=\"account_password_confirmation\" name=\"account[password_confirmation]\" type=\"password\">"
    end
  end

  describe "PATCH /passwords/:token" do
    setup %{conn: conn, auth_conn: auth_conn} do
      expired_reset_account =
        insert(
          :auth_account,
          password_reset_token: "expired",
          reset_token_sent_at: Timex.shift(NaiveDateTime.utc_now(), hours: -25)
        )

      valid_reset_account =
        insert(
          :auth_account,
          password_reset_token: "valid",
          reset_token_sent_at: NaiveDateTime.utc_now()
        )

      valid_params = %{
        auth_account: %{
          password: "herpderp",
          password_confirmation: "herpderp"
        }
      }

      {:ok,
       conn: conn,
       auth_conn: auth_conn,
       expired_reset_account: expired_reset_account,
       valid_reset_account: valid_reset_account,
       valid_params: valid_params}
    end

    test "when authenticated", %{auth_conn: conn, valid_params: params} do
      conn =
        conn
        |> patch(password_path(conn, :update, "12345"), params)

      assert redirected_to(conn) == dashboard_path(conn, :index)
      assert get_flash(conn, :info) == "Already logged in"
    end

    test "when unauthenticated and token is invalid", %{conn: conn, valid_params: params} do
      conn =
        conn
        |> patch(password_path(conn, :update, "12345"), params)

      assert redirected_to(conn) == password_path(conn, :new)
      assert get_flash(conn, :error) == "Invalid reset token"
    end

    test "when unauthenticated and token is expired", %{conn: conn, valid_params: params} do
      conn =
        conn
        |> patch(password_path(conn, :update, "expired"), params)

      assert redirected_to(conn) == password_path(conn, :new)
      assert get_flash(conn, :error) == "Password reset token expired"

      assert Connect.Repo.get_by(Connect.AuthAccount, password_reset_token: "expired") == nil
    end

    test "when unauthenticated, token is valid, and params are invalid", %{conn: conn} do
      conn =
        conn
        |> patch(password_path(conn, :update, "valid"), %{auth_account: %{password: "herpderp"}})

      assert get_flash(conn, :error) == "There was a problem resetting your password"

      response = html_response(conn, 200)
      assert response =~ "Reset Password"
      assert response =~ "Please enter a new password for your account"

      assert response =~
               "<input class=\"form-control\" id=\"account_password\" maxlength=\"32\" minlength=\"8\" name=\"account[password]\" required=\"required\" type=\"password\">"

      assert response =~
               "<input class=\"form-control\" id=\"account_password_confirmation\" name=\"account[password_confirmation]\" type=\"password\"><span class=\"help-block\">can&#39;t be blank</span>"
    end

    test "when unauthenticated, token is valid, and params are valid", %{
      conn: conn,
      valid_params: params,
      valid_reset_account: %{id: id, password_hash: password_hash}
    } do
      conn =
        conn
        |> patch(password_path(conn, :update, "valid"), params)

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :info) == "Password reset successfully!"

      updated_account = Connect.Repo.get(Connect.AuthAccount, id)
      assert is_nil(updated_account.password_reset_token)
      assert is_nil(updated_account.reset_token_sent_at)
      refute updated_account.password_hash == password_hash
    end
  end
end
