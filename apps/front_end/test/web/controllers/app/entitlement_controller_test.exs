defmodule FrontEnd.App.EntitlementControllerTest do
  use FrontEnd.ConnCase

  setup %{conn: conn} do
    oa = create_organization_account("customer")
    ent = insert(:entitlement, organization: oa.organization)

    {:ok, ent: ent, cust: oa.customer, conn: conn, oa: oa}
  end

  describe "entitlement routes while unauthenticated" do
    test "GET /app/entitlements/:id/edit redirects with flash error", %{
      conn: conn,
      ent: ent
    } do
      conn =
        conn
        |> get(entitlement_path(conn, :edit, ent.id))

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end

    test "GET /app/entitlements/new redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> get(entitlement_path(conn, :new))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "POST /app/entitlements redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> post(entitlement_path(conn, :create), %{entitlement: params_for(:entitlement)})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "PATCH /app/entitlements/:id redirects with flash error", %{conn: conn, ent: ent} do
      conn =
        conn
        |> patch(entitlement_path(conn, :update, ent.id), %{entitlement: %{name: "FOOBAR"}})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "DELETE /app/entitlements/:id redirects with flash error", %{conn: conn, ent: ent} do
      conn =
        conn
        |> delete(entitlement_path(conn, :delete, ent.id))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "entitlement routes while authenticated as organization" do
    setup %{ent: ent, conn: conn} do
      conn =
        conn
        |> sign_in(ent.organization)

      {:ok, conn: conn, ent: ent}
    end

    test "GET /app/entitlements/:id/edit redirects with flash error", %{
      conn: conn,
      ent: ent
    } do
      conn =
        conn
        |> get(entitlement_path(conn, :edit, ent.id))

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end

    test "GET /app/entitlements/new redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> get(entitlement_path(conn, :new))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "POST /app/entitlements redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> post(entitlement_path(conn, :create), %{entitlement: params_for(:entitlement)})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "PATCH /app/entitlements/:id redirects with flash error", %{conn: conn, ent: ent} do
      conn =
        conn
        |> patch(entitlement_path(conn, :update, ent.id), %{entitlement: %{name: "FOOBAR"}})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "DELETE /app/entitlements/:id redirects with flash error", %{conn: conn, ent: ent} do
      conn =
        conn
        |> delete(entitlement_path(conn, :delete, ent.id))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "entitlement routes while authenticated as a customer" do
    setup %{ent: ent, cust: cust, conn: conn, oa: oa} do
      conn =
        conn
        |> sign_in(oa)

      {:ok, conn: conn, cust: cust, ent: ent}
    end

    test "GET /app/entitlements/:id/edit displays editable entitlement data", %{
      conn: conn,
      ent: ent
    } do
      conn =
        conn
        |> get(entitlement_path(conn, :edit, ent.id))

      res = html_response(conn, 200)
      assert res =~ "Editing Entitlement"

      assert res =~ "entitlement[name]"
      assert res =~ ent.name
      assert res =~ "entitlement[description]"
      assert res =~ "entitlement[type]"
      assert res =~ ent.type
      assert res =~ "entitlement[entitlement_identifier]"
      assert res =~ ent.entitlement_identifier
      assert res =~ "entitlement[product_identifier]"
      assert res =~ ent.product_identifier
      assert res =~ "entitlement[platform]"
      assert res =~ ent.platform
    end

    test "GET /app/entitlements/new displays editable entitlement data", %{conn: conn} do
      conn =
        conn
        |> get(entitlement_path(conn, :new))

      res = html_response(conn, 200)
      assert res =~ "New Entitlement"

      assert res =~ "entitlement[name]"
      assert res =~ "entitlement[description]"
      assert res =~ "entitlement[type]"
      assert res =~ "entitlement[entitlement_identifier]"
      assert res =~ "entitlement[product_identifier]"
      assert res =~ "entitlement[platform]"

      Connect.Entitlements.valid_types()
      |> Enum.each(fn t ->
        assert res =~ t
      end)

      Connect.Entitlements.valid_entitlement_platforms()
      |> Enum.each(fn p ->
        assert res =~ p
      end)
    end

    test "POST /app/entitlements with valid attrs creates a new entitlement", %{
      conn: conn
    } do
      conn =
        conn
        |> post(entitlement_path(conn, :create), %{entitlement: params_for(:entitlement)})

      assert redirected_to(conn) == entitlement_path(conn, :index)

      assert get_flash(conn, :success) == "Successfully created entitlement"
    end

    test "POST /app/entitlements with invalid attrs displays an error", %{
      conn: conn,
      ent: ent
    } do
      conn =
        conn
        |> post(entitlement_path(conn, :create), %{
          entitlement:
            params_for(
              :entitlement,
              entitlement_identifier: ent.entitlement_identifier,
              product_identifier: ent.product_identifier,
              platform: ent.platform
            )
        })

      res = html_response(conn, 200)
      assert res =~ "New Entitlement"
      assert res =~ "already exists for this platform"
    end

    test "PATCH /app/entitlements/:id with valid attrs updates an entitlement", %{
      conn: conn,
      ent: ent
    } do
      conn =
        conn
        |> patch(entitlement_path(conn, :update, ent.id), %{
          entitlement: %{entitlement_identifier: "my-fancy-entitlement"}
        })

      assert redirected_to(conn) == entitlement_path(conn, :index)
      assert get_flash(conn, :success) == "Successfully updated entitlement"

      conn =
        conn
        |> get(entitlement_path(conn, :edit, ent.id))

      assert html_response(conn, 200) =~ "my-fancy-entitlement"
    end

    test "DELETE /app/entitlements/:id is successful", %{
      conn: conn,
      cust: cust
    } do
      ent = insert(:entitlement, organization: cust.organization)

      conn =
        conn
        |> delete(entitlement_path(conn, :delete, ent.id))

      assert redirected_to(conn) == entitlement_path(conn, :index)

      assert get_flash(conn, :success) ==
               "Successfully removed entitlement from #{cust.organization.name}"

      res =
        assert_error_sent(404, fn ->
          conn
          |> get(entitlement_path(conn, :edit, ent.id))
        end)

      {404, [_h | _t], html} = res
      assert html =~ "404 - Act Cool"
    end
  end
end
