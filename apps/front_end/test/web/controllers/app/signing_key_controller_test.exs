defmodule FrontEnd.App.SigningKeyControllerTest do
  use FrontEnd.ConnCase

  setup %{conn: conn} do
    oa = create_organization_account("customer")

    {:ok, conn: conn, cust: oa.customer, oa: oa}
  end

  describe "while unauthenticated" do
    test "GET /app/odd_connect.pub redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> get(signing_key_path(conn, :index))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "while authenticated as organization" do
    setup %{conn: conn, cust: cust} do
      conn =
        conn
        |> sign_in(cust.organization)

      {:ok, conn: conn, cust: cust}
    end

    test "GET /app/odd_connect.pub redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> get(signing_key_path(conn, :index))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "while authenticated as customer" do
    setup %{conn: conn, oa: oa, cust: cust} do
      conn =
        conn
        |> sign_in(oa)

      {:ok, conn: conn, cust: cust}
    end

    test "GET /app/odd_connect.pub", %{conn: conn} do
      conn =
        conn
        |> get(signing_key_path(conn, :index))

      res = text_response(conn, 200)

      assert res == Auth.ConnectionToken.public_key()
    end
  end
end
