defmodule FrontEnd.App.DashboardControllerTest do
  use FrontEnd.ConnCase

  describe "GET /app while unauthenticated" do
    test "redirects with flash error", %{conn: conn} do
      conn =
        conn
        |> get(dashboard_path(conn, :index))

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "GET /app while authenticated as organization" do
    test "redirects with flash error", %{conn: conn} do
      org = insert(:organization)

      conn =
        conn
        |> sign_in(org)

      conn =
        conn
        |> get(dashboard_path(conn, :index))

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "GET /app while authenticated as a customer" do
    setup %{conn: conn} do
      acct = create_organization_account("customer")

      conn =
        conn
        |> sign_in(acct)
        |> get(dashboard_path(conn, :index))

      {:ok, acct: acct, conn: conn}
    end

    test "shows Transaction and Connection overview", %{conn: conn} do
      assert html_response(conn, 200) =~ "Transactions"
      assert html_response(conn, 200) =~ "Connections"
    end

    test "shows sign out link", %{conn: conn} do
      assert html_response(conn, 200) =~ "Log Out"
    end

    test "shows link to manage organization settings", %{
      conn: conn,
      acct: %{organization: %{urn: urn}}
    } do
      assert html_response(conn, 200) =~ "<a href=\"#{organization_path(conn, :edit, urn)}\">"
    end
  end
end
