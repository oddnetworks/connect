defmodule FrontEnd.App.AccountControllerTest do
  use FrontEnd.ConnCase

  describe "while unauthenticated" do
    test "GET /app/accounts/edit redirects with error", %{conn: conn} do
      conn =
        conn
        |> get(account_path(conn, :edit))

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end

    test "PUT /app/accounts redirects with error", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :update), %{})

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end

    test "PUT /app/accounts/password redirects with error", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :password), %{})

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "while authenticated as an organization" do
    setup %{conn: conn} do
      org = insert(:organization)

      conn =
        conn
        |> sign_in(org)

      {:ok, conn: conn}
    end

    test "GET /app/accounts/edit redirects with error", %{conn: conn} do
      conn =
        conn
        |> get(account_path(conn, :edit))

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end

    test "PUT /app/accounts redirects with error", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :update), %{})

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end

    test "PUT /app/accounts/password redirects with error", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :password), %{})

      assert redirected_to(conn) == session_path(conn, :new)

      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "while authenticated with an organization account" do
    setup %{conn: conn} do
      acct = create_organization_account("customer")

      conn =
        conn
        |> sign_in(acct)

      {:ok, conn: conn, acct: acct}
    end

    test "GET /app/accounts/edit displays account and password edit forms", %{
      conn: conn,
      acct: %{account: %{name: name, timezone: timezone}}
    } do
      conn =
        conn
        |> get(account_path(conn, :edit))

      response = html_response(conn, 200)

      assert response =~ "Editing Account"

      assert response =~
               "<form accept-charset=\"UTF-8\" action=\"#{account_path(conn, :update)}\" method=\"post\"><input name=\"_method\" type=\"hidden\" value=\"put\">"

      assert response =~
               "<input class=\"form-control\" id=\"auth_account_name\" name=\"auth_account[name]\" required=\"required\" type=\"text\" value=\"#{
                 name
               }\">"

      assert response =~
               "<option selected=\"selected\" value=\"#{timezone}\">#{timezone}</option>"

      assert response =~ "New Password"

      assert response =~
               "<form accept-charset=\"UTF-8\" action=\"#{account_path(conn, :password)}\" method=\"post\"><input name=\"_method\" type=\"hidden\" value=\"put\">"

      assert response =~
               "<input class=\"form-control\" id=\"auth_account_current_password\" name=\"auth_account[current_password]\" required=\"required\" type=\"password\">"

      assert response =~
               "<input class=\"form-control\" id=\"auth_account_password\" maxlength=\"32\" minlength=\"8\" name=\"auth_account[password]\" required=\"required\" type=\"password\">"

      assert response =~
               "<input class=\"form-control\" id=\"auth_account_password_confirmation\" name=\"auth_account[password_confirmation]\" type=\"password\">"
    end

    test "PUT /accounts with invalid attributes", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :update, %{"auth_account" => %{"name" => nil}}))

      assert get_flash(conn, :error) == "There was a problem updating your account"
      response = html_response(conn, 200)
      assert response =~ "can&#39;t be blank"
    end

    test "PUT /accounts with valid attributes", %{conn: conn} do
      name = "Updated Name"

      conn =
        conn
        |> put(account_path(conn, :update, %{"auth_account" => %{"name" => name}}))

      assert get_flash(conn, :success) == "Account updated"
      redirect_path = redirected_to(conn)
      assert redirect_path == account_path(conn, :edit)
      conn = get(recycle(conn), redirect_path)
      assert html_response(conn, 200) =~ name
    end

    test "PUT /accounts/password with invalid attributes", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :password), %{
          "auth_account" => %{
            "current_password" => "firesale",
            "password" => "foobarbaz",
            "password_confirmation" => "bazbarfoo"
          }
        })

      assert get_flash(conn, :error) == "There was a problem updating your password"
      response = html_response(conn, 200)
      assert response =~ "does not match password"
    end

    test "PUT /accounts/password with valid attributes", %{conn: conn} do
      conn =
        conn
        |> put(account_path(conn, :password), %{
          "auth_account" => %{
            "current_password" => "firesale",
            "password" => "foobarbaz",
            "password_confirmation" => "foobarbaz"
          }
        })

      assert get_flash(conn, :success) == "Password updated"
    end
  end
end
