defmodule FrontEnd.App.WebhookControllerTest do
  use FrontEnd.ConnCase

  setup %{conn: conn} do
    oa = create_organization_account("customer")
    cust = oa.customer
    hook = insert(:webhook, organization: cust.organization)

    {:ok, oa: oa, conn: conn, cust: cust, org: cust.organization, hook: hook}
  end

  describe "webhook routes while unauthenticated" do
    test "GET /app/webhooks/new redirects and assigns flash", %{conn: conn} do
      conn =
        conn
        |> get(webhook_path(conn, :new))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "POST /app/webhooks redirects and assigns flash", %{conn: conn} do
      conn =
        conn
        |> post(webhook_path(conn, :create), %{webhook: params_for(:webhook)})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "GET /app/webhooks/:id/edit redirects and assigns flash", %{conn: conn, hook: hook} do
      conn =
        conn
        |> get(webhook_path(conn, :edit, hook.id))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "PATCH /app/webhooks/:id redirects and assigns flash", %{conn: conn, hook: hook} do
      conn =
        conn
        |> patch(webhook_path(conn, :update, hook.id), %{
          webhook: %{url: "http://example.com/idk/wut"}
        })

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "DELETE /app/webhooks/:id redirects and assigns flash", %{conn: conn, hook: hook} do
      conn =
        conn
        |> delete(webhook_path(conn, :delete, hook.id))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "webhook routes while authenticated as organization" do
    setup %{conn: conn, hook: hook} do
      conn =
        conn
        |> sign_in(hook.organization)

      {:ok, conn: conn, hook: hook}
    end

    test "GET /app/webhooks/new redirects and assigns flash", %{conn: conn} do
      conn =
        conn
        |> get(webhook_path(conn, :new))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "POST /app/webhooks redirects and assigns flash", %{conn: conn} do
      conn =
        conn
        |> post(webhook_path(conn, :create), %{webhook: params_for(:webhook)})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "GET /app/webhooks/:id/edit redirects and assigns flash", %{conn: conn, hook: hook} do
      conn =
        conn
        |> get(webhook_path(conn, :edit, hook.id))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "PATCH /app/webhooks/:id redirects and assigns flash", %{conn: conn, hook: hook} do
      conn =
        conn
        |> patch(webhook_path(conn, :update, hook.id), %{
          webhook: %{url: "http://example.com/idk/wut"}
        })

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "DELETE /app/webhooks/:id redirects and assigns flash", %{conn: conn, hook: hook} do
      conn =
        conn
        |> delete(webhook_path(conn, :delete, hook.id))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "webhook routes while authenticated as customer" do
    setup %{conn: conn, oa: oa, cust: cust, org: org} do
      conn =
        conn
        |> sign_in(oa)

      {:ok, conn: conn, cust: cust, org: org}
    end

    test "GET /app/webhooks/new displays editable webhook fields", %{conn: conn} do
      conn =
        conn
        |> get(webhook_path(conn, :new))

      res = html_response(conn, 200)
      assert res =~ "New Webhook"

      Connect.Webhook.valid_types()
      |> Enum.each(fn type ->
        assert res =~ "<option value=\"#{type}\">#{type}</option>"
      end)

      assert res =~
               "<input class=\"form-control\" id=\"webhook_url\" name=\"webhook[url]\" required=\"required\" type=\"url\">"

      assert res =~
               "<textarea class=\"form-control\" id=\"webhook_headers\" name=\"webhook[headers]\">\n{}</textarea>"
    end

    test "POST /app/webhooks with valid data creates a webhook", %{conn: conn, org: org} do
      webhook_params = params_for(:webhook, organization: org)

      conn =
        conn
        |> post(webhook_path(conn, :create), %{webhook: webhook_params})

      assert redirected_to(conn) == webhook_path(conn, :index)
      assert get_flash(conn, :success) == "Successfully created webhook"

      conn =
        conn
        |> get(webhook_path(conn, :index))

      res = html_response(conn, 200)
      assert res =~ webhook_params.type
      assert res =~ webhook_params.url
    end

    test "POST /app/webhooks with invalid data displays editable webhook fields and error flash",
         %{conn: conn} do
      conn =
        conn
        |> post(webhook_path(conn, :create), %{webhook: %{headers: "123"}})

      res = html_response(conn, 200)
      assert res =~ "New Webhook"
      assert res =~ "can&#39;t be blank"
      assert res =~ "must be a JSON object `{}`"
    end

    test "GET /app/webhooks/:id/edit displays editable webhook fields", %{
      conn: conn,
      hook: hook
    } do
      conn =
        conn
        |> get(webhook_path(conn, :edit, hook.id))

      res = html_response(conn, 200)
      assert res =~ "Editing Webhook"

      Connect.Webhook.valid_types()
      |> Enum.each(fn type ->
        if hook.type == type do
          assert res =~ "<option selected=\"selected\" value=\"#{type}\">#{type}</option>"
        else
          assert res =~ "<option value=\"#{type}\">#{type}</option>"
        end
      end)

      assert res =~
               "<input class=\"form-control\" id=\"webhook_url\" name=\"webhook[url]\" required=\"required\" type=\"url\" value=\"#{
                 hook.url
               }\">"

      headers_as_string =
        hook.headers
        |> Poison.encode!()
        |> Phoenix.HTML.html_escape()
        |> Phoenix.HTML.safe_to_string()

      assert res =~
               "<textarea class=\"form-control\" id=\"webhook_headers\" name=\"webhook[headers]\">\n#{
                 headers_as_string
               }</textarea>"
    end

    test "PATCH /app/webhooks/:id with valid data updates a webhook", %{
      conn: conn,
      hook: hook
    } do
      conn =
        conn
        |> patch(webhook_path(conn, :update, hook.id), %{
          webhook: %{url: "http://example.com/junk"}
        })

      assert redirected_to(conn) == webhook_path(conn, :index)
      assert get_flash(conn, :success) == "Successfully updated webhook"

      conn =
        conn
        |> get(redirected_to(conn))

      assert html_response(conn, 200) =~ "http://example.com/junk"
    end

    test "PATCH /app/webhooks/:id with invalid data displays editable webhook fields and error flash",
         %{conn: conn, hook: hook} do
      conn =
        conn
        |> patch(webhook_path(conn, :update, hook.id), %{webhook: %{url: nil, headers: "a"}})

      res = html_response(conn, 200)
      assert res =~ "Some errors were found."
      assert res =~ "Editing Webhook"
      assert res =~ "can&#39;t be blank"
      assert res =~ "invalid JSON"
    end

    test "DELETE /app/webhooks/:id removes webhook", %{conn: conn, hook: hook, org: org} do
      conn =
        conn
        |> delete(webhook_path(conn, :delete, hook.id))

      assert redirected_to(conn) == webhook_path(conn, :index)
      assert get_flash(conn, :success) == "Successfully removed webhook from #{org.name}"
    end
  end
end
