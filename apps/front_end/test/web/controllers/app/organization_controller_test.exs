defmodule FrontEnd.App.OrganizationControllerTest do
  use FrontEnd.ConnCase

  setup %{conn: conn} do
    oa = create_organization_account("customer")

    {:ok, org: oa.organization, cust: oa.customer, conn: conn, oa: oa}
  end

  describe "organization routes while unauthenticated" do
    test "GET /app/organizations/:urn/edit redirects with flash error", %{conn: conn, org: org} do
      conn =
        conn
        |> get(organization_path(conn, :edit, org.urn))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "PATCH /app/organizations/:urn", %{conn: conn, org: org} do
      conn =
        conn
        |> patch(organization_path(conn, :update, org.urn), %{organization: %{name: "FOOBAR"}})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "organization routes while authenticated as organization" do
    setup %{conn: conn, org: org} do
      conn =
        conn
        |> sign_in(org)

      {:ok, conn: conn, org: org}
    end

    test "GET /app/organizations/:urn/edit redirects with flash error", %{conn: conn, org: org} do
      conn =
        conn
        |> get(organization_path(conn, :edit, org.urn))

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end

    test "PATCH /app/organizations/:urn", %{conn: conn, org: org} do
      conn =
        conn
        |> patch(organization_path(conn, :update, org.urn), %{organization: %{name: "FOOBAR"}})

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Please log in"
    end
  end

  describe "organization routes while authenticated as a customer" do
    setup %{conn: conn, cust: cust, org: org, oa: oa} do
      conn =
        conn
        |> sign_in(oa)

      {:ok, conn: conn, org: org, cust: cust}
    end

    test "GET /app/organizations/:urn/edit displays editable organization data", %{
      conn: conn,
      org: org
    } do
      conn =
        conn
        |> get(organization_path(conn, :edit, org.urn))

      res = html_response(conn, 200)
      assert res =~ "Settings"
      assert res =~ "Templates"
      assert res =~ "Advanced"
      assert res =~ "Warning: Updating the URN value can affect pending connections"

      assert res =~
               "<input class=\"form-control\" id=\"organization_urn\" maxlength=\"50\" minlength=\"3\" name=\"organization[urn]\" required=\"required\" type=\"text\" value=\"#{
                 org.urn
               }\">"
    end

    test "PATCH /app/organizations/:urn with valid attrs updates an organization", %{
      conn: conn,
      org: org
    } do
      new_name = "BOOFARZAB"

      conn =
        conn
        |> patch(organization_path(conn, :update, org.urn), %{organization: %{name: new_name}})

      assert redirected_to(conn) == organization_path(conn, :edit, org.urn)
      assert get_flash(conn, :success) == "Successfully updated #{new_name}"

      conn =
        conn
        |> get(organization_path(conn, :edit, org.urn))

      res = html_response(conn, 200)

      assert res =~
               "<input class=\"form-control\" id=\"organization_name\" maxlength=\"255\" minlength=\"1\" name=\"organization[name]\" required=\"required\" type=\"text\" value=\"#{
                 new_name
               }\">"
    end
  end
end
