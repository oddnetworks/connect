defmodule FrontEnd.SessionControllerTest do
  use FrontEnd.ConnCase

  setup %{conn: conn} do
    oa = create_organization_account("customer")
    customer = oa.customer

    {:ok, conn: conn, customer: customer, oa: oa}
  end

  describe "GET /sessions/new when authenticated" do
    test "redirects to app dashboard", %{conn: conn, oa: oa} do
      conn =
        conn
        |> sign_in(oa)

      conn =
        conn
        |> get(session_path(conn, :new))

      assert redirected_to(conn) == dashboard_path(conn, :index)

      assert get_flash(conn, :info) == "Already logged in"
    end
  end

  describe "GET /sessions/new when unauthenticated" do
    test "displays login form", %{conn: conn} do
      conn =
        conn
        |> get(session_path(conn, :new))

      response = html_response(conn, 200)

      assert response =~ "Log In"
      assert response =~ "Account"
      assert response =~ "<form accept-charset=\"UTF-8\" action=\"/sessions\" method=\"post\">"

      assert response =~
               "<input class=\"form-control\" id=\"session_password\" name=\"session[password]\" placeholder=\"Password\" type=\"password\">"

      assert response =~
               "<input class=\"form-control\" id=\"session_email\" name=\"session[email]\" placeholder=\"Email\" type=\"email\">"

      assert response =~ "<button class=\"btn btn-primary\" type=\"submit\">Log In</button>"
    end
  end

  describe "POST /sessions when authenticated" do
    test "redirects to app dashboard", %{conn: conn, oa: oa} do
      conn =
        conn
        |> sign_in(oa)

      conn =
        conn
        |> post(session_path(conn, :create), %{
          session: %{
            email: "foo@example.com",
            password: "firesale"
          }
        })

      assert redirected_to(conn) == dashboard_path(conn, :index)

      assert get_flash(conn, :info) == "Already logged in"
    end
  end

  describe "POST /sessions with invalid username/password" do
    test "displays login form with error flash", %{conn: conn} do
      conn =
        conn
        |> post(session_path(conn, :create), %{
          session: %{email: "foo@example.com", password: "1234"}
        })

      assert redirected_to(conn) == session_path(conn, :new)
      assert get_flash(conn, :error) == "Invalid email/password combination"
    end
  end

  describe "POST /sessions with valid credentials" do
    test "redirects to app dashbaord", %{conn: conn, oa: oa} do
      conn =
        conn
        |> post(session_path(conn, :create), %{
          session: %{
            email: oa.account.email,
            password: "firesale"
          }
        })

      assert redirected_to(conn) == organization_account_path(conn, :index)
      assert get_flash(conn, :success) == "Welcome Back!"
    end
  end

  describe "DELETE /sessions" do
    test "signs out customer and redirects to main page", %{
      conn: conn,
      oa: oa,
      customer: customer
    } do
      conn =
        conn
        |> sign_in(oa)

      conn =
        conn
        |> delete(session_path(conn, :delete, customer.id))

      assert redirected_to(conn) == page_path(conn, :index)
      assert get_flash(conn, :info) == "You have successfully logged out"
      assert nil == Guardian.Plug.current_resource(conn)
    end
  end
end
