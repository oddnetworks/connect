defmodule FrontEnd.ErrorViewTest do
  use FrontEnd.ConnCase

  alias FrontEnd.ErrorView

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 400.json, 400.json-api" do
    assert render(ErrorView, "400.json", []) ==
             %{
               errors: [
                 %{
                   status: "400",
                   title: "Bad Request",
                   detail: "Invalid/malformed request"
                 }
               ]
             }

    assert render(ErrorView, "400.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "400",
                   title: "Bad Request",
                   detail: "Invalid/malformed request"
                 }
               ]
             }
  end

  test "renders 401.json, 401.json-api" do
    assert render(ErrorView, "401.json", []) ==
             %{
               errors: [
                 %{
                   status: "401",
                   title: "Unauthorized",
                   detail: "Invalid authorization"
                 }
               ]
             }

    assert render(ErrorView, "401.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "401",
                   title: "Unauthorized",
                   detail: "Invalid authorization"
                 }
               ]
             }
  end

  test "renders 403.json, 403.json-api" do
    assert render(ErrorView, "403.json", []) ==
             %{
               errors: [
                 %{
                   status: "403",
                   title: "Forbidden",
                   detail: "Invalid permissions"
                 }
               ]
             }

    assert render(ErrorView, "403.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "403",
                   title: "Forbidden",
                   detail: "Invalid permissions"
                 }
               ]
             }
  end

  test "renders 404.json, 404.json-api" do
    assert render(ErrorView, "404.json", []) ==
             %{
               errors: [
                 %{
                   status: "404",
                   title: "Not Found",
                   detail: "Resource not found"
                 }
               ]
             }

    assert render(ErrorView, "404.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "404",
                   title: "Not Found",
                   detail: "Resource not found"
                 }
               ]
             }
  end

  test "renders 422.json, 422.json-api" do
    assert render(ErrorView, "422.json", []) ==
             %{
               errors: [
                 %{
                   status: "422",
                   title: "Unprocessable Entity",
                   detail: "Entity data not valid"
                 }
               ]
             }

    assert render(ErrorView, "422.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "422",
                   title: "Unprocessable Entity",
                   detail: "Entity data not valid"
                 }
               ]
             }
  end

  test "renders 422.json, 422.json-api with a changeset" do
    changeset = %{
      errors: [
        {:field_name, {"a message", []}}
      ]
    }

    assert render(ErrorView, "422.json", reason: %{changeset: changeset}) ==
             %{
               errors: [
                 %{
                   status: "422",
                   title: "Unprocessable Entity",
                   detail: "Field name a message",
                   source: %{
                     pointer: "/data/attributes/field_name"
                   }
                 }
               ]
             }

    assert render(ErrorView, "422.json-api", reason: %{changeset: changeset}) ==
             %{
               errors: [
                 %{
                   status: "422",
                   title: "Unprocessable Entity",
                   detail: "Field name a message",
                   source: %{
                     pointer: "/data/attributes/field_name"
                   }
                 }
               ]
             }
  end

  test "render 500.json, 500.json-api" do
    assert render(ErrorView, "500.json", []) ==
             %{
               errors: [
                 %{
                   status: "500",
                   title: "Internal Error",
                   detail: "Internal server error"
                 }
               ]
             }

    assert render(ErrorView, "500.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "500",
                   title: "Internal Error",
                   detail: "Internal server error"
                 }
               ]
             }
  end

  @tag :skip
  test "render any other *.json, *.json-api error" do
    assert render(ErrorView, "503.json", []) ==
             %{
               errors: [
                 %{
                   status: "500",
                   title: "Internal Error",
                   detail: "Internal server error"
                 }
               ]
             }

    assert render(ErrorView, "503.json-api", []) ==
             %{
               errors: [
                 %{
                   status: "500",
                   title: "Internal Error",
                   detail: "Internal server error"
                 }
               ]
             }
  end

  @tag :skip
  test "renders 404.html", %{conn: _conn} do
    assert render_to_string(ErrorView, "404.html", []) == "Page not found"
  end

  @tag :skip
  test "renders 500.html", %{conn: _conn} do
    assert render_to_string(ErrorView, "505.html", []) == "Internal server error"
  end

  @tag :skip
  test "renders any other *.html", %{conn: _conn} do
    assert render_to_string(ErrorView, "505.html", []) == "Internal server error"
  end
end
