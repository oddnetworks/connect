defmodule Transactions.CreationServiceTest do
  use Transactions.Case

  alias Transactions.CreationService
  alias Connect.Repo

  describe "create_transaction/3" do
    test "produces Ecto.Multi to create a transaction" do
      external_identifier = "ext1"
      platform = "AMAZON"
      product_identifier = "prod1"
      receipt = %{"a" => "field"}

      transaction = %{
        "external_identifier" => external_identifier,
        "platform" => platform,
        "product_identifier" => product_identifier,
        "receipt" => receipt
      }

      organization_id = "org-id"
      email = "email@foo.com"
      multi = CreationService.create_transaction(organization_id, email, transaction)

      assert [
               device_user: {:run, {Transactions.DeviceUser, :insert_or_find, [^email]}},
               platform_identity:
                 {:run, {Transactions.PlatformIdentity, :insert_or_find, [^platform, nil]}},
               entitlement:
                 {:run,
                  {Transactions.Entitlement, :find_scoped_to_organization,
                   [^organization_id, ^platform, ^product_identifier]}},
               transaction:
                 {:run,
                  {Transactions.Transaction, :insert_or_find,
                   [^platform, ^external_identifier, ^receipt]}}
             ] = Ecto.Multi.to_list(multi)
    end
  end

  describe "run/3" do
    setup _tags do
      dev = insert(:device_user)

      platform =
        [
          "AMAZON",
          "APPLE",
          "GOOGLE",
          "ROKU",
          "WEB"
        ]
        |> Enum.random()

      product_identifier = "external-id-fhfh120930201239012"

      ent = insert(:entitlement, product_identifier: product_identifier, platform: platform)

      external_identifier = "76741c83-6ab9-4c1a-aa95-de1f0c066e26"

      platform_identity = "user-id-102390395498053840954890534859043"

      receipt = %{
        "derp" => 1
      }

      valid_data = %{
        "platform" => platform,
        "product_identifier" => product_identifier,
        "external_identifier" => external_identifier,
        "platform_identity" => platform_identity,
        "receipt" => receipt
      }

      {:ok, org: ent.organization, dev: dev, ent: ent, valid_data: valid_data}
    end

    test "given new DeviceUser email and PlatformIdentity, with valid Transaction data - creates DeviceUser, PlatformIdentity, and Transaction",
         %{org: org, valid_data: %{"platform" => platform} = valid_data, ent: ent} do
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      email = "example@example.com"

      assert {:ok,
              %{
                device_user: device_user,
                platform_identity: platform_identity,
                transaction: transaction
              }} = CreationService.run(org.id, email, valid_data)

      assert device_user.email == email
      assert platform_identity.device_user_id == device_user.id
      assert platform_identity.external_identifier == valid_data["platform_identity"]
      assert transaction.platform == platform
      assert transaction.entitlement_id == ent.id
      assert transaction.device_user_id == device_user.id
      assert transaction.receipt == valid_data["receipt"]
      assert transaction.external_identifier == valid_data["external_identifier"]
      assert transaction.invalidated_at == nil

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 2
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 1
    end

    test "given new DeviceUser email, with valid Transaction data - creates DeviceUser, and Transaction",
         %{org: org, valid_data: %{"platform" => platform} = data, ent: ent} do
      valid_data =
        data
        |> Map.drop(["platform_identity"])

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      email = "example@example.com"

      assert {:ok, %{device_user: device_user, platform_identity: nil, transaction: transaction}} =
               CreationService.run(org.id, email, valid_data)

      assert device_user.email == email
      assert transaction.platform == platform
      assert transaction.entitlement_id == ent.id
      assert transaction.device_user_id == device_user.id
      assert transaction.receipt == valid_data["receipt"]
      assert transaction.external_identifier == valid_data["external_identifier"]
      assert transaction.invalidated_at == nil

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 2
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 1
    end

    test "given new invalid DeviceUser email - returns :error with changeset", %{
      org: org,
      valid_data: valid_data
    } do
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      email = "example.com"

      assert {:error, :device_user, changeset} = CreationService.run(org.id, email, valid_data)

      [email: {"has invalid format", _}] = changeset.errors

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0
    end

    test "given existing DeviceUser email, with valid Transaction data - creates Transaction", %{
      ent: ent,
      org: org,
      dev: dev,
      valid_data: %{"platform" => platform} = valid_data
    } do
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      assert {:ok, %{device_user: device_user, transaction: transaction}} =
               CreationService.run(org.id, dev.email, valid_data)

      assert device_user == dev
      assert transaction.platform == platform
      assert transaction.entitlement_id == ent.id
      assert transaction.device_user_id == device_user.id
      assert transaction.receipt == valid_data["receipt"]
      assert transaction.external_identifier == valid_data["external_identifier"]
      assert transaction.invalidated_at == nil

      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
    end

    test "given duplicate external_identifier - returns :error", %{
      org: org,
      dev: dev,
      valid_data: valid_data
    } do
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      {:ok, _} = CreationService.run(org.id, dev.email, valid_data)

      assert {:error, :duplicate} = CreationService.run(org.id, dev.email, valid_data)

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 1
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 1
    end

    test "given unknown product_identifier - returns :error", %{
      org: org,
      dev: dev,
      valid_data: valid_data
    } do
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      invalid_data = Map.merge(valid_data, %{"product_identifier" => "NOTREAL"})

      assert {:error, :entitlement_not_found} =
               CreationService.run(org.id, dev.email, invalid_data)

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0
    end

    test "given invalid Transaction data - returns :error with changeset", %{
      org: org,
      dev: dev,
      valid_data: valid_data
    } do
      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0

      invalid_data =
        Map.merge(valid_data, %{
          "external_identifier" =>
            "25555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555554444"
        })

      assert {:error, :transaction, %Ecto.Changeset{}} =
               CreationService.run(org.id, dev.email, invalid_data)

      assert Repo.aggregate(Transactions.DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Transactions.PlatformIdentity, :count, :id) == 0
      assert Repo.aggregate(Transactions.Transaction, :count, :id) == 0
    end
  end
end
