defmodule Transactions.Validator.RokuTest do
  use Transactions.Case

  alias Transactions.Validator.Roku

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes/roku"

  setup_all do
    HTTPoison.start()
  end

  setup _tags do
    ExVCR.Config.cassette_library_dir(@cassette_dir)

    :ok
  end

  describe "validate/2" do
    setup do
      trans =
        build(
          :transaction,
          platform: "ROKU",
          external_identifier: "D59DE103-BFE1-43E5-AFE8-A7A0008C6BA8"
        )

      api_key = "16623F49189176488B31A4A000D17C005642"

      {:ok, trans: trans, api_key: api_key}
    end

    test "with valid transaction returns {:ok, transaction, changes}", %{
      trans: trans,
      api_key: api_key
    } do
      use_cassette "success" do
        {:ok, ^trans, changes} = Roku.validate(trans, %{api_key: api_key})
        assert changes |> Map.keys() == [:last_validated_at, :receipt]
      end
    end

    test "with expired transaction returns {:ok, transaction, changes}", %{
      trans: trans,
      api_key: api_key
    } do
      use_cassette "expired" do
        {:ok, ^trans, changes} = Roku.validate(trans, %{api_key: api_key})

        assert changes |> Map.keys() == [
                 :invalidated_at,
                 :invalidation_reason,
                 :last_validated_at,
                 :receipt
               ]

        assert changes[:invalidation_reason] == "Canceled/Expired"
      end
    end

    test "with cancelled and expired transaction returns {:ok, transaction, changes}", %{
      trans: trans,
      api_key: api_key
    } do
      use_cassette "cancelled_expired" do
        {:ok, ^trans, changes} = Roku.validate(trans, %{api_key: api_key})

        assert changes |> Map.keys() == [
                 :invalidated_at,
                 :invalidation_reason,
                 :last_validated_at,
                 :receipt
               ]

        assert changes[:invalidation_reason] == "Canceled/Expired"
      end
    end

    test "with cancelled transaction, but not expired returns {:ok, transaction, changes}", %{
      trans: trans,
      api_key: api_key
    } do
      use_cassette "cancelled_not_expired" do
        {:ok, ^trans, changes} = Roku.validate(trans, %{api_key: api_key})

        assert changes |> Map.keys() == [
                 :last_validated_at,
                 :receipt
               ]
      end
    end

    test "with invalid api key returns {:error, message}", %{trans: trans} do
      use_cassette "invalid_api_key" do
        {:error, message} =
          Roku.validate(trans, %{api_key: "99993F49189176488B31A4A000D17C00564A"})

        assert message == "Roku - Unable to process body"
      end
    end

    test "with invalid transaction id returns {:error, message}", %{
      trans: trans,
      api_key: api_key
    } do
      trans =
        trans
        |> Map.put(:external_identifier, "D59DE103-BFE1-43E5-AFE8-A7A0008C6BA7")

      use_cassette "invalid_transaction_id" do
        {:error, message} = Roku.validate(trans, %{api_key: api_key})
        assert message == "Roku - UNAUTHORIZED"
      end
    end
  end
end
