defmodule Transactions.Validator.AmazonTest do
  use Transactions.Case

  alias Transactions.Validator.Amazon

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes/amazon"

  setup_all do
    HTTPoison.start()
  end

  setup _tags do
    ExVCR.Config.cassette_library_dir(@cassette_dir)

    :ok
  end

  describe "validate/2" do
    setup do
      receipt = %{
        "user_id" => "my-user-id-idk"
      }

      settings =
        build(
          :organization_settings,
          amazon_shared_secret:
            "2:dbSJ286aE3mfwID7K57BIvY7DEPLxbu4FF-WwdZUBZku5-lrnLVfr-ce2Ltwwlp_:h8E8Y1EuDHbYUYWOz7ysag==",
          amazon_use_sandbox: false
        )

      entitlement =
        build(
          :entitlement,
          product_identifier: "com.oddnetworks.gold_monthly",
          organization: settings.organization,
          platform: "AMAZON"
        )

      trans =
        build(:transaction,
          platform: "AMAZON",
          receipt: receipt,
          entitlement: entitlement,
          external_identifier: "amazon-receipt-id"
        )

      {:ok, trans: trans, receipt: receipt, settings: settings}
    end

    test "when given invalid shared secret", %{trans: trans} do
      use_cassette "invalid_shared_secret" do
        assert {:error, "Invalid Amazon shared_secret"} =
                 Amazon.validate(trans, %{shared_secret: "bad", use_sandbox: false})
      end
    end

    test "when given invalid receipt id", %{trans: trans, settings: settings} do
      use_cassette "invalid_receipt_id" do
        assert {:ok, ^trans,
                %{
                  invalidated_at: invalidated_at,
                  invalidation_reason: "Invalid/Not Found by RVS",
                  last_validated_at: invalidated_at
                }} =
                 Amazon.validate(trans, %{
                   shared_secret: settings.amazon_shared_secret,
                   use_sandbox: false
                 })

        refute is_nil(invalidated_at)
      end
    end

    test "when given invalid user id", %{trans: trans, settings: settings} do
      use_cassette "invalid_user_id" do
        assert {:error, "Invalid Amazon user_id"} =
                 Amazon.validate(trans, %{
                   shared_secret: settings.amazon_shared_secret,
                   use_sandbox: false
                 })
      end
    end

    test "when given other amazon rvs error (500)", %{trans: trans, settings: settings} do
      use_cassette "rvs_500_error" do
        assert {:error, "Invalid Amazon response"} =
                 Amazon.validate(trans, %{
                   shared_secret: settings.amazon_shared_secret,
                   use_sandbox: false
                 })
      end
    end

    test "when receipt comes back valid", %{trans: trans, settings: settings} do
      use_cassette "valid_receipt_response" do
        assert {:ok, ^trans,
                changes = %{
                  last_validated_at: last_validated_at,
                  receipt: %{
                    beta_product: true,
                    cancel_date: nil,
                    parent_product_id: nil,
                    product_id: "sub1",
                    product_type: "SUBSCRIPTION",
                    purchase_date: "2014-05-22T18:44:01.000Z",
                    quantity: nil,
                    receipt_id: "JyGJ5iEtYgFu1ngnQovTqSIHQxR53GsMLqkR1tKLp5c=:3:11",
                    renewal_date: nil,
                    term: "1 Week",
                    term_sku: "sub1-weekly",
                    test_transaction: true,
                    user_id: "my-user-id-idk"
                  }
                }} =
                 Amazon.validate(trans, %{
                   shared_secret: settings.amazon_shared_secret,
                   use_sandbox: false
                 })

        refute Map.has_key?(changes, :invalidated_at)
        refute Map.has_key?(changes, :invalidation_reason)

        refute is_nil(last_validated_at)
      end
    end

    test "when receipt comes back invalid", %{trans: trans, settings: settings} do
      use_cassette "invalid_receipt_response" do
        assert {:ok, ^trans,
                %{
                  invalidated_at: invalidated_at,
                  invalidation_reason: "Subscription Cancelled",
                  last_validated_at: invalidated_at,
                  receipt: %{
                    beta_product: true,
                    cancel_date: "2014-05-22T18:46:11.000Z",
                    parent_product_id: nil,
                    product_id: "sub1",
                    product_type: "SUBSCRIPTION",
                    purchase_date: "2014-05-22T18:44:01.000Z",
                    quantity: nil,
                    receipt_id: "JyGJ5iEtYgFu1ngnQovTqSIHQxR53GsMLqkR1tKLp5c=:3:11",
                    renewal_date: nil,
                    term: "1 Week",
                    term_sku: "sub1-weekly",
                    test_transaction: true,
                    user_id: "my-user-id-idk"
                  }
                }} =
                 Amazon.validate(trans, %{
                   shared_secret: settings.amazon_shared_secret,
                   use_sandbox: false
                 })

        refute is_nil(invalidated_at)
      end
    end
  end
end
