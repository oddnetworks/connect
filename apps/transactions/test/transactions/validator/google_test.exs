defmodule Transactions.Validator.GoogleTest do
  use Transactions.Case

  alias Transactions.Validator.Google

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes/google"

  setup_all do
    HTTPoison.start()
  end

  setup _tags do
    ExVCR.Config.cassette_library_dir(@cassette_dir)

    :ok
  end

  describe "validate/2" do
    setup do
      receipt = %{
        "token" =>
          "mgcagcmcjcjjceafhdhmmbnn.AO-J1Oxl8LpCaZiHjO7_LgZpsZcjbg4pHXNZ6CAauvO-Y9TExp76oHMTKWI_QfvyLegA7pCm0ff-eFHzujo8z4Ho7xiiWhVFvwowYLd217qkFqTJiUZliYRFiPMRiqpQ6VstckSiHhll"
      }

      settings =
        build(
          :organization_settings,
          google_package_name: "com.oddnetworks.bills",
          google_service_account:
            "odd-connect-staging-test@oddnetworks-bills.iam.gserviceaccount.com",
          google_service_account_private_key:
            "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCiV8hD9Mrc4AIA\nEbx7ePBm/siL3gUq2dkMUHTOJVvOLFcu5E1WGZTB2lU3t9QFjyIsNj5fJSML4caD\nsDOVIBYKUSbKxaMloufr/FAgO2Arjhf0JJIrROSFZUyUmfvalmL17Q8W6FkKumKl\nQiTj+4HSlk/muHrt4K6ysIHvlcKQHpSEH38/bjYTvrSyXWQOvhxxnnHFesSiODYs\n4ztY2nJHYaAOweA/9Rv7IAa7YuuOwAJKgT9f2l53fdL9W+nZXWtrD2TQ5ozqMCuR\no1a4E3nLA5KPAPrkoAAgSJuncc4eyjPNhHx4/W7MWTNGuLfsGujgKDVtEf9sqlgK\ngktXV+xDAgMBAAECggEABr2kXXfFntYCUQf5uTc0c1QkvSyvZ8xsiqMiX14+20qb\nXz3a5DZxQ5dZIH8IVXz6/Quj/yJDSilYju2z8UY555zfrSTbB3i0EiD1WIopRuIl\n31z6RFJXNslrOPKZpKkdP3tVn55vTA+EgbqYz/xeCJ4gf987HF4kSfhARAyUa+np\n/TnsgyyvnQEd7+YDlg2I/MYvUdm/P6MeJYcLbaTPXx9RwPAD4ebVu1/s8e+ukjaN\nb0CKmLVU2leIj0/UqjVhewmz6DO0K+8KI5lzPyBpKGDwUamXHtLehSAoRIoyp5Ns\npP4HK8QAT6g8xQsdIsqRSsfdIn38faDF+tzNuAhgCQKBgQDQ8QnhWdvcDSaJ4CY2\n4SLDf6JU266+cZS4uaKKBEl1Cb90n3Sqnz/AQsM51yY0NDZpqEW+UAWzlVOo5kGr\n8yWfM+t+HzN309qr2lWWz1ex7EHPmlBRmYXdb7PC8IYoSi/yAJUZxYny+e50p5Xy\njIKkNOeTkTmb2d38aecWJASppQKBgQDG6AA5OhMTwnpUeWRe5Vshybmqfk3pe1TU\n/pzT+AwEeFLi4OSqt7XXm7D3vX4HrCw+bw7FFhFAY7ZDKQNk5SMT/1C9DHIalpH4\n2I0LpfOKarth5agxrJYII91UyWDQ6CxbNBfqq3Wb3FEC7N+7C2TVICG116nIDQTn\nGiwKJnhJxwKBgBjNxIKADF2fIggUF/VD3MCgkkDIo4HwMduMDgOcZqwCMDxEAbGP\nnh3QzyQ6qkpgOGVqmHzzOvoKrBfM3dbv0nOH0ljyk0WSXM8x6B8zFhIFHfrrMsW9\nF+slq41kyEhYJkDwWMLqdGnnyasHigF1NL0Rmrwl7VTXFEc2uzwhonwRAoGAcR/X\nqLRvzygnXM3hmNsG1pOdJetJsoWzPKRGW4PSieRI98ICK1nqcOKaZnv+Jqz0SNnu\n44Zmb2GGcnHr/UEYjzIBIFPovVhvUsEOjtZmSY3kXow3+XB3r899/M+/YXD6cTCS\nfaPgcrF93ZUMtZuIUHJC2eH7LMzCwhEvhOJWPVcCgYEAh9kOAPSBuCKSQRcxA5SC\n+94kTYIPXINlOnoCmDTmCXel8Ud/OqB3QQYbdugSfhNmU6dVKPHERfLUVNA6bUBF\n5TfV479g5xwzIPTZ//qsni4wCSqxNL2uQUU2IMCY9PvqFV9CBfSDlm9ummpRF7Zc\n/BRKgq4O60HCmU47WSR6Cek=\n-----END PRIVATE KEY-----\n"
        )

      entitlement =
        build(
          :entitlement,
          product_identifier: "gold_monthly",
          organization: settings.organization,
          platform: "GOOGLE"
        )

      trans = build(:transaction, platform: "GOOGLE", receipt: receipt, entitlement: entitlement)

      {:ok, trans: trans, receipt: receipt, settings: settings}
    end

    test "when given invalid service account returns error", %{trans: trans, settings: settings} do
      use_cassette "invalid_service_account" do
        assert {:error,
                "Service Account - 401 - invalid_client - The OAuth client was not found."} ==
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: "bad_service_account",
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )
      end
    end

    test "when given invalid service account private key returns error", %{
      trans: trans,
      settings: settings
    } do
      use_cassette "invalid_service_account_private_key" do
        assert {:error, "Google Service Account - Bad/Missing Service Account Private Key"} ==
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: settings.google_service_account,
                     service_account_private_key: "bad_service_account_private_key"
                   }
                 )
      end
    end

    test "when given invalid package name returns error", %{trans: trans, settings: settings} do
      use_cassette "invalid_package" do
        assert {:error,
                "Android Publisher - 404 - No application was found for the given package name."} ==
                 Google.validate(
                   trans,
                   %{
                     package_name: "bad.package.name",
                     service_account: settings.google_service_account,
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )
      end
    end

    test "when given invalid subscription_id", %{trans: trans, settings: settings} do
      use_cassette "invalid_subscription_id" do
        ent =
          build(
            :entitlement,
            product_identifier: "bad_subscription_id",
            organization: settings.organization,
            platform: "GOOGLE"
          )

        trans = %Transactions.Transaction{trans | entitlement: ent}

        assert {:error,
                "Android Publisher - 400 - The subscription purchase token does not match the subscription ID."} ==
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: settings.google_service_account,
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )
      end
    end

    test "when given invalid token", %{trans: trans, settings: settings} do
      use_cassette "invalid_token" do
        trans = %Transactions.Transaction{trans | receipt: %{token: "12345"}}

        assert {:error, "Android Publisher - 404 - Invalid Subscription Token"} ==
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: settings.google_service_account,
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )
      end
    end

    test "when subscription cancelled and expired", %{trans: trans, settings: settings} do
      use_cassette "cancelled_expired_subscription" do
        assert {:ok, ^trans, changes} =
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: settings.google_service_account,
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )

        assert Map.keys(changes) == [
                 :invalidated_at,
                 :invalidation_reason,
                 :last_validated_at,
                 :receipt
               ]

        refute is_nil(changes.receipt.token)
      end
    end

    test "when subscription cancelled, but still valid", %{trans: trans, settings: settings} do
      use_cassette "cancelled_still_valid_subscription" do
        assert {:ok, ^trans, changes} =
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: settings.google_service_account,
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )

        assert Map.keys(changes) == [:last_validated_at, :receipt]
        refute is_nil(changes.receipt.token)
      end
    end

    test "when subscription valid", %{trans: trans, settings: settings} do
      use_cassette "valid_subscription" do
        assert {:ok, ^trans, changes} =
                 Google.validate(
                   trans,
                   %{
                     package_name: settings.google_package_name,
                     service_account: settings.google_service_account,
                     service_account_private_key: settings.google_service_account_private_key
                   }
                 )

        assert Map.keys(changes) == [:last_validated_at, :receipt]
        refute is_nil(changes.receipt.token)
      end
    end
  end
end
