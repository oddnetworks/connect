defmodule Transactions.Validator.AppleTest do
  use Transactions.Case

  alias Transactions.Validator.Apple

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @cassette_dir "test/fixture/vcr_cassettes/apple"

  setup_all do
    HTTPoison.start()
  end

  setup _tags do
    ExVCR.Config.cassette_library_dir(@cassette_dir)

    :ok
  end

  describe "validate/2" do
    setup do
      latest_receipt =
        %{
          quantity: 1,
          product_id: "thing",
          transaction_id: "12345",
          original_transaction_id: "12345",
          purchase_date: "2017-10-01T01:45:00Z",
          original_purchase_date: "2017-10-1T01:45:00Z",
          expires_date: "2017-11-1T01:45:00Z",
          cancellation_date: nil,
          app_item_id: "thing1",
          version_external_identifier: "version_external_identifier",
          web_order_line_item_id: "web_order_line_item_id"
        }
        |> JOSE.encode()
        |> Base.encode64()

      receipt = %{
        "latest_receipt" => latest_receipt
      }

      entitlement =
        build(
          :entitlement,
          product_identifier: "com.oddconnect.monthly_subscription",
          platform: "APPLE"
        )

      trans =
        build(
          :transaction,
          platform: entitlement.platform,
          receipt: receipt,
          entitlement: entitlement
        )

      {:ok, trans: trans}
    end

    test "when given invalid receipt data returns error", %{trans: trans} do
      use_cassette "failure" do
        assert {:error, "Apple - The data in the receipt-data property was malformed or missing."} ==
                 Apple.validate(trans, %{shared_secret: "herpderp", use_sandbox: false})
      end
    end

    test "when given valid receipt data/password and transaction is invalid due to cancellation, returns success with changes",
         %{trans: trans} do
      use_cassette "success_invalid_cancelled" do
        assert {:ok, ^trans, changes} =
                 Apple.validate(trans, %{shared_secret: "herpderp", use_sandbox: false})

        assert Map.keys(changes) == [
                 :invalidated_at,
                 :invalidation_reason,
                 :last_validated_at,
                 :receipt
               ]

        assert changes[:invalidation_reason] == "Customer canceled their subscription."
        assert changes[:receipt][:cancellation_reason] == "1"
        assert changes[:receipt][:is_trial_period] == "false"
        assert changes[:receipt][:latest_receipt] == "Zm9vYmFy"
        assert changes[:receipt][:original_transaction_id] == "1000000235888600"
        assert changes[:receipt][:product_id] == "com.oddconnect.monthly_subscription"
        assert changes[:receipt][:quantity] == "1"
        assert changes[:receipt][:transaction_id] == "1000000235894410"
        assert changes[:receipt][:web_order_line_item_id] == "1000000033229426"
        refute is_nil(changes[:receipt][:cancellation_date])
        refute is_nil(changes[:receipt][:expires_date])
        refute is_nil(changes[:receipt][:purchase_date])
        refute is_nil(changes[:receipt][:original_purchase_date])
      end
    end

    test "when given valid receipt data/password and transaction is invalid due to expiration, returns success with changes",
         %{trans: trans} do
      use_cassette "success_invalid_expired" do
        assert {:ok, ^trans, changes} =
                 Apple.validate(trans, %{shared_secret: "herpderp", use_sandbox: false})

        assert Map.keys(changes) == [
                 :invalidated_at,
                 :invalidation_reason,
                 :last_validated_at,
                 :receipt
               ]

        assert changes[:invalidation_reason] ==
                 "Billing error; for example customer’s payment information was no longer valid."

        assert changes[:receipt][:product_id] == "com.oddconnect.monthly_subscription"
        assert changes[:receipt][:expiration_intent] == "2"
        assert changes[:receipt][:is_trial_period] == "false"
        assert changes[:receipt][:latest_receipt] == "Zm9vYmFy"
        assert changes[:receipt][:original_transaction_id] == "1000000235888600"
        assert changes[:receipt][:quantity] == "1"
        assert changes[:receipt][:transaction_id] == "1000000235894410"
        assert changes[:receipt][:web_order_line_item_id] == "1000000033229426"
        refute is_nil(changes[:receipt][:expires_date])
        refute is_nil(changes[:receipt][:purchase_date])
      end
    end

    test "when given valid receipt data/password and transaction is valid, returns success with changes",
         %{trans: trans} do
      use_cassette "success_valid" do
        assert {:ok, ^trans, changes} =
                 Apple.validate(trans, %{shared_secret: "herpderp", use_sandbox: false})

        assert Map.keys(changes) == [:last_validated_at, :receipt]
        assert changes[:receipt][:is_trial_period] == "false"
        assert changes[:receipt][:latest_receipt] == "Zm9vYmFy"
        assert changes[:receipt][:original_transaction_id] == "1000000235888600"
        assert changes[:receipt][:product_id] == "com.oddconnect.monthly_subscription"
        assert changes[:receipt][:quantity] == "1"
        assert changes[:receipt][:transaction_id] == "1000000235894410"
        assert changes[:receipt][:web_order_line_item_id] == "1000000033229426"
        refute is_nil(changes[:receipt][:expires_date])
        refute is_nil(changes[:receipt][:purchase_date])
        refute is_nil(changes[:receipt][:original_purchase_date])
      end
    end
  end
end
