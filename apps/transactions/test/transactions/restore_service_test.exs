defmodule Transactions.RestoreServiceTest do
  use Transactions.Case

  import ExUnit.CaptureLog

  alias Transactions.RestoreService

  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @apple_cassette_dir "test/fixture/vcr_cassettes/apple"
  @amazon_cassette_dir "test/fixture/vcr_cassettes/amazon"

  setup_all do
    HTTPoison.start()
  end

  describe "run/2 transaction does not exist - can't create" do
    test "returns error" do
      assert capture_log([level: :error], fn ->
               assert {:error, "Transaction Not Found - Unable To Create"} =
                        RestoreService.run(
                          %{
                            "external_identifier" => "9b8d26d5-664e-4be7-b4ef-a1ef386ec1dc",
                            "platform" => "APPLE",
                            "receipt" => %{}
                          },
                          "ca30b62e-9c99-43a5-92a9-9e3b376db449"
                        )
             end) =~
               "Transactions.RestoreService Failed: Transaction Not Found - Unable To Create"
    end
  end

  describe "run/2 transaction does not exist - failed to create" do
    test "returns error" do
      assert capture_log([level: :error], fn ->
               assert {:error, "Transaction Not Found - Unable To Match Platform Identity"} =
                        RestoreService.run(
                          %{
                            "external_identifier" => "9b8d26d5-664e-4be7-b4ef-a1ef386ec1dc",
                            "product_identifier" => "amazon.product.one",
                            "platform" => "AMAZON",
                            "platform_identity" => "amazon.user.account",
                            "receipt" => %{}
                          },
                          "ca30b62e-9c99-43a5-92a9-9e3b376db449"
                        )
             end) =~
               "Transactions.RestoreService Failed: Transaction Not Found - Unable To Match Platform Identity"
    end
  end

  describe "run/2 platform not restorable" do
    setup _tags do
      trans = insert(:transaction, platform: "ROKU")
      {:ok, %{trans: trans}}
    end

    test "returns error", %{trans: trans} do
      assert capture_log([level: :error], fn ->
               assert {:error, "Transactions from \"ROKU\" Platform are not able to be restored"} =
                        RestoreService.run(
                          %{
                            "external_identifier" => trans.external_identifier,
                            "platform" => "ROKU",
                            "receipt" => %{}
                          },
                          trans.entitlement.organization.id
                        )
             end) =~
               "Transactions.RestoreService Failed: Transactions from \"ROKU\" Platform are not able to be restored"
    end
  end

  describe "run/2 AMAZON transaction doesn't exist - creatable" do
    setup _tags do
      ExVCR.Config.cassette_library_dir(@amazon_cassette_dir)

      org = insert(:organization)

      Ecto.Changeset.change(org.settings,
        amazon_shared_secret:
          "2:dbSJ286aE3mfwID7K57BIvY7DEPLxbu4FF-WwdZUBZku5-lrnLVfr-ce2Ltwwlp_:h8E8Y1EuDHbYUYWOz7ysag==",
        amazon_use_sandbox: false
      )
      |> Connect.Repo.update!()

      ent =
        insert(:entitlement,
          product_identifier: "com.amazon.subscription",
          platform: "AMAZON",
          organization: org
        )

      du = insert(:device_user)

      pi =
        insert(:platform_identity,
          device_user: du,
          platform: "AMAZON",
          external_identifier: "user-id-amazon-appstore-123"
        )

      {:ok, org: org, ent: ent, du: du, pi: pi}
    end

    test "invalid receipt returns error - creates transaction and invalidates", %{
      ent: ent,
      pi: pi
    } do
      use_cassette "invalid_receipt_id" do
        assert capture_log([level: :info], fn ->
                 assert {:ok,
                         %{
                           invalidated_at: invalidated_at,
                           invalidation_reason: "Invalid/Not Found by RVS"
                         }} =
                          RestoreService.run(
                            %{
                              "external_identifier" => "amazon-receipt-id",
                              "receipt" => %{
                                "user_id" => "my-user-id-idk"
                              },
                              "product_identifier" => ent.product_identifier,
                              "platform" => ent.platform,
                              "platform_identity" => pi.external_identifier
                            },
                            ent.organization_id
                          )

                 refute is_nil(invalidated_at)
               end) =~ "Transactions.RestoreService Successful"
      end
    end

    test "valid canceled receipt returns success", %{ent: ent, pi: pi} do
      use_cassette "invalid_receipt_response" do
        assert capture_log([level: :info], fn ->
                 assert {:ok,
                         %{
                           invalidated_at: invalidated_at,
                           invalidation_reason: "Subscription Cancelled"
                         }} =
                          RestoreService.run(
                            %{
                              "external_identifier" => "amazon-receipt-id",
                              "receipt" => %{
                                "user_id" => "my-user-id-idk"
                              },
                              "product_identifier" => ent.product_identifier,
                              "platform" => ent.platform,
                              "platform_identity" => pi.external_identifier
                            },
                            ent.organization_id
                          )

                 refute is_nil(invalidated_at)
               end) =~ "Transactions.RestoreService Successful"
      end
    end

    test "valid subscribed receipt returns success", %{ent: ent, pi: pi} do
      use_cassette "valid_receipt_response" do
        assert capture_log([level: :info], fn ->
                 assert {:ok,
                         %{
                           invalidated_at: nil,
                           invalidation_reason: nil
                         }} =
                          RestoreService.run(
                            %{
                              "external_identifier" => "amazon-receipt-id",
                              "receipt" => %{
                                "user_id" => "my-user-id-idk"
                              },
                              "product_identifier" => ent.product_identifier,
                              "platform" => ent.platform,
                              "platform_identity" => pi.external_identifier
                            },
                            ent.organization_id
                          )
               end) =~ "Transactions.RestoreService Successful"
      end
    end
  end

  describe "run/2 APPLE transaction exists" do
    setup _tags do
      ExVCR.Config.cassette_library_dir(@apple_cassette_dir)

      latest_receipt =
        %{
          quantity: 1,
          product_id: "thing",
          transaction_id: "12345",
          original_transaction_id: "12345",
          purchase_date: "2017-10-01T01:45:00Z",
          original_purchase_date: "2017-10-1T01:45:00Z",
          expires_date: "2017-11-1T01:45:00Z",
          cancellation_date: nil,
          app_item_id: "thing1",
          version_external_identifier: "version_external_identifier",
          web_order_line_item_id: "web_order_line_item_id"
        }
        |> JOSE.encode()
        |> Base.encode64()

      receipt = %{
        "latest_receipt" => latest_receipt
      }

      org = insert(:organization)

      ent =
        insert(
          :entitlement,
          product_identifier: "com.oddconnect.monthly_subscription",
          platform: "APPLE",
          organization: org
        )

      trans =
        insert(
          :transaction,
          platform: ent.platform,
          receipt: receipt,
          entitlement: ent
        )

      {:ok, trans: trans, org: org}
    end

    test "invalid receipt returns error", %{
      trans: trans,
      org: %{id: organization_id}
    } do
      use_cassette "failure" do
        assert capture_log([level: :error], fn ->
                 assert {:error,
                         "Apple - The data in the receipt-data property was malformed or missing."} =
                          RestoreService.run(
                            %{
                              "external_identifier" => trans.external_identifier,
                              "platform" => trans.platform,
                              "receipt" => trans.receipt
                            },
                            organization_id
                          )
               end) =~
                 "Transactions.RestoreService Failed: Apple - The data in the receipt-data property was malformed or missing."
      end
    end

    test "valid cancelled receipt returns success", %{
      trans: trans,
      org: %{id: organization_id}
    } do
      use_cassette "success_invalid_cancelled" do
        assert {:ok, _transaction} =
                 RestoreService.run(
                   %{
                     "external_identifier" => trans.external_identifier,
                     "platform" => trans.platform,
                     "receipt" => trans.receipt
                   },
                   organization_id
                 )
      end
    end

    test "valid expired receipt returns success", %{
      trans: trans,
      org: %{id: organization_id}
    } do
      use_cassette "success_invalid_expired" do
        assert {:ok, _transaction} =
                 RestoreService.run(
                   %{
                     "external_identifier" => trans.external_identifier,
                     "platform" => trans.platform,
                     "receipt" => trans.receipt
                   },
                   organization_id
                 )
      end
    end

    test "valid receipt returns success", %{
      trans: trans,
      org: %{id: organization_id}
    } do
      use_cassette "success_valid" do
        assert {:ok, _transaction} =
                 RestoreService.run(
                   %{
                     "external_identifier" => trans.external_identifier,
                     "platform" => trans.platform,
                     "receipt" => trans.receipt
                   },
                   organization_id
                 )
      end
    end
  end
end
