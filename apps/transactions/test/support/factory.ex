defmodule Transactions.Factory do
  use ExMachina.Ecto, repo: Connect.Repo

  alias Transactions.{
    DeviceUser,
    Entitlement,
    Organization,
    PlatformIdentity,
    Transaction
  }

  def device_user_factory do
    %DeviceUser{
      email: sequence(:device_user_email, &"devuser-#{&1}@foo.com")
    }
  end

  def platform_identity_factory do
    platform =
      [
        "AMAZON",
        "APPLE",
        "GOOGLE",
        "ROKU",
        "WEB"
      ]
      |> Enum.random()

    %PlatformIdentity{
      device_user: build(:device_user),
      platform: platform,
      external_identifier: sequence(:platform_identity, &"platform-id-#{&1}")
    }
  end

  def entitlement_factory do
    org = build(:organization)

    platform =
      [
        "AMAZON",
        "APPLE",
        "GOOGLE",
        "ROKU",
        "WEB"
      ]
      |> Enum.random()

    type =
      [
        "subscription",
        "asset"
      ]
      |> Enum.random()

    %Entitlement{
      type: type,
      organization: org,
      name: sequence(:entitlement_name, &"#{type |> String.capitalize()} Entitlement #{&1}"),
      platform: platform,
      entitlement_identifier: sequence(:entitlement_entitlement_identifier, &"external-id-#{&1}"),
      product_identifier: sequence(:entitlement_product_identifier, &"product-id-#{&1}")
    }
  end

  def organization_factory do
    name = sequence(:organization_name, &"Org #{&1}")

    %Organization{
      name: name,
      urn: Slugger.slugify(name |> String.downcase()),
      settings: %Organization.Settings{}
    }
  end

  def organization_settings_factory do
    %Organization.Settings{
      amazon_shared_secret: "fake-amazon-shared-secret",
      amazon_use_sandbox: false,
      in_review_mode: false,
      use_apple_sandbox_environment: false,
      apple_app_shared_secret: "fake-shared-secret",
      roku_api_key: "fake-roku-api-key",
      google_package_name: "com.oddnetworks.test",
      google_service_account: "fake@fake.iam.gserviceaccount.com",
      google_service_account_private_key: ~S"""
      -----BEGIN RSA PRIVATE KEY-----
      MIIEpQIBAAKCAQEAzfCfOMnDGVQWDdf0rWjs+fpBoH65Alrpzy1ZzXgsfP7I5C+A
      8fohyS4XAIrakHAX0o3Fg5KoQX+SNPO2/z0nDz+oczWMR6xT4KwL4jtP6G2/jbJr
      LEH8nraSS0wRK3HhjdbD27Oo0E7TW6Kl+Pjccz6vO5zuWo6st8qrY/CGkiKl3x25
      q8coXuk8QuWjodS2k52eGxFUbkrK0qw5XWwyqbyUfUAHsKAZdJEBbSlO6lrqpAlu
      TKJ9byBGd59UwMKzGpdm0RyZY9NcGid8q6YyISWfuLo18beVph49DC7i/bz8LZg6
      ky0+FGdz4fZlBRlGn1RyXdRvKbrssrn0V++u0QIDAQABAoIBAQDEwHAOXE0GZNyR
      K1K7XjSR+3M5t/hf9KXO5wAWGws9/FnIEE58YMJop50YSpDYUA+ifQpIUxRnR09T
      QckC/9TSb7orH+y/CTg1vYm5AXutjzTkVqKn7P9CTyFjyO/advnMfnQu8e2F3tl1
      geZaAMD1Zd0a8iqS4B7JlVq7p6+Z6sMv0qEkhClTxv2gR7YbFMoNZdODuRNoLStT
      beRsFywP53tZ6CgAB3GxptxVghc0BL68DgQ4tSSBzIVX0u0IInBk0YR+SK3kbcPF
      T/UkmVLiNv/T59EYTV+PKs3bZzlPrkhpqQDT0pdEdA+VA5B76k8dOZ6AmmqRM+Fo
      Yuat2sx1AoGBAO/kXCh6wlIglE4wYtsVgrrTp3Jhr1ppzQVP84YxvDOr0an8YC4u
      dnjAGMGmU4fj6muO5Qki4XkQsSvJUs9yxK0dHIh3G4FkMNavF9Sod65LPyZjFGgz
      WHjpkuFnB8PJ9Psnh5oLsTpTuFynCfLBf+YIW/xmGaOhy38hcBgoENZvAoGBANvE
      o9r6XX+9tgZKx+/AI8LaHe7QXMRoHKkz541F/shszGfiERS5Fk9qNMEjhJp4nTCj
      riOk8Cp/3c6JjKWkJktFQ5XQ/5euZ17WeV2KTd9kxNyBDtDDvwLYYMywsMc+k6xs
      JcmnNddtim+stHNpNxU9FFq6fac/BGd2T4S6hG6/AoGBAIU/AHlzhk/7zKSJd79c
      +VowigecfrvnnHtg51EYewLwYuraRcohAKkdeiLF8gu+6syHeGxUtYx7Ww2JK7gS
      dW/6hYi/L8X1ErOMWd5DHQbwwMjfq8wdCP1QL8eHZOa5XJvMXyOPbweIzRSZq4rt
      M89fwUg2pfMfwVqCBjNEveV9AoGASiAsSFr0GHcLo1mr0GEVbs6VFAzxjUm0iRm5
      uzvlsQKJe2yT6FJ5hPXEr3RkcJnIIzTCMsGaRCt2zAg3CVOxmufU4P5qh2XK9gEP
      VI6mr3fUsq9RKPctQS9sjv310m537stXf4nHrk8DR6GQB/FwI9jDOcr3QOcPfDo6
      ngEvVA8CgYEA0gh2jI9Ufqghvy++2TQv1+dsX0oJ6z3Uf6FwlxhI7MhtB3fIXDud
      sB+6+D1b80ksI6sZQ9kirK2PxoEZAsaEiPlZZw6uWaSBJSOUmADEX6pKtbVT5Txt
      pCe5nofYucVxRu3WwMmNeFmz9+iM2TULu7ar0VY4+NUUgcinbsJIujg=
      -----END RSA PRIVATE KEY-----
      """
    }
  end

  def transaction_factory do
    entitlement = build(:entitlement)

    %Transaction{
      platform: entitlement.platform,
      device_user: build(:device_user),
      entitlement: entitlement,
      receipt: %{},
      external_identifier: sequence(:transaction_external_identifier, &"external-id-#{&1}"),
      last_validated_at: DateTime.utc_now(),
      invalidated_at: nil,
      invalidation_reason: nil
    }
  end
end
