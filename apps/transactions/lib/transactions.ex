defmodule Transactions do
  @moduledoc """
  Responsible for Transaction related logic within Connect.
  """

  alias Transactions.{DeviceUser, Entitlement, Transaction}
  alias Connect.Repo
  import Ecto.Query, only: [from: 2]

  @webhook_module Application.get_env(:transactions, :webhook_module) || Webhooks

  def list_paged(organization_id, params) do
    query =
      from(
        t in Transaction,
        join: d in DeviceUser,
        on: d.id == t.device_user_id,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where: e.organization_id == ^organization_id and is_nil(t.deleted_at),
        order_by: [desc: t.inserted_at],
        select: %{
          entitlement_id: field(e, :id),
          device_user_id: field(d, :id),
          transaction_id: field(t, :id),
          product_identifier: field(e, :product_identifier),
          platform: field(t, :platform),
          entitlement: field(e, :name),
          entitlement_identifier: field(e, :entitlement_identifier),
          invalidated_at: field(t, :invalidated_at),
          invalidation_reason: field(t, :invalidation_reason),
          email: field(d, :email),
          date: field(t, :inserted_at)
        }
      )

    query
    |> Repo.paginate(params)
  end

  @doc """
  Find a specific Transaction within an Organization
  """
  def find(organization_id, id) do
    query =
      from(
        t in Transaction,
        left_join: e in Entitlement,
        on: t.entitlement_id == e.id,
        where: e.organization_id == ^organization_id and t.id == ^id and is_nil(t.deleted_at),
        preload: [:device_user, :entitlement]
      )

    query
    |> Repo.one()
  end

  @doc """
  List all Transactions within an Organization scoped to a specific DeviceUser
  """
  def list_device_user_transactions(organization_id, email) do
    query =
      from(
        t in Transaction,
        distinct: t.id,
        left_join: e in Entitlement,
        on: t.entitlement_id == e.id,
        left_join: d in DeviceUser,
        on: t.device_user_id == d.id,
        where:
          d.email == ^email and e.organization_id == ^organization_id and is_nil(t.deleted_at),
        preload: [:device_user, :entitlement]
      )

    query
    |> Repo.all()
  end

  def list_valid_device_user_transactions(organization_id, device_user_id) do
    query =
      from(
        t in Transaction,
        distinct: t.id,
        left_join: e in Entitlement,
        on: t.entitlement_id == e.id,
        left_join: d in DeviceUser,
        on: t.device_user_id == d.id,
        where:
          d.id == ^device_user_id and e.organization_id == ^organization_id and
            is_nil(t.deleted_at) and is_nil(t.invalidated_at),
        preload: [:entitlement]
      )

    query
    |> Repo.all()
  end

  @like_metacharacter_regex ~r/([\\%_])/
  def search_by_email(_organization_id, nil), do: []
  def search_by_email(_organization_id, ""), do: []

  def search_by_email(organization_id, email) do
    email = Regex.replace(@like_metacharacter_regex, email, fn _, x -> "\\#{x}" end)
    like_email = "%#{email}%"

    from(
      t in Transaction,
      join: e in Entitlement,
      on: e.id == t.entitlement_id,
      join: du in DeviceUser,
      on: du.id == t.device_user_id,
      where:
        e.organization_id == ^organization_id and is_nil(t.deleted_at) and
          like(du.email, ^like_email),
      order_by: [desc: t.inserted_at],
      select: %{
        id: t.id,
        email: du.email,
        invalidated_at: t.invalidated_at,
        invalidation_reason: t.invalidation_reason,
        platform: t.platform,
        date: t.inserted_at
      }
    )
    |> Repo.all()
  end

  @spec delete!(String.t(), String.t()) :: :ok | no_return()
  def delete!(organization_id, id) do
    transaction =
      from(
        t in Transaction,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where: e.organization_id == ^organization_id and t.id == ^id and is_nil(t.deleted_at),
        preload: [:device_user, :entitlement]
      )
      |> Repo.one!()

    transaction
    |> Transaction.build_changeset(%{deleted_at: DateTime.utc_now()})
    |> Repo.update!()

    @webhook_module.trigger(:deleted_transaction, {id, organization_id})
  end

  @spec delete_multiple!(String.t(), list(String.t())) ::
          {integer(), nil | [term()]} | no_return()
  def delete_multiple!(organization_id, ids) when is_list(ids) do
    now = DateTime.utc_now()

    query =
      from(
        t in Transaction,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where: e.organization_id == ^organization_id and t.id in ^ids and is_nil(t.deleted_at)
      )

    {count, _} =
      results =
      query
      |> Repo.update_all(set: [deleted_at: now])

    if count > 0 do
      Enum.each(ids, &@webhook_module.trigger(:deleted_transaction, {&1, organization_id}))
    end

    results
  end

  @spec invalidate!(String.t(), String.t(), String.t()) :: :ok | no_return()
  def invalidate!(organization_id, id, reason) when is_binary(id) do
    transaction =
      from(
        t in Transaction,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where:
          e.organization_id == ^organization_id and t.id == ^id and is_nil(t.deleted_at) and
            is_nil(t.invalidated_at),
        preload: [:device_user, :entitlement]
      )
      |> Repo.one!()

    now = DateTime.utc_now()

    transaction
    |> Transaction.build_changeset(%{
      updated_at: now,
      invalidated_at: now,
      invalidation_reason: reason
    })
    |> Repo.update!()

    @webhook_module.trigger(:invalidated_transaction, {id, organization_id})
  end

  @spec invalidate_multiple!(String.t(), list(String.t()), String.t()) ::
          {integer(), nil | [term()]} | no_return()
  def invalidate_multiple!(organization_id, ids, reason) when is_list(ids) do
    now = DateTime.utc_now()

    query =
      from(
        t in Transaction,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where:
          e.organization_id == ^organization_id and t.id in ^ids and is_nil(t.deleted_at) and
            is_nil(t.invalidated_at)
      )

    {count, _} =
      results =
      query
      |> Repo.update_all(set: [updated_at: now, invalidated_at: now, invalidation_reason: reason])

    if count > 0 do
      Enum.each(ids, &@webhook_module.trigger(:invalidated_transaction, {&1, organization_id}))
    end

    results
  end

  def update(organization_id, transaction_id, params) do
    transaction =
      from(
        t in Transaction,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where:
          e.organization_id == ^organization_id and t.id == ^transaction_id and
            is_nil(t.deleted_at),
        preload: [:device_user, :entitlement]
      )
      |> Repo.one!()

    res =
      transaction
      |> Transaction.build_changeset(params)
      |> Repo.update()

    case res do
      {:ok, %{invalidated_at: invalidated_at} = transaction} when not is_nil(invalidated_at) ->
        @webhook_module.trigger(:invalidated_transaction, {transaction_id, organization_id})

        {:ok, transaction}

      res ->
        res
    end
  end
end
