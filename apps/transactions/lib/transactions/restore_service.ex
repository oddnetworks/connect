defmodule Transactions.RestoreService do
  @moduledoc """
  Responsible for processing a transaction restore request
  """

  require Logger
  alias Connect.Repo
  alias Transactions.{CreationService, PlatformIdentity, Transaction}
  alias Transactions.Validator.{Amazon, Apple}

  @doc """
  ## Parameters

  - `transaction` - map of transaction attributes
  - `organization_id` - string identifying the organization scope

  """
  @spec run(Map.t(), String.t()) ::
          {:ok, Transaction.t()} | {:error, String.t()} | {:error, atom, Ecto.Changeset.t()}
  def run(
        %{
          "external_identifier" => _external_identifier,
          "platform" => _platform,
          "receipt" => _receipt
        } = transaction_params,
        organization_id
      ) do
    with {:ok, transaction} <- find_or_create_transaction(transaction_params, organization_id),
         {:ok, transaction, changes} <- restore_transaction(transaction, transaction_params),
         {:ok, %{id: id}} <- update_transaction(transaction, changes),
         transaction <- reload_transaction(id),
         :ok <- log_success(transaction, transaction_params) do
      {:ok, transaction}
    else
      {:error, %{errors: errors}} = error ->
        log_failure(errors, transaction_params, nil)
        error

      {:error, prefix, %{errors: errors}} = error ->
        log_failure(errors, transaction_params, prefix)
        error

      {:error, message} = error ->
        log_failure(message, transaction_params, nil)
        error
    end
  end

  def run(_, _), do: {:error, "Invalid Restore Request"}

  defp find_or_create_transaction(
         %{"external_identifier" => external_identifier} = transaction_params,
         organization_id
       ) do
    q =
      Transaction.find_by_external_identifier_and_organization_id_query(
        external_identifier,
        organization_id
      )

    case Repo.one(q) do
      nil ->
        create_transaction(transaction_params, organization_id)

      t ->
        {:ok, t}
    end
  end

  defp restore_transaction(
         %{
           entitlement: %{
             organization: %{
               settings: %{
                 apple_app_shared_secret: shared_secret,
                 use_apple_sandbox_environment: use_sandbox
               }
             }
           }
         } = transaction,
         %{"platform" => "APPLE", "receipt" => receipt} = _transaction_params
       ) do
    transaction = %{transaction | receipt: receipt}

    Apple.validate(transaction, %{shared_secret: shared_secret, use_sandbox: use_sandbox})
  end

  defp restore_transaction(
         %{
           entitlement: %{
             organization: %{
               settings: %{amazon_shared_secret: shared_secret, amazon_use_sandbox: use_sandbox}
             }
           }
         } = transaction,
         %{"platform" => "AMAZON", "receipt" => receipt} = _transaction_params
       ) do
    transaction = %{transaction | receipt: receipt}

    Amazon.validate(transaction, %{shared_secret: shared_secret, use_sandbox: use_sandbox})
  end

  defp restore_transaction(transaction, %{"platform" => platform} = _transaction_params),
    do: {:error, "Transactions from \"#{platform}\" Platform are not able to be restored"}

  defp update_transaction(transaction, changes) do
    Repo.update(Transaction.build_changeset(transaction, changes))
  end

  defp reload_transaction(id) do
    Repo.get(Transaction, id)
    |> Repo.preload([:device_user, :entitlement])
  end

  defp log_success(transaction, event_data) do
    event_data =
      event_data
      |> Map.put(:last_validated_at, transaction.last_validated_at)
      |> Map.put(:invalidated_at, transaction.invalidated_at)
      |> Map.put(:invalidation_reason, transaction.invalidation_reason)

    Logger.info("Transactions.RestoreService Successful",
      event: %{type: "restore_transaction", data: event_data}
    )
  end

  defp log_failure(errors, event_data, prefix) when is_list(errors) do
    event_data =
      event_data
      |> Map.put(:changeset_errors, errors)
      |> Map.put(:changeset_prefix, prefix)

    Logger.error("Transactions.RestoreService Failed: Repo",
      event: %{type: "restore_transaction", data: event_data}
    )
  end

  defp log_failure(reason, event_data, _prefix) when is_binary(reason) do
    Logger.error("Transactions.RestoreService Failed: #{reason}",
      event: %{type: "restore_transaction", data: event_data}
    )
  end

  defp create_transaction(
         %{"platform_identity" => platform_identity, "platform" => platform} = transaction_params,
         organization_id
       ) do
    case find_platform_identity(platform_identity, platform) do
      nil ->
        {:error, "Transaction Not Found - Unable To Match Platform Identity"}

      %{email: email} ->
        CreationService.run(organization_id, email, transaction_params)
        |> normalize_creation_service_results()
    end
  end

  defp create_transaction(_transaction_params, _organization_id),
    do: {:error, "Transaction Not Found - Unable To Create"}

  defp normalize_creation_service_results({:ok, %{transaction: %{id: id}}}) do
    transaction =
      Repo.get(Transaction, id)
      |> Repo.preload(entitlement: [organization: [:settings]])

    {:ok, transaction}
  end

  defp normalize_creation_service_results({:error, :entitlement_not_found}),
    do: {:error, "Entitlement Not Found"}

  defp normalize_creation_service_results({:error, :duplicate}),
    do: {:error, "Duplicate Transaction"}

  defp normalize_creation_service_results(error), do: error

  defp find_platform_identity(external_identifier, platform) do
    PlatformIdentity.find_query(external_identifier, platform)
    |> Repo.one()
  end
end
