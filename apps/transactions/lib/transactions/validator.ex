defmodule Transactions.Validator do
  use GenServer

  alias Transactions.ValidationService

  @task_supervisor Application.get_env(:transactions, :task_supervisor) || Task.Supervisor

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    {:ok, %{}}
  end

  @doc """
  Triggers validation for all of a DeviceUser's currently valid
  transactions within the given Organization.

  ## Payload

  The payload is a tuple containing a `Transactions.DeviceUser` id and a `Transactions.Organization` id

      {device_user_id, organization_id}

  See `Transactions.ValidationService.validate_transactions_for_device_user/2`
  """
  def trigger_for_device_user({device_user_id, organization_id}) do
    GenServer.cast(
      __MODULE__,
      {:validate_transactions_for_device_user, {device_user_id, organization_id}}
    )
  end

  @doc """
  Triggers a single transaction validation.

  ## Payload

  The payload is a tuple containing a `Transactions.Transaction` id and a `Transactions.Organization` id

      {transaction_id, organization_id}

  See `Transactions.ValidationService.validate_transaction/2`
  """
  def trigger_for_transaction({transaction_id, organization_id}) do
    GenServer.cast(__MODULE__, {:validate_transaction, {transaction_id, organization_id}})
  end

  def handle_cast({:validate_transaction, {transaction_id, organization_id}}, state) do
    Task.start(ValidationService, :run, [transaction_id, organization_id])

    {:noreply, state}
  end

  def handle_cast({:validate_transactions_for_device_user, {device_user_id, organization_id}}, state) do
    Task.start(ValidationService, :run_for_device_user, [device_user_id, organization_id])

    {:noreply, state}
  end
end
