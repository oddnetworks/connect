defmodule Transactions.CreationService do
  @moduledoc """
  Responsible for creating a Transaction within Connect.Repo
  """

  alias Transactions.{DeviceUser, Entitlement, PlatformIdentity, Transaction}
  alias Connect.Repo
  alias Ecto.Multi

  @webhook_module Application.get_env(:transactions, :webhook_module) || Webhooks

  @doc """
  ## Parameters

    - `organization_id` - `string` the Organization scope
    - `email` - `string` - the email of the DeviceUser making the Transaction
    - `transaction` - `map` - specific details of the transaction
      - `platform` - `string` - identifies the external platform where the purchase occured
      - `product_identifier` - `string` - identifies the item purchased on the external platform
      - `external_identifier` - `string` - identifies the receipt/purchase on the external platform
      - `receipt` - `map` - the actual receipt data from the external platform
      - `platform_identity` - `string` _(optional)_ - identifies the user on the external platform (if email is not used natively)
  """
  @spec run(String.t(), String.t(), Map.t()) ::
          {:ok, Map.t()} | {:error, atom} | {:error, atom, Ecto.Changeset.t()}
  def run(organization_id, email, transaction) do
    create_transaction(organization_id, email, transaction)
    |> Repo.transaction()
    |> send_webhook()
    |> normalize_results()
  end

  def create_transaction(
        organization_id,
        email,
        %{
          "platform" => platform,
          "product_identifier" => product_identifier,
          "external_identifier" => external_identifier,
          "receipt" => receipt
        } = transaction
      ) do
    platform_identity = Map.get(transaction, "platform_identity")
    platform = normalize_platform(platform)

    Multi.new()
    |> Multi.run(:device_user, DeviceUser, :insert_or_find, [email])
    |> Multi.run(:platform_identity, PlatformIdentity, :insert_or_find, [
      platform,
      platform_identity
    ])
    |> Multi.run(:entitlement, Entitlement, :find_scoped_to_organization, [
      organization_id,
      platform,
      product_identifier
    ])
    |> Multi.run(:transaction, Transaction, :insert_or_find, [
      platform,
      external_identifier,
      receipt
    ])
  end

  # Private ####################################################################

  defp normalize_platform(platform) do
    platform = platform |> String.upcase()

    case platform do
      "AMAZON" <> _ -> "AMAZON"
      "APPLE" <> _ -> "APPLE"
      "GOOGLE" <> _ -> "GOOGLE"
      _ -> platform
    end
  end

  defp send_webhook(
         {:ok, %{transaction: %{id: tid}, entitlement: %{organization_id: oid}}} = result
       ) do
    @webhook_module.trigger(:new_transaction, {tid, oid})

    result
  end

  defp send_webhook(result) do
    result
  end

  defp normalize_results({:error, :entitlement, :not_found, _changes}),
    do: {:error, :entitlement_not_found}

  defp normalize_results({:error, :transaction, :duplicate, _changes}), do: {:error, :duplicate}

  defp normalize_results({:error, prefix, %Ecto.Changeset{} = changeset, _changes}),
    do: {:error, prefix, changeset}

  defp normalize_results({:ok, %{transaction: %{id: tid}} = changes}) do
    transaction =
      Transaction
      |> Repo.get(tid)
      |> Repo.preload([:device_user, :entitlement])

    changes = %{changes | transaction: transaction}

    {:ok, changes}
  end
end
