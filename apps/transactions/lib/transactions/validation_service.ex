defmodule Transactions.ValidationService do
  @moduledoc """
  Handles validation calls and distributes to the correct module implementing
  `Transactions.Validator` behaviour.
  """

  require Logger

  import Ecto.Query, only: [from: 2]

  alias Connect.Repo
  alias Transactions.{Entitlement, Organization, Transaction}
  alias Transactions.Validator.{Amazon, Apple, Google, Roku}

  @doc """
  Queries for currently valid Transactions scoped to a DeviceUser
  within an Organization. Runs validation on each result.
  """
  def run_for_device_user(device_user_id, _organization_id) when is_nil(device_user_id), do: :ok

  def run_for_device_user(device_user_id, organization_id) do
    Transaction.valid_device_user_transactions_query(organization_id, device_user_id)
    |> Repo.all()
    |> Enum.each(fn %{id: tid} ->
      Transactions.Validator.trigger_for_transaction({tid, organization_id})
    end)
  end

  @doc """
  Runs validation on the given Transaction.

  Makes a request to the third-party provider from which the Transaction
  originated. If outside the third-party provider's validation threshold,
  it will mark the Transaction's `last_validated_at` and if invalid, will
  also mark `invalidated_at` and `invalidation_reason` when a reason is
  given. May or may not update the Transaction's `receipt`.
  """
  def run(transaction_id, organization_id) do
    case {find_transaction(transaction_id, organization_id),
          find_organization_with_settings(organization_id)} do
      {nil, _org} ->
        log_results({:error, "Transaction Not Found"},
          event: %{
            type: "validate_transaction",
            data: %{
              transaction_id: transaction_id,
              organization_id: organization_id
            }
          }
        )

      {_trans, nil} ->
        log_results({:error, "Organization Not Found"},
          event: %{
            type: "validate_transaction",
            data: %{
              transaction_id: transaction_id,
              organization_id: organization_id
            }
          }
        )

      {trans, org} ->
        run_with_transaction(trans, org)
    end
  end

  defp find_transaction(transaction_id, organization_id) do
    from(
      t in Transaction,
      join: e in Entitlement,
      on: t.entitlement_id == e.id,
      where:
        e.organization_id == ^organization_id and t.id == ^transaction_id and is_nil(t.deleted_at),
      preload: [:entitlement]
    )
    |> Repo.one()
  end

  defp find_organization_with_settings(organization_id) do
    Organization
    |> Repo.get(organization_id)
    |> Repo.preload([:settings])
  end

  defp run_with_transaction(transaction, organization) do
    event_data = %{
      transaction_id: transaction.id,
      organization_urn: organization.urn,
      within_validation_threshold: true,
      platform: transaction.platform
    }

    case Transaction.check_within_validation_threshold(transaction, Timex.now()) do
      {:ok, false} ->
        event_data =
          event_data
          |> Map.put(:within_validation_threshold, false)
          |> Map.put(:validation_needed, true)

        transaction
        |> validate_for_platform(organization)
        |> update_transaction()
        |> log_results(event: %{type: "validate_transaction", data: event_data})

      {:ok, _, reason} ->
        event_data =
          event_data
          |> Map.put(:validation_needed, false)
          |> Map.put(:reason, reason)

        log_results({:ok, true}, event: %{type: "validate_transaction", data: event_data})

      {:ok, true} ->
        event_data =
          event_data
          |> Map.put(:validation_needed, false)

        log_results({:ok, true}, event: %{type: "validate_transaction", data: event_data})

      results ->
        log_results(results, event: %{type: "validate_transaction", data: event_data})
    end
  end

  defp validate_for_platform(%{platform: "AMAZON" <> _} = transaction, organization) do
    settings = Map.get(organization, :settings, %{})
    shared_secret = Map.get(settings, :amazon_shared_secret)
    use_sandbox = Map.get(settings, :amazon_use_sandbox, false)

    case {shared_secret, use_sandbox} do
      {nil, _} ->
        {:error, "Missing Amazon Shared Secret"}

      {^shared_secret, false} ->
        Amazon.validate(transaction, %{shared_secret: shared_secret, use_sandbox: false})

      {^shared_secret, true} ->
        Amazon.validate(transaction, %{shared_secret: shared_secret, use_sandbox: true})

      _ ->
        {:error, "Error in Organization Settings"}
    end
  end

  defp validate_for_platform(%{platform: "APPLE" <> _} = transaction, organization) do
    settings = Map.get(organization, :settings, %{})
    shared_secret = Map.get(settings, :apple_app_shared_secret)
    use_sandbox = Map.get(settings, :use_apple_sandbox_environment, false)

    case {shared_secret, use_sandbox} do
      {nil, _} ->
        {:error, "Missing Apple API Key"}

      {^shared_secret, false} ->
        Apple.validate(transaction, %{shared_secret: shared_secret, use_sandbox: false})

      {^shared_secret, true} ->
        Apple.validate(transaction, %{shared_secret: shared_secret, use_sandbox: true})

      _ ->
        {:error, "Error in Organization Settings"}
    end
  end

  defp validate_for_platform(%{platform: "GOOGLE" <> _} = transaction, organization) do
    settings = Map.get(organization, :settings) || %{}
    package = Map.get(settings, :google_package_name)
    account = Map.get(settings, :google_service_account)
    key = Map.get(settings, :google_service_account_private_key)

    params = %{
      package_name: package,
      service_account: account,
      service_account_private_key: key
    }

    case {package, account, key} do
      {nil, _, _} -> {:error, "Missing Google Package Name"}
      {_, nil, _} -> {:error, "Missing Google Service Account"}
      {_, _, nil} -> {:error, "Missing Google Service Account Private Key"}
      _params -> Google.validate(transaction, params)
    end
  end

  defp validate_for_platform(%{platform: "ROKU"} = transaction, organization) do
    settings = Map.get(organization, :settings) || %{}

    case Map.get(settings, :roku_api_key) do
      nil -> {:error, "Missing Roku API Key"}
      api_key -> Roku.validate(transaction, %{api_key: api_key})
    end
  end

  defp validate_for_platform(_transaction, _organization), do: {:error, "Unknown Platform"}

  defp update_transaction({:ok, transaction, changes}) do
    transaction
    |> Transaction.build_changeset(changes)
    |> Repo.update()
  end

  defp update_transaction(result), do: result

  defp log_results({:ok, %Transaction{} = transaction}, meta) do
    [event: %{type: "validate_transaction", data: event_data}] = meta

    last_validated_at =
      Map.get(transaction, :last_validated_at)
      |> to_string()

    invalidated_at =
      Map.get(transaction, :invalidated_at)
      |> to_string()

    invalidation_reason = Map.get(transaction, :invalidation_reason)

    event_data =
      event_data
      |> Map.put(:last_validated_at, last_validated_at)
      |> Map.put(:invalidated_at, invalidated_at)
      |> Map.put(:invalidation_reason, invalidation_reason)

    log_results({:ok, nil}, event: %{type: "validate_transaction", data: event_data})
  end

  defp log_results({:ok, _}, meta) do
    Logger.info("Transactions.Validator Successful", meta)
  end

  defp log_results({:error, error}, meta) do
    Logger.error("Transactions.Validator Failed: #{inspect(error)}", meta)
  end
end
