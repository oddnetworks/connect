defmodule Transactions.ValidatorClient do
  @callback validate(transaction :: Transaction.t(), options :: Map.t()) :: {:ok, Transaction.t(), Map.t()} | {:error, String.t()}

end
