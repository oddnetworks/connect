defmodule Transactions.Transaction do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias Transactions.{Transaction, DeviceUser, Entitlement}
  alias Connect.Repo

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "transactions" do
    field(:external_identifier, :string)
    field(:platform, :string)
    field(:last_validated_at, :naive_datetime)
    field(:invalidated_at, :naive_datetime)
    field(:invalidation_reason, :string)
    field(:receipt, :map)
    field(:deleted_at, :naive_datetime)

    belongs_to(:device_user, DeviceUser, type: :binary_id)
    belongs_to(:entitlement, Entitlement, type: :binary_id)

    timestamps()
  end

  @params [
    :device_user_id,
    :entitlement_id,
    :external_identifier,
    :platform,
    :receipt,
    :last_validated_at,
    :invalidated_at,
    :invalidation_reason,
    :inserted_at,
    :deleted_at
  ]

  @required_params [
    :external_identifier,
    :platform,
    :receipt,
    :last_validated_at
  ]

  @valid_platforms [
    "AMAZON",
    "APPLE",
    "GOOGLE",
    "ROKU",
    "WEB"
  ]

  def build_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params)
    |> validate_required(@required_params)
    |> validate_length(:external_identifier, min: 1, max: 255)
    |> validate_inclusion(:platform, @valid_platforms)
    |> unique_constraint(
      :external_identifier,
      name: :transactions_platform_external_identifier_index
    )
    |> assoc_constraint(:device_user)
    |> assoc_constraint(:entitlement)
  end

  def valid_device_user_transactions_query(organization_id, device_user_id) do
    from(
      t in Transaction,
      distinct: t.id,
      join: e in Entitlement,
      on: t.entitlement_id == e.id,
      join: d in DeviceUser,
      on: t.device_user_id == d.id,
      where:
        d.id == ^device_user_id and e.organization_id == ^organization_id and is_nil(t.deleted_at) and
          is_nil(t.invalidated_at),
      preload: [:entitlement]
    )
  end

  def find_by_external_identifier_and_organization_id_query(external_identifier, organization_id) do
    from(
      t in Transaction,
      join: e in Entitlement,
      on: t.entitlement_id == e.id,
      where:
        e.organization_id == ^organization_id and t.external_identifier == ^external_identifier and
          is_nil(t.deleted_at),
      preload: [entitlement: [organization: [:settings]]]
    )
  end

  def check_within_validation_threshold(transaction, current_datetime) do
    case validation_threshold(transaction) do
      {:ok, threshold_seconds} ->
        {:ok,
         Timex.before?(
           current_datetime,
           Timex.shift(transaction.last_validated_at, seconds: threshold_seconds)
         )}

      {:error, reason} ->
        {:ok, true, reason}
    end
  end

  def insert_or_find(
        %{entitlement: %{id: ent_id}, device_user: %{id: du_id}},
        platform,
        external_identifier,
        receipt
      ) do
    case Repo.get_by(Transaction, external_identifier: external_identifier, platform: platform) do
      nil ->
        %Transaction{}
        |> Transaction.build_changeset(%{
          device_user_id: du_id,
          entitlement_id: ent_id,
          external_identifier: external_identifier,
          platform: platform,
          receipt: receipt,
          last_validated_at: NaiveDateTime.utc_now()
        })
        |> Repo.insert()

      _transaction ->
        {:error, :duplicate}
    end
  end

  defp validation_threshold(transaction) do
    transaction.platform
    |> fetch_validation_threshold()
    |> threshold_to_integer()
  end

  defp fetch_validation_threshold("APPLE") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_APPLE")
  end

  defp fetch_validation_threshold("GOOGLE") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_GOOGLE")
  end

  defp fetch_validation_threshold("ROKU") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_ROKU")
  end

  defp fetch_validation_threshold("AMAZON") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_AMAZON")
  end

  defp fetch_validation_threshold(_), do: {:error, "Platform Not Validated"}

  defp threshold_to_integer({:error, reason}), do: {:error, reason}

  defp threshold_to_integer(threshold) when is_binary(threshold) do
    {:ok, String.to_integer(threshold)}
  end

  @default_validation_threshold 900
  defp threshold_to_integer(threshold) when is_nil(threshold),
    do: {:ok, @default_validation_threshold}
end
