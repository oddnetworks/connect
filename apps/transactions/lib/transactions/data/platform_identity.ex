defmodule Transactions.PlatformIdentity do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias Transactions.{DeviceUser, PlatformIdentity}
  alias Connect.Repo

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "platform_identities" do
    field(:platform, :string)
    field(:external_identifier, :string)

    belongs_to(:device_user, DeviceUser)

    timestamps()
  end

  @params [:platform, :external_identifier, :device_user_id]

  @valid_platforms [
    "AMAZON",
    "APPLE",
    "GOOGLE",
    "ROKU",
    "WEB"
  ]

  def build_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @params)
    |> validate_required(@params)
    |> validate_inclusion(:platform, @valid_platforms)
    |> unique_constraint(
      :external_identifier,
      name: :platform_identities_platform_external_identifier_index
    )
    |> assoc_constraint(:device_user)
  end

  def insert_or_find(_, _, nil) do
    {:ok, nil}
  end

  def insert_or_find(%{device_user: %{id: du_id}}, platform, ext_id) do
    case Repo.get_by(PlatformIdentity, platform: platform, external_identifier: ext_id) do
      nil -> %PlatformIdentity{}
      pi -> pi
    end
    |> PlatformIdentity.build_changeset(%{
      device_user_id: du_id,
      platform: platform,
      external_identifier: ext_id
    })
    |> Repo.insert_or_update()
  end

  @doc """
  Finds the PlatformIdentity. Preloads DeviceUser values.

  ## Parameters

  - `external_identifier` - string identifying the user on an external platform
  - `platform` - string identifying the platform

  ## Returns

  `nil` or Map of:

  - `id` - PlatformIdentity id
  - `device_user_id`
  - `email` - DeviceUser email address
  - `external_identifier`
  - `platform`
  """
  @spec find_query(String.t(), String.t()) :: nil | Map.t()
  def find_query(external_identifier, platform) do
    from(p in PlatformIdentity,
      join: d in DeviceUser,
      on: d.id == p.device_user_id,
      where: p.external_identifier == ^external_identifier and p.platform == ^platform,
      select: %{
        id: p.id,
        device_user_id: d.id,
        email: d.email,
        external_identifier: p.external_identifier,
        platform: p.platform
      }
    )
  end
end
