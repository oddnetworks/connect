defmodule Transactions.Organization.Settings do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organization_settings" do
    field(:in_review_mode, :boolean, default: false)
    field(:roku_api_key, Cloak.EncryptedBinaryField)
    field(:apple_app_shared_secret, Cloak.EncryptedBinaryField)
    field(:use_apple_sandbox_environment, :boolean, default: false)
    field(:google_package_name, :string)
    field(:google_service_account, :string)
    field(:google_service_account_private_key, Cloak.EncryptedBinaryField)
    field(:amazon_shared_secret, Cloak.EncryptedBinaryField)
    field(:amazon_use_sandbox, :boolean, default: false)
    field(:encryption_version, :binary)

    belongs_to(:organization, Transactions.Organization)
  end
end
