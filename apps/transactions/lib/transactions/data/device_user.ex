defmodule Transactions.DeviceUser do
  use Ecto.Schema
  import Ecto.Changeset

  alias Transactions.{DeviceUser, PlatformIdentity}
  alias Connect.Repo

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "device_users" do
    field(:email, :string)

    has_many(:platform_identities, PlatformIdentity)

    timestamps()
  end

  def build_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email])
    |> validate_required([:email])
    |> validate_length(:email, min: 5, max: 255)
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  def insert_or_find(_, email) do
    case Repo.get_by(DeviceUser, email: email) do
      nil -> %DeviceUser{}
      du -> du
    end
    |> DeviceUser.build_changeset(%{email: email})
    |> Repo.insert_or_update()
  end
end
