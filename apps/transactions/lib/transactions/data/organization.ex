defmodule Transactions.Organization do
  use Ecto.Schema

  alias Transactions.{Organization.Settings, Entitlement}

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organizations" do
    field(:urn, :string)
    field(:name, :string)

    has_one(:settings, Settings)
    has_many(:entitlements, Entitlement)

    timestamps()
  end
end
