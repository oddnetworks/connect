defmodule Transactions.Entitlement do
  use Ecto.Schema
  import Ecto.Query, only: [from: 2]

  alias Transactions.{Organization, Entitlement}
  alias Connect.Repo

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "entitlements" do
    field(:type, :string)
    field(:name, :string)
    field(:description, :string)
    field(:entitlement_identifier, :string)
    field(:product_identifier, :string)
    field(:platform, :string)

    belongs_to(:organization, Organization)

    timestamps()
  end

  def find_scoped_to_organization(_, organization_id, platform, product_identifier) do
    res =
      from(
        e in Entitlement,
        where:
          e.organization_id == ^organization_id and e.platform == ^platform and
            e.product_identifier == ^product_identifier
      )
      |> Repo.one()

    case res do
      nil ->
        {:error, :not_found}

      e ->
        {:ok, e}
    end
  end
end
