defmodule Transactions.Validator.Amazon do
  @moduledoc """
  Responsible for handling validation of Transactions
  against Amazon's Recepit Validation Service
  """

  @behaviour Transactions.ValidatorClient

  require Logger
  alias Transactions.Validator.Amazon.Client
  alias Timber.Events.{HTTPRequestEvent, HTTPResponseEvent}

  @doc """
  Validates an Amazon transaction

  ## Parameters

  - transaction - A valid Transaction struct with platform type `AMAZON` and
  a valid Amazon `receipt`
  - options - a map containing options
  """
  def validate(transaction, %{
        shared_secret: shared_secret,
        use_sandbox: use_sandbox
      }) do
    build_request_url(transaction, shared_secret, use_sandbox)
    |> send_request(transaction)
    |> check_if_valid(transaction)
  end

  defp build_request_url(
         transaction = %{external_identifier: receipt_id, receipt: receipt},
         shared_secret,
         true = _use_sandbox
       ) do
    user_id = Map.get(receipt, "user_id")

    rvs_sandbox_base_url() <>
      "/version/1.0/verifyReceiptId/developer/" <>
      shared_secret <> "/user/" <> user_id <> "/receiptId/" <> receipt_id
  end

  defp build_request_url(
         transaction = %{external_identifier: receipt_id, receipt: receipt},
         shared_secret,
         false = _use_sandbox
       ) do
    user_id = Map.get(receipt, "user_id")

    rvs_base_url() <>
      "/version/1.0/verifyReceiptId/developer/" <>
      shared_secret <> "/user/" <> user_id <> "/receiptId/" <> receipt_id
  end

  defp send_request(url, transaction) do
    Logger.info(fn ->
      event =
        HTTPRequestEvent.new(
          direction: "outgoing",
          service_name: "Transactions.Validator.Amazon",
          method: :get,
          url: url,
          headers: Client.process_headers([])
        )

      {"Sent GET to #{url} from Transactions.Validator.Amazon", [event: event]}
    end)

    timer = Timber.start_timer()

    case Client.get(url) do
      {:ok, %{body: body, headers: resp_headers, status_code: status}} ->
        resp_body =
          case body do
            {:ok, resp_body} -> resp_body
            {:error, _} -> "Unable to parse response body"
          end

        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Amazon",
              status: status,
              headers: resp_headers,
              body: resp_body,
              time_ms: duration
            )

          {"Received #{status} response from Transactions.Validator.Amazon in #{duration}ms",
           [event: event]}
        end)

        to_amazon_transaction(status, body, transaction)

      {:error, error} ->
        message = HTTPoison.Error.message(error)
        Logger.error(message, event: %{type: "amazon_validation", data: Map.from_struct(error)})
        {:error, message}
    end
  end

  defp to_amazon_transaction(_status, {:error, message}, _transaction),
    do: {:error, "Invalid Amazon response"}

  defp to_amazon_transaction(400, _body, _transaction), do: {:error, :invalid}

  defp to_amazon_transaction(496, _body, _transaction),
    do: {:error, "Invalid Amazon shared_secret"}

  defp to_amazon_transaction(497, _body, _transaction), do: {:error, "Invalid Amazon user_id"}

  defp to_amazon_transaction(200, {:ok, body}, %{receipt: receipt}) do
    cancel_date =
      body["cancelDate"]
      |> convert_date_time()

    purchase_date =
      body["purchaseDate"]
      |> convert_date_time()

    renewal_date =
      body["renewalDate"]
      |> convert_date_time()

    {:ok,
     %{
       user_id: Map.get(receipt, "user_id"),
       beta_product: body["betaProduct"],
       cancel_date: cancel_date,
       parent_product_id: body["parentProductId"],
       product_id: body["productId"],
       product_type: body["productType"],
       purchase_date: purchase_date,
       quantity: body["quantity"],
       receipt_id: body["receiptId"],
       renewal_date: renewal_date,
       term: body["term"],
       term_sku: body["termSku"],
       test_transaction: body["testTransaction"]
     }}
  end

  defp check_if_valid({:ok, %{cancel_date: nil} = amazon_transaction}, transaction) do
    # valid subscription
    Logger.info("Subscription Valid")

    changes = %{
      last_validated_at: DateTime.utc_now(),
      receipt: amazon_transaction
    }

    {:ok, transaction, changes}
  end

  defp check_if_valid({:ok, amazon_transaction}, transaction) do
    # invalid subscription
    Logger.info("Subscription Cancelled")
    now = DateTime.utc_now()

    changes = %{
      last_validated_at: now,
      invalidated_at: now,
      invalidation_reason: "Subscription Cancelled",
      receipt: amazon_transaction
    }

    {:ok, transaction, changes}
  end

  defp check_if_valid({:error, :invalid}, transaction) do
    # invalidate the transaction
    Logger.info("Subscription invalid/not found by RVS")
    now = DateTime.utc_now()

    changes = %{
      last_validated_at: now,
      invalidated_at: now,
      invalidation_reason: "Invalid/Not Found by RVS"
    }

    {:ok, transaction, changes}
  end

  defp check_if_valid(error, _transaction), do: error

  defp rvs_sandbox_base_url() do
    Application.get_env(:transactions, Transactions.Validator)[:amazon_rvs_sandbox_base_url] ||
      "http://amazonrvs:8080/RVSSandbox"
  end

  defp rvs_base_url() do
    "https://appstore-sdk.amazon.com"
  end

  defp convert_date_time(nil), do: nil

  defp convert_date_time(milliseconds_since_epoch) do
    case DateTime.from_unix(milliseconds_since_epoch, :millisecond) do
      {:ok, dt} ->
        dt
        |> DateTime.to_iso8601()

      {:error, _} ->
        milliseconds_since_epoch
    end
  end
end
