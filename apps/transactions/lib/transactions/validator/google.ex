defmodule Transactions.Validator.Google do
  @moduledoc """
  Responsible for validating Google transactions through the Play Store.
  """

  @behaviour Transactions.ValidatorClient

  require Logger
  alias Transactions.Validator.Google.{AndroidPublisher, ServiceAccountAuthenticator}

  @doc """
  Validates a Google transaction

  ## Parameters

  - transaction - A valid Transaction struct with platform type `GOOGLE` and
  a valid Play Store `receipt` (contains `subscription_id` and `token`)
  - options - a map of options
    - package_name - Your app's applicationId (`com.example.app`).
    - service_account
    - service_account_private_key
  """
  def validate(%{receipt: receipt} = transaction, %{
        package_name: package_name,
        service_account: service_account,
        service_account_private_key: service_account_private_key
      }) do
    subscription_id = transaction.entitlement.product_identifier
    token = Map.get(receipt, "token", "")

    authenticate(service_account, service_account_private_key)
    |> fetch_subscription(package_name, subscription_id, token)
    |> check_if_valid(transaction, subscription_id, token)
  end

  defp authenticate(service_account, private_key) do
    ServiceAccountAuthenticator.request_access_token(service_account, private_key)
  end

  defp fetch_subscription(
         {:ok, %{"access_token" => access_token}},
         package_name,
         subscription_id,
         token
       ) do
    AndroidPublisher.get_subscription(access_token, package_name, subscription_id, token)
  end

  defp fetch_subscription(error, _package_name, _subscription_id, _token), do: error

  defp check_if_valid({:ok, google_transaction}, transaction, subscription_id, token) do
    now = DateTime.utc_now()

    # esnure subscription_id and token are present
    google_transaction =
      Map.merge(google_transaction, %{subscription_id: subscription_id, token: token})

    cond do
      not is_nil(google_transaction.cancel_reason) &&
          Timex.after?(Timex.now(), google_transaction.expiry_time) ->
        # ********************************************
        #
        # Subscription can be cancelled, but not invalid
        # - if cancel_reason is not nil, check if
        #   expiry_time is past current time
        #
        # ********************************************

        "Subscription Cancelled"
        |> Logger.info()

        changes = %{
          last_validated_at: now,
          invalidated_at: now,
          invalidation_reason: "Subscription Cancelled",
          receipt: google_transaction
        }

        {:ok, transaction, changes}

      Timex.after?(Timex.now(), google_transaction.expiry_time) ->
        "Subscription Expired"
        |> Logger.info()

        changes = %{
          last_validated_at: now,
          invalidated_at: now,
          invalidation_reason: "Subscription Expired",
          receipt: google_transaction
        }

        {:ok, transaction, changes}

      true ->
        "Subscription Valid"
        |> Logger.info()

        changes = %{
          last_validated_at: now,
          receipt: google_transaction
        }

        {:ok, transaction, changes}
    end
  end

  defp check_if_valid(error, _transaction, _subscription_id, _token), do: error
end
