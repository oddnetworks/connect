defmodule Transactions.Validator.Roku do
  @moduledoc """
  Responsible for validating Roku transactions
  """

  @behaviour Transactions.ValidatorClient

  require Logger
  alias Transactions.Validator.Roku.Client
  alias Timber.Events.{HTTPRequestEvent, HTTPResponseEvent}

  @doc """
  Validates a Roku transaction

  ## Parameters

  - transaction
  - options - a map of options
    - api_key - a valid Roku API key belonging to the channel
  in which the transaction was made
  """
  def validate(transaction, %{api_key: api_key}) do
    case make_request(api_key, transaction.external_identifier) do
      {:ok, roku_transaction} ->
        roku_transaction
        |> check_if_valid(transaction)

      {:error, reason} ->
        {:error, "Roku - #{reason}"}
    end
  end

  defp check_if_valid(roku_transaction, transaction) do
    now = DateTime.utc_now()

    # From Roku https://sdkdocs.roku.com/display/sdkdoc/Web+Service+API#WebServiceAPI-ValidateTransaction
    #
    # isEntitled  | expirationDate        | Subscription State      | Action
    # ------------|-----------------------|-------------------------|--------------------------------------------------------------------------|
    # True        | Future Date           | Active Subscription     | Save the new expirationDate and entitle user till the new expirationDate
    # True        | Current or Past Date  | In Dunning              | Entitle user and check again next day
    # False       | Past Date             | Cancelled Subscription  | Cancel Subscription

    cond do
      not is_nil(roku_transaction.expiration_date) &&
        Timex.after?(Timex.now(), roku_transaction.expiration_date) &&
          not roku_transaction.is_entitled ->
        Logger.info("Transactions.Validator.Roku: Invalid - Canceled")

        changes = %{
          last_validated_at: now,
          invalidated_at: now,
          invalidation_reason: "Canceled/Expired",
          receipt: roku_transaction
        }

        {:ok, transaction, changes}

      true ->
        Logger.info(
          "Transactions.Validator.Roku: Valid Unitl #{inspect(roku_transaction.expiration_date)}"
        )

        changes = %{
          last_validated_at: roku_transaction.expiration_date,
          receipt: roku_transaction
        }

        {:ok, transaction, changes}
    end
  end

  defp make_request(api_key, transaction_id) do
    path = "/validate-transaction/#{api_key}/#{transaction_id}"

    Logger.info(fn ->
      req_url = Client.process_url(path)
      req_headers = Client.process_headers([])

      event =
        HTTPRequestEvent.new(
          direction: "outgoing",
          service_name: "Transactions.Validator.Roku",
          method: :get,
          url: req_url,
          headers: req_headers
        )

      {"Sent GET to #{req_url} from Transactions.Validator.Roku", [event: event]}
    end)

    timer = Timber.start_timer()

    case Client.get(path) do
      {:ok, %{body: body, headers: resp_headers, status_code: status}} ->
        resp_body =
          case body do
            {:ok, resp_body} -> resp_body
            {:error, _} -> "Unable to parse response body"
          end

        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Roku",
              status: status,
              headers: resp_headers,
              body: resp_body,
              time_ms: duration
            )

          {"Received #{status} response from Transactions.Validator.Roku in #{duration}ms",
           [event: event]}
        end)

        to_roku_transaction(body)

      {:error, error} ->
        message = HTTPoison.Error.message(error)
        Logger.error(message, event: %{type: "roku_validation", data: Map.from_struct(error)})
        {:error, message}
    end
  end

  defp to_roku_transaction({:error, _}), do: {:error, "Unable to process body"}

  defp to_roku_transaction({:ok, body}) do
    case parse_status(body) do
      :ok ->
        purchase_date =
          body["purchaseDate"]
          |> convert_date_time()

        expiration_date =
          body["expirationDate"]
          |> convert_date_time()

        original_purchase_date =
          body["originalPurchaseDate"]
          |> convert_date_time()

        {:ok,
         %{
           is_entitled: body["isEntitled"],
           transaction_id: body["transactionId"],
           cancelled: body["cancelled"],
           purchase_date: purchase_date,
           channel_name: body["channelName"],
           product_name: body["productName"],
           product_id: body["productId"],
           amount: body["amount"],
           currency: body["currency"],
           quantity: body["quantity"],
           roku_customer_id: body["rokuCustomerId"],
           expiration_date: expiration_date,
           original_purchase_date: original_purchase_date
         }}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp parse_status(body) do
    case body["status"] do
      0 ->
        :ok

      1 ->
        Logger.info("Transactions.Validator.Roku - Bad Status",
          event: %{
            type: "roku_validation",
            data: %{
              api_error_message: Map.get(body, "errorMessage"),
              api_error_detail: Map.get(body, "errorDetail"),
              api_error_code: Map.get(body, "errorCode")
            }
          }
        )

        {:error, body["errorMessage"]}
    end
  end

  def convert_date_time("/Date(" <> raw) do
    {timestamp, _} =
      raw
      |> String.replace(")/", "")
      |> String.slice(0..-6)
      |> Integer.parse()

    offset =
      raw
      |> String.replace(")/", "")
      |> String.slice(-5..-1)

    time_zone =
      offset
      |> Timex.Timezone.name_of()

    case DateTime.from_unix(timestamp, :millisecond) do
      {:ok, dt} -> Timex.Timezone.convert(dt, time_zone)
      {:error, _} -> raw
    end
  end

  def convert_date_time(nil), do: nil

  def convert_date_time(raw) do
    case DateTime.from_iso8601(raw) do
      {:ok, dt} -> dt
      {:ok, dt, _} -> dt
      {:error, _} -> raw
    end
  end
end
