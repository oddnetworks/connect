defmodule Transactions.Validator.Apple do
  @moduledoc """
  Responsible for validating Apple transactions.
  """

  @behaviour Transactions.ValidatorClient

  require Logger
  alias Transactions.Validator.Apple.Client
  alias Timber.Events.{HTTPRequestEvent, HTTPResponseEvent}

  @doc """
  Validates an Apple transaction

  ## Parameters

  - transaction - A valid Transaction struct with platform type `APPLE` and
  a valid Apple `receipt`
  - options - a map containing options
    - app_shared_secret - Your app's shared secret (a hexadecimal string).
    - use_sandbox - boolean to specify if the app is in the sandbox
  """
  def validate(transaction, %{
        shared_secret: app_shared_secret,
        use_sandbox: use_sandbox
      }) do
    case make_request(transaction, app_shared_secret, use_sandbox) do
      {:ok, apple_transaction} ->
        apple_transaction
        |> check_if_valid(transaction)

      {:error, reason} ->
        {:error, "Apple - #{reason}"}
    end
  end

  defp check_if_valid(apple_transaction, transaction) do
    now = DateTime.utc_now()

    cond do
      not is_nil(apple_transaction.cancellation_date) ->
        reason =
          case apple_transaction.cancellation_reason do
            "1" -> "Customer canceled their subscription."
            "0" -> "Cancelled - Other"
            nil -> "Cancelled - Unknown"
          end

        "Transactions.Validator.Apple: Invalid - #{reason}"
        |> Logger.info()

        changes = %{
          last_validated_at: now,
          invalidated_at: now,
          invalidation_reason: reason,
          receipt: apple_transaction
        }

        {:ok, transaction, changes}

      not is_nil(apple_transaction.expires_date) &&
          Timex.after?(Timex.now(), apple_transaction.expires_date) ->
        reason =
          case apple_transaction.expiration_intent do
            "1" ->
              "Customer canceled their subscription."

            "2" ->
              "Billing error; for example customer’s payment information was no longer valid."

            "3" ->
              "Customer did not agree to a recent price increase."

            "4" ->
              "Product was not available for purchase at the time of renewal."

            "5" ->
              "Unknown error."

            _ ->
              "Expired"
          end

        "Transactions.Validator.Apple: Invalid - #{reason}"
        |> Logger.info()

        changes = %{
          last_validated_at: now,
          invalidated_at: now,
          invalidation_reason: reason,
          receipt: apple_transaction
        }

        {:ok, transaction, changes}

      true ->
        "Transactions.Validator.Apple: Valid"
        |> Logger.info()

        changes = %{
          last_validated_at: now,
          receipt: apple_transaction
        }

        {:ok, transaction, changes}
    end
  end

  defp make_request(transaction = %{receipt: receipt}, app_shared_secret, use_sandbox) do
    receipt_data = Map.get(receipt, "latest_receipt") || Map.get(receipt, :latest_receipt)

    body = %{
      # Base64 encoded receipt
      "receipt-data": transaction.receipt["latest_receipt"],
      password: app_shared_secret,
      "exclude-old-transactions": true
    }

    url =
      if use_sandbox do
        "https://sandbox.itunes.apple.com/verifyReceipt"
      else
        "https://buy.itunes.apple.com/verifyReceipt"
      end

    Logger.info(fn ->
      req_headers = Client.process_headers([])

      event =
        HTTPRequestEvent.new(
          direction: "outgoing",
          service_name: "Transactions.Validator.Apple",
          method: :post,
          url: url,
          headers: req_headers,
          body: body
        )

      {"Sent POST to #{url} from Transactions.Validator.Apple", [event: event]}
    end)

    timer = Timber.start_timer()

    case Client.post(url, body, [{"content-type", "application/json"}]) do
      {:ok, %{body: body, headers: resp_headers, status_code: status}} ->
        resp_body =
          case body do
            {:ok, resp_body} -> resp_body
            {:error, _} -> "Unable to parse response body"
          end

        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Apple",
              status: status,
              headers: resp_headers,
              body: resp_body,
              time_ms: duration
            )

          {"Received #{status} response from Transactions.Validator.Apple in #{duration}ms",
           [event: event]}
        end)

        to_apple_transaction(body, transaction)

      {:error, error} ->
        message = HTTPoison.Error.message(error)
        Logger.error(message, event: %{type: "apple_validation", data: Map.from_struct(error)})
        {:error, message}
    end
  end

  defp to_apple_transaction({:error, _}, _transaction), do: {:error, "Unable to process body"}

  defp to_apple_transaction({:ok, body}, transaction) do
    case parse_status(body) do
      :ok ->
        receipt = parse_latest_receipt_info(body["latest_receipt_info"], transaction)
        latest_receipt_encoded = body["latest_receipt"]

        {:ok,
         %{
           quantity: Map.get(receipt, "quantity"),
           product_id: Map.get(receipt, "product_id"),
           transaction_id: Map.get(receipt, "transaction_id"),
           original_transaction_id: Map.get(receipt, "original_transaction_id"),
           purchase_date:
             Map.get(receipt, "purchase_date")
             |> parse_date(),
           original_purchase_date:
             Map.get(receipt, "original_purchase_date")
             |> parse_date(),
           expires_date:
             Map.get(receipt, "expires_date")
             |> parse_date(),
           expiration_intent: Map.get(receipt, "expiration_intent"),
           is_in_billing_retry_period: Map.get(receipt, "is_in_billing_retry_period"),
           is_trial_period: Map.get(receipt, "is_trial_period"),
           cancellation_date:
             Map.get(receipt, "cancellation_date")
             |> parse_date(),
           cancellation_reason: Map.get(receipt, "cancellation_reason"),
           app_item_id: Map.get(receipt, "app_item_id"),
           version_external_identifier: Map.get(receipt, "version_external_identifier"),
           web_order_line_item_id: Map.get(receipt, "web_order_line_item_id"),
           auto_renew_status: Map.get(receipt, "auto_renew_status"),
           auto_renew_product_id: Map.get(receipt, "auto_renew_product_id"),
           price_consent_status: Map.get(receipt, "price_consent_status"),
           # NEW Base64 encoded receipt
           latest_receipt: latest_receipt_encoded
         }}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp parse_status(body) do
    case body["status"] do
      21_000 ->
        {:error, "The App Store could not read the JSON object you provided."}

      21_002 ->
        {:error, "The data in the receipt-data property was malformed or missing."}

      21_003 ->
        {:error, "The receipt could not be authenticated."}

      21_004 ->
        {:error,
         "The shared secret you provided does not match the shared secret on file for your account."}

      21_005 ->
        {:error, "The receipt server is not currently available."}

      21_007 ->
        {:error,
         "This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead."}

      21_008 ->
        {:error,
         "This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead."}

      21_100..21_199 ->
        {:error, "Internal data access error."}

      _ ->
        :ok
    end
  end

  defp parse_latest_receipt_info(latest_receipt_info, transaction) do
    product_identifier = transaction.entitlement.product_identifier

    latest_receipt_info
    |> filter_product_transactions(product_identifier)
    |> sort_latest_transaction()
    |> List.first()
  end

  defp filter_product_transactions(transactions, product_identifier) do
    transactions
    |> Enum.filter(fn transaction ->
      transaction["product_id"] == product_identifier
    end)
  end

  defp sort_latest_transaction(transactions) do
    transactions
    |> Enum.sort_by(
      fn transaction ->
        Timex.parse(transaction["purchase_date"], "%F %T %Z", :strftime)
      end,
      &>=/2
    )
  end

  defp parse_date(raw_date) when not is_nil(raw_date) do
    case Timex.parse(raw_date, "%F %T %Z", :strftime) do
      {:ok, date} -> date
      _ -> nil
    end
  end

  defp parse_date(_), do: nil
end
