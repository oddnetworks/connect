defmodule Transactions.Validator.Google.ServiceAccountAuthenticator do
  alias Transactions.Validator.Google.Token

  require Logger
  alias Timber.Events.{HTTPRequestEvent, HTTPResponseEvent}

  @url "https://www.googleapis.com/oauth2/v4/token"
  @headers [
    {"content-type", "application/x-www-form-urlencoded"},
    {"accept", "application/json"}
  ]
  @request_options [[ssl: [{:versions, [:"tlsv1.2"]}]]]

  @doc """
  Requests a Service Account OAuth token from Google.
  """
  @spec request_access_token(String.t(), String.t()) :: {:ok, map()} | {:error, String.t()}
  def request_access_token(service_account, private_key) do
    case Token.jwt(service_account, private_key) do
      {:ok, jwt} ->
        make_request(jwt)

      {:error, error} ->
        Logger.info(error.message,
          event: %{type: "google_service_account", data: Map.from_struct(error)}
        )

        {:error, "Google Service Account - #{error.message}"}
    end
  end

  defp make_request(jwt) do
    Logger.info(fn ->
      event =
        HTTPRequestEvent.new(
          direction: "outgoing",
          service_name: "Transactions.Validator.Google.ServiceAccountAuthenticator",
          method: :post,
          url: @url,
          headers: @headers
        )

      {"Sent POST to #{@url} from Transactions.Validator.Google.ServiceAccountAuthenticator",
       [event: event]}
    end)

    timer = Timber.start_timer()

    case HTTPoison.post(@url, body(jwt), @headers, @request_options) do
      {:ok, %{body: body, status_code: 200, headers: resp_headers}} ->
        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Google.ServiceAccountAuthenticator",
              status: 200,
              headers: resp_headers,
              time_ms: duration
            )

          {"Received 200 from Transactions.Validator.Google.ServiceAccountAuthenticator in #{
             duration
           }ms", [event: event]}
        end)

        body
        |> decode_body()
        |> to_access_token()

      {:ok, resp = %{status_code: status, headers: resp_headers}} ->
        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Google.ServiceAccountAuthenticator",
              status: status,
              headers: resp_headers,
              time_ms: duration
            )

          {"Received #{status} response from Transactions.Validator.Google.ServiceAccountAuthenticator in #{
             duration
           }ms", [event: event]}
        end)

        resp
        |> to_error

      {:error, error} ->
        message = HTTPoison.Error.message(error)

        Logger.error(message,
          event: %{type: "google_service_account", data: Map.from_struct(error)}
        )

        {:error, "Google Service Account - #{message}"}
    end
  end

  @grant_type "urn:ietf:params:oauth:grant-type:jwt-bearer" |> URI.encode_www_form()
  defp body(jwt) do
    "grant_type=#{@grant_type}&assertion=#{jwt}"
  end

  defp decode_body(body) do
    Poison.decode(body)
  end

  defp to_access_token({:ok, result}), do: {:ok, result}
  defp to_access_token(_), do: {:error, "Service Account - 200 - Unable to parse body"}

  defp to_error(%{status_code: status_code, body: body}) do
    reason =
      body
      |> decode_body()
      |> error_body_to_string()

    {:error, "Service Account - #{status_code} - #{reason}"}
  end

  defp error_body_to_string({:ok, %{"error" => error, "error_description" => description}}) do
    "#{error} - #{description}"
  end

  defp error_body_to_string(_), do: "Unable to parse body"
end
