defmodule Transactions.Validator.Google.AndroidPublisher do
  require Logger
  alias Timber.Events.{HTTPRequestEvent, HTTPResponseEvent}

  @base_url "https://www.googleapis.com/androidpublisher/v2"
  @request_options [[ssl: [{:versions, [:"tlsv1.2"]}]]]

  @spec get_subscription(String.t(), String.t(), String.t(), String.t()) ::
          {:ok, map()} | {:error, String.t()}
  def get_subscription(access_token, package_name, subscription_id, token) do
    url = url(package_name, subscription_id, token)
    headers = headers(access_token)

    Logger.info(fn ->
      event =
        HTTPRequestEvent.new(
          direction: "outgoing",
          service_name: "Transactions.Validator.Google.AndroidPublisher",
          method: :get,
          url: url,
          headers: headers
        )

      {"Sent GET to #{url} from Transactions.Validator.Google.AndroidPublisher", [event: event]}
    end)

    timer = Timber.start_timer()

    case HTTPoison.get(
           url,
           headers,
           @request_options
         ) do
      {:ok, %{body: body, status_code: 200, headers: resp_headers}} ->
        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Google.AndroidPublisher",
              status: 200,
              headers: resp_headers,
              time_ms: duration
            )

          {"Received 200 from Transactions.Validator.Google.AndroidPublisher in #{duration}ms",
           [event: event]}
        end)

        body
        |> decode_body()
        |> to_google_transaction()

      {:ok, response = %{status_code: status, headers: resp_headers}} ->
        Logger.info(fn ->
          duration = Timber.duration_ms(timer)

          event =
            HTTPResponseEvent.new(
              direction: "incoming",
              service_name: "Transactions.Validator.Google.AndroidPublisher",
              status: status,
              headers: resp_headers,
              time_ms: duration
            )

          {"Received #{status} response from Transactions.Validator.Google.AndroidPublisher in #{
             duration
           }ms", [event: event]}
        end)

        response
        |> to_error()

      {:error, error} ->
        message = HTTPoison.Error.message(error)
        Logger.error(message, event: %{type: "google_validation", data: Map.from_struct(error)})
        {:error, "Google Transaction - #{message}"}
    end
  end

  defp url(package_name, subscription_id, token) do
    @base_url <>
      "/applications/#{package_name}/purchases/subscriptions/#{subscription_id}/tokens/#{token}"
  end

  defp headers(access_token) do
    [{"Accept", "application/json"}, {"Authorization", "Bearer #{access_token}"}]
  end

  defp decode_body(body) do
    Poison.decode(body)
  end

  defp to_google_transaction({:ok, body}) do
    {:ok,
     %{
       order_id: body["orderId"],
       kind: body["kind"],
       start_time: body["startTimeMillis"] |> convert_time(),
       expiry_time: body["expiryTimeMillis"] |> convert_time(),
       auto_renewing: body["autoRenewing"],
       price_amount: body["priceAmountMicros"] |> convert_price(),
       price_currency_code: body["priceCurrencyCode"],
       country_code: body["countryCode"],
       developer_payload: body["developerPayload"],
       payment_state: body["paymentState"],
       cancel_reason: body["cancelReason"],
       user_cancellation_time:
         if(
           body["cancelReason"] == 0 && not is_nil(body["userCancellationTimeMillis"]),
           do: body["userCancellationTimeMillis"] |> convert_time(),
           else: nil
         )
     }}
  end

  defp to_google_transaction(_), do: {:error, "Android Publisher - 200 - Unable to parse body"}

  defp to_error(%{status_code: 404, body: "Not Found"}),
    do: {:error, "Android Publisher - 404 - Invalid Subscription Token"}

  defp to_error(%{status_code: status_code, body: body}) do
    reason =
      body
      |> decode_body()
      |> error_body_to_string()

    {:error, "Android Publisher - #{status_code} - #{reason}"}
  end

  defp error_body_to_string({:ok, body}) do
    error = Map.get(body, "error", %{})

    errors = Map.get(error, "errors", [])

    errors
    |> Enum.reduce("", fn error, acc ->
      acc <> Map.get(error, "message", "")
    end)
  end

  defp error_body_to_string(_), do: "Unable to parse body"

  defp convert_time(time) when is_binary(time) do
    {time, _} =
      time
      |> Integer.parse(10)

    time
    |> convert_time()
  end

  defp convert_time(time) when is_integer(time) do
    time
    |> DateTime.from_unix!(:millisecond)
  end

  defp convert_time(time), do: time

  defp convert_price(price) when is_binary(price) do
    {price, _} =
      price
      |> Integer.parse(10)

    price
    |> convert_price
  end

  defp convert_price(price) when is_integer(price) do
    price / 1_000_000
  end

  defp convert_price(price), do: price
end
