defmodule Transactions.Validator.Google.Token do
  import Joken

  @spec jwt(String.t(), String.t()) :: {:ok, String.t()} | {:error, Timber.Events.ErrorEvent.t()}
  def jwt(service_account, service_account_private_key) do
    case key_to_jwk(service_account_private_key) do
      jwk = %JOSE.JWK{} ->
        {:ok, issue_jwt(service_account, jwk)}

      _ ->
        error =
          Timber.Events.ErrorEvent.new(
            "Transactions.Validator.Google.Token",
            "Bad/Missing Service Account Private Key",
            backtrace: :erlang.get_stacktrace()
          )

        {:error, error}
    end
  end

  defp issue_jwt(service_account, jwk) do
    issued_at =
      DateTime.utc_now()
      |> DateTime.to_unix()

    expires_at = issued_at + 3600

    %{
      iss: service_account,
      aud: "https://www.googleapis.com/oauth2/v4/token",
      scope: "https://www.googleapis.com/auth/androidpublisher",
      iat: issued_at,
      exp: expires_at
    }
    |> token()
    |> sign(private_key(jwk))
    |> get_compact()
  end

  defp private_key(jwk) do
    jwk
    |> rs256()
  end

  defp key_to_jwk(key) do
    JOSE.JWK.from_pem(key)
  end
end
