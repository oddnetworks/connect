defmodule Transactions.Validator.Apple.Client do
  use HTTPoison.Base

  defp process_response_body(body) do
    Poison.decode(body)
  end

  defp process_request_body(body) do
    case body do
      "" -> body
      _ -> body |> Poison.encode!()
    end
  end

  defp process_request_headers(headers) do
    [{"accept", "application/json"} | headers]
  end

  defp process_request_options(options) do
    [[ssl: [{:versions, [:"tlsv1.2"]}]] | options]
  end
end
