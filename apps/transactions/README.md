# Transactions

Handles verification of in-app purchases from the following platforms:

- Roku
- Web (manual life-cycle management via API)
- Apple
- Google (planned)
- Amazon

## Variables

The following are production environment variables used by Transactions.

###

- `AMAZON_RVS_SANDBOX_BASE_URL` - default `http://amazonrvs:8080/RVSSandbox`
- `TRANSACTION_VALIDATION_THRESHOLD_ROKU` - default `900` (seconds)
- `TRANSACTION_VALIDATION_THRESHOLD_APPLE` - default `900` (seconds)
- `TRANSACTION_VALIDATION_THRESHOLD_GOOGLE` - default `900` (seconds)
- `TRANSACTION_VALIDATION_THRESHOLD_AMAZON` - default `900` (seconds)
