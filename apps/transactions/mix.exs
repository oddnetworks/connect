defmodule Transactions.Mixfile do
  use Mix.Project

  def project do
    [
      app: :transactions,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [main: "Transactions"]
    ]
  end

  def application do
    [extra_applications: [:logger, :timber], mod: {Transactions.Application, []}]
  end

  defp deps do
    [
      {:httpoison, "~> 0.12.0"},
      {:poison, "~> 3.1.0"},
      {:hackney, "~> 1.8.0"},
      {:joken, "~> 1.4.1"},
      {:timex, "~> 3.1"},
      {:timber, "~> 2.6"},
      {:meck, "~> 0.8.7", only: :test},
      {:exvcr, "~> 0.8", only: :test},
      {:connect, in_umbrella: true},
      {:webhooks, in_umbrella: true}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
