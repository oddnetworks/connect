use Mix.Config

config :transactions, ecto_repos: []

config :transactions, Transactions.Validator,
  amazon_rvs_sandbox_base_url: System.get_env("AMAZON_RVS_SANDBOX_BASE_URL")

import_config "#{Mix.env()}.exs"
