use Mix.Config

config :transactions, Transactions.Validator,
  amazon_rvs_sandbox_base_url: "${AMAZON_RVS_SANDBOX_BASE_URL}"
