use Mix.Config

config :transactions, :task_supervisor, Transactions.TestTaskSupervisor
config :transactions, :webhook_module, Webhooks.TestTrigger
