defmodule Proxy.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    cowboy =
      Plug.Adapters.Cowboy.child_spec(
        scheme(),
        Proxy.Plug,
        [],
        port: port(),
        dispatch: [
          {:_, [phoenix_live_reload(), websocket(), proxy()]}
        ]
      )

    children = [
      cowboy
    ]

    opts = [strategy: :one_for_one, name: Proxy.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp phoenix_live_reload() do
    {
      "/phoenix/live_reload/socket/websocket",
      Phoenix.Endpoint.CowboyWebSocket,
      {
        Phoenix.Transports.WebSocket,
        {FrontEnd.Endpoint, Phoenix.LiveReloader.Socket, :websocket}
      }
    }
  end

  defp websocket() do
    {"/socket/websocket", Phoenix.Endpoint.CowboyWebSocket,
     {Phoenix.Transports.WebSocket, {FrontEnd.Endpoint, FrontEnd.UserSocket, :websocket}}}
  end

  defp proxy() do
    {:_, Plug.Adapters.Cowboy.Handler, {Proxy.Plug, []}}
  end

  defp port() do
    case Application.get_env(:proxy, Proxy.Plug)[:port] do
      port when is_binary(port) ->
        port =
          port
          |> Integer.parse(10)
          |> elem(0)

        port

      port ->
        port
    end
  end

  defp scheme() do
    Application.get_env(:proxy, Proxy.Plug)[:scheme]
  end
end
