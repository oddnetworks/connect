defmodule Proxy.Plug do
  def init(options) do
    options
  end

  def call(conn, _opts) do
    cond do
      conn.request_path =~ ~r{/api} ->
        Api.Endpoint.call(conn, [])

      true ->
        FrontEnd.Endpoint.call(conn, [])
    end
  end
end
