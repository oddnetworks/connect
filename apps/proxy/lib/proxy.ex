defmodule Proxy do
  @moduledoc """
  Proxies requests to Web apps that are part of the platform.
  Useful for Heroku deployment when just one web port is exposed.

  See `Proxy.Plug`.
  """
end
