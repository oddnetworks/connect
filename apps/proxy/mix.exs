defmodule Proxy.Mixfile do
  use Mix.Project

  def project do
    [
      app: :proxy,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.5",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [extra_applications: [:logger], mod: {Proxy.Application, []}]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.3.4"},
      {:cowboy, "~> 1.0"},
      {:api, in_umbrella: true},
      {:front_end, in_umbrella: true}
    ]
  end
end
