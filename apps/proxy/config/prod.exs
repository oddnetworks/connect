use Mix.Config

config :proxy, Proxy.Plug,
  port: "${PORT}",
  scheme: :http
