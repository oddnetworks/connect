# This file is responsible for configuring your application
use Mix.Config

config :proxy, ecto_repos: []

config :proxy, Proxy.Plug,
  port: 4000,
  scheme: :http

import_config "#{Mix.env()}.exs"
