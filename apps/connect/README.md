# Connect

Contains main business logic of the platform.

See:

  - [Connect](lib/connect.ex)

## Connect.Auth

Right now, `Connect.Auth` just handles a simple email/password combination.

In the future this could be extended to support:

  - different authentication strategies, e.g.: username+password for customers, LDAP for operators, OAuth for partners
  - 2FA
  - tracking sign in count
  - tracking sign in attempts and locking down
  - tracking IP
  - tracking sign ins from multiple devices
  - showing active sessions across devices (using Phoenix Presence)

## Generating RSA Keys

The following will generate an RSA key without a passphrase and its PEM file:

    ssh-keygen -t rsa -b 2048 -m PEM -f odd_connect_rsa.pem -C accounts@oddnetworks.com -N '' && \
    ssh-keygen -f odd_connect_rsa.pem -e -m PEM > odd_connect_rsa.pub

The `CONNECT_RSA_PRIVATE_KEY` in this case will be `cat odd_connect_rsa.pem`

The `CONNECT_RSA_PUBLIC_KEY` in this case will be `cat odd_connect_rsa.pub` (not `*.pem.pub`)
