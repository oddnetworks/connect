defmodule Connect.Mixfile do
  use Mix.Project

  def project do
    [
      app: :connect,
      version: "1.2.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      docs: [main: "Connect"]
    ]
  end

  def application do
    [extra_applications: [:logger, :timber], mod: {Connect.Application, []}]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:comeonin, "~> 3.0"},
      {:cloak, "~> 0.5.0"},
      {:ecto, "~> 2.2"},
      {:postgrex, ">= 0.13.5"},
      {:uuid, "~> 1.1.7"},
      {:joken, "~> 1.4.1"},
      {:poison, "~> 3.1.0", override: true},
      {:scrivener_ecto, "~> 1.0"},
      {:slugger, "~> 0.1.0"},
      {:timex, "~> 3.1"},
      {:timber, "~> 2.6"},
      {:ex_machina, "~> 2.2", only: [:dev, :test]},
      {:messenger, in_umbrella: true}
    ]
  end

  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "ecto.seed"],
      "ecto.seed": ["run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
