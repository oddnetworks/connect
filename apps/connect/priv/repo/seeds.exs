defmodule Connect.Seeds do
  alias Connect.{
    AuthAccount,
    Customer,
    Device,
    DeviceUser,
    Entitlement,
    Organization,
    Repo,
    Transaction,
    VerifiedConnection,
    Webhook
  }

  def run! do
    org =
      Organization.build(%{
        name: "Odd Networks",
        urn: "odd",
        settings: %{
          roku_api_key: "fake-roku-api-key"
        }
      })
      |> Repo.insert!()

    _acct =
      Connect.Accounts.new_changeset(%{
        email: "admin@oddnetworks.com",
        name: "Odd Networks",
        password: "firesale",
        is_admin: true
      })
      |> Repo.insert!()

    [c_acct, a_acct, o_acct] =
      [
        Connect.Accounts.new_changeset(%{
          email: "org-customer@oddnetworks.com",
          name: "Organization Customer",
          password: "firesale",
          is_admin: false
        }),
        Connect.Accounts.new_changeset(%{
          email: "org-admin@oddnetworks.com",
          name: "Organization Admin",
          password: "firesale",
          is_admin: false
        }),
        Connect.Accounts.new_changeset(%{
          email: "org-owner@oddnetworks.com",
          name: "Organization Owner",
          password: "firesale",
          is_admin: false
        })
      ]
      |> Enum.map(&Repo.insert!(&1))

    [
      Customer.build(%{
        organization: org,
        auth_account: c_acct,
        role: "customer"
      }),
      Customer.build(%{
        organization: org,
        auth_account: a_acct,
        role: "admin"
      }),
      Customer.build(%{
        organization: org,
        auth_account: o_acct,
        role: "owner"
      })
    ]
    |> Enum.each(&Repo.insert!(&1))

    device_ios =
      Device.changeset(%Device{}, %{
        external_identifier: "573eb388-af1b-4828-9e11-661063b3b0f8",
        platform: "APPLE_MOBILE"
      })
      |> Repo.insert!()

    device_android =
      Device.changeset(%Device{}, %{
        id: "cdfef7ac-7ca9-4ded-8b82-4314662eb5a3",
        external_identifier: "3fed49d1-399a-4736-a5c5-f75e6a7bff08",
        platform: "GOOGLE_TV"
      })
      |> Repo.insert!()

    device_user_foo =
      DeviceUser.changeset(%DeviceUser{}, %{
        id: "8a32a1d8-fc79-441c-80db-f47f86110f0e",
        email: "foo@example.com"
      })
      |> Repo.insert!()

    device_user_bar =
      DeviceUser.changeset(%DeviceUser{}, %{
        id: "38de14a1-1c14-4415-9a99-e9477faf1837",
        email: "bar@example.com"
      })
      |> Repo.insert!()

    entitlements =
      [
        entitlement_roku_silver,
        entitlement_roku_gold,
        entitlement_web_silver,
        entitlement_web_gold,
        entitlement_google_silver,
        entitlement_google_gold,
        entitlement_amazon_silver,
        entitlement_amazon_gold,
        entitlement_apple_silver,
        entitlement_apple_gold
      ] =
      [
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Silver",
          entitlement_identifier: "silver",
          platform: "ROKU",
          product_identifier: "roku-monthly-silver"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Gold",
          entitlement_identifier: "gold",
          platform: "ROKU",
          product_identifier: "roku-monthly-gold"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Silver",
          entitlement_identifier: "silver",
          platform: "WEB",
          product_identifier: "web-monthly-silver"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Gold",
          entitlement_identifier: "gold",
          platform: "WEB",
          product_identifier: "web-monthly-gold"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Silver",
          entitlement_identifier: "silver",
          platform: "GOOGLE",
          product_identifier: "google-monthly-silver"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Gold",
          entitlement_identifier: "gold",
          platform: "GOOGLE",
          product_identifier: "google-monthly-gold"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Silver",
          entitlement_identifier: "silver",
          platform: "AMAZON",
          product_identifier: "amazon-monthly-silver"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Gold",
          entitlement_identifier: "gold",
          platform: "AMAZON",
          product_identifier: "amazon-monthly-gold"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Silver",
          entitlement_identifier: "silver",
          platform: "APPLE",
          product_identifier: "apple-monthly-silver"
        }),
        Entitlement.build(%{
          organization: org,
          type: "subscription",
          name: "Monthly - Gold",
          entitlement_identifier: "gold",
          platform: "APPLE",
          product_identifier: "apple-monthly-gold"
        })
      ]
      |> Enum.map(&Repo.insert!(&1))

    transaction1 =
      Transaction.build(%{
        device_user: device_user_foo,
        entitlement: entitlement_roku_silver,
        platform: entitlement_roku_silver.platform,
        external_identifier: "885dc39e-ceb0-4374-8929-0b3a06b023e9",
        last_validated_at: DateTime.utc_now(),
        receipt: %{
          code: "ABCDEFG",
          cost: "$3.99",
          expirationDate: "2017-07-22T15:30:10Z",
          freeTrialQuantity: 1,
          freeTrialType: "Months",
          name: "Silver",
          productType: "MonthlySub",
          purchaseDate: "2017-06-22T15:30:10Z",
          purchaseId: "885dc39e-ceb0-4374-8929-0b3a06b023e9",
          qty: 1,
          renewalDate: "2017-07-22T15:30:10Z"
        }
      })
      |> Repo.insert!()

    transaction2 =
      Transaction.build(%{
        device_user: device_user_foo,
        entitlement: entitlement_roku_gold,
        platform: entitlement_roku_gold.platform,
        external_identifier: "ad3c7362-0aca-455f-bd1f-b73f5b8129ed",
        last_validated_at: DateTime.utc_now(),
        receipt: %{
          code: "HIJKLMNOP",
          cost: "$9.99",
          expirationDate: "2017-06-22T15:30:10Z",
          freeTrialQuantity: 1,
          freeTrialType: "Months",
          name: "Gold",
          productType: "MonthlySub",
          purchaseDate: "2017-05-22T15:30:10Z",
          purchaseId: "ad3c7362-0aca-455f-bd1f-b73f5b8129ed",
          qty: 1,
          renewalDate: "2017-06-22T15:30:10Z"
        }
      })
      |> Repo.insert!()

    1..50
    |> Enum.map(fn i ->
      days_ago = 0..-365 |> Enum.random()
      hours = -12..12 |> Enum.random()
      minutes = -30..30 |> Enum.random()

      inserted_at =
        Timex.shift(DateTime.utc_now(), days: days_ago, hours: hours, minutes: minutes)

      deleted_at =
        if Integer.mod(i, 25) == 0 do
          Timex.shift(inserted_at, days: 1)
        else
          nil
        end

      invalidated_at =
        if Integer.mod(i, 15) == 0 do
          Timex.shift(inserted_at, days: 30)
        else
          nil
        end

      invalidation_reason =
        if not is_nil(inserted_at) do
          [
            "Canceled by user",
            "Subscription lapsed",
            "Refunded"
          ]
          |> Enum.random()
        else
          nil
        end

      entitlement =
        entitlements
        |> Enum.random()

      device_user =
        DeviceUser.changeset(%DeviceUser{}, %{
          email: "user#{i}@example.com"
        })
        |> Repo.insert!()

      Transaction.build(%{
        device_user: device_user,
        entitlement: entitlement,
        platform: entitlement.platform,
        external_identifier: UUID.uuid4(),
        last_validated_at: DateTime.utc_now(),
        invalidated_at: invalidated_at,
        invalidation_reason: invalidation_reason,
        inserted_at: inserted_at,
        deleted_at: deleted_at,
        receipt: %{
          name: "Test Transaction #{i}"
        }
      })
    end)
    |> Enum.each(&Repo.insert!(&1))

    Transaction.build(%{
      device_user: device_user_bar,
      entitlement: entitlement_web_gold,
      platform: entitlement_web_gold.platform,
      external_identifier: "886af5a8-0145-4f6e-83fa-89fb3847125e",
      last_validated_at: DateTime.utc_now(),
      receipt: %{
        name: "Gold - Monthly",
        purchaseId: "886af5a8-0145-4f6e-83fa-89fb3847125e"
      }
    })
    |> Repo.insert!()

    {:ok, invalidated_at, _} = DateTime.from_iso8601("2017-05-31T09:30:00Z")

    transaction2
    |> Ecto.Changeset.cast(
      %{invalidated_at: invalidated_at, invalidation_reason: "Subscription expired"},
      [:invalidated_at, :invalidation_reason]
    )
    |> Repo.update!()

    [
      VerifiedConnection.build(%{
        id: "ad767fd0-2959-448e-8ce4-c6c3098062b0",
        organization: org,
        device_user: device_user_foo,
        device: device_android,
        verified_at: NaiveDateTime.utc_now()
      }),
      VerifiedConnection.build(%{
        id: "98ad28e1-fac9-4e88-818a-485e112a13e2",
        organization: org,
        device_user: device_user_bar,
        device: device_ios,
        verified_at: NaiveDateTime.utc_now()
      }),
      Webhook.build(%{organization: org, type: "verified_connection", url: "http://example.com"}),
      Webhook.build(%{
        organization: org,
        type: "verified_connection",
        url: "http://example.com/private",
        headers: %{authorization: "Bearer 123"}
      }),
      Webhook.build(%{organization: org, type: "new_transaction", url: "http://example.com"}),
      Webhook.build(%{
        organization: org,
        type: "invalidated_transaction",
        url: "http://example.com"
      })
    ]
    |> Enum.each(&Repo.insert!(&1))
  end
end

Connect.Seeds.run!()
