defmodule Connect.Repo.Migrations.AddDeviceTable do
  use Ecto.Migration

  def change do
    create table(:devices, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :urn, :string, null: false

      timestamps()
    end

    create unique_index(:devices, [:urn])
  end
end
