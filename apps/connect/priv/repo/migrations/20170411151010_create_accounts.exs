defmodule Connect.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:auth_accounts, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :email, :citext, null: false
      add :password_hash, :string

      timestamps()
    end

    create unique_index(:auth_accounts, [:email])
  end
end
