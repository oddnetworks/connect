defmodule Connect.Repo.Migrations.CreateOrganizationTemplates do
  use Ecto.Migration
  import Ecto.Query

  def up do
    create table(:organization_templates, primary_key: false) do
      add(:id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()"))

      add(
        :organization_id,
        references(:organizations, on_delete: :delete_all, type: :uuid),
        null: false
      )

      add(:device_connection_email_subject, :text, null: false)
      add(:device_connection_email_html, :text, null: false)
      add(:device_connection_email_text, :text, null: false)
    end

    create(unique_index(:organization_templates, [:organization_id]))

    flush()

    new_organization_templates =
      from(
        o in "organizations",
        select: %{
          id: o.id,
          templates: o.templates
        }
      )
      |> Connect.Repo.all()
      |> Enum.map(fn org ->
        %{
          organization_id: org.id,
          device_connection_email_subject:
            Map.get(
              org.templates,
              "device_connection_email_subject",
              Connect.Organization.Email.device_connection_email_subject_default()
            ),
          device_connection_email_html:
            Map.get(
              org.templates,
              "device_connection_email_html",
              Connect.Organization.Email.device_connection_email_html_default()
            ),
          device_connection_email_text:
            Map.get(
              org.templates,
              "device_connection_email_text",
              Connect.Organization.Email.device_connection_email_text_default()
            )
        }
      end)

    Connect.Repo.insert_all("organization_templates", new_organization_templates)

    alter table(:organizations) do
      remove(:templates)
    end
  end

  def down do
    raise "Can't migrate down"
  end
end
