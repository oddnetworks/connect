defmodule Connect.Repo.Migrations.AddProductsTable do
  use Ecto.Migration

  def change do
    create table(:products, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :entitlement_id, references(:entitlements, on_delete: :nothing, type: :uuid), null: false
      add :platform, :string, null: false
      add :urn, :string, null: false

      timestamps()
    end

    create index(:products, [:entitlement_id])
    create index(:products, [:platform])
    create index(:products, [:urn], unique: true)
  end
end
