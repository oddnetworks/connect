defmodule Connect.Repo.Migrations.CreateOrganizationSettings do
  use Ecto.Migration
  import Ecto.Query

  def up do
    create table(:organization_settings, primary_key: false) do
      add(:id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()"))

      add(
        :organization_id,
        references(:organizations, on_delete: :delete_all, type: :uuid),
        null: false
      )

      add(:in_review_mode, :boolean, null: false, default: false)
      add(:roku_api_key, :binary)
      add(:apple_app_shared_secret, :binary)
      add(:use_apple_sandbox_environment, :boolean, null: false, default: false)
      add(:google_package_name, :string)
      add(:google_service_account, :string)
      add(:google_service_account_private_key, :binary)

      add(:encryption_version, :binary)
    end

    create(index(:organization_settings, [:encryption_version]))
    create(unique_index(:organization_settings, [:organization_id]))

    flush()

    new_organization_settings =
      from(
        o in "organizations",
        select: %{
          id: o.id,
          settings: o.settings
        }
      )
      |> Connect.Repo.all()
      |> Enum.map(fn org ->
        %{
          organization_id: org.id,
          in_review_mode: Map.get(org.settings, "in_review_mode", false),
          roku_api_key: Map.get(org.settings, "roku_api_key") |> Cloak.encrypt(),
          apple_app_shared_secret:
            Map.get(org.settings, "apple_app_shared_secret") |> Cloak.encrypt(),
          use_apple_sandbox_environment:
            Map.get(org.settings, "use_apple_sandbox_environment", false),
          google_package_name: Map.get(org.settings, "google_package_name"),
          google_service_account: Map.get(org.settings, "google_service_account"),
          google_service_account_private_key:
            Map.get(org.settings, "google_service_account_private_key") |> Cloak.encrypt(),
          encryption_version: Cloak.version()
        }
      end)

    Connect.Repo.insert_all("organization_settings", new_organization_settings)

    alter table(:organizations) do
      remove(:settings)
    end
  end

  def down do
    raise "Unable to migrate down"
  end
end
