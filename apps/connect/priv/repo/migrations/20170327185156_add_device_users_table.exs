defmodule Connect.Repo.Migrations.AddDeviceUsersTable do
  use Ecto.Migration

  def change do
    # add case insensitive text type
    execute("CREATE EXTENSION IF NOT EXISTS citext;")
    execute("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")

    create table(:device_users, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :email, :citext, null: false

      timestamps()
    end

    create unique_index(:device_users, [:email])
  end
end
