defmodule Connect.Repo.Migrations.AddPlatformIdentitiesTable do
  use Ecto.Migration

  def change do
    create table(:platform_identities, primary_key: false) do
      add(:id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()"))

      add(:device_user_id, references(:device_users, on_delete: :nothing, type: :uuid),
        null: false
      )

      add(:platform, :string, null: false)
      add(:external_identifier, :string, null: false)

      timestamps()
    end

    create(index(:platform_identities, [:device_user_id]))
    create(index(:platform_identities, [:platform, :external_identifier], unique: true))
  end
end
