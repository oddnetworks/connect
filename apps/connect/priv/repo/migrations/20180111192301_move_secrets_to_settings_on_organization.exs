defmodule Connect.Repo.Migrations.MoveSecretsToSettingsOnOrganization do
  use Ecto.Migration
  import Ecto.Query

  def up do
    alter table(:organizations) do
      add(:settings, :map)
    end

    flush()

    from(o in "organizations", update: [set: [settings: o.secrets]])
    |> Connect.Repo.update_all([])

    alter table(:organizations) do
      remove(:secrets)
    end
  end

  def down do
    alter table(:organizations) do
      add(:secrets, :map)
    end

    flush()

    from(o in "organizations", update: [set: [secrets: o.settings]])
    |> Connect.Repo.update_all([])

    alter table(:organizations) do
      remove(:settings)
    end
  end
end
