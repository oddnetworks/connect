defmodule Connect.Repo.Migrations.ConvertUrnsToIdentifiersOnDevices do
  use Ecto.Migration
  import Ecto.Query

  def up do
    # migrate devices.urn to devices.external_identifier with unique index tied to devices.platform
    alter table(:devices) do
      add :platform, :citext
    end
    flush()
    from(d in "devices", update: [set: [platform: "WEB"]])
    |> Connect.Repo.update_all([])
    alter table(:devices) do
      modify :platform, :citext, null: false
    end

    drop unique_index(:devices, [:urn])
    rename table(:devices), :urn, to: :external_identifier
    create unique_index(:devices, [:platform, :external_identifier])
  end

  def down do
    # reverse migration of devices.urn to devices.external_identifier with unique index tied to devices.platform
    drop unique_index(:devices, [:platform, :external_identifier])
    alter table(:devices) do
      remove :platform
    end
    rename table(:devices), :external_identifier, to: :urn
    create unique_index(:devices, [:urn])
  end
end
