defmodule Connect.Repo.Migrations.AddUniqueAccountOrganizationIndexToCustomers do
  use Ecto.Migration

  def change do
    create(
      unique_index(
        :customers,
        [:auth_account_id, :organization_id],
        name: :customers_auth_account_organization_index
      )
    )
  end
end
