defmodule Connect.Repo.Migrations.ConvertUrnsToIdentifiersOnProducts do
  use Ecto.Migration
  import Ecto.Query

  def up do
    # migrate products.urn to products.external_identifier with unique index tied to products.platform
    drop unique_index(:products, [:urn])
    rename table(:products), :urn, to: :external_identifier
    create unique_index(:products, [:platform, :external_identifier])
    flush()
    from(p in "products", update: [set: [external_identifier: fragment("split_part(?, ':', 4)", p.external_identifier)]])
    |> Connect.Repo.update_all([])
  end

  def down do
    # reverse migration of products.urn to products.external_identifier with unique index tied to products.platform
    drop unique_index(:products, [:platform, :external_identifier])
    rename table(:products), :external_identifier, to: :urn
    create unique_index(:products, [:urn])
    flush()
    from(p in "products", update: [set: [urn: fragment("(CASE WHEN platform IN ('AMAZON', 'AMAZON_MOBILE', 'AMAZON_TV') THEN concat('urn:amazon:product:', urn) WHEN platform IN ('ANDROID', 'GOOGLE', 'GOOGLE_MOBILE', 'GOOGLE_TV') THEN concat('urn:android:product:', urn) WHEN platform IN ('APPLE', 'APPLE_MOBILE', 'APPLE_TV') THEN concat('urn:apple:product:', urn) WHEN platform='ROKU' THEN concat('urn:roku:product:', urn) WHEN platform='WEB' THEN concat('urn:web:product:', urn) ELSE urn END)")]])
    |> Connect.Repo.update_all([])
  end
end
