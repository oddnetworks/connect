defmodule Connect.Repo.Migrations.AddCustomersTable do
  use Ecto.Migration

  def change do
    create table(:customers, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :name, :string, null: false
      add :email, :citext, null: false
      add :organization_roles, {:array, :string}, null: false, default: []
      add :auth_account_id, references(:auth_accounts, on_delete: :nothing, type: :uuid), null: false
      add :organization_id, references(:organizations, on_delete: :nothing, type: :uuid), null: false

      timestamps()
    end

    create index(:customers, [:email], unique: true)
    create index(:customers, [:auth_account_id])
    create index(:customers, [:organization_id])
  end
end
