defmodule Connect.Repo.Migrations.UpdateProductAndTransactionPlatforms do
  use Ecto.Migration
  import Ecto.Query

  def up do
    from(p in "products", update: [set: [platform: fragment("(CASE WHEN platform IN ('AMAZON', 'AMAZON_MOBILE', 'AMAZON_TV') THEN 'AMAZON' WHEN platform IN ('ANDROID', 'GOOGLE', 'GOOGLE_MOBILE', 'GOOGLE_TV') THEN 'GOOGLE' WHEN platform IN ('APPLE', 'APPLE_MOBILE', 'APPLE_TV') THEN 'APPLE' WHEN platform='ROKU' THEN 'ROKU' WHEN platform='WEB' THEN 'WEB' ELSE 'WEB' END)")]])
    |> Connect.Repo.update_all([])
    from(t in "transactions", update: [set: [platform: fragment("(CASE WHEN platform IN ('AMAZON', 'AMAZON_MOBILE', 'AMAZON_TV') THEN 'AMAZON' WHEN platform IN ('ANDROID', 'GOOGLE', 'GOOGLE_MOBILE', 'GOOGLE_TV') THEN 'GOOGLE' WHEN platform IN ('APPLE', 'APPLE_MOBILE', 'APPLE_TV') THEN 'APPLE' WHEN platform='ROKU' THEN 'ROKU' WHEN platform='WEB' THEN 'WEB' ELSE 'WEB' END)")]])
    |> Connect.Repo.update_all([])
  end

  def down do
    from(p in "products", update: [set: [platform: fragment("(CASE WHEN platform IN ('AMAZON', 'AMAZON_MOBILE', 'AMAZON_TV') THEN 'AMAZON_MOBILE' WHEN platform IN ('ANDROID', 'GOOGLE', 'GOOGLE_MOBILE', 'GOOGLE_TV') THEN 'GOOGLE_MOBILE' WHEN platform IN ('APPLE', 'APPLE_MOBILE', 'APPLE_TV') THEN 'APPLE_MOBILE' WHEN platform='ROKU' THEN 'ROKU' WHEN platform='WEB' THEN 'WEB' ELSE 'WEB' END)")]])
    |> Connect.Repo.update_all([])
    from(t in "transactions", update: [set: [platform: fragment("(CASE WHEN platform IN ('AMAZON', 'AMAZON_MOBILE', 'AMAZON_TV') THEN 'AMAZON_MOBILE' WHEN platform IN ('ANDROID', 'GOOGLE', 'GOOGLE_MOBILE', 'GOOGLE_TV') THEN 'GOOGLE_MOBILE' WHEN platform IN ('APPLE', 'APPLE_MOBILE', 'APPLE_TV') THEN 'APPLE_MOBILE' WHEN platform='ROKU' THEN 'ROKU' WHEN platform='WEB' THEN 'WEB' ELSE 'WEB' END)")]])
    |> Connect.Repo.update_all([])
  end
end
