defmodule Connect.Repo.Migrations.MakePlatformColumnsCaseInsensitive do
  use Ecto.Migration

  def up do
    alter table(:products) do
      modify :platform, :citext, null: false
    end

    alter table(:transactions) do
      modify :platform, :citext, null: false
    end
  end

  def down do
    alter table(:products) do
      modify :platform, :string, null: false
    end

    alter table(:transactions) do
      modify :platform, :string, null: false
    end
  end
end
