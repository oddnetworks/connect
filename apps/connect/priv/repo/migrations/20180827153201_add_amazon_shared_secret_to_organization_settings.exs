defmodule Connect.Repo.Migrations.AddAmazonSharedSecretToOrganizationSettings do
  use Ecto.Migration

  def change do
    alter table(:organization_settings) do
      add(:amazon_shared_secret, :binary)
      add(:amazon_use_sandbox, :boolean, default: false)
    end
  end
end
