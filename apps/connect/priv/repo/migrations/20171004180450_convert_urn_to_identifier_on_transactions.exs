defmodule Connect.Repo.Migrations.ConvertUrnToIdentifierOnTransactions do
  use Ecto.Migration
  import Ecto.Query

  def up do
    # migrate transactions.urn to transactions.external_identifier with unique index tied to transactions.platform
    drop unique_index(:transactions, [:urn])
    rename table(:transactions), :urn, to: :external_identifier
    create unique_index(:transactions, [:platform, :external_identifier])
    flush()
    from(p in "transactions", update: [set: [external_identifier: fragment("split_part(?, ':', 4)", p.external_identifier)]])
    |> Connect.Repo.update_all([])
  end

  def down do
    # reverse migration of transactions.urn to transactions.external_identifier with unique index tied to transactions.platform
    drop unique_index(:transactions, [:platform, :external_identifier])
    rename table(:transactions), :external_identifier, to: :urn
    create unique_index(:transactions, [:urn])
    flush()
    from(p in "transactions", update: [set: [urn: fragment("(CASE WHEN platform IN ('AMAZON', 'AMAZON_TV', 'AMAZON_MOBILE') THEN concat('urn:amazon:transaction:', urn) WHEN platform IN ('GOOGLE', 'ANDROID', 'GOOGLE_TV', 'GOOGLE_MOBILE') THEN concat('urn:android:transaction:', urn) WHEN platform IN ('APPLE', 'APPLE_TV', 'APPLE_MOBILE') THEN concat('urn:apple:transaction:', urn) WHEN platform='ROKU' THEN concat('urn:roku:transaction:', urn) WHEN platform='WEB' THEN concat('urn:web:transaction:', urn) ELSE urn END)")]])
    |> Connect.Repo.update_all([])
  end
end
