defmodule Connect.Repo.Migrations.AddOrganizationsTable do
  use Ecto.Migration

  def change do
    create table(:organizations, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :urn, :string, null: false
      add :name, :string, null: false
      add :subdomain, :citext, null: false

      timestamps()
    end

    create unique_index(:organizations, [:urn])
    create unique_index(:organizations, [:subdomain])
  end
end
