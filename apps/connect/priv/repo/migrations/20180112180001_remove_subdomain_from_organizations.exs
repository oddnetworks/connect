defmodule Connect.Repo.Migrations.RemoveSubdomainFromOrganizations do
  use Ecto.Migration
  import Ecto.Query

  def up do
    from(o in "organizations", update: [set: [urn: o.subdomain]])
    |> Connect.Repo.update_all([])
    alter table(:organizations) do
      remove :subdomain
    end
  end

  def down do
    alter table(:organizations) do
      add :subdomain, :citext
    end
    flush()
    from(o in "organizations", update: [set: [subdomain: o.urn]])
    |> Connect.Repo.update_all([])
    alter table(:organizations) do
      modify :subdomain, :citext, null: false
    end
    create unique_index(:organizations, [:subdomain])
  end
end
