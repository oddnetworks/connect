defmodule Connect.Repo.Migrations.AddEntitlementsTable do
  use Ecto.Migration

  def change do
    create table(:entitlements, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :organization_id, references(:organizations, on_delete: :nothing, type: :uuid), null: false
      add :name, :string, null: false
      add :urn, :string, null: false

      timestamps()
    end

    create index(:entitlements, [:organization_id])
    create index(:entitlements, [:organization_id, :urn], unique: true)
  end
end
