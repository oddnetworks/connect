defmodule Connect.Repo.Migrations.AddDefaultsForOrganizationSecrets do
  use Ecto.Migration

  def change do
    alter table(:organizations) do
      modify :secrets, :map, default: "{}", null: false
    end
  end
end
