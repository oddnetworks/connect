defmodule Connect.Repo.Migrations.ConvertUrnsToIdentifiersOnEntitlements do
  use Ecto.Migration

  def up do
    # migrate entitlements.urn to entitlements.external_identifier keeping unique index tied to entitlements.organization_id
    rename table(:entitlements), :urn, to: :external_identifier
  end

  def down do
    # reverse migration of entitlements.urn to entitlements.external_identifier keeping unique index tied to entitlements.organization_id
    rename table(:entitlements), :external_identifier, to: :urn
  end
end
