defmodule Connect.Repo.Migrations.AddPendingConnectionsTable do
  use Ecto.Migration

  def change do
    create table(:pending_connections, primary_key: false) do
      add(:id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()"))

      add(:organization_id, references(:organizations, on_delete: :nothing, type: :uuid),
        null: false
      )

      add(:access_token, :string, null: false)
      add(:email, :string, null: false)
      add(:device_identifier, :string, null: false)
      add(:platform, :string, null: false)
      add(:platform_identity, :string)
      add(:uri, :string, null: false)

      add(:expires_at, :utc_datetime, null: false)
      add(:verified_at, :utc_datetime)
      add(:jwt, :text)
      add(:verified_during_review, :boolean, default: false, null: false)

      timestamps()
    end

    create(index(:pending_connections, [:access_token], unique: true))
    create(index(:pending_connections, [:expires_at]))
    create(index(:pending_connections, [:verified_at]))
    create(index(:pending_connections, [:jwt], unique: true))
  end
end
