defmodule Connect.Repo.Migrations.RemoveEmailAndNameFromCustomers do
  use Ecto.Migration
  import Ecto.Query

  def up do
    alter table(:auth_accounts) do
      add(:name, :string)
    end

    flush()

    from(
      a in "auth_accounts",
      join: c in "customers",
      on: a.id == c.auth_account_id,
      where: not a.is_admin,
      update: [set: [name: c.name]]
    )
    |> Connect.Repo.update_all([])

    from(
      a in "auth_accounts",
      where: a.is_admin,
      update: [set: [name: "Administrator"]]
    )
    |> Connect.Repo.update_all([])

    from(
      a in "auth_accounts",
      where: is_nil(a.name),
      update: [set: [name: "Unknown"]]
    )
    |> Connect.Repo.update_all([])

    flush()

    alter table(:auth_accounts) do
      modify(:name, :string, null: false)
    end

    alter table(:customers) do
      remove(:name)
      remove(:email)
    end
  end

  def down do
    alter table(:customers) do
      add(:name, :string)
    end

    flush()

    from(
      c in "customers",
      join: a in "auth_accounts",
      on: a.id == c.auth_account_id,
      where: not a.is_admin,
      update: [set: [name: a.name]]
    )
    |> Connect.Repo.update_all([])

    from(
      c in "customers",
      where: is_nil(c.name),
      update: [set: [name: "Unknown"]]
    )
    |> Connect.Repo.update_all([])

    flush()

    alter table(:customers) do
      modify(:name, :string, null: false)
    end

    alter table(:auth_accounts) do
      remove(:name)
    end
  end
end
