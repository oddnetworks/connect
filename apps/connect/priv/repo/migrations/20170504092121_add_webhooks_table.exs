defmodule Connect.Repo.Migrations.AddWebhooksTable do
  use Ecto.Migration

  def change do
    create table(:webhooks, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :organization_id, references(:organizations, on_delete: :nothing, type: :uuid), null: false
      add :type, :string, null: false
      add :url, :string, null: false
      add :headers, :jsonb

      timestamps()
    end

    create index(:webhooks, [:organization_id])
    create index(:webhooks, [:organization_id, :type])
  end
end
