defmodule Connect.Repo.Migrations.MoveTimezoneFromCustomersToAccounts do
  use Ecto.Migration
  import Ecto.Query

  def up do
    alter table(:auth_accounts) do
      add(:timezone, :string, null: false, default: "America/New_York")
    end

    flush()

    from(
      a in "auth_accounts",
      join: c in "customers",
      on: a.id == c.auth_account_id,
      where: not a.is_admin,
      update: [set: [timezone: c.timezone]]
    )
    |> Connect.Repo.update_all([])

    alter table(:customers) do
      remove(:timezone)
    end
  end

  def down do
    alter table(:customers) do
      add(:timezone, :string, null: false, default: "America/New_York")
    end

    flush()

    from(
      c in "customers",
      join: a in "auth_accounts",
      on: a.id == c.auth_account_id,
      where: not a.is_admin,
      update: [set: [timezone: a.timezone]]
    )
    |> Connect.Repo.update_all([])

    alter table(:auth_accounts) do
      remove(:timezone)
    end
  end
end
