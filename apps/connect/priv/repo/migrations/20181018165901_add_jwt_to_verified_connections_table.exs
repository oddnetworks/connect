defmodule Connect.Repo.Migrations.AddJwtToVerifiedConnectionsTable do
  use Ecto.Migration

  def change do
    alter table(:verified_connections) do
      add(:jwt, :text)
    end

    create(index(:verified_connections, [:jwt], unique: true))
  end
end
