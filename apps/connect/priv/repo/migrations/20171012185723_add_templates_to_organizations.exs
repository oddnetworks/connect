defmodule Connect.Repo.Migrations.AddTemplatesToOrganizations do
  use Ecto.Migration

  def change do
    alter table(:organizations) do
      add :templates, :map, default: "{}", null: false
    end
  end
end
