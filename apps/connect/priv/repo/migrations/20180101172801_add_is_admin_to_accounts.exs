defmodule Connect.Repo.Migrations.AddIsAdminToAccounts do
  use Ecto.Migration

  def change do
    alter table(:auth_accounts) do
      add :is_admin, :boolean, default: false
    end
  end
end
