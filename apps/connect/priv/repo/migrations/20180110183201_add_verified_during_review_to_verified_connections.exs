defmodule Connect.Repo.Migrations.AddVerifiedDuringReviewToVerifiedConnections do
  use Ecto.Migration

  def change do
    alter table(:verified_connections) do
      add :verified_during_review, :boolean, default: false
    end
  end
end
