defmodule Connect.Repo.Migrations.AddTransactionsTable do
  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :device_user_id, references(:device_users, on_delete: :nothing, type: :uuid), null: false
      add :entitlement_id, references(:entitlements, on_delete: :nothing, type: :uuid), null: false
      add :product_id, references(:products, on_delete: :nothing, type: :uuid), null: false
      add :urn, :string, null: false
      add :platform, :string, null: false

      add :last_validated_at, :utc_datetime, null: false
      add :invalidated_at, :utc_datetime
      add :invalidation_reason, :string

      add :receipt, :map

      timestamps()
    end

    create index(:transactions, [:device_user_id])
    create index(:transactions, [:entitlement_id])
    create index(:transactions, [:product_id])
    create index(:transactions, [:urn], unique: true)
    create index(:transactions, [:invalidated_at])
  end
end
