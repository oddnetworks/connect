defmodule Connect.Repo.Migrations.AddVerifiedConnectionsTable do
  use Ecto.Migration

  def change do
    create table(:verified_connections, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :organization_id, references(:organizations, on_delete: :nothing, type: :uuid), null: false
      add :device_user_id, references(:device_users, on_delete: :nothing, type: :uuid), null: false
      add :device_id, references(:devices, on_delete: :nothing, type: :uuid), null: false
      add :verified_at, :utc_datetime, null: false
    end

    create index(:verified_connections, [:verified_at])
    create index(:verified_connections, [:organization_id, :device_user_id])
    create index(:verified_connections, [:organization_id, :device_id])
    create index(:verified_connections, [:device_user_id, :device_id])
  end
end
