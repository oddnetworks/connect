defmodule Connect.Repo.Migrations.AddDeletedAtToTransactions do
  use Ecto.Migration

  def change do
    alter table(:transactions) do
      add(:deleted_at, :utc_datetime)
    end

    create(index(:transactions, [:deleted_at]))
  end
end
