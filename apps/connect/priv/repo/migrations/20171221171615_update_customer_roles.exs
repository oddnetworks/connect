defmodule Connect.Repo.Migrations.UpdateCustomerRoles do
  use Ecto.Migration
  import Ecto.Query

  def up do
    alter table(:customers) do
      add(:role, :string, null: false, default: "customer")
    end

    flush()

    from(
      c in "customers",
      update: [
        set: [
          role:
            fragment(
              "CASE WHEN 'owner' = ANY(organization_roles) THEN 'owner' WHEN 'admin' = ANY(organization_roles) THEN 'admin' ELSE 'customer' END"
            )
        ]
      ]
    )
    |> Connect.Repo.update_all([])

    alter table(:customers) do
      remove(:organization_roles)
    end

    create(unique_index(:customers, [:auth_account_id, :organization_id]))
  end

  def down do
    drop(unique_index(:customers, [:auth_account_id, :organization_id]))

    alter table(:customers) do
      add(:organization_roles, {:array, :string}, null: false, default: [])
    end

    flush()

    from(
      c in "customers",
      update: [
        set: [
          organization_roles:
            fragment(
              "CASE WHEN role = 'owner' THEN '{owner}' WHEN role = 'billing' THEN '{billing}' ELSE '{}' END"
            )
        ]
      ]
    )

    alter table(:customers) do
      remove(:role)
    end
  end
end
