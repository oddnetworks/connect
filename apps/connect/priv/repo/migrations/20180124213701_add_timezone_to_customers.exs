defmodule Connect.Repo.Migrations.AddTimezoneToCustomers do
  use Ecto.Migration

  def change do
    alter table(:customers) do
      add(:timezone, :string, null: false, default: "America/New_York")
    end
  end
end
