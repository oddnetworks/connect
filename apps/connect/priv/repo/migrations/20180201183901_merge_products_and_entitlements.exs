defmodule Connect.Repo.Migrations.MergeProductsAndEntitlements do
  use Ecto.Migration
  import Ecto.Query

  def up do
    # - create new entitlements table
    create table(:new_entitlements, primary_key: false) do
      add(:id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()"))
      add(:type, :string, null: false)

      add(
        :organization_id,
        :uuid,
        null: false
      )

      add(:name, :string, null: false)
      add(:description, :text)
      add(:platform, :citext, null: false)
      add(:entitlement_identifier, :string, null: false)
      add(:product_identifier, :string, null: false)

      timestamps()
    end

    create(
      unique_index(
        :new_entitlements,
        [:organization_id, :entitlement_identifier, :product_identifier],
        name: :entitlements_organization_identifiers_index
      )
    )

    create(
      unique_index(
        :new_entitlements,
        [:organization_id, :platform, :product_identifier],
        name: :entitlements_organization_platform_product_index
      )
    )

    alter table(:transactions) do
      add(
        :new_entitlement_id,
        references(:new_entitlements, on_delete: :nothing, type: :uuid)
      )
    end

    flush()

    # - fetch all possible combinations of product & entitlement
    now = Ecto.DateTime.utc()

    new_entitlements =
      from(
        e in "entitlements",
        join: p in "products",
        on: p.entitlement_id == e.id,
        select: %{
          organization_id: e.organization_id,
          entitlement_id: e.id,
          entitlement_identifier: e.external_identifier,
          product_id: p.id,
          product_identifier: p.external_identifier,
          platform: p.platform,
          name: e.name
        }
      )
      |> Connect.Repo.all()
      |> Enum.map(fn r ->
        %{
          type: "subscription",
          organization_id: r.organization_id,
          name: r.name,
          platform: r.platform,
          entitlement_identifier: r.entitlement_identifier,
          product_identifier: r.product_identifier,
          inserted_at: now,
          updated_at: now
        }
      end)

    # - populate new entitlements table
    Connect.Repo.insert_all("new_entitlements", new_entitlements, on_conflict: :raise)

    # - update transactions tables with new entitlement_id
    from(
      t in "transactions",
      join: p in "products",
      on: p.id == t.product_id,
      join: e in "entitlements",
      on: e.id == t.entitlement_id,
      join: ne in "new_entitlements",
      on:
        ne.entitlement_identifier == e.external_identifier and
          ne.product_identifier == p.external_identifier,
      update: [set: [new_entitlement_id: ne.id]]
    )
    |> Connect.Repo.update_all([])

    # - rename old entitlements table
    rename(table(:entitlements), to: table(:old_entitlements))

    # - rename new entitlements table
    rename(table(:new_entitlements), to: table(:entitlements))

    # - remove product_id column from transactions
    alter table(:transactions) do
      remove(:product_id)
      remove(:entitlement_id)
    end

    rename(table(:transactions), :new_entitlement_id, to: :entitlement_id)

    alter table(:transactions) do
      modify(
        :entitlement_id,
        references(:entitlements, on_delete: :nothing, type: :uuid),
        null: false
      )
    end

    alter table(:entitlements) do
      modify(
        :organization_id,
        references(:organizations, on_delete: :nothing, type: :uuid),
        null: false
      )
    end
  end

  def down do
    raise Ecto.MigrationError, "Can't reverse this one"
  end
end
