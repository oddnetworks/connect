defmodule Connect.Repo.Migrations.AddPasswordResetTokenAndSentAtToAuthAccounts do
  use Ecto.Migration

  def change do
    alter table(:auth_accounts) do
      add(:password_reset_token, :string)
      add(:reset_token_sent_at, :utc_datetime)
    end

    create(unique_index(:auth_accounts, [:password_reset_token]))
  end
end
