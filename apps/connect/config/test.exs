# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :connect, env: :test

# Configure your database
config :connect, Connect.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox,
  url: System.get_env("DATABASE_URL") || "postgres://postgres:postgres@postgres/odd_connect_test"

config :logger,
  backends: [:console],
  utc_log: true,
  log_level: :warn

config :logger, :console,
  format: "[$level] $message\n",
  level: :warn

## Comeonin
config :comeonin, :bcrypt_log_rounds, 4
