# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :connect, env: :prod

config :connect, Connect.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: "${DATABASE_URL}",
  pool_size: "${POOL_SIZE}",
  ssl: true,
  loggers: [Ecto.LogEntry, {Timber.Integrations.EctoLogger, :log, []}]

# Do not print debug messages in production
config :logger, level: :info
