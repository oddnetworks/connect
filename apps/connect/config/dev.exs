# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :connect, env: :dev

# Configure your database
config :connect, Connect.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("DATABASE_URL") || "postgres://postgres:postgres@postgres/odd_connect_dev",
  pool_size: 10,
  loggers: [Ecto.LogEntry, {Timber.Integrations.EctoLogger, :log, []}]
