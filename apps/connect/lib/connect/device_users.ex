defmodule Connect.DeviceUsers do
  @moduledoc """
  Responsible for DeviceUser related logic within Connect.
  """

  alias Connect.{DeviceUser, Entitlement, Repo, Transaction, VerifiedConnection}
  import Ecto.Query, only: [from: 2]

  @doc """

  """
  def find(organization_id, email) do
    from(
      d in DeviceUser,
      distinct: d.id,
      left_join: t in Transaction,
      on: t.device_user_id == d.id,
      left_join: e in Entitlement,
      on: t.entitlement_id == e.id,
      left_join: v in VerifiedConnection,
      on: v.device_user_id == d.id,
      where:
        ((e.organization_id == ^organization_id and not is_nil(e.id) and is_nil(t.deleted_at)) or
           (v.organization_id == ^organization_id and not is_nil(v.id))) and d.email == ^email
    )
    |> Repo.one()
  end

  def list(organization_id) do
    from(
      d in DeviceUser,
      distinct: d.id,
      left_join: t in Transaction,
      on: t.device_user_id == d.id,
      left_join: e in Entitlement,
      on: t.entitlement_id == e.id,
      left_join: v in VerifiedConnection,
      on: v.device_user_id == d.id,
      where:
        (e.organization_id == ^organization_id and not is_nil(e.id) and is_nil(t.deleted_at)) or
          (v.organization_id == ^organization_id and not is_nil(v.id))
    )
    |> Repo.all()
  end
end
