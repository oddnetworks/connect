defmodule Connect.Entitlements do
  @moduledoc """
  Responsible for Entitlement related logic within Connect.
  """

  alias Connect.{DeviceUser, Entitlement, Repo, Transaction}
  import Ecto.Query, only: [from: 2]

  defdelegate valid_entitlement_platforms(), to: Connect.Platform

  defdelegate valid_types(), to: Connect.Entitlement

  defdelegate build_changeset(struct, params \\ %{}), to: Connect.Entitlement, as: :changeset

  @doc """
  Lists valid entitlements scoped to the given DeviceUser
  within an Organization
  """
  @spec list_for_device_user(String.t(), String.t()) :: list(Entitlement.t())
  def list_for_device_user(device_user_email, organization_id) do
    query =
      from(
        e in Entitlement,
        distinct: e.id,
        left_join: t in Transaction,
        on: t.entitlement_id == e.id,
        left_join: d in DeviceUser,
        on: d.id == t.device_user_id,
        where:
          d.email == ^device_user_email and e.organization_id == ^organization_id and
            is_nil(t.deleted_at) and is_nil(t.invalidated_at),
        order_by: [desc: t.inserted_at],
        select: %{
          id: field(e, :id),
          type: field(e, :type),
          name: field(e, :name),
          description: field(e, :description),
          entitlement_identifier: field(e, :entitlement_identifier),
          product_identifier: field(e, :product_identifier),
          transaction_identifier: field(t, :external_identifier),
          receipt: field(t, :receipt),
          platform: field(e, :platform),
          assigned_at: field(t, :inserted_at)
        }
      )

    query
    |> Repo.all()
  end

  @doc """
  Lists all Entitlements scoped to an Organization
  """
  @spec list(String.t()) :: list(Entitlement.t())
  def list(organization_id) do
    query = from(e in Entitlement, where: e.organization_id == ^organization_id)

    query
    |> Repo.all()
  end

  @doc """
  Lists all Entitlements scoped to Organization
  """
  def list_paged(organization_id, params) do
    query = from(e in Entitlement, where: e.organization_id == ^organization_id)

    query
    |> Repo.paginate(params)
  end

  @doc """
  Creates an Entitlement from the given params
  """
  def create(params) do
    %Entitlement{}
    |> Entitlement.changeset(params)
    |> Repo.insert()
  end

  @doc """
  Finds an Entitlement with the given clauses.
  Preloads specified data.
  """
  @spec find(clauses :: Keyword.t(), preloads :: list(atom())) ::
          Ecto.Schema.t() | nil | no_return
  def find(clauses, preloads \\ []) do
    Entitlement
    |> Repo.get_by(clauses)
    |> Repo.preload(preloads)
  end

  @doc """
  Updates an Entitlement
  """
  @spec update(String.t(), map()) :: {:ok, Entitlement.t()} | {:error, Ecto.Changeset.t()}
  def update(id, entitlement_params) do
    org = Repo.get!(Entitlement, id)

    Entitlement.changeset(org, entitlement_params)
    |> Repo.update()
  end

  @doc """
  Deletes an Entitlement
  """
  @spec delete(String.t()) :: {:ok, Entitlement.t()} | {:error, Ecto.Changeset.t()}
  def delete(id) do
    entitlement = Repo.get!(Entitlement, id)

    entitlement
    |> Ecto.Changeset.change()
    |> Repo.delete()
  end
end
