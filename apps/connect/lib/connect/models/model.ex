defmodule Connect.Model do
  @moduledoc false

  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset
      import Ecto.Query, only: [from: 1, from: 2, where: 2]

      alias Connect.{
        AuthAccount,
        Customer,
        Device,
        DeviceUser,
        Entitlement,
        Platform,
        Product,
        Transaction,
        Organization,
        Repo,
        VerifiedConnection,
        Webhook
      }
    end
  end
end
