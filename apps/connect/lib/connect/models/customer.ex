defmodule Connect.Customer do
  use Connect.Model

  alias Connect.AuthAccount

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "customers" do
    field(:role, :string)

    belongs_to(:auth_account, AuthAccount)
    belongs_to(:organization, Organization)

    timestamps()
  end

  @valid_roles ~w(admin owner customer)

  @doc """
  Builds a changeset based on the `struct` and `params`
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(role)a)
    |> validate_inclusion(:role, @valid_roles)
    |> unique_constraint(:auth_account_id, name: :customers_auth_account_organization_index)
  end

  def build(%{organization: organization, auth_account: auth_account} = params) do
    changeset(%Connect.Customer{}, params)
    |> put_assoc(:organization, organization)
    |> put_assoc(:auth_account, auth_account)
  end
end
