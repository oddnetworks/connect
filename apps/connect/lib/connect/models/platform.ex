defmodule Connect.Platform do
  @doc """
  Returns the valid Device `platform` values

  ## Examples

      iex> Connect.Platform.valid_device_platforms()
      ["AMAZON_MOBILE", "AMAZON_TV", "APPLE_MOBILE", "APPLE_TV", "GOOGLE_MOBILE", "GOOGLE_TV", "ROKU", "WEB"]

  """
  def valid_device_platforms do
    ~w(AMAZON_MOBILE AMAZON_TV APPLE_MOBILE APPLE_TV GOOGLE_MOBILE GOOGLE_TV ROKU WEB)
  end

  @doc """
  Returns the valid Product `platform` values

  ## Examples

      iex> Connect.Platform.valid_entitlement_platforms()
      ["AMAZON", "APPLE", "GOOGLE", "ROKU", "WEB"]

  """
  def valid_entitlement_platforms do
    ~w(AMAZON APPLE GOOGLE ROKU WEB)
  end

  def normalize_entitlement_platform(platform) do
    platform =
      platform
      |> String.upcase()

    case platform do
      "AMAZON" <> _ -> "AMAZON"
      "APPLE" <> _ -> "APPLE"
      "GOOGLE" <> _ -> "GOOGLE"
      _ -> platform
    end
  end
end
