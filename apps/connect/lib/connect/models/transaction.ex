defmodule Connect.Transaction do
  use Connect.Model

  import Connect.Platform, only: [valid_entitlement_platforms: 0]

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "transactions" do
    field(:external_identifier, :string)
    field(:platform, :string)
    field(:last_validated_at, :naive_datetime)
    field(:invalidated_at, :naive_datetime)
    field(:invalidation_reason, :string)
    field(:receipt, :map)
    field(:deleted_at, :naive_datetime)

    belongs_to(:device_user, DeviceUser, type: :binary_id)
    belongs_to(:entitlement, Entitlement, type: :binary_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(
      params,
      ~w(external_identifier platform receipt last_validated_at invalidated_at invalidation_reason inserted_at deleted_at)a
    )
    |> validate_required(~w(external_identifier platform receipt last_validated_at)a)
    |> validate_length(:external_identifier, min: 1, max: 255)
    |> validate_inclusion(:platform, valid_entitlement_platforms())
    |> unique_constraint(
      :external_identifier,
      name: :transactions_platform_external_identifier_index
    )
  end

  def build(%{device_user: device_user, entitlement: entitlement} = params) do
    params =
      params
      |> Map.update(:last_validated_at, DateTime.utc_now(), & &1)

    changeset(%Transaction{}, params)
    |> put_assoc(:device_user, device_user)
    |> put_assoc(:entitlement, entitlement)
  end

  def check_within_validation_threshold(transaction, current_datetime) do
    case validation_threshold(transaction) do
      {:ok, threshold_seconds} ->
        {:ok,
         Timex.before?(
           current_datetime,
           Timex.shift(transaction.last_validated_at, seconds: threshold_seconds)
         )}

      {:error, reason} ->
        {:ok, true, reason}
    end
  end

  defp validation_threshold(transaction) do
    transaction.platform
    |> fetch_validation_threshold()
    |> threshold_to_integer()
  end

  defp fetch_validation_threshold("APPLE") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_APPLE")
  end

  defp fetch_validation_threshold("GOOGLE") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_GOOGLE")
  end

  defp fetch_validation_threshold("ROKU") do
    System.get_env("TRANSACTION_VALIDATION_THRESHOLD_ROKU")
  end

  defp fetch_validation_threshold(_), do: {:error, "Platform Not Validated"}

  defp threshold_to_integer({:error, reason}), do: {:error, reason}

  defp threshold_to_integer(threshold) when is_binary(threshold) do
    {:ok, String.to_integer(threshold)}
  end

  @default_validation_threshold 900
  defp threshold_to_integer(threshold) when is_nil(threshold),
    do: {:ok, @default_validation_threshold}
end
