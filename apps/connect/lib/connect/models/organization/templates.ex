defmodule Connect.Organization.Templates do
  use Ecto.Schema
  import Ecto.Changeset

  alias Connect.Organization
  alias Connect.Organization.Email

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organization_templates" do
    field(:device_connection_email_subject)
    field(:device_connection_email_html)
    field(:device_connection_email_text)

    belongs_to(:organization, Organization)
  end

  @fields [
    :device_connection_email_subject,
    :device_connection_email_html,
    :device_connection_email_text
  ]

  def changeset(struct, data) do
    struct
    |> cast(data, @fields)
    |> ensure_defaults()
    |> validate_required(@fields)
    |> validate_contains("{{link}}", [
      :device_connection_email_html,
      :device_connection_email_text
    ])
  end

  defp ensure_defaults(changeset) do
    @fields
    |> Enum.reduce(changeset, fn field, ch ->
      value = get_field(ch, field)

      ch
      |> put_change(field, default_value(field, value))
    end)
  end

  defp default_value(field, value) when is_nil(value) or value == "" do
    case field do
      :device_connection_email_subject -> Email.device_connection_email_subject_default()
      :device_connection_email_html -> Email.device_connection_email_html_default()
      :device_connection_email_text -> Email.device_connection_email_text_default()
      _ -> nil
    end
  end

  defp default_value(_field, value), do: value

  defp validate_contains(changeset, text, fields) do
    fields
    |> Enum.reduce(changeset, fn field, ch ->
      value = get_field(ch, field)

      if value =~ text do
        ch
      else
        ch
        |> add_error(field, "must contain #{text}")
      end
    end)
  end
end
