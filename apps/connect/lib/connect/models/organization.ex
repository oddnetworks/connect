defmodule Connect.Organization do
  use Connect.Model

  alias Connect.Organization.{Settings, Templates}

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "organizations" do
    field(:name, :string)
    field(:urn, :string)

    has_one(:templates, Templates)
    has_one(:settings, Settings)
    has_many(:customers, Customer)
    has_many(:verified_connections, VerifiedConnection)
    has_many(:device_users, through: [:verified_connections, :device_user])
    has_many(:devices, through: [:verified_connections, :device])
    has_many(:webhooks, Webhook)
    has_many(:entitlements, Entitlement)

    timestamps()
  end

  @blacklisted_urns ~w(www oddconnect connect)

  @doc """
  Builds a changeset based on the `struct` and `params`
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:urn, :name])
    |> cast_assoc(:settings)
    |> cast_assoc(:templates)
    |> validate_required([:name])
    |> validate_length(:name, min: 1, max: 255)
    |> put_urn(struct)
    |> validate_required([:urn])
    |> validate_urn()
    |> validate_length(:urn, min: 3, max: 50)
    |> unique_constraint(:urn)
  end

  def build(%{name: _name, urn: _urn} = params) do
    default_settings =
      %Settings{}
      |> Map.from_struct()

    default_templates =
      %Templates{}
      |> Map.from_struct()

    params = Map.merge(%{settings: default_settings, templates: default_templates}, params)
    changeset(%Organization{}, params)
  end

  defp put_urn(changeset = %{changes: %{urn: urn}}, _existing) when is_binary(urn), do: changeset

  defp put_urn(changeset = %{changes: %{name: name}}, %{urn: urn})
       when is_binary(name) and is_nil(urn) do
    urn =
      name
      |> Slugger.slugify()
      |> String.downcase()

    put_change(changeset, :urn, urn)
  end

  defp put_urn(changeset, _existing), do: changeset

  defp validate_urn(changeset) do
    urn = get_field(changeset, :urn)

    cond do
      urn == nil ->
        changeset
        |> add_error(:urn, "can't be blank")

      urn in @blacklisted_urns ->
        changeset
        |> add_error(:urn, "not allowed")

      urn =~ ~r/^[\d-]+/ ->
        changeset
        |> add_error(:urn, "invalid format (can't start with hyphen or digit)")

      urn =~ ~r/[^a-z0-9|-]/ ->
        changeset
        |> add_error(:urn, "invalid format (can only contain a-z 0-9 and hyphen)")

      true ->
        changeset
    end
  end
end
