defmodule Connect.Device do
  use Connect.Model

  @type t :: %__MODULE__{}

  import Connect.Platform, only: [valid_device_platforms: 0]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "devices" do
    field(:external_identifier, :string)
    field(:platform, :string)

    has_many(:verified_connections, VerifiedConnection)
    has_many(:device_users, through: [:verified_connections, :device_user])
    has_many(:organizations, through: [:verified_connections, :organization])

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(external_identifier platform)a)
    |> validate_required(~w(external_identifier platform)a)
    |> validate_length(:external_identifier, min: 1, max: 255)
    |> validate_inclusion(:platform, valid_device_platforms())
    |> unique_constraint(:external_identifier)
  end

  def build(%{external_identifier: _external_identifier} = params) do
    changeset(%Device{}, params)
  end
end
