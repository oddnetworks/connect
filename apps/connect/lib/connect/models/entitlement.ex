defmodule Connect.Entitlement do
  use Connect.Model

  @type t :: %__MODULE__{}

  import Connect.Platform, only: [valid_entitlement_platforms: 0]

  @valid_types ~w(subscription asset)

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "entitlements" do
    field(:type, :string)
    field(:name, :string)
    field(:description, :string)
    field(:entitlement_identifier, :string)
    field(:product_identifier, :string)
    field(:platform, :string)

    belongs_to(:organization, Organization, type: :binary_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(type name description platform entitlement_identifier product_identifier organization_id)a)
    |> validate_required(~w(type name platform entitlement_identifier product_identifier organization_id)a)
    |> validate_inclusion(:type, @valid_types)
    |> validate_inclusion(:platform, valid_entitlement_platforms())
    |> validate_length(:name, min: 1, max: 255)
    |> validate_length(:entitlement_identifier, min: 1, max: 255)
    |> validate_length(:product_identifier, min: 1, max: 255)
    |> assoc_constraint(:organization)
    |> unique_constraint(
      :product_identifier,
      name: :entitlements_organization_identifiers_index,
      message: "already exists for this entitlement"
    )
    |> unique_constraint(
      :product_identifier,
      name: :entitlements_organization_platform_product_index,
      message: "already exists for this platform"
    )
  end

  def valid_types() do
    @valid_types
  end
end
