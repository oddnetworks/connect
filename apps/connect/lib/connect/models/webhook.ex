defmodule Connect.Webhook do
  use Connect.Model

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "webhooks" do
    field(:type, :string)
    field(:url, :string)
    field(:headers, :map)

    belongs_to(:organization, Organization, type: :binary_id)

    timestamps()
  end

  @valid_types ~w(verified_connection new_transaction invalidated_transaction deleted_transaction entitlement)

  def valid_types do
    @valid_types
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(url type)a)
    |> cast_headers(params)
    |> validate_required(~w(url type)a)
    |> validate_inclusion(:type, @valid_types)
  end

  def build(%{organization: organization} = params) do
    changeset(%Webhook{}, params)
    |> put_assoc(:organization, organization)
  end

  defp cast_headers(changeset, %{"headers" => headers_json}) when is_binary(headers_json) do
    changeset
    |> parse_headers_to_changeset(headers_json)
  end

  defp cast_headers(changeset, %{headers: headers_json}) when is_binary(headers_json) do
    changeset
    |> parse_headers_to_changeset(headers_json)
  end

  defp cast_headers(changeset, _params), do: changeset

  defp parse_headers_to_changeset(changeset, headers_json) do
    case Poison.decode(headers_json, keys: :atoms) do
      {:ok, %{} = headers} ->
        changeset
        |> cast(%{headers: headers}, [:headers])

      {:error, _} ->
        changeset
        |> add_error(:headers, "invalid JSON")

      _ ->
        changeset
        |> add_error(:headers, "must be a JSON object `{}`")
    end
  end
end
