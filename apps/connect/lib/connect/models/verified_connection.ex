defmodule Connect.VerifiedConnection do
  use Connect.Model

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "verified_connections" do
    field(:verified_at, :naive_datetime)
    field(:verified_during_review, :boolean, default: false)

    belongs_to(:device_user, DeviceUser, type: :binary_id)
    belongs_to(:organization, Organization, type: :binary_id)
    belongs_to(:device, Device, type: :binary_id)
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> validate_uuid(params)
    |> cast(params, ~w(verified_at verified_during_review)a)
    |> validate_required(~w(id verified_at)a)
  end

  defp validate_uuid(struct, params) do
    id =
      params
      |> Map.get(:id)

    case UUID.info(id) do
      {:error, _} ->
        change(struct)
        |> add_error(:id, "must be a valid uuid")

      {:ok, _} ->
        change(struct, id: id)
    end
  end

  def build(%{device_user: device_user, organization: organization, device: device} = params) do
    changeset(%VerifiedConnection{}, params)
    |> put_assoc(:device_user, device_user)
    |> put_assoc(:organization, organization)
    |> put_assoc(:device, device)
  end
end
