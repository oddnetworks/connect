defmodule Connect.DeviceUser do
  use Connect.Model

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "device_users" do
    field(:email, :string)

    has_many(:verified_connections, VerifiedConnection)
    has_many(:devices, through: [:verified_connections, :device])
    has_many(:organizations, through: [:verified_connections, :organization])
    has_many(:transactions, Transaction)
    has_many(:entitlements, through: [:transactions, :entitlement])

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(email)a)
    |> validate_required(~w(email)a)
    |> validate_length(:email, min: 1, max: 255)
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  def build(%{email: _email} = params) do
    changeset(%DeviceUser{}, params)
  end
end
