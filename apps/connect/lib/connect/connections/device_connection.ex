defmodule Connect.DeviceConnection do
  use Connect.Model
  alias Ecto.Multi

  @doc """
  Stores a VerifiedConnection, linking Organization, Customer, and Device

  ## Parameters

    - `id` -
    - `email` -
    - `organization_urn` -
    - `device_identifier` -
    - `platform` -
    - `verified_at` - a DateTime

  """
  @spec create(Map.t()) ::
          {:ok,
           %{
             organization: Organization.t(),
             device: Device.t(),
             device_user: DeviceUser.t(),
             verified_connection: VerifiedConnection.t()
           }}
          | {:error, atom, String.t(), map}
          | {:error, atom, Ecto.Changeset.t(), map}
  def create(
        params = %{
          id: id,
          email: email,
          organization_urn: organization_urn,
          device_identifier: device_identifier,
          platform: platform,
          verified_at: verified_at
        }
      ) do
    verified_during_review = Map.get(params, :verified_during_review, false)

    Multi.new()
    |> Multi.run(:organization, Connect.DeviceConnection, :find_organization, [organization_urn])
    |> Multi.run(:device, Connect.DeviceConnection, :insert_or_find_device, [
      device_identifier,
      platform
    ])
    |> Multi.run(:device_user, Connect.DeviceConnection, :insert_or_find_device_user, [email])
    |> Multi.run(
      :verified_connection,
      Connect.DeviceConnection,
      :insert_or_find_verified_connection,
      [id, verified_at, verified_during_review]
    )
    |> Repo.transaction()
  end

  def find_organization(_, organization_urn) do
    case Connect.find_organization(urn: organization_urn) do
      nil -> {:error, "Organization not found"}
      organization -> {:ok, organization}
    end
  end

  def insert_or_find_device(_, device_identifier, platform) do
    case Repo.get_by(Device, external_identifier: device_identifier, platform: platform) do
      nil -> %Device{}
      device -> device
    end
    |> Device.changeset(%{external_identifier: device_identifier, platform: platform})
    |> Repo.insert_or_update()
  end

  def insert_or_find_device_user(_, email) do
    case Repo.get_by(DeviceUser, email: email) do
      nil -> %DeviceUser{}
      device_user -> device_user
    end
    |> DeviceUser.changeset(%{email: email})
    |> Repo.insert_or_update()
  end

  def insert_or_find_verified_connection(changes_so_far, id, verified_at, verified_during_review) do
    verified_connection =
      case UUID.info(id) do
        {:error, _} ->
          nil

        {:ok, _} ->
          Repo.get(VerifiedConnection, id)
      end

    case verified_connection do
      nil ->
        changes_so_far
        |> Map.merge(%{
          id: id,
          verified_at: verified_at,
          verified_during_review: verified_during_review
        })
        |> VerifiedConnection.build()
        |> Repo.insert()

      ^verified_connection ->
        verified_connection
        |> validate_unchanged_verified_connection(changes_so_far, verified_at)
    end
  end

  defp validate_unchanged_verified_connection(
         %{organization_id: existing},
         %{organization: %{id: new}},
         _verified_at
       )
       when existing != new,
       do: {:error, "Connection Exists"}

  defp validate_unchanged_verified_connection(
         %{device_user_id: existing},
         %{device_user: %{id: new}},
         _verified_at
       )
       when existing != new,
       do: {:error, "Connection Exists"}

  defp validate_unchanged_verified_connection(
         %{device_id: existing},
         %{device: %{id: new}},
         _verified_at
       )
       when existing != new,
       do: {:error, "Connection Exists"}

  defp validate_unchanged_verified_connection(
         %{verified_at: existing},
         _changes_so_far,
         verified_at
       )
       when existing != verified_at,
       do: {:error, "Connection Exists"}

  defp validate_unchanged_verified_connection(verified_connection, _changes_so_far, _verified_at),
    do: {:ok, verified_connection}
end
