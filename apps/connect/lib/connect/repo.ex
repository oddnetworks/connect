defmodule Connect.Repo do
  use Ecto.Repo, otp_app: :connect
  use Scrivener, page_size: 10

  def init(_type, config) do
    Application.get_env(:connect, :env)
    |> setup_cloak()

    {:ok, config}
  end

  defp setup_cloak(:prod) do
    # Cloak settings
    cdek = System.get_env("CONNECT_DATA_ENCRYPTION_KEY")

    if is_nil(cdek) do
      raise "Missing CONNECT_DATA_ENCRYPTION_KEY"
    end

    cloak_config = [
      tag: "AES",
      default: true,
      keys: [
        %{
          tag: <<1>>,
          key: :base64.decode(cdek),
          default: true
        }
      ]
    ]

    Application.put_env(:cloak, Cloak.AES.CTR, cloak_config)
  end

  defp setup_cloak(_) do
    cloak_config = [
      tag: "AES",
      default: true,
      keys: [
        %{
          tag: <<1>>,
          key:
            :base64.decode(
              System.get_env("CONNECT_DATA_ENCRYPTION_KEY") ||
                "ZuFjk73FTN2k2dxqx4Dq3OiN32vYleCpPFo7mSn10ks="
            ),
          default: true
        }
      ]
    ]

    Application.put_env(:cloak, Cloak.AES.CTR, cloak_config)
  end
end
