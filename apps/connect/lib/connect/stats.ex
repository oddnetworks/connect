defmodule Connect.Stats do
  @moduledoc """
  Responsible for reporting needs of Connect
  """

  alias Connect.{Device, Entitlement, Repo, Transaction, VerifiedConnection}
  import Ecto.Query, only: [from: 2]

  def transaction_stats(organization_id) do
    query =
      from(
        t in Transaction,
        join: e in Entitlement,
        on: e.id == t.entitlement_id,
        where: e.organization_id == ^organization_id and is_nil(t.deleted_at),
        group_by: [e.organization_id, t.platform],
        select: %{
          platform: field(t, :platform),
          organization_id: field(e, :organization_id),
          last_day:
            fragment(
              "coalesce(count(?) filter(where invalidated_at is null and ? between current_timestamp and current_timestamp - '24 hours'::interval), 0)",
              t.id,
              t.inserted_at
            ),
          total_valid:
            fragment("coalesce(count(?) filter(where invalidated_at is null), 0)", t.id),
          total_invalid:
            fragment("coalesce(count(?) filter(where invalidated_at is not null), 0)", t.id)
        }
      )

    query
    |> Repo.all()
  end

  def connection_stats(organization_id) do
    query =
      from(
        vc in VerifiedConnection,
        join: d in Device,
        on: vc.device_id == d.id,
        group_by: [vc.organization_id, d.platform],
        where: vc.organization_id == ^organization_id,
        select: %{
          platform: field(d, :platform),
          last_day:
            fragment(
              "coalesce(count(?) filter(where verified_at between current_timestamp and current_timestamp - '24 hours'::interval), 0)",
              vc.id
            ),
          total: fragment("count(?)", vc.id)
        }
      )

    query
    |> Repo.all()
  end
end
