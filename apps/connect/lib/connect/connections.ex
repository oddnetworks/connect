defmodule Connect.Connections do
  @moduledoc """
  Responsible for Connection related logic within Connect
  """

  alias Connect.{Device, DeviceUser, Repo, VerifiedConnection}
  import Ecto.Query, only: [from: 2]

  def list_paged(organization_id, params) do
    query =
      from(
        vc in VerifiedConnection,
        distinct: vc.id,
        left_join: du in DeviceUser,
        on: vc.device_user_id == du.id,
        left_join: d in Device,
        on: vc.device_id == d.id,
        where: vc.organization_id == ^organization_id,
        order_by: [desc: vc.verified_at],
        select: %{
          id: field(vc, :id),
          platform: field(d, :platform),
          device_user_id: field(vc, :device_user_id),
          email: field(du, :email),
          date: field(vc, :verified_at)
        }
      )

    query
    |> Repo.paginate(params)
  end

  def list_device_user_connections(organization_id, email) do
    query =
      from(
        vc in VerifiedConnection,
        distinct: vc.id,
        left_join: d in DeviceUser,
        on: vc.device_user_id == d.id,
        where: d.email == ^email and vc.organization_id == ^organization_id,
        preload: [:device]
      )

    query
    |> Repo.all()
  end
end
