defmodule Connect do
  @moduledoc """
  Contains main business logic of the project.
  """
  use Connect.Model

  def get_organization(organization_id) do
    Organization
    |> Repo.get(organization_id)
  end

  @doc """
  Returns the Organization described by the given clauses

  ## Parameters

    - `clauses` - Keyword list

  """
  @spec find_organization(clauses :: Keyword.t()) :: Ecto.Schema.t() | nil | no_return
  def find_organization(clauses) do
    Organization
    |> Repo.get_by(clauses)
    |> Repo.preload([:settings, :templates, :webhooks, :entitlements])
  end

  def edit_organization(%Organization{} = organization) do
    Organization
    |> Repo.get(organization.id)
    |> Repo.preload([:settings, :templates])
    |> Organization.changeset()
  end

  def build_organization(organization), do: Organization.build(organization)

  def update_organization(urn, organization_params) do
    org =
      Repo.get_by!(Organization, urn: urn)
      |> Repo.preload([:settings, :templates])

    Organization.changeset(org, organization_params)
    |> Repo.update()
  end

  def list_organizations() do
    Organization
    |> Repo.all()
  end

  def list_organizations_for_account(account_id) do
    query =
      from(
        o in Organization,
        join: c in Customer,
        on: c.organization_id == o.id,
        where: c.auth_account_id == ^account_id
      )

    query
    |> Repo.all()
  end

  def build_webhook(%Webhook{} = webhook) do
    webhook
    |> Map.from_struct()
    |> Webhook.build()
  end

  def build_webhook(params) do
    params
    |> Webhook.build()
  end

  def create_webhook(params) do
    Webhook.build(params)
    |> Repo.insert()
  end

  @spec find_webhook(clauses :: Keyword.t()) :: Ecto.Schema.t() | nil | no_return
  def find_webhook(clauses) do
    Webhook
    |> Repo.get_by(clauses)
    |> Repo.preload(:organization)
  end

  def update_webhook(id, webhook_params) do
    org = Repo.get!(Webhook, id)

    Webhook.changeset(org, webhook_params)
    |> Repo.update()
  end

  def delete_webhook(id) do
    webhook = Repo.get!(Webhook, id)

    webhook
    |> Repo.delete()
  end

  def valid_webhook_types() do
    Webhook.valid_types()
  end

  def list_webhooks(%{id: id}, params) do
    query = from(w in Webhook, where: w.organization_id == ^id)

    query
    |> Repo.paginate(params)
  end

  def purge_verified_connections_verified_during_review!(organization_id) do
    from(
      vc in VerifiedConnection,
      where: vc.organization_id == ^organization_id,
      where: vc.verified_during_review == true
    )
    |> Repo.delete_all()
  end
end
