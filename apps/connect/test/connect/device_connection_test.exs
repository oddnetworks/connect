defmodule Connect.DeviceConnectionTest do
  use Connect.Case
  doctest Connect.DeviceConnection

  describe "create/5" do
    setup _tags do
      org = insert(:organization)
      duser = insert(:device_user)
      dev = insert(:device)

      vc =
        insert(
          :verified_connection,
          id: UUID.uuid4(),
          organization: org,
          device_user: duser,
          device: dev
        )

      {:ok, org: org, duser: duser, dev: dev, vc: vc}
    end

    test "given existing organization, new device_identifier and new email - creates Device, DeviceUser, and VerifiedConnection",
         %{org: org} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      platform =
        Connect.Platform.valid_device_platforms()
        |> Enum.random()

      {:ok, %{verified_connection: verified_connection}} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: "foo@example.com",
          organization_urn: org.urn,
          device_identifier: "device:abc",
          platform: platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 2
      assert Repo.aggregate(Device, :count, :id) == 2
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2

      new_duser = last(DeviceUser)
      new_dev = last(Device)

      assert verified_connection.organization_id == org.id
      assert verified_connection.device_user_id == new_duser.id
      assert verified_connection.device_id == new_dev.id
    end

    test "given existing organization, existing device_identifier, and new email - creates DeviceUser and VerifiedConnection",
         %{org: org, dev: dev} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      {:ok, %{verified_connection: verified_connection}} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: "foo@example.com",
          organization_urn: org.urn,
          device_identifier: dev.external_identifier,
          platform: dev.platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 2
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2

      new_duser = last(DeviceUser)

      assert verified_connection.organization_id == org.id
      assert verified_connection.device_user_id == new_duser.id
      assert verified_connection.device_id == dev.id
    end

    test "given existing organization, existing device_identifier, and existing email - creates VerifiedConnection",
         %{org: org, dev: dev, duser: duser} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      {:ok, %{verified_connection: verified_connection}} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: duser.email,
          organization_urn: org.urn,
          device_identifier: dev.external_identifier,
          platform: dev.platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2

      assert verified_connection.organization_id == org.id
      assert verified_connection.device_user_id == duser.id
      assert verified_connection.device_id == dev.id
    end

    test "given verified_during_review, existing organization, existing device_identifier, and existing email - creates VerifiedConnection with verified_during_review",
         %{org: org, dev: dev, duser: duser} do
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1

      {:ok, %{verified_connection: verified_connection}} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: duser.email,
          organization_urn: org.urn,
          device_identifier: dev.external_identifier,
          platform: dev.platform,
          verified_at: DateTime.utc_now(),
          verified_during_review: true
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 2

      assert verified_connection.organization_id == org.id
      assert verified_connection.device_user_id == duser.id
      assert verified_connection.device_id == dev.id
      assert verified_connection.verified_during_review
    end

    test "given existing VerifiedConnection id - when attributes unchanged, returns existing VerifiedConnection",
         %{vc: vc} do
      {:ok,
       %{
         organization: %Organization{},
         device: %Device{},
         device_user: %DeviceUser{},
         verified_connection: %VerifiedConnection{}
       }} =
        Connect.DeviceConnection.create(%{
          id: vc.id,
          email: vc.device_user.email,
          organization_urn: vc.organization.urn,
          device_identifier: vc.device.external_identifier,
          platform: vc.device.platform,
          verified_at: vc.verified_at
        })

      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given existing VerifiedConnection id - when organization_urn changed, returns :error with reason",
         %{vc: vc} do
      new_org = insert(:organization)

      {:error, :verified_connection, "Connection Exists", _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: vc.id,
          email: vc.device_user.email,
          organization_urn: new_org.urn,
          device_identifier: vc.device.external_identifier,
          platform: vc.device.platform,
          verified_at: vc.verified_at
        })

      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given existing VerifiedConnection id - when email changed, returns :error with reason",
         %{vc: vc} do
      {:error, :verified_connection, "Connection Exists", _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: vc.id,
          email: "email@email.email",
          organization_urn: vc.organization.urn,
          device_identifier: vc.device.external_identifier,
          platform: vc.device.platform,
          verified_at: vc.verified_at
        })

      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
      assert Repo.aggregate(DeviceUser, :count, :id) == 1
    end

    test "given existing VerifiedConnection id - when device changed, returns :error with reason",
         %{vc: vc} do
      {:error, :verified_connection, "Connection Exists", _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: vc.id,
          email: vc.device_user.email,
          organization_urn: vc.organization.urn,
          device_identifier: "different-device-identifier",
          platform: vc.device.platform,
          verified_at: vc.verified_at
        })

      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
    end

    test "given existing VerifiedConnection id - when verified_at changed, returns :error with reason",
         %{vc: vc} do
      {:error, :verified_connection, "Connection Exists", _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: vc.id,
          email: vc.device_user.email,
          organization_urn: vc.organization.urn,
          device_identifier: vc.device.external_identifier,
          platform: vc.device.platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given invalid organization - returns :error with reason", %{dev: dev, duser: duser} do
      {:error, :organization, "Organization not found", _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: duser.email,
          organization_urn: "this is not real",
          device_identifier: dev.external_identifier,
          platform: dev.platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(Organization, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given invalid email - returns :error with reason", %{org: org, dev: dev} do
      {:error, :device_user, %Ecto.Changeset{}, _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: "not-an-email",
          organization_urn: org.urn,
          device_identifier: dev.external_identifier,
          platform: dev.platform,
          verified_at: ""
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(Organization, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given invalid device_identifier - returns :error with reason", %{org: org, duser: duser} do
      platform =
        Connect.Platform.valid_device_platforms()
        |> Enum.random()

      {:error, :device, %Ecto.Changeset{}, _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: duser.email,
          organization_urn: org.urn,
          device_identifier: "",
          platform: platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(Organization, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given invalid platform - returns :error with reason", %{org: org, duser: duser} do
      {:error, :device, %Ecto.Changeset{}, _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: duser.email,
          organization_urn: org.urn,
          device_identifier: UUID.uuid4(),
          platform: "DERP",
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(Organization, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given invalid verified_at - returns :error with reason", %{
      org: org,
      dev: dev,
      duser: duser
    } do
      platform =
        Connect.Platform.valid_device_platforms()
        |> Enum.random()

      {:error, :verified_connection, %Ecto.Changeset{}, _changes_so_far} =
        Connect.DeviceConnection.create(%{
          id: UUID.uuid4(),
          email: duser.email,
          organization_urn: org.urn,
          device_identifier: dev.external_identifier,
          platform: platform,
          verified_at: "not a datetime"
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(Organization, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end

    test "given invalid id - returns :error with reason", %{org: org, dev: dev, duser: duser} do
      {:error, :verified_connection, %Ecto.Changeset{}, _changese_so_far} =
        Connect.DeviceConnection.create(%{
          id: "invalid id",
          email: duser.email,
          organization_urn: org.urn,
          device_identifier: dev.external_identifier,
          platform: dev.platform,
          verified_at: DateTime.utc_now()
        })

      assert Repo.aggregate(DeviceUser, :count, :id) == 1
      assert Repo.aggregate(Device, :count, :id) == 1
      assert Repo.aggregate(Organization, :count, :id) == 1
      assert Repo.aggregate(VerifiedConnection, :count, :id) == 1
    end
  end
end
