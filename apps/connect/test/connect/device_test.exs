defmodule Connect.DeviceTest do
  use Connect.Case

  @valid_attrs %{
    external_identifier: "uuidokok",
    platform: Connect.Platform.valid_device_platforms() |> Enum.random()
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Device.changeset(%Device{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Device.changeset(%Device{}, @invalid_attrs)
    refute changeset.valid?
  end
end
