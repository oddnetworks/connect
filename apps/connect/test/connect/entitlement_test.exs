defmodule Connect.EntitlementTest do
  use Connect.Case
  doctest Connect.Entitlement

  @valid_attrs %{
    type: "subscription",
    name: "Platinum Subscription",
    entitlement_identifier: "platinum",
    product_identifier: "product-id",
    platform: Connect.Entitlements.valid_entitlement_platforms() |> Enum.random()
  }
  @invalid_attrs %{}

  describe "build/1" do
    setup _tags do
      organization = insert(:organization)

      {:ok, organization: organization}
    end

    test "with valid attributes returns valid changeset", %{organization: %{id: organization_id}} do
      changeset = Entitlement.changeset(%Entitlement{}, Map.put(@valid_attrs, :organization_id, organization_id))
      assert changeset.valid?
    end

    test "with invalid attributes returns invalid changeset", %{organization: %{id: organization_id}} do
      changeset = Entitlement.changeset(%Entitlement{}, Map.put(@invalid_attrs, :organization, organization_id))
      refute changeset.valid?
    end
  end
end
