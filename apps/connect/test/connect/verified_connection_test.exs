defmodule Connect.VerifiedConnectionTest do
  use Connect.Case

  @valid_attrs %{
    id: UUID.uuid4(),
    organization_id: UUID.uuid4(),
    device_user_id: UUID.uuid4(),
    device_id: UUID.uuid4(),
    verified_at: DateTime.utc_now()
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = VerifiedConnection.changeset(%VerifiedConnection{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = VerifiedConnection.changeset(%VerifiedConnection{}, @invalid_attrs)
    refute changeset.valid?
  end
end
