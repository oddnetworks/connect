defmodule Connect.Organization.EmailTest do
  use Connect.Case
  alias Connect.Organization.Email

  describe "device_connection_email_subject_default/0" do
    test "returns subject template" do
      assert Email.device_connection_email_subject_default() ==
               "{{organization_name}} - New Device Connection"
    end
  end

  describe "device_connection_email_text_default/0" do
    test "returns text template" do
      assert Email.device_connection_email_text_default() == """
             An attempt has been made to connect a new device to {{organization_name}} for the following account:

             {{email}}

             To verify this connection, please click the following link:

             {{link}}

             If you did not attempt to connect your device to {{organization_name}}, you may safely ignore this message.
             """
    end
  end

  describe "device_connection_email_html_default/0" do
    test "returns html template" do
      assert Email.device_connection_email_html_default() == """
             <h3>New Device Connection</h3>
             <p>An attempt has been made to connect a new device to <strong>{{organization_name}}</strong> for the following account:</p>
             <p>{{email}}</p>
             <p>To verify this connection, please click the following link:</p>
             <p><a href="{{link}}">{{link}}</a><br></p>
             <hr>
             <p>If you did not attempt to connect your device to {{organization_name}}, you may safely ignore this message.</p>
             """
    end
  end

  describe "device_connection_email_fields/0" do
    test "returns template fields" do
      assert Email.device_connection_email_fields() == [:email, :link, :organization_name]
    end
  end

  describe "prepare_device_connection_email/3" do
    test "returns struct with custom subject/html/text" do
      org = %{
        name: "Foo",
        urn: "foobar",
        templates: %{
          device_connection_email_subject: "{{organization_name}}{{email}}{{link}}",
          device_connection_email_html: "{{link}}{{email}}{{organization_name}}",
          device_connection_email_text: "{{email}}{{organization_name}}{{link}}"
        }
      }

      link = "http://example.com"
      email = "foo@example.com"

      assert %Email{
               to: email,
               from: "no-reply@#{org.urn}.oddconnect.com",
               subject: "#{org.name}#{email}#{link}",
               html: "#{link}#{email}#{org.name}",
               text: "#{email}#{org.name}#{link}"
             } == Email.prepare_device_connection_email(org, link, email)
    end

    test "returns struct with default subject/html/text" do
      email = "foo@example.com"
      link = "http://example.com"

      org = %{
        name: "Foo",
        urn: "foobar",
        templates: %{}
      }

      from = "no-reply@#{org.urn}.oddconnect.com"

      assert %Email{
               to: ^email,
               from: ^from,
               subject: subject,
               html: html,
               text: text
             } = Email.prepare_device_connection_email(org, link, email)

      params = %{
        email: email,
        organization_name: org.name,
        link: link
      }

      expected_html =
        Email.device_connection_email_fields()
        |> Enum.reduce(Email.device_connection_email_html_default(), fn field, tpl ->
          placeholder = "{{#{field}}}"
          value = Map.get(params, field)

          tpl
          |> String.replace(placeholder, value)
        end)

      expected_text =
        Email.device_connection_email_fields()
        |> Enum.reduce(Email.device_connection_email_text_default(), fn field, tpl ->
          placeholder = "{{#{field}}}"
          value = Map.get(params, field)

          tpl
          |> String.replace(placeholder, value)
        end)

      assert subject == "#{org.name} - New Device Connection"
      assert html == expected_html
      assert text == expected_text
    end
  end
end
