defmodule Connect.TransactionTest do
  use Connect.Case
  doctest Connect.Transaction

  @valid_attrs %{
    platform: "ROKU",
    external_identifier: "1092u34j12ij3o1",
    last_validated_at: DateTime.utc_now(),
    receipt: %{
      code: "123456000000",
      cost: "$3.99",
      expirationDate: "2017-07-29T18:31:01Z",
      freeTrialQuantity: 0,
      freeTrialType: "Months",
      name: "Thing 1",
      productType: "MonthlySub",
      purchaseDate: "2017-06-29T18:31:01Z",
      purchaseId: "1092u34j12ij3o1",
      qty: 1,
      renewalDate: "2017-07-29T18:31:01Z"
    }
  }
  @invalid_attrs %{}

  describe "build/1" do
    setup _tags do
      entitlement = insert(:entitlement)
      device_user = insert(:device_user)

      {:ok, entitlement: entitlement, device_user: device_user}
    end

    test "with valid attributes returns valid changeset", %{
      entitlement: entitlement,
      device_user: device_user
    } do
      attrs = Map.put(@valid_attrs, :entitlement, entitlement)
      attrs = Map.put(attrs, :device_user, device_user)
      attrs = Map.put(attrs, :platform, entitlement.platform)
      changeset = Transaction.build(attrs)
      assert changeset.valid?
    end

    test "with invalid attributes returns invalid changeset", %{
      entitlement: entitlement,
      device_user: device_user
    } do
      attrs = Map.put(@invalid_attrs, :entitlement, entitlement)
      attrs = Map.put(attrs, :device_user, device_user)
      attrs = Map.put(attrs, :platform, entitlement.platform)
      changeset = Transaction.build(attrs)
      refute changeset.valid?
    end
  end

  describe "check_within_validation_threshold/2" do
    setup do
      transaction = insert(:transaction, platform: "ROKU")

      {:ok, transaction: transaction}
    end

    test "given current_datetime after Transaction.last_validated_at + validation threshold", %{
      transaction: t
    } do
      future = Timex.add(Timex.now(), Timex.Duration.from_hours(24))

      {:ok, result} = Transaction.check_within_validation_threshold(t, future)
      refute result
    end

    test "given current_datetime before Transaction.last_validated_at + validation threshold", %{
      transaction: t
    } do
      past = Timex.subtract(Timex.now(), Timex.Duration.from_hours(24))

      {:ok, result} = Transaction.check_within_validation_threshold(t, past)
      assert result
    end

    test "given transaction with unknown platfom, returns {:ok, true, reason}" do
      t = build(:transaction, platform: "WEB")

      {:ok, true, "Platform Not Validated"} =
        t
        |> Transaction.check_within_validation_threshold(Timex.now())
    end
  end
end
