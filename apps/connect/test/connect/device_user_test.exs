defmodule Connect.DeviceUserTest do
  use Connect.Case

  @valid_attrs %{email: "foo@example.com"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DeviceUser.changeset(%DeviceUser{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DeviceUser.changeset(%DeviceUser{}, @invalid_attrs)
    refute changeset.valid?
  end
end
