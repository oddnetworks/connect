defmodule Connect.CustomerTest do
  use Connect.Case

  @valid_attrs %{role: "customer"}
  @invalid_attrs %{role: "herpderp"}

  test "changeset with valid attributes" do
    changeset = Customer.changeset(%Customer{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Customer.changeset(%Customer{}, @invalid_attrs)
    refute changeset.valid?
  end
end
