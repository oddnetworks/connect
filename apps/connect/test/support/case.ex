defmodule Connect.Case do
  use ExUnit.CaseTemplate

  using do
    quote do
      import Ecto
      import Ecto.Changeset
      import Ecto.Query

      import Connect.Factory

      use Connect.Model

      alias Connect.Auth

      def last(model) when is_atom(model) do
        Connect.Repo.one(from(x in model, order_by: [desc: x.inserted_at], limit: 1))
      end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Connect.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Connect.Repo, {:shared, self()})
    end

    :ok
  end
end
