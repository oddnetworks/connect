defmodule Auth do
  @moduledoc ~S"""
  Authentication system for the platform

  See `register/1` for creating an account and `sign_in/2` for signing in.
  """

  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]
  import Ecto.Query, only: [from: 2]

  alias Auth.{
    PasswordReset,
    PasswordResetEmail,
    Account,
    Customer,
    Organization,
    OrganizationAccount
  }

  alias Connect.Repo

  @doc """
  Authenticates an Auth.Account using the given `email`/`password`

  Upon success, returns `{:ok, %Auth.Account{}}
  """
  @spec authenticate_account(
          email :: String.t(),
          password :: String.t()
        ) :: {:ok, Account.t()} | {:error, atom}
  def authenticate_account(email, password) do
    sign_in(email, password)
  end

  @spec authenticate_organization_account(
          organization :: Organization.t(),
          account :: Account.t()
        ) :: {:ok, OrganizationAccount.t()} | {:error, atom}
  def authenticate_organization_account(organization = %Organization{}, account) do
    account
    |> assemble_organization_account(organization)
  end

  def authenticate_organization_account(organization_urn, account) do
    account
    |> assemble_organization_account(organization_urn)
  end

  def find_account(clauses) do
    Repo.get_by(Account, clauses)
  end

  def find_organization_account(account_id, organization_id, customer_id \\ nil) do
    account = Repo.get_by(Account, id: account_id)

    organization =
      from(
        o in Organization,
        preload: [:webhooks, :settings],
        where: o.id == ^organization_id
      )
      |> Repo.one()

    customer =
      if String.length(to_string(customer_id)) > 0 do
        Repo.get_by(Customer, id: customer_id)
      else
        nil
      end

    make_organization_account(account, organization, customer)
  end

  def initiate_password_reset(params) do
    case find_account(email: params["email"]) do
      nil ->
        # do nothing
        {:error, :not_found}

      account ->
        case generate_password_reset(account) do
          {:ok, account} ->
            account
            |> PasswordResetEmail.prepare()
            |> Messenger.deliver_email()

            {:ok, :auth}

          _error ->
            {:error, :failed}
        end
    end
  end

  def edit_password(token) do
    case find_account(password_reset_token: token) do
      nil ->
        {:error, :invalid}

      account ->
        with false <- PasswordReset.expired?(account) do
          changeset =
            account
            |> PasswordReset.password_changeset()

          {:ok, changeset}
        else
          _error ->
            account
            |> expire_password_reset()

            {:error, :expired}
        end
    end
  end

  def reset_password(token, params) do
    case find_account(password_reset_token: token) do
      nil ->
        {:error, :invalid}

      account ->
        with false <- PasswordReset.expired?(account) do
          account
          |> PasswordReset.password_changeset(params)
          |> Repo.update()
        else
          _error ->
            account
            |> expire_password_reset()

            {:error, :expired}
        end
    end
  end

  defp expire_password_reset(account) do
    account
    |> PasswordReset.expire_changeset()
    |> Repo.update!()
  end

  defp generate_password_reset(account) do
    account
    |> PasswordReset.reset_changeset()
    |> Repo.update()
  end

  defp sign_in(email, password) do
    account = Repo.get_by(Account, email: email)
    do_sign_in(account, password)
  end

  defp do_sign_in(%Account{password_hash: password_hash} = account, password) do
    if checkpw(password, password_hash) do
      {:ok, account}
    else
      {:error, :unauthorized}
    end
  end

  defp do_sign_in(nil, _) do
    dummy_checkpw()
    {:error, :not_found}
  end

  defp assemble_organization_account(account = %{is_admin: true}, organization = %Organization{}),
    do: make_organization_account(account, organization, nil)

  defp assemble_organization_account(account = %{is_admin: true}, organization_urn) do
    with {:ok, organization} <- fetch_organization(organization_urn) do
      make_organization_account(account, organization, nil)
    else
      res -> res
    end
  end

  defp assemble_organization_account(
         account = %Account{id: auth_account_id, is_admin: false},
         organization = %Organization{urn: organization_urn}
       ) do
    with {:ok, customer} <- fetch_customer(auth_account_id, organization_urn) do
      make_organization_account(account, organization, customer)
    else
      res -> res
    end
  end

  defp assemble_organization_account(
         account = %Account{id: auth_account_id, is_admin: false},
         organization_urn
       ) do
    with {:ok, customer} <- fetch_customer(auth_account_id, organization_urn),
         {:ok, organization} <- fetch_organization(organization_urn) do
      make_organization_account(account, organization, customer)
    else
      res -> res
    end
  end

  defp assemble_organization_account({:error, reason}, _organization_urn), do: {:error, reason}

  defp fetch_customer(auth_account_id, organization_urn) do
    query =
      from(
        c in Customer,
        join: o in Organization,
        on: o.id == c.organization_id,
        where: o.urn == ^organization_urn and c.auth_account_id == ^auth_account_id
      )

    case Repo.one(query) do
      nil ->
        {:error, :not_found}

      c ->
        {:ok, c}
    end
  end

  defp fetch_organization(urn) do
    case Repo.get_by(Organization, urn: urn) do
      nil ->
        {:error, :not_found}

      o ->
        {:ok, o}
    end

    # |> Repo.preload([:webhooks, :entitlements])
  end

  defp make_organization_account(account, organization, customer) do
    OrganizationAccount.new(account, organization, customer)
  end
end
