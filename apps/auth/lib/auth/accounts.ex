defmodule Auth.Accounts do
  import Ecto.Changeset
  import Comeonin.Bcrypt, only: [checkpw: 2]
  alias Auth.Account
  alias Connect.Repo

  def new_changeset(params \\ %{}) do
    %Account{}
    |> cast(params, ~w(email name password timezone is_admin)a)
    |> validate_required(~w(email name password)a)
    |> validate_format(:email, ~r/.*@.*/)
    |> validate_length(:password, min: 8)
    |> unique_constraint(:email)
    |> put_password_hash()
  end

  def edit_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(name timezone)a)
    |> validate_required(~w(name)a)
    |> validate_inclusion(:timezone, Timex.timezones())
  end

  def password_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(current_password password)a)
    |> validate_required(~w(current_password password)a)
    |> validate_current_password()
    |> validate_length(:password, min: 8, max: 32)
    |> validate_confirmation(:password, required: true, message: "does not match password")
    |> put_password_hash()
  end

  def update(struct, params) do
    struct
    |> edit_changeset(params)
    |> Repo.update()
  end

  def update_password(struct, params) do
    struct
    |> password_changeset(params)
    |> Repo.update()
  end

  ##############################################################################

  defp put_password_hash(%{changes: %{password: password}} = changeset) do
    put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))
  end

  defp put_password_hash(%{changes: %{}} = changeset), do: changeset

  defp validate_current_password(
         changeset = %{
           data: %{password_hash: password_hash},
           changes: %{current_password: current_password}
         }
       ) do
    if checkpw(current_password, password_hash) do
      changeset
    else
      changeset
      |> add_error(:current_password, "invalid")
    end
  end

  defp validate_current_password(changeset), do: changeset
end
