defmodule Auth.ApiToken do
  alias Connect.Repo

  def generate(organization_id) do
    organization =
      Auth.Organization
      |> Repo.get(organization_id)

    claims =
      Guardian.Claims.app_claims(%{"org" => organization.urn})
      |> Guardian.Claims.ttl({ttl_years(), :years})

    {:ok, jwt, _claims} = Guardian.encode_and_sign(organization, :access, claims)
    jwt
  end

  @default_ttl_years 3

  @doc """
  Returns the public key needed to verify the VerifiedConnection's JWT
  """
  @spec ttl_years() :: integer | nil
  def ttl_years do
    case Application.get_env(:front_end, FrontEnd.ApiToken)[:ttl_years] do
      nil ->
        @default_ttl_years

      ttl when is_integer(ttl) ->
        ttl

      ttl ->
        case Integer.parse(ttl, 10) do
          {ttl, _} -> ttl
          _ -> @default_ttl_years
        end
    end
  end
end
