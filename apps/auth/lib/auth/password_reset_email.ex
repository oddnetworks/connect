defmodule Auth.PasswordResetEmail do
  defstruct from: nil, html: nil, subject: nil, text: nil, to: nil

  def prepare(%{password_reset_token: password_reset_token, email: email}) do
    link = "https://#{host()}/passwords/#{password_reset_token}/edit"

    %Auth.PasswordResetEmail{
      to: email,
      from: "no-reply@oddconnect.com",
      subject: "Odd Connect - Reset Your Password",
      html: password_reset_html(link),
      text: password_reset_text(link)
    }
  end

  defp password_reset_html(link) do
    """
    <h3>Odd Networks - Reset Your Password</h3>
    <p>An attempt has been made to reset your password on Odd Connect.</p>
    <p>If you did not attempt to reset your password, you may safely ignore this message.<p>
    <p>To proceed with resetting your password, please visit the following link:</p>
    <p><a href="#{link}">#{link}</a></p>
    """
  end

  defp password_reset_text(link) do
    """
    An attempt has been made to reset your password on Odd Connect.

    If you did not attempt to reset your password, you may safely ignore this message.

    To proceed with resetting your password, please visit the following link:

    #{link}
    """
  end

  defp host do
    System.get_env("HOST")
  end
end
