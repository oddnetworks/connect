defmodule Auth.ConnectionToken do
  import Joken

  @doc """
  """
  def generate_jwt(%{
        organization_urn: organization_urn,
        platform: platform,
        device_identifier: device_identifier,
        email: email,
        id: id
      }) do
    claims = %{
      iss: issuer(),
      aud: [
        organization_urn,
        platform,
        device_identifier
      ],
      sub: email,
      jti: id,
      iat: current_time()
    }

    jwt =
      claims
      |> token()
      |> sign(rs256(key_to_jwk(private_key())))
      |> get_compact()

    {:ok, jwt}
  end

  @doc """
  """
  @spec public_key() :: binary | nil
  def public_key do
    Application.get_env(:auth, Auth.ConnectionToken)[:public_key]
  end

  defp key_to_jwk(key) do
    JOSE.JWK.from_pem(key)
  end

  defp issuer do
    Application.get_env(:auth, Auth.ConnectionToken)[:issuer]
  end

  defp private_key do
    Application.get_env(:auth, Auth.ConnectionToken)[:private_key]
  end
end
