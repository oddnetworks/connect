defmodule Auth.Permissions do
  @moduledoc """
  Responsible for calculating the permissions for a given combination of
  Auth.Account, Auth.Organization, and Auth.Customer
  """

  alias Auth.{Account, Customer, Organization}

  @admin :admin
  @organization_customer :organization_customer
  @organization_admin :organization_admin
  @organization_owner :organization_owner

  @doc """
  Returns a hash of permissions available to the given
  Auth.OrganizationAccount

  ## Examples

  Internal Admin

      iex> Auth.Permissions.permissions_for(%Auth.OrganizationAccount{account: %{id: "acct-1", is_admin: true}, organization: %{id: "org-1"}})
      {:ok, :admin}

  Organization Owner

      iex> Auth.Permissions.permissions_for(%Auth.OrganizationAccount{account: %{id: "acct-1", is_admin: false}, customer: %{auth_account_id: "acct-1", organization_id: "org-1", role: "owner"}, organization: %{id: "org-1"}})
      {:ok, :organization_owner}

  Organization Admin

      iex> Auth.Permissions.permissions_for(%Auth.OrganizationAccount{account: %{id: "acct-1", is_admin: false}, customer: %{auth_account_id: "acct-1", organization_id: "org-1", role: "admin"}, organization: %{id: "org-1"}})
      {:ok, :organization_admin}

  Organization Customer

      iex> Auth.Permissions.permissions_for(%Auth.OrganizationAccount{account: %{id: "acct-1", is_admin: false}, customer: %{auth_account_id: "acct-1", organization_id: "org-1", role: "customer"}, organization: %{id: "org-1"}})
      {:ok, :organization_customer}

  Unprivileged Account

      iex> Auth.Permissions.permissions_for(%Auth.OrganizationAccount{account: %{id: "acct-1", is_admin: false}, customer: %{auth_account_id: "acct-1", organization_id: "org-1"}, organization: %{id: "org-2"}})
      {:error, :denied}

  """
  @spec permissions_for(Account.t(), Organization.t(), Customer.t() | nil) ::
          {:ok, Hash.t()} | {:error, :denied}
  def permissions_for(%{id: aid, is_admin: true}, %{id: oid}, _)
      when not is_nil(aid) and not is_nil(oid),
      do: {:ok, @admin}

  def permissions_for(%{id: aid}, %{id: oid}, %{
        auth_account_id: aid,
        organization_id: oid,
        role: "owner"
      })
      when not is_nil(aid) and not is_nil(oid),
      do: {:ok, @organization_owner}

  def permissions_for(%{id: aid}, %{id: oid}, %{
        auth_account_id: aid,
        organization_id: oid,
        role: "admin"
      })
      when not is_nil(aid) and not is_nil(oid),
      do: {:ok, @organization_admin}

  def permissions_for(%{id: aid}, %{id: oid}, %{
        auth_account_id: aid,
        organization_id: oid,
        role: "customer"
      })
      when not is_nil(aid) and not is_nil(oid),
      do: {:ok, @organization_customer}

  def permissions_for(_, _, _), do: {:error, :denied}
end
