defmodule Auth.PasswordReset do
  import Ecto.Changeset

  alias Auth.Account

  def expire_changeset(struct = %Account{}) do
    struct
    |> change()
    |> put_change(:password_reset_token, nil)
    |> put_change(:reset_token_sent_at, nil)
    |> unique_constraint(:password_reset_token)
  end

  def reset_changeset(struct = %Account{} \\ %Account{}) do
    struct
    |> change()
    |> put_change(:password_reset_token, generate_reset_token())
    |> put_change(:reset_token_sent_at, NaiveDateTime.utc_now())
    |> unique_constraint(:password_reset_token)
  end

  def password_changeset(struct = %Account{}, params \\ %{}) do
    struct
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_length(:password, min: 8, max: 32)
    |> validate_confirmation(:password, required: true)
    |> put_password_hash()
    |> put_change(:reset_token_sent_at, nil)
    |> put_change(:password_reset_token, nil)
  end

  def expired?(%Account{reset_token_sent_at: datetime}) when is_nil(datetime), do: true

  def expired?(%Account{reset_token_sent_at: datetime}) do
    Timex.after?(Timex.now(), Timex.shift(datetime, days: 1))
  end

  defp put_password_hash(%{changes: %{password: password}} = changeset) do
    put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))
  end

  defp put_password_hash(%{changes: %{}} = changeset), do: changeset

  defp generate_reset_token(length \\ 48) do
    length
    |> :crypto.strong_rand_bytes()
    |> Base.url_encode64()
    |> binary_part(0, length)
  end
end
