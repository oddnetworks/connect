defmodule Auth.Account do
  use Ecto.Schema

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "auth_accounts" do
    field(:email, :string)
    field(:name, :string)
    field(:password_hash, :string)
    field(:current_password, :string, virtual: true)
    field(:password, :string, virtual: true)
    field(:is_admin, :boolean)
    field(:timezone, :string)
    field(:password_reset_token, :string)
    field(:reset_token_sent_at, :naive_datetime)

    timestamps()
  end
end
