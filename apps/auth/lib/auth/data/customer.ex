defmodule Auth.Customer do
  use Ecto.Schema

  alias Auth.{Account, Organization}

  @type t :: %__MODULE__{}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "customers" do
    field(:role, :string)

    belongs_to(:auth_account, Account)
    belongs_to(:organization, Organization)

    timestamps()
  end
end
