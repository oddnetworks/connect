defmodule Auth.OrganizationAccount do
  @moduledoc """
  Struct for a combination of an account, customer, and organization.
  """

  alias Auth.{Customer, OrganizationAccount}
  import Auth.Permissions, only: [permissions_for: 3]

  @type t :: %__MODULE__{}

  defstruct [:account, :customer, :organization, :permissions]

  @doc """

  """
  def new(account, organization, customer) do
    with {:ok, permissions} <- permissions_for(account, organization, customer),
         {:ok, customer} <- customer_for(customer) do
      {:ok,
       %OrganizationAccount{
         account: account,
         customer: customer,
         organization: organization,
         permissions: permissions
       }}
    else
      _ -> {:error, :denied}
    end
  end

  def customer_for(nil), do: {:ok, %Customer{}}

  def customer_for(c), do: {:ok, c}
end
