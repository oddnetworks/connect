defmodule Auth.Webhook do
  use Ecto.Schema
  import Ecto.Query, only: [from: 2]

  @type t :: %__MODULE__{}

  alias Auth.{Organization, Webhook}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "webhooks" do
    field(:type, :string)
    field(:url, :string)
    field(:headers, :map)

    belongs_to(:organization, Organization)

    timestamps()
  end

  def find_by_organization_and_type_query(organization_id, type) do
    from(
      w in Webhook,
      where: w.organization_id == ^organization_id and w.type == ^type,
      select: %{
        url: w.url,
        headers: w.headers
      }
    )
  end
end
