defmodule Auth.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias Auth.Organization
  alias Connect.Repo

  def for_token(%{id: id, email: _email, name: _name, password_hash: _password_hash}),
    do: {:ok, "auth:#{id}"}

  def for_token(%{
        account: %{id: account_id},
        organization: %{id: organization_id},
        customer: %{id: customer_id}
      }),
      do: {:ok, "account:#{account_id}:#{organization_id}:#{customer_id}"}

  def for_token(%{id: id, urn: _urn, name: _name}), do: {:ok, "organization:#{id}"}
  def for_token(_resource), do: {:error, "Unknown resource type"}

  def from_token("auth:" <> auth_account_id) do
    case Auth.find_account(id: auth_account_id) do
      nil ->
        {:error, "Invalid Auth"}

      account ->
        %Timber.Contexts.UserContext{
          id: account.id,
          email: account.email,
          name: account.name
        }
        |> Timber.add_context()

        {:ok, account}
    end
  end

  def from_token("account:" <> organization_account) do
    [aid, oid, cid] = organization_account |> String.split(":")

    case Auth.find_organization_account(aid, oid, cid) do
      {:ok, oa} ->
        %Timber.Contexts.UserContext{
          id: oa.account.id,
          email: oa.account.email,
          name: oa.account.name
        }
        |> Timber.add_context()

        {:ok, oa}

      {:error, :denied} ->
        {:error, "Invalid Account"}
    end
  end

  def from_token("organization:" <> id) do
    organization =
      Organization
      |> Repo.get(id)
      |> Repo.preload([:settings])

    if organization do
      %Timber.Contexts.UserContext{
        id: organization.id,
        email: organization.urn,
        name: organization.name
      }
      |> Timber.add_context()
    end

    {:ok, organization}
  end

  def from_token(_resource), do: {:error, "Unknown resource type"}
end
