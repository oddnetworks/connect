use Mix.Config

config :auth, Auth.ConnectionToken,
  issuer: System.get_env("CONNECT_TOKEN_ISSUER") || "odd_connect:#{Mix.env()}",
  private_key:
    System.get_env("CONNECT_RSA_PRIVATE_KEY") ||
      ~S"""
      -----BEGIN RSA PRIVATE KEY-----
      MIIEpQIBAAKCAQEAzfCfOMnDGVQWDdf0rWjs+fpBoH65Alrpzy1ZzXgsfP7I5C+A
      8fohyS4XAIrakHAX0o3Fg5KoQX+SNPO2/z0nDz+oczWMR6xT4KwL4jtP6G2/jbJr
      LEH8nraSS0wRK3HhjdbD27Oo0E7TW6Kl+Pjccz6vO5zuWo6st8qrY/CGkiKl3x25
      q8coXuk8QuWjodS2k52eGxFUbkrK0qw5XWwyqbyUfUAHsKAZdJEBbSlO6lrqpAlu
      TKJ9byBGd59UwMKzGpdm0RyZY9NcGid8q6YyISWfuLo18beVph49DC7i/bz8LZg6
      ky0+FGdz4fZlBRlGn1RyXdRvKbrssrn0V++u0QIDAQABAoIBAQDEwHAOXE0GZNyR
      K1K7XjSR+3M5t/hf9KXO5wAWGws9/FnIEE58YMJop50YSpDYUA+ifQpIUxRnR09T
      QckC/9TSb7orH+y/CTg1vYm5AXutjzTkVqKn7P9CTyFjyO/advnMfnQu8e2F3tl1
      geZaAMD1Zd0a8iqS4B7JlVq7p6+Z6sMv0qEkhClTxv2gR7YbFMoNZdODuRNoLStT
      beRsFywP53tZ6CgAB3GxptxVghc0BL68DgQ4tSSBzIVX0u0IInBk0YR+SK3kbcPF
      T/UkmVLiNv/T59EYTV+PKs3bZzlPrkhpqQDT0pdEdA+VA5B76k8dOZ6AmmqRM+Fo
      Yuat2sx1AoGBAO/kXCh6wlIglE4wYtsVgrrTp3Jhr1ppzQVP84YxvDOr0an8YC4u
      dnjAGMGmU4fj6muO5Qki4XkQsSvJUs9yxK0dHIh3G4FkMNavF9Sod65LPyZjFGgz
      WHjpkuFnB8PJ9Psnh5oLsTpTuFynCfLBf+YIW/xmGaOhy38hcBgoENZvAoGBANvE
      o9r6XX+9tgZKx+/AI8LaHe7QXMRoHKkz541F/shszGfiERS5Fk9qNMEjhJp4nTCj
      riOk8Cp/3c6JjKWkJktFQ5XQ/5euZ17WeV2KTd9kxNyBDtDDvwLYYMywsMc+k6xs
      JcmnNddtim+stHNpNxU9FFq6fac/BGd2T4S6hG6/AoGBAIU/AHlzhk/7zKSJd79c
      +VowigecfrvnnHtg51EYewLwYuraRcohAKkdeiLF8gu+6syHeGxUtYx7Ww2JK7gS
      dW/6hYi/L8X1ErOMWd5DHQbwwMjfq8wdCP1QL8eHZOa5XJvMXyOPbweIzRSZq4rt
      M89fwUg2pfMfwVqCBjNEveV9AoGASiAsSFr0GHcLo1mr0GEVbs6VFAzxjUm0iRm5
      uzvlsQKJe2yT6FJ5hPXEr3RkcJnIIzTCMsGaRCt2zAg3CVOxmufU4P5qh2XK9gEP
      VI6mr3fUsq9RKPctQS9sjv310m537stXf4nHrk8DR6GQB/FwI9jDOcr3QOcPfDo6
      ngEvVA8CgYEA0gh2jI9Ufqghvy++2TQv1+dsX0oJ6z3Uf6FwlxhI7MhtB3fIXDud
      sB+6+D1b80ksI6sZQ9kirK2PxoEZAsaEiPlZZw6uWaSBJSOUmADEX6pKtbVT5Txt
      pCe5nofYucVxRu3WwMmNeFmz9+iM2TULu7ar0VY4+NUUgcinbsJIujg=
      -----END RSA PRIVATE KEY-----
      """,
  public_key:
    System.get_env("CONNECT_RSA_PUBLIC_KEY") ||
      ~S"""
      -----BEGIN RSA PUBLIC KEY-----
      MIIBCgKCAQEAzfCfOMnDGVQWDdf0rWjs+fpBoH65Alrpzy1ZzXgsfP7I5C+A8foh
      yS4XAIrakHAX0o3Fg5KoQX+SNPO2/z0nDz+oczWMR6xT4KwL4jtP6G2/jbJrLEH8
      nraSS0wRK3HhjdbD27Oo0E7TW6Kl+Pjccz6vO5zuWo6st8qrY/CGkiKl3x25q8co
      Xuk8QuWjodS2k52eGxFUbkrK0qw5XWwyqbyUfUAHsKAZdJEBbSlO6lrqpAluTKJ9
      byBGd59UwMKzGpdm0RyZY9NcGid8q6YyISWfuLo18beVph49DC7i/bz8LZg6ky0+
      FGdz4fZlBRlGn1RyXdRvKbrssrn0V++u0QIDAQAB
      -----END RSA PUBLIC KEY-----
      """
