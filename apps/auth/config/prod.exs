use Mix.Config

config :auth, Auth.ConnectionToken,
  issuer: "${CONNECT_TOKEN_ISSUER}",
  private_key: "${CONNECT_RSA_PRIVATE_KEY}",
  public_key: "${CONNECT_RSA_PUBLIC_KEY}"

config :auth, Auth.ApiToken, ttl_years: "${CONNECT_WEB_API_TOKEN_TTL_YEARS}"

config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "${CONNECT_WEB_TOKEN_ISSUER}",
  ttl: {30, :days},
  verify_issuer: true,
  secret_key: "${CONNECT_WEB_TOKEN_SECRET}",
  serializer: Auth.GuardianSerializer,
  permissions:
    %{
      # default: [
      #   :read_token,
      #   :revoke_token,
      # ],
      # profile: [
      #   :full,
      #   :update,
      #   :read_settings,
      #   :update_settings
      # ]
    }
