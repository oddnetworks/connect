use Mix.Config

config :auth, ecto_repos: []

config :auth, Auth.ApiToken, ttl_years: System.get_env("CONNECT_WEB_API_TOKEN_TTL_YEARS")

config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: System.get_env("CONNECT_WEB_TOKEN_ISSUER") || "FrontEnd.#{Mix.env()}",
  ttl: {30, :days},
  verify_issuer: true,
  secret_key: System.get_env("CONNECT_WEB_TOKEN_SECRET") || to_string(Mix.env()),
  serializer: Auth.GuardianSerializer,
  permissions:
    %{
      # default: [
      #   :read_token,
      #   :revoke_token,
      # ],
      # profile: [
      #   :full,
      #   :update,
      #   :read_settings,
      #   :update_settings
      # ]
    }


import_config "#{Mix.env()}.exs"
