defmodule Auth.PasswordResetTest do
  use Auth.Case

  alias Auth.{Account, PasswordReset}

  describe "expire_changeset/1" do
    test "returns a changeset contining empty password_reset_token and reset_token_sent_at" do
      assert %{changes: %{password_reset_token: nil, reset_token_sent_at: nil}} =
               PasswordReset.expire_changeset(%Account{
                 password_reset_token: "foo",
                 reset_token_sent_at: NaiveDateTime.utc_now()
               })
    end
  end

  describe "reset_changeset/1" do
    test "returns a changeset containing new password_reset_token and reset_token_sent_at" do
      assert %{changes: %{password_reset_token: token, reset_token_sent_at: %NaiveDateTime{}}} =
               PasswordReset.reset_changeset()

      refute is_nil(token)
    end
  end

  describe "password_changeset/1" do
    test "returns invalid when password and confirmation do not match" do
      assert %{
               errors: [
                 password_confirmation:
                   {"does not match confirmation", [validation: :confirmation]}
               ]
             } =
               PasswordReset.password_changeset(%Account{}, %{
                 password: "foobar1234",
                 password_confirmation: "barfoo1234"
               })
    end

    test "returns invalid when password_confirmation is missing" do
      assert %{
               errors: [password_confirmation: {"can't be blank", [validation: :required]}]
             } = PasswordReset.password_changeset(%Account{}, %{password: "herpderp"})
    end

    test "returns invalid when password_confirmation does not match" do
      assert %{
               errors: [
                 password_confirmation:
                   {"does not match confirmation", [validation: :confirmation]}
               ]
             } =
               PasswordReset.password_changeset(%Account{}, %{
                 password: "herpderp",
                 password_confirmation: "derpherp"
               })
    end

    test "returns invalid when password is not long enough" do
      short_pass = "1234"

      assert %{
               errors: [
                 password:
                   {"should be at least %{count} character(s)",
                    [count: 8, validation: :length, min: 8]}
               ]
             } =
               PasswordReset.password_changeset(%Account{}, %{
                 password: short_pass,
                 password_confirmation: short_pass
               })
    end

    test "returns invalid when password is too long" do
      long_pass = "1234567890123456789012345678901234567890"

      assert %{
               errors: [
                 password:
                   {"should be at most %{count} character(s)",
                    [count: 32, validation: :length, max: 32]}
               ]
             } =
               PasswordReset.password_changeset(%Account{}, %{
                 password: long_pass,
                 password_confirmation: long_pass
               })
    end

    test "returns invalid when password is missing" do
      assert %{
               errors: [
                 password_confirmation: {"can't be blank", [validation: :required]},
                 password: {"can't be blank", [validation: :required]}
               ]
             } = PasswordReset.password_changeset(%Account{})
    end

    test "returns valid when conditions met with new password_hash, blank password_reset_token, and blank reset_token_sent_at " do
      assert %{changes: %{password_hash: hash}} =
               PasswordReset.password_changeset(
                 %Account{
                   password_reset_token: "ok",
                   reset_token_sent_at: NaiveDateTime.utc_now()
                 },
                 %{
                   password: "okok1234",
                   password_confirmation: "okok1234"
                 }
               )

      refute is_nil(hash)
    end
  end

  describe "expired?/1" do
    test "returns true when reset_token_sent_at is nil" do
      assert PasswordReset.expired?(%Account{})
    end

    test "returns true when current time 1 day after reset_token_sent_at" do
      expired_datetime = Timex.shift(NaiveDateTime.utc_now(), hours: -25)
      assert PasswordReset.expired?(%Account{reset_token_sent_at: expired_datetime})
    end

    test "returns false when current time within 1 day of reset_token_sent_at" do
      refute PasswordReset.expired?(%Account{reset_token_sent_at: NaiveDateTime.utc_now()})
    end
  end
end
