defmodule Auth.ConnectionTokenTest do
  use Auth.Case

  import Joken, only: [token: 1, with_signer: 2, verify!: 1, rs256: 1]

  setup _tags do
    vc = %{
      organization_urn: "myorg",
      platform: "ROKU",
      device_identifier: "my-device-id",
      email: "foo@foobar.foo",
      id: "70015a2d-ef4b-4c87-b209-cc2217735367"
    }

    pub = Application.get_env(:auth, Auth.ConnectionToken)[:public_key]
    pem = JOSE.JWK.from_pem(pub)
    iss = Application.get_env(:auth, Auth.ConnectionToken)[:issuer]

    {:ok, vc: vc, pub: pub, pem: pem, iss: iss}
  end

  describe "generate_jwt/1" do
    test "uses the given VerifiedConnection to generate a signed jwt", %{
      vc: vc,
      pem: pem,
      iss: iss
    } do
      {:ok, jwt} = Auth.ConnectionToken.generate_jwt(vc)

      {:ok, claims} =
        jwt
        |> token()
        |> with_signer(rs256(pem))
        |> verify!()

      assert String.length(iss) > 0
      assert claims["sub"] == vc.email
      assert claims["iss"] == iss

      assert claims["aud"] == [
               vc.organization_urn,
               vc.platform,
               vc.device_identifier
             ]

      assert claims["jti"] == vc.id
      assert claims["iat"] <= :os.system_time(:second)
    end
  end

  describe "public_key/0" do
    test "returns the public key used to sign a jwt", %{pub: pub} do
      assert String.length(pub) > 0
      assert pub == Auth.ConnectionToken.public_key()
    end
  end
end
