defmodule Auth.PasswordResetEmailTest do
  use Auth.Case

  alias Auth.PasswordResetEmail

  describe "prepare/1" do
    test "returns struct with email details" do
      email = "foo@example.com"
      token = "1234567890"

      assert %{
               to: ^email,
               from: "no-reply@oddconnect.com",
               subject: "Odd Connect - Reset Your Password",
               html: html,
               text: text
             } = PasswordResetEmail.prepare(%{email: email, password_reset_token: token})

      url = "https://#{System.get_env("HOST")}/passwords/#{token}/edit"

      assert text =~ url
      assert html =~ "<a href=\"#{url}\">#{url}</a>"
    end
  end
end
